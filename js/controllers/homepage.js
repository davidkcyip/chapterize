define([
  'underscore',
  'controller',
  'js/views/pages/homepage'
], function( _, BackBoneController, HomeView ){
  var homeController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      //window.app.vent.bind('tock:10sec', this.test);
    	this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.homepageView != "undefined"){
        window.views.homepageView.setElement('.mainContent');
        window.views.homepageView.initialize();
      }else{
        window.views.homepageView = new HomeView();
      }
      
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return homeController;
});