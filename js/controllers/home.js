define([
  'underscore',
  'controller',
  'js/views/pages/home',
  'js/views/items/followSuggestions',
  'js/models/Feed',
  'js/models/User'
], function( _, BackBoneController, HomeView, FollowSuggestionsView, FeedModel, UserModel ){
  var homeController = BackBoneController.extend({
  	el : $('.mainContainer'),
    models: {},
    homeFeed: {},
    subscriptionList: {},
    basicStats: {},
    earliest: 0,
    latest: 0,

    initialize : function(){
      
      window.app.vent.bind('home:searchNewsfeed', this.searchNewsfeed, this);
      window.app.vent.bind('home:recallInitialize',this.reCallInitialize, this);
      this.models.userModel = new UserModel.UserModel();
      this.models.userModel.on('change:suggestions', this.renderSuggestions,this);
      var that = this;
      if(typeof window.storage.get('profile') == "object"){
        var profile = window.storage.get('profile');
        this.models.feedModel = new FeedModel.FeedModel();
        this.models.feedModel.on('change:initialFeed',this.initialFeedLoaded, this);
        this.models.feedModel.on('change:previousFeed',this.previousFeedLoaded, this);
        this.models.feedModel.on('change:mySubscriptionList',this.subscriptionListLoaded, this);
        this.models.feedModel.on('change:basicStats',this.basicStatsLoaded,this);
        this.models.feedModel.getInitialFeed('home',profile.pen_name);
        //that.renderView(profile, feed, subscriptions, basicStats);

      }else{
        $.getJSON("/api/getProfile", function(data){
          if(typeof data != "undefined" && typeof data != false){
            window.storage.set('profile',data);
            var profile = window.storage.get('profile');
            this.models.feedModel = new FeedModel.FeedModel();
            this.models.feedModel.on('change:initialFeed',this.initialFeedLoaded, this);
            this.models.feedModel.on('change:previousFeed',this.previousFeedLoaded, this);
            this.models.feedModel.on('change:mySubscriptionList',this.subscriptionListLoaded, this);
            this.models.feedModel.on('change:basicStats',this.basicStatsLoaded,this);
            this.models.feedModel.getInitialFeed('home',profile.pen_name);
          }else{
            window.location.href = "/login";
          }
        });
      }
    },

    previousFeedLoaded : function(results){
      window.views.homeView.appendItems(results.get('previousFeed'));
    },

    initialFeedLoaded : function(feed){
      console.log('here')
      this.homeFeed = feed.get('initialFeed');
      this.models.feedModel.getSubscriptionList(window.storage.get('profile').id);
    },

    subscriptionListLoaded : function(list){
      this.subscriptionList = list.get('mySubscriptionList');
      this.models.feedModel.getBasicStats(window.storage.get('profile').id);
    },

    basicStatsLoaded : function(stats){
      this.basicStats = stats.get('basicStats');
      var profile = window.storage.get('profile');
      this.renderView(profile, this.homeFeed, this.subscriptionList, this.basicStats);
    },

    renderView : function(profile, feed, subscriptions, basicStats){
      var that = this;
      if(typeof window.views.homeView != "undefined"){
        window.views.homeView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.homeView = new HomeView(profile, feed, subscriptions, basicStats);
      this.models.userModel.getFollowSuggestions();
    },

    searchNewsfeed : function(e){
      $(e).parent().find('.search').removeClass('search');
      $(e).parent().addClass('loading');
    },

    renderSuggestions : function(results){
      if(typeof window.views.followSuggestionsView != "undefined"){
        window.views.followSuggestionsView.remove();
      }
      window.views.followSuggestionsView = new FollowSuggestionsView(results.get('suggestions'));

    }

  });

  return homeController;
});