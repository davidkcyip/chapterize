define([
  'underscore',
  'controller',
  'js/views/pages/about'
], function( _, BackBoneController, AboutView ){
  var aboutController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	 this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.aboutView != "undefined"){
        window.views.aboutView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.aboutView = new AboutView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return aboutController;
});