define([
  'underscore',
  'controller',
  'js/models/Story',
  'js/models/User',
  'js/models/Brainstorm',
  'js/views/pages/story'
], function( _, BackBoneController, StoryModel, UserModel, BrainstormModel, StoryView ){
  var storyController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    author: null,
    story: null,
    chapter: null,
    counter: 0,
    renderCount: 0,
    modelCounter: 0,
    initCounter: 0,
    ajaxCounter : 0,
    models: {},
    collections: {},
    page: 1,

    initialize : function(options){
      var that = this;

      if(typeof this.collections.stories != 'undefined'){
          console.log("storiesreset")
          this.collections.stories.reset();
        }
      if(typeof this.collections.authors != "undefined"){
        console.log("authorsreset")
          this.collections.authors.reset();
        }

      if(typeof window.views.storyView != "undefined"){
        console.log('kill zombie')
        window.views.storyView.undelegateEvents();
        delete this.$el; 
        delete this.el;
        window.views.storyView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }

      this.author = options.author;
      this.id = options.id;
      this.story = options.story;
      this.chapter = options.chapter;
      this.page = options.page;
      window.app.vent.bind('stories:loadingComplete',this.storiesLoaded, this);
      window.app.vent.bind('users:loadingComplete',this.modelsLoaded, this);
      window.app.vent.bind('chapter:rateChapter',this.rateChapter, this);
      window.app.vent.bind('chapter:commentChapter',this.commentChapter, this);

      this.models.story = new StoryModel.StoryModel();
      this.models.author = new UserModel.UserModel();
      this.models.brainstorm = new BrainstormModel.BrainstormModel();

      this.collections.stories = new StoryModel.StoryCollection();
      this.collections.authors = new UserModel.UserCollection();
      this.collections.stories.on("change:authorChapters",this.loadChapters,this);
      this.models.story.on("change:authorChapters",this.loadChapters,this);

      this.models.story.readChapter(options.id,options.chapter);

      if((options.author != null && typeof options.author != "undefined") && (options.id === null || typeof options.id === "undefined") && (options.story === null || typeof options.story === "undefined") && (options.chapter === null || typeof options.chapter === "undefined")){
        var data = this.models.story.getStories(options.author,options.page);
        
        var data2 = this.models.author.getUser(this.author);

        $.when( data, data2 ).done( this.storiesLoaded(data,data2) );

      }else{ 
        var data = this.models.story.getStory(options.id);
        //var data2 = this.models.author.getUser(this.author);
        this.storiesLoaded2(data);
      }
        
      this.models.story.getCategories();
    },

    loadChapters : function(response, results){
      window.views.storyView.renderChapters(results.response);
    },

    events : {
      'click .test' : 'test'
    },

    storiesLoaded : function(data, user){
      var that = this;
      console.log(data);
      var fetchStories = _.each(data.results, function(story){
          that.models.story = new StoryModel.StoryModel();
          that.models.story.set('id',story.id);
          that.models.story.set('story_name',story.story_name);
          that.models.story.set('story_description',story.story_description);
          that.models.story.set('story_category',story.story_category);
          that.models.story.set('written_by',story.written_by);
          that.models.story.set('is_public',story.is_public);
          that.models.story.set('chapter_count',story.chapter_count);
          that.models.story.set('slug',story.slug);
          that.models.story.set('created_at',story.created_at);
          that.models.story.set('categories',story.categories);
          that.models.story.set('tags',story.tags);
          that.models.story.set('comments',story.comments);
          that.models.story.set('story_type',story.story_type);
          that.models.story.set('album',story.album);
          that.models.story.set('have_i_subscribed',story.have_i_subscribed);
          that.models.story.set('helpful',story.helpful);
          that.models.story.set('unhelpful',story.unhelpful);
          that.models.story.set('challenge_details',story.challenge_details);
          that.collections.stories.add(that.models.story);
        });

          this.models.author.set('id',user.id);
          this.models.author.set('pen_name',user.pen_name);
          this.models.author.set('real_name',user.real_name);
          this.models.author.set('email',user.email);
          this.models.author.set('country',user.country);
          this.models.author.set('avatar',user.avatar);
          this.models.author.set('bio',user.bio);
          this.models.author.set('website_link',user.website_link);
          this.models.author.set('facebook_link',user.facebook_link);
          this.models.author.set('twitter_link',user.twitter_link);
          this.models.author.set('google_link',user.google_link);
          that.collections.authors.add(that.models.author);
          
          that.renderView(that.collections.stories.models,that.collections.authors.models);
    },

    storiesLoaded2 : function(data, user){
      var that = this;
      console.log(data);
      var fetchStories = _.each(data.results, function(story){
          that.models.story = new StoryModel.StoryModel();
          that.models.story.set('id',story.id);
          that.models.story.set('story_name',story.story_name);
          that.models.story.set('story_description',story.story_description);
          that.models.story.set('story_category',story.story_category);
          that.models.story.set('written_by',story.written_by);
          that.models.story.set('is_public',story.is_public);
          that.models.story.set('chapter_count',story.chapter_count);
          that.models.story.set('slug',story.slug);
          that.models.story.set('created_at',story.created_at);
          that.models.story.set('chapters',story.chapters);
          that.models.story.set('categories',story.categories);
          that.models.story.set('tags',story.tags);
          that.models.story.set('comments',story.comments);
          that.models.story.set('story_type',story.type);
          that.models.story.set('album',story.album);
          that.models.story.set('have_i_subscribed',story.have_i_subscribed);
          that.models.story.set('helpful',story.helpful);
          that.models.story.set('unhelpful',story.unhelpful);
          that.models.story.set('challenge_details',story.challenge_details);
          that.collections.stories.add(that.models.story);
        });
      this.renderView(this.collections.stories.models,this.collections.authors.models);
    },

    renderView : function(stories, users){
        var that = this;
        var options = {};
        options.stories = stories;
        options.users = users;
        options.id = this.id;
        options.author = this.author;
        options.story = this.story;
        options.chapter = this.chapter;
        console.log(options)
        window.views.storyView = new StoryView(options);
        this.models.story.getChaptersFromAuthor(options.author);
    },

    rateChapter : function(options){

      

    },

    commentChapter : function(options){
      
    }
    
  });

  return storyController;
});