define([
  'underscore',
  'controller',
  'js/views/pages/editStory',
  'js/models/Story'
], function( _, BackBoneController, EditStoryView, StoryModel ){
  var editStoryController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    model : {}, 

    initialize : function(options){
      var that = this;
      this.model = new StoryModel.StoryModel();
      var storyInfo = this.model.getStory(options.id);
      var categories = this.model.getCategories();
      var storyTypes = this.model.getStoryTypes();

      $.when(storyInfo,categories,storyTypes).done(this.renderView(this.model,storyInfo.results,categories,storyTypes));

      window.app.vent.on('editStory:sendEdit',this.editStoryChapter, this);
    },

    events : {
      'click .test' : 'test'
    },

    editStoryChapter : function(options){
      var that = this;
      var isDone = this.model.editStory(options);
      $.when(isDone).done(window.app.vent.trigger('editStory:done', isDone));
    },

    renderView : function(model,storyInfo,categories,storyTypes){
      if(storyInfo[0].written_by != storage.get('profile').id){
        appRouter.navigate("/404", {trigger: true});
      }
      if(typeof window.views.editStoryView != "undefined"){
        window.views.editStoryView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.editStoryView = new EditStoryView(this.model,storyInfo,categories,storyTypes);
    },

    
  });

  return editStoryController;
});