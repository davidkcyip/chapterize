define([
  'underscore',
  'controller',
  'js/views/pages/editChapter',
  'js/models/Story'
], function( _, BackBoneController, EditChapterView, StoryModel ){
  var editChapterController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(options){
      var that = this;
        console.log('here')
        this.model = new StoryModel.StoryModel();
        var chapterInfo = this.model.getChapter(options.chapterId);

        $.when(chapterInfo).done(this.renderView(this.model,chapterInfo.response[0],options.action));
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(model,chapterInfo, action){
      var that = this;
      if(chapterInfo.written_by != storage.get('profile').id){
        appRouter.navigate("/404", {trigger: true});
      }
      if(typeof window.views.editChapterView != "undefined"){
        window.views.editChapterView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.editChapterView = new EditChapterView(model,chapterInfo, action);
    },

    
  });

  return editChapterController;
});