define([
  'underscore',
  'controller',
  'js/views/pages/challenge',
  'js/views/items/challengeRightContent',
  'js/models/Challenge'
], function( _, BackBoneController, ChallengeView, ChallengeRightContentView, ChallengeModel ){
  var challengeController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    challengeOptions: {},
    page: 1,

    initialize : function(options){
      this.challengeOptions = options;
      this.page = options.page;
      this.renderView();
      this.models.challengeModel = new ChallengeModel.ChallengeModel();
      this.models.challengeModel.on('change:activeChallenges',this.renderActiveChallenges, this);
      this.models.challengeModel.on('change:previousChallenges',this.renderPreviousChallenges, this);
      this.models.challengeModel.on('change:myChallenges',this.renderMyChallenges, this);
      this.models.challengeModel.on('change:challengeWinners',this.renderChallengeWinners, this);
      this.models.challengeModel.on('change:challenge',this.renderChallenge, this);
      this.models.challengeModel.on('change:challengeDetails',this.renderEditChallenge, this);
      this.models.challengeModel.on('change:challengeLeaderboard',this.renderChallengeLeaderboard, this);
      if(options.action == "myChallenges"){
        this.models.challengeModel.getMyChallenges(null,options);
      }else if(options.action == "readChallenge"){
        this.models.challengeModel.getChallenge(options);
      }else if(options.action == "activeChallenges"){
        this.models.challengeModel.getActiveChallenges(null,options);
      }else if(options.action == "previousChallenges"){
        this.models.challengeModel.getPreviousChallenges(null,options);
      }else if(options.action == "newChallenge"){
        this.renderRightView(options.action);
      }else if(options.action == "editChallenge"){
        this.models.challengeModel.getChallengeDetails(options);
      }else if(options.action == "challengeWinners"){
        this.models.challengeModel.getChallengeWinners(null,options);
      }else if(options.action == "submitChallenge"){
        this.renderSubmitChallenge(options);
      }else if(options.action == "challengeLeaderboard"){
        this.models.challengeModel.getLeaderboard();
      }
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.challengeView != "undefined"){
        window.views.challengeView.render();
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.challengeView = new ChallengeView();
    },

    renderRightView : function(action, results){
      if(typeof window.views.challengeRightContentView != "undefined"){
        window.views.challengeRightContentView.setElement('.rightContent');
        window.views.challengeRightContentView.initialize(action,results,this.challengeOptions);
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }else{
        window.views.challengeRightContentView = new ChallengeRightContentView(action, results, this.challengeOptions);
      }
    },

    renderActiveChallenges : function(results){
      this.renderRightView("activeChallenges",results.get('activeChallenges'));
    },

    renderPreviousChallenges : function(results){
      this.renderRightView("previousChallenges",results.get('previousChallenges'));
    },

    renderMyChallenges : function(results){
      this.renderRightView("myChallenges",results.get('myChallenges'));
    },

    renderChallengeWinners : function(results){
      this.renderRightView("challengeWinners",results.get('challengeWinners'));
    },

    renderChallenge : function(results){
      this.renderRightView("readChallenge",results.get('challenge'));
    },

    renderEditChallenge : function(results){
      if(results.get('challengeDetails')[0].challenge_by != storage.get('profile').id){
        appRouter.navigate("/404", {trigger: true});
      }
      this.renderRightView("editChallenge",results.get('challengeDetails'));
    },

    renderChallengeLeaderboard : function(results){
      this.renderRightView("challengeLeaderboard", results.get('challengeLeaderboard'));
    },

    renderSubmitChallenge : function(options){
      var that = this;
      $.getJSON('/api/categories', function(categories){
        $.getJSON('/api/storyTypes', function(storyTypes){
          options.storyTypes = storyTypes;
          options.categories = categories;
          that.renderRightView("submitChallenge",options);
        });
      });
    }
    
  });

  return challengeController;
});