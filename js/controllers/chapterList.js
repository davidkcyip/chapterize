define([
  'underscore',
  'controller',
  'js/views/pages/chapterList',
  'js/models/User',
  'js/models/Story'
], function( _, BackBoneController, ChapterListView, UserModel, StoryModel ){
  var chapterListController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    page: 1,

    initialize : function(options){
      this.page = options.page;
    	 this.renderView();
       this.models.userModel = new UserModel.UserModel();
       this.models.storyModel = new StoryModel.StoryModel();
       this.models.userModel.on('change:profileUser',this.renderLeft, this);
       this.models.storyModel.on('change:authorChapters',this.renderChapters, this);
       this.models.userModel.getUser(options.author);
       this.models.storyModel.getChaptersFromAuthor(options.author,options.page);
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.chapterListView != "undefined"){
        window.views.chapterListView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.chapterListView = new ChapterListView(this.page);
    },

    renderLeft : function(results){
      views.chapterListView.populateLeft(results.get('profileUser'));
    },

    renderChapters : function(results){
      console.log('here')
      views.chapterListView.renderChapters(results.get('authorChapters'));
    }

  });

  return chapterListController;
});