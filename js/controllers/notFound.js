define([
  'underscore',
  'controller',
  'js/views/pages/notFound'
], function( _, BackBoneController, NotFoundView ){
  var notFoundController = Backbone.Controller.extend({

    initialize : function(){
    	this.renderView();
    },

    renderView : function(){
    	var that = this;
      if(typeof window.views.notFoundView != "undefined"){
        window.views.notFoundView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.notFoundView = new NotFoundView();
    }
  });

  return notFoundController;
});