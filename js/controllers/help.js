define([
  'underscore',
  'controller',
  'js/views/pages/help'
], function( _, BackBoneController, HelpView ){
  var helpController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	 this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.helpView != "undefined"){
        window.views.helpView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.helpView = new HelpView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return helpController;
});