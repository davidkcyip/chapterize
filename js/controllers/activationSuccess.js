define([
  'underscore',
  'controller',
  'js/views/items/activationSuccess'
], function( _, BackBoneController, ActivationSuccessView ){
  var activationSuccessController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      //window.app.vent.bind('tock:10sec', this.test);
    	this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.activationSuccessView != "undefined"){
        window.views.activationSuccessView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.activationSuccessView = new ActivationSuccessView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return activationSuccessController;
});