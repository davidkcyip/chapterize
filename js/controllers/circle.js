define([
  'underscore',
  'controller',
  'js/views/pages/circle',
  'js/views/items/newCircle',
  'js/views/items/editCircle',
  'js/views/items/viewCircles',
  'js/views/items/searchCircles',
  'js/views/pages/newCircleLeft',
  'js/views/pages/newTopicLeft',
  'js/views/items/newTopicRight',
  'js/views/items/editTopicRight',
  'js/views/items/viewTopicRight',
  'js/views/items/circleTopicsRight',
  'js/views/items/manageCircleMembers',
  'js/models/Circle'
], function( _, BackBoneController, CircleView, NewCircleView, EditCircleView, ViewCirclesView, SearchCirclesView, NewCircleLeftView, NewTopicLeftView, NewTopicRightView, EditTopicRightView, ViewTopicRightView, CircleTopicsRightView, ManageCircleMembersView, CircleModel ){
  var circleController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    circleOptions: {},

    initialize : function(options){
      var that = this;
      this.circleOptions = options;
      this.models.circleModel = new CircleModel.CircleModel();
      this.models.circleModel.on('change:myCircles',this.renderMyCirclesLeft,this);
      this.models.circleModel.on('change:circleDetails',this.renderEditView,this);
      this.models.circleModel.on('change:categories',this.renderCategories,this);
      this.models.circleModel.on('change:browseCirclesActive', this.renderBrowseCircles, this);
      this.models.circleModel.on('change:browseCircles', this.renderBrowseCircles, this);
      this.models.circleModel.on('change:searchCircles', this.renderSearchCircles, this);
      this.models.circleModel.on('change:topicDetails', this.renderViewTopicRight, this);
      this.models.circleModel.on('change:circleMembers', this.renderManageCircleMembers, this);
      if(this.circleOptions.action == "viewCircleTopics"){ //done

        var result = this.models.circleModel.getCircleDetails(this.circleOptions.id);
        $.when(result).done(
          that.renderCircleTopicsRight()
        );
      }else if(this.circleOptions.action == "editCircle"){ //done
        this.models.circleModel.getCircleDetails(this.circleOptions.id);
        this.models.circleModel.getCategories();
      }else if(this.circleOptions.action == "viewTopic"){ //done
        this.models.circleModel.getCircleDetails(this.circleOptions.id);
        this.models.circleModel.getTopicDetails(this.circleOptions);
      }else if(this.circleOptions.action == "editTopic"){ //done
        var result = this.models.circleModel.getCircleDetails(this.circleOptions.id);
        this.models.circleModel.getTopicDetails(this.circleOptions);

        setTimeout( function(){
          that.renderEditTopicRight()
        },100);
      }else if(this.circleOptions.action == "newCircle"){ //done
        if(typeof window.views.newCircleLeftView != "undefined"){
          window.views.newCircleLeftView.remove();
          $('.mainContainer').html('<div class="mainContent"></div>');
        }
        window.views.newCircleLeftView = new NewCircleLeftView(this.circleOptions);
        if(typeof window.views.newCircleView != "undefined"){
          window.views.newCircleView.setElement('.rightContent');
          window.views.newCircleView.initialize(this.circleOptions);
        }else{
          window.views.newCircleView = new NewCircleView(this.circleOptions);
        }
        this.models.circleModel.getCategories();
      }else if(this.circleOptions.action == "manageUsers"){ //todo
        this.models.circleModel.getCircleDetails(this.circleOptions.id);
        this.models.circleModel.getCircleMembers(this.circleOptions);
      }else if(this.circleOptions.action == "searchCircles"){ //done
        this.renderView();
        this.models.circleModel.getMyCircles();
        this.models.circleModel.searchCircles(this.circleOptions.term);
      }else if(this.circleOptions.action == "browseCircle"){ //done
        this.renderView();
        this.models.circleModel.getMyCircles();
        this.models.circleModel.browseCircles(this.circleOptions);
        this.models.circleModel.getCategories();
      }else if(this.circleOptions.action == "newTopic"){ //done
        var result = this.models.circleModel.getCircleDetails(this.circleOptions.id);

        setTimeout( function(){
          that.renderNewTopicRight();
        },100);
      }
    },

    renderViewTopicRight : function(results){
      var that = this;
      if(this.circleOptions.action == "viewTopic"){
        if(typeof window.views.viewTopicRightView != "undefined"){
          window.views.viewTopicRightView.remove();
        }
        setTimeout( function(){
          window.views.viewTopicRightView = new ViewTopicRightView(that.circleOptions, results.get('topicDetails'), results.get('circleDetails'));
        },100);
      }
    },

    renderCircleTopicsRight : function(){
      var that = this;
      if(typeof window.views.circleTopicsRightView  != "undefined"){
        window.views.circleTopicsRightView.remove();
      }
      setTimeout( function(){
        window.views.circleTopicsRightView = new CircleTopicsRightView(that.circleOptions, that.models.circleModel.get('circleDetails'));
      },100);
    },

    renderNewTopicLeft : function(){
      var that = this;
      if(typeof window.views.newTopicLeftView  != "undefined"){
        window.views.newTopicLeftView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.newTopicLeftView = new NewTopicLeftView(that.circleOptions);
    },

    renderNewTopicRight : function(){
      if(typeof window.views.newTopicRightView != "undefined"){
        window.views.newTopicRightView.remove();
      }
      window.views.newTopicRightView = new NewTopicRightView(this.circleOptions);
    },

    renderEditTopicRight : function(){
      if(typeof window.views.editTopicRightView != "undefined"){
        window.views.editTopicRightView.remove();
      }
      window.views.editTopicRightView = new EditTopicRightView(this.circleOptions, this.models.circleModel.get('topicDetails'));
    },

    renderMyCirclesLeft : function(results){
      if(this.circleOptions.action == "browseCircle"){
        require(['text!templates/items/myCirclesLeft.html'], function(MyCirclesLeftTemplate){
          var myCirclesLeftTemplate = _.template(MyCirclesLeftTemplate);
          $('.myCircles').html(myCirclesLeftTemplate({results: results.get('myCircles')}));
        });
      }
    },

    renderView : function(options){
      var that = this;
      if(typeof window.views.circleView != "undefined" && this.circleOptions.action != "editCircle" && this.circleOptions.action != "newCircle"){
        window.views.circleView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.circleView = new CircleView(this.circleOptions);
    },

    renderBrowseCircles : function(results){
      var that = this;
      if(typeof window.views.viewCirclesView != "undefined"){
        window.views.viewCirclesView.setElement('.rightContent');
        window.views.viewCirclesView.render(results, this.circleOptions);
        if(this.circleOptions.type != "overview"){
          setTimeout( function(){
            $('.circleHeader').html($('#circleType :selected').text().toUpperCase() + " CIRCLES");
          },100);
        }
      }else{
        window.views.viewCirclesView = new ViewCirclesView(results, this.circleOptions);
        if(this.circleOptions.type != "overview"){
          setTimeout( function(){
            $('.circleHeader').html($('#circleType :selected').text().toUpperCase() + " CIRCLES");
          },100);
        }
      }
    },

    renderSearchCircles : function(results){
      if(typeof window.views.searchCirclesView != "undefined"){
        window.views.searchCirclesView.setElement('.rightContent');
        window.views.searchCirclesView.render(results, this.circleOptions);
      }else{
        window.views.searchCirclesView = new SearchCirclesView(results.get('searchCircles'), this.circleOptions);
      }
    },

    renderCategories : function(results){
      _.each(results.get('categories'), function(category){
        $('#circleCategory').append("<option value='"+ category.id +"'>"+category.circle_category+"</option>");
      });
    },

    renderEditView : function(results){
      if((this.circleOptions.action == "editCircle" || this.circleOptions.action == "manageUsers") && results.get('circleDetails')[0].am_i_admin == 0){
        appRouter.navigate("/404", {trigger: true});
      }

      if(this.circleOptions.action == "editCircle" || this.circleOptions.action == "newCircle"){
        if(typeof window.views.newCircleLeftView != "undefined"){
          window.views.newCircleLeftView.remove();
          $('.mainContainer').html('<div class="mainContent"></div>');
        }
        console.log('hi')
        window.views.newCircleLeftView = new NewCircleLeftView(this.circleOptions);
        if(typeof window.views.editCircleView != "undefined"){
          window.views.editCircleView.setElement('.rightContent');
          window.views.editCircleView.render(this.circleOptions, results);
        }else{
          window.views.editCircleView = new EditCircleView(this.circleOptions, results);
        }
      }else{
        var that = this;
        if(typeof window.views.newTopicLeftView  != "undefined"){
          window.views.newTopicLeftView.remove();
          $('.mainContainer').html('<div class="mainContent"></div>');
        }
        window.views.newTopicLeftView = new NewTopicLeftView(that.circleOptions, results.get('circleDetails'));
      }
    },

    renderManageCircleMembers : function(results){
      var that = this;
      setTimeout( function(){
        if(typeof window.views.manageCircleMembersView != "undefined"){
          window.views.manageCircleMembersView.setElement('.rightContent');
          window.views.manageCircleMembersView.render(results.get('circleMembers'), that.circleOptions);
        }else{
          window.views.manageCircleMembersView = new ManageCircleMembersView(results.get('circleMembers'), that.circleOptions);
        }
      },100);
    }

  });

  return circleController;
});