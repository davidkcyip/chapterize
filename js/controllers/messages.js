define([
  'underscore',
  'controller',
  'js/views/pages/messages',
  'js/models/Message',
  'js/views/items/messagesInbox',
  'js/views/items/messagesSent',
  'js/views/items/readMessage',
  'js/views/items/composeMessage',
], function( _, BackBoneController, MessagesView, MessageModel, MessagesInboxView, MessagesSentView, ReadMessageView, ComposeMessageView ){
  var messagesController = Backbone.Controller.extend({
  	el : '.mainContainer',
    models: {},

    initialize : function(options){
      this.renderView();
      this.models.messageModel = new MessageModel.MessageModel();
      this.models.messageModel.on("change:inbox",this.postRenderInbox, this);
      this.models.messageModel.on("change:sent",this.postRenderSent, this);
      this.models.messageModel.on("change:message",this.postRenderSingleMessage, this);
      var that = this;
      if(options.action == "read"){
          that.models.messageModel.getMessage(options);
      }else if(options.action == "compose"){
        this.postRenderCompose();
      }else if(options.action == "inbox"){
        this.models.messageModel.getAllOfAuthorsInbox();
      }else if(options.action == "sent"){
        this.models.messageModel.getAllOfAuthorsSent();
      }
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.messagesView != "undefined"){
        window.views.messagesView.render();
      }else{
        window.views.messagesView = new MessagesView();
      }
    },

    postRenderInbox : function(results){
      if(typeof window.views.messagesInboxView != "undefined"){
        window.views.messagesInboxView.setElement('.rightContent');
        window.views.messagesInboxView.render(results.get('inbox'));
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }else{
        window.views.messagesInboxView = new MessagesInboxView(results.get('inbox'));
      }
    },

    postRenderSent : function(results){
      if(typeof window.views.messagesSentView != "undefined"){
        window.views.messagesSentView.setElement('.rightContent');
        window.views.messagesSentView.render(results.get('sent'));
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }else{
        window.views.messagesSentView = new MessagesSentView(results.get('sent'));
      }
    },

    postRenderSingleMessage : function(results){
      var recipients = _.where(results.get('message')[0].recipients, {"id": storage.get('profile').id});
      if(recipients.length == 0){
        appRouter.navigate("/404", {trigger: true});
      }
      if(typeof window.views.readMessageView != "undefined"){
        window.views.readMessageView.setElement('.rightContent');
        window.views.readMessageView.render(results.get('message'));
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }else{
        window.views.readMessageView = new ReadMessageView(results.get('message'));
      }
    },

    postRenderCompose : function(){
      if(typeof window.views.composeMessageView != "undefined"){
        window.views.composeMessageView.setElement('.rightContent');
        window.views.composeMessageView.render();
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }else{
        window.views.composeMessageView = new ComposeMessageView();
      }
    }

  });

  return messagesController;
});