define([
  'underscore',
  'controller',
  'js/views/pages/notifications'
], function( _, BackBoneController, NotificationsView ){
  var notificationsController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      window.controllers.mainContentController.models.notificationModel.on("change:allNotifications", this.renderView, this);
      window.controllers.mainContentController.models.notificationModel.getAllNotifications();
    },

    renderView : function(results){
      var that = this;
      if(typeof window.views.notificationsView != "undefined"){
        window.views.notificationsView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.notificationsView = new NotificationsView(results.get('allNotifications'));
    }

  });

  return notificationsController;
});