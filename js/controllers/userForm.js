define([
  'underscore',
  'controller',
  'js/views/items/userForm',
  'js/models/User',
], function( _, BackBoneController, UserFormView, UserModel ){
  var userFormController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      var user = new UserModel();
      var userForm = new Backbone.Form({
        model: user
      }).render();
    	this.renderView(userForm.$el.html());
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(userForm){
      var that = this;
      if(typeof window.views.userFormView != "undefined"){
        window.views.userFormView.remove();
        $('.mainContent').html('<div class="mainContent"></div>');
      }
      window.views.userFormView = new UserFormView(userForm);
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return userFormController;
});