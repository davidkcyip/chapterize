define([
  'underscore',
  'controller',
  'js/views/items/activationError'
], function( _, BackBoneController, ActivationErrorView ){
  var activationErrorController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      //window.app.vent.bind('tock:10sec', this.test);
    	this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.activationErrorView != "undefined"){
        window.views.activationErrorView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.activationErrorView = new ActivationErrorView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return activationErrorController;
});