define([
  'underscore',
  'controller',
  'js/views/pages/contact'
], function( _, BackBoneController, ContactView ){
  var contactController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	this.renderView();
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.contactView != "undefined"){
        window.views.contactView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.contactView = new ContactView();
    }
  });

  return contactController;
});