define([
  'underscore',
  'controller',
  'js/views/pages/search',
  'js/models/Story',
  'js/models/User',
  'js/models/Search',
  'js/models/Chapter',
  'js/views/items/searchStories',
  'js/views/items/searchAuthors',
  'js/views/items/searchResults',
  'js/views/items/searchChapters',
  'js/views/items/searchFavourites',
  'js/views/items/searchBookmarks',
  'js/views/items/searchAllTags',
  'js/views/items/searchTagStories'
], function( _, BackBoneController, SearchView, StoryModel, UserModel, SearchModel, ChapterModel, SearchStoriesView, SearchAuthorsView, SearchResultsView, SearchChaptersView, SearchFavouritesView, SearchBookmarksView, SearchAllTagsView, SearchTagStoriesView ){
  var searchController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    page: "",
    query: "",

    initialize : function(options){
      if(options.page == null){
        this.page = 1;
      }else{
        this.page = options.page;
      }
      this.renderView(options.type, options.settings);
    },

    renderView : function(type, settings){
      if(typeof window.views.searchView != "undefined"){
        window.views.searchView.remove();
        //window.views.searchView.render();
        $('.mainContainer').html('<div class="mainContent"></div>');
        window.views.searchView = new SearchView();
      }else{
        window.views.searchView = new SearchView();
      }
      this.postRender(type, settings);
    },

    postRender : function(type, settings){
      var period = $('#period').val();
      if(typeof period == "undefined" || period == null){
        period = "allTime";
      }
      var that = this;
      this.models.searchModel = new SearchModel.SearchModel();
      this.models.searchModel.on('change:searchResults', this.postRenderSearchResults, this);
      if(type == "stories" || type == null || typeof type == "undefined" || !isNaN(type)){
        this.models.storyModel = new StoryModel.StoryModel();
        this.models.storyModel.on('change:categories',this.postRenderCategories, this);
        this.models.storyModel.on('change:storySearch', this.postRenderSearchStories, this);
        if(typeof settings == "undefined" || settings == null || settings == "all"){
          this.models.storyModel.getAllStories(period, this.page);
          this.models.storyModel.getCategories();
        }else if(settings == "recent"){
          this.models.storyModel.getRecentStories(period, this.page);
          this.models.storyModel.getCategories();
        }else if(settings == "popular"){
          this.models.storyModel.getPopularStories(period, this.page);
          this.models.storyModel.getCategories();
        }
      }else if(type == "authors"){
        this.models.userModel = new UserModel.UserModel();
        this.models.userModel.on('change:userSearch', this.postRenderSearchAuthors, this);
        if(typeof settings == "undefined" || settings == null){
          settings = "all";
        }
        if(typeof settings == "undefined" || settings == null || settings == "all"){
          this.models.userModel.getAllAuthors(period,this.page);
        }else if(settings == "recent"){
          this.models.userModel.getRecentAuthors(period,this.page);
        }else if(settings == "popular"){
          this.models.userModel.getPopularAuthors(period,this.page);
        }
      }else if(type == "chapters"){
        this.models.chapterModel = new ChapterModel.ChapterModel();
        this.models.chapterModel.on('change:chapterSearch', this.postRenderSearchChapters, this);
        if(typeof settings == "undefined" || settings == null){
          settings = "all";
        }
        if(typeof settings == "undefined" || settings == null || settings == "all"){
          this.models.chapterModel.getAllChapters(period);
        }else if(settings == "recent"){
          this.models.chapterModel.getRecentChapters(period);
        }else if(settings == "popular"){
          this.models.chapterModel.getPopularChapters(period);
        }
      }else if(type == "favourites"){
        this.renderFavouritesPage();
      }else if(type == "bookmarks"){
        this.renderBookmarksPage();
      }else if(type == "tag"){
        if(settings == null || typeof settings == "undefined"){
          this.models.searchModel.on('change:searchTags',this.renderAllTags, this);
          this.models.searchModel.getAllTags();
        }else{
          this.models.searchModel.on('change:searchTagStories', this.renderSearchTagStories, this);
          this.models.searchModel.getSearchTagStories(settings,this.page);
        }
      }else{
        this.query = type;
        this.models.searchModel.searchResults(type,this.page);
      }
    },

    renderSearchTagStories : function(results){
      if(typeof window.views.searchTagStoriesView != "undefined"){
        window.views.searchTagStoriesView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchTagStoriesView = new SearchTagStoriesView(results.get('searchTagStories'), this.page);
      }else{
        window.views.searchTagStoriesView = new SearchTagStoriesView(results.get('searchTagStories'), this.page);
      }
    },

    renderAllTags : function(results){
      if(typeof window.views.searchAllTagsView != "undefined"){
        window.views.searchAllTagsView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchAllTagsView = new SearchAllTagsView(results.get('searchTags'));
      }else{
        window.views.searchAllTagsView = new SearchAllTagsView(results.get('searchTags'));
      }
    },

    renderFavouritesPage : function(){
      var favourites = window.controllers.mainContentController.models.favouriteModel.get('favourites');
      if(typeof window.views.searchFavouritesView != "undefined"){
        window.views.searchFavouritesView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchFavouritesView = new SearchFavouritesView(favourites);
      }else{
        window.views.searchFavouritesView = new SearchFavouritesView(favourites);
      }
    },

    renderBookmarksPage : function(){
      var bookmarks = window.controllers.mainContentController.models.bookmarkModel.get('bookmarks');
      if(typeof window.views.searchBookmarksView != "undefined"){
        window.views.searchBookmarksView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchBookmarksView = new searchBookmarksView(bookmarks);
      }else{
        window.views.searchBookmarksView = new SearchBookmarksView(bookmarks);
      }
    },

    postRenderSearchStories : function(results){
      if(typeof window.views.searchStoriesView != "undefined"){
        window.views.searchStoriesView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchStoriesView = new SearchStoriesView(results.get('storySearch'), this.page);
      }else{
        window.views.searchStoriesView = new SearchStoriesView(results.get('storySearch'), this.page);
      }
    },

    postRenderSearchChapters : function(results){
      if(typeof window.views.searchChaptersView != "undefined"){
        window.views.searchChaptersView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchChaptersView = new SearchChaptersView(results.get('chapterSearch'), this.page);
      }else{
        window.views.searchChaptersView = new SearchChaptersView(results.get('chapterSearch'), this.page);
      }
    },

    postRenderSearchAuthors : function(results){
      if(typeof window.views.searchAuthorsView != "undefined"){
        window.views.searchAuthorsView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchAuthorsView = new SearchAuthorsView(results.get('userSearch'), this.page);
      }else{
        window.views.searchAuthorsView = new SearchAuthorsView(results.get('userSearch'), this.page);
      }
    },

    postRenderSearchResults : function(results){
      if(typeof window.views.searchResultsView != "undefined"){
        window.views.searchResultsView.remove();
        if($('.rightContent').length == 0){
          $('.mainContent').append('<div class="rightContent searchDiv"></div>');
        }
        window.views.searchResultsView = new SearchResultsView(results.get('searchResults'), this.page, this.query);
      }else{
        window.views.searchResultsView = new SearchResultsView(results.get('searchResults'), this.page, this.query);
      }
    },

    postRenderCategories : function(results){
      var result = results.get('categories');
      this.categories = result;
      _.each(result, function(category){
        $('#category').append("<option value='" + category.id + "'>" + category.category + "</option>")
      });
    }

  });

  return searchController;
});