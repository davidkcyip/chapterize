define([
  'underscore',
  'controller',
  'js/views/pages/settings',
  'js/views/items/accountSettings',
  'js/views/items/emailSettings',
  'js/views/items/subscriptionSettings',
  'js/models/Settings'
], function( _, BackBoneController, SettingsView, AccountSettingsView, EmailSettingsView, SubscriptionSettingsView, SettingsModel ){
  var settingsController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},

    initialize : function(options){
      this.renderView();
      this.models.settingsModel = new SettingsModel.SettingsModel();
      this.models.settingsModel.on('change:emailSettings',this.renderEmailView, this);
    	if(options.type == "account"){
        this.renderAccountView();
      }else if(options.type == "email"){
        this.models.settingsModel.getEmailSettings();
      }else if(options.type == "subscription"){
        this.renderSubscriptionView();
      }
    },

    renderAccountView : function(){
      var that = this;
      if(typeof window.views.accountSettingsView != "undefined"){
        window.views.accountSettingsView.setElement('.rightContent');
        window.views.accountSettingsView.render();
      }
      window.views.accountSettingsView = new AccountSettingsView();
    },

    renderSubscriptionView : function(){

    },

    renderEmailView : function(results){
      var that = this;
      if(typeof window.views.emailSettingsView != "undefined"){
        window.views.emailSettingsView.setElement('.rightContent');
        window.views.emailSettingsView.render(results.get('emailSettings'));
      }
      window.views.emailSettingsView = new EmailSettingsView(results.get('emailSettings'));
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.settingsView != "undefined"){
        window.views.settingsView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.settingsView = new SettingsView();
    }
  });

  return settingsController;
});