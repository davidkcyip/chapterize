define([
  'underscore',
  'controller',
  'js/views/pages/mobileRegister'
], function( _, BackBoneController, MobileRegisterView ){
  var mobileRegisterController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	 this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.mobileRegisterView != "undefined"){
        window.views.mobileRegisterView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.mobileRegisterView = new MobileRegisterView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return mobileRegisterController;
});