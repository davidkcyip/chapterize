define([
  'underscore',
  'controller',
  'js/views/pages/storyList',
  'js/models/User',
  'js/models/Story'
], function( _, BackBoneController, StoryListView, UserModel, StoryModel ){
  var storyListController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    page: 1,

    initialize : function(options){
      this.page = options.page;
    	 this.renderView();
       this.models.userModel = new UserModel.UserModel();
       this.models.storyModel = new StoryModel.StoryModel();
       this.models.userModel.on('change:profileUser',this.renderLeft, this);
       this.models.storyModel.on('change:authorStories',this.renderStories, this);
       this.models.userModel.getUser(options.author);
       this.models.storyModel.getStories(options.author,options.page);
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.storyListView != "undefined"){
        window.views.storyListView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.storyListView = new StoryListView(this.page);
    },

    renderLeft : function(results){
      views.storyListView.populateLeft(results.get('profileUser'));
    },

    renderStories : function(results){
      views.storyListView.renderStories(results.get('authorStories'));
    }

  });

  return storyListController;
});