define([
  'underscore',
  'controller',
  'js/views/pages/mobileLogin'
], function( _, BackBoneController, MobileLoginView ){
  var mobileLoginController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	 this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.mobileLoginView != "undefined"){
        window.views.mobileLoginView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.mobileLoginView = new MobileLoginView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return mobileLoginController;
});