define([
  'underscore',
  'controller',
  'js/views/pages/mobileForgotPassword'
], function( _, BackBoneController, MobileForgotPasswordView ){
  var mobileForgotPasswordController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
    	 this.renderView();
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.mobileForgotPasswordView != "undefined"){
        window.views.mobileForgotPasswordView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.mobileForgotPasswordView = new MobileForgotPasswordView();
    },

    test : function(vars){
      $(vars.thisEl).html(vars.test);
      //console.log(userModel.getBaseUrl());
    }
  });

  return mobileForgotPasswordController;
});