define([
  'underscore',
  'controller',
  'js/views/items/verify'
], function( _, BackBoneController, VerifyView ){
  var verifyController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      var that = this;
      if(!window.storage.get('profile')){
    	 $.getJSON('/api/getProfile',function(data){
          if(!data.password && !data.pen_name){
            that.renderView();
          }else{
            window.location.href = '/home';
          }
       });
      }
    },

    renderView : function(){
      var that = this;
      window.views.verifyView = new VerifyView();
    }
  });

  return verifyController;
});