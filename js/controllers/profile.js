define([
  'underscore',
  'controller',
  'js/views/pages/profile',
  'js/models/Feed',
  'js/models/Challenge'
], function( _, BackBoneController, ProfileView, FeedModel, ChallengeModel ){
  var profileController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    feedProfile: {},
    profileFeed: {},
    profileSubscriptionList: {},
    mySubscriptionList: {},
    basicStats: {},
    profileOption: {},

    initialize : function(options){
      var that = this;
      this.profileOption = options;
      window.app.vent.on('profile:follow',this.follow, this);
      window.app.vent.on('profile:editProfile',this.editProfile, this);
      this.models.feedModel = new FeedModel.FeedModel();
      this.models.challengeModel = new ChallengeModel.ChallengeModel();
      this.models.challengeModel.on('change:challengeLeaderboard', this.renderChallengePosition, this);
      this.models.feedModel.on('change:profile',this.profileLoaded,this);
      this.models.feedModel.on('change:status',this.initialFeedLoaded,this);
      this.models.feedModel.on('change:initialFeed',this.initialFeedLoaded,this);
      this.models.feedModel.on('change:profileSubscriptionList',this.profileSubscriptionListLoaded,this);
      this.models.feedModel.on('change:mySubscriptionList',this.mySubscriptionListLoaded,this);
      this.models.feedModel.on('change:basicStats',this.basicStatsLoaded,this);
      this.models.feedModel.getProfile(options.username);

    },

    profileLoaded : function(results){
      this.feedProfile = results.get('profile');
      console.log(this.feedProfile.pen_name)
      if(typeof this.profileOption.id == "undefined" || this.profileOption.id == null){
        this.models.feedModel.getInitialFeed('profile',this.feedProfile.pen_name);
      }else{
        this.models.feedModel.getStatus(this.profileOption.id);
      }
    },

    initialFeedLoaded : function(results){
      this.profileFeed = results.get('initialFeed');
      this.models.feedModel.getSubscriptionList(this.feedProfile.id);
    },

    profileSubscriptionListLoaded : function(results){
      this.profileSubscriptionList = results.get('profileSubscriptionList');
      this.models.feedModel.getSubscriptionList(window.storage.get('profile').id);
    },

    mySubscriptionListLoaded : function(results){
      this.mySubscriptionList = results.get('mySubscriptionList');
      this.models.feedModel.getBasicStats(this.feedProfile.id);
    },

    basicStatsLoaded : function(results){
      this.basicStats = results.get('basicStats');
      console.log('here')
      this.renderView(this.feedProfile,this.profileFeed,this.profileSubscriptionList,this.mySubscriptionList,this.basicStats);
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(profile, feed, subscriptions, mySubscriptions, basicStats){
      var that = this;
      console.log
      if(typeof window.views.profileView != "undefined"){
        window.views.profileView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.profileView = new ProfileView(profile, feed, subscriptions, mySubscriptions, basicStats);
      this.models.challengeModel.getLeaderboard('allTime');
    },

    renderChallengePosition : function(results){
      window.views.profileView.renderChallengePosition(results.get('challengeLeaderboard'));
    },  

    follow : function(user_id){
      $.ajax({
        type: 'POST',
        url: '/api/user/followUnfollow',
        dataType: "json",
        data: {"followUnfollow": "follow", "type": "user", "user_id": user_id},
        success: function(data){
          window.app.vent.trigger('profile:followSuccess');
        }
      });
    },

    editProfile : function(){
      $.ajax({
        type: 'POST',
        url: '/api/user/edit',
        dataType: "json",
        data: {"real_name": window.storage.get('profile').real_name, "bio": window.storage.get('profile').bio, "country": window.storage.get('profile').country, "website_link": window.storage.get('profile').website_link,"facebook_link": window.storage.get('profile').facebook_link,"twitter_link": window.storage.get('profile').twitter_link,"google_link": window.storage.get('profile').google_link},
        success: function(data){
          window.app.vent.trigger('profile:editProfileSuccess');
        }
      });
    }
  });

  return profileController;
});