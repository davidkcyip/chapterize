define([
  'underscore',
  'controller',
  'js/views/pages/newChapter',
  'js/models/Story'
], function( _, BackBoneController, NewChapterView, StoryModel ){
  var newChapterController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(options){
      var that = this;
      if(options.action == "new"){
        console.log('here')
        this.model = new StoryModel.StoryModel();
        var chapterInfo = this.model.getChapter(options.chapterId);

        $.when(chapterInfo).done(this.renderView(this.model,chapterInfo.response[0],options.action));
      }

      
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(model,chapterInfo, action){
      var that = this;
      if(typeof window.views.newChapterView != "undefined"){
        window.views.newChapterView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.newChapterView = new NewChapterView(model,chapterInfo, action);
    },

    
  });

  return newChapterController;
});