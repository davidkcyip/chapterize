define([
  'underscore',
  'controller',
  'js/views/pages/newStory',
  'js/models/Story'
], function( _, BackBoneController, NewStoryView, Story ){
  var newStoryController = Backbone.Controller.extend({
  	el : $('.mainContainer'),

    initialize : function(){
      var that = this;
      this.model = new Story.StoryModel();
      this.collection = new Story.StoryCollection();
      this.collection.add(this.model);

      window.app.vent.on('newStory:saveStep1',this.saveStep1, this);
      window.app.vent.on('newStory:saveStep2',this.saveStep2, this);
      window.app.vent.on('newStory:saveDraft',this.saveDraft, this);
      window.app.vent.on('newStory:publish',this.publish, this);

      $.getJSON('/api/categories', function(categories){
        $.getJSON('/api/storyTypes', function(storyTypes){
          $.getJSON('/api/story/authorId/basic/'+window.storage.get('profile').id, function(storyList){
            that.renderView(categories, storyTypes, that.model, storyList.results);
          });
        });
      });
    },

    events : {
      'click .test' : 'test'
    },

    renderView : function(categories, storyTypes, storyModel, storyList){
      var that = this;
      if(typeof window.views.newStoryView != "undefined"){
        window.views.newStoryView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.newStoryView = new NewStoryView(categories, storyTypes, storyModel, storyList);
    },

    saveStep1 : function(){
      this.model.set({story_name: $('#storyName').val(), 
                      album: $('.album').attr('src'),
                      story_description: $('#storyDescription').val(), 
                      story_type: $('#storyType').val(), 
                      story_category: $('#category').val(), 
                      written_by: window.storage.get('profile').id, 
                      is_public: $('#isPublic').val(), 
                      tags: $('input[name=tags]').val()});
      window.app.vent.trigger('newStory:step1Saved', this.model.get('chapters'));
    },

    saveStep2 : function(){
      var chapters = this.model.get('chapters');
      if(typeof chapters == "undefined"){
        chapters = {};
      }
      chapters.chapter_name = $('#chapterName').val();
      chapters.written_by = window.storage.get('profile').id;
      chapters.chapter_body = new nicEditors.findEditor('storyContent').getContent();
      chapters.branch = 1;
      this.model.set('chapters',chapters);
      console.log(this.model.get('chapters'));
      window.app.vent.trigger('newStory:step2Saved', this.model);
    },

    publish : function(){
      var chapters = this.model.get('chapters');
      if(typeof chapters == "undefined"){
        chapters = {};
      }
      this.model.set('album',$('.album').attr('src'));
      chapters.chapter_name = $('#chapterName').val();
      chapters.written_by = window.storage.get('profile').id;
      var nicE = new nicEditors.findEditor('storyContent').getContent();
      chapters.chapter_body = nicE;
      chapters.branch = 1;
      this.model.set('chapters',chapters);

      this.model.publishNewStory();
    },

    saveDraft : function(){
      var chapters = this.model.get('chapters');
      if(typeof chapters == "undefined"){
        chapters = {};
      }
      this.model.set('album',$('.album').attr('src'));
      chapters.chapter_name = $('#chapterName').val();
      chapters.written_by = window.storage.get('profile').id;
      var nicE = new nicEditors.findEditor('storyContent').getContent();
      chapters.chapter_body = nicE;
      chapters.branch = 1;
      this.model.set('chapters',chapters);

      this.model.saveNewDraft();
    }
  });

  return newStoryController;
});