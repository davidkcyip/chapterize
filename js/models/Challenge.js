define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var ChallengeModel = Backbone.Model.extend({
		
		newChallenge: function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        async: false,
		        url: '/api/challenge/new',
		            dataType: "json",
		            data: { "challenge_name": options.challenge_name, "challenge_description": options.challenge_description, "challenge_start_date": options.challenge_start_date, "challenge_end_date": options.challenge_end_date},
		            success: function(data){
		            	data2 = data.results;
		            }
		      });
			return data2;
		},

		getActiveChallenges: function(type,options){
			var that = this;
			var data2 = {};
			if(type == "recent" || type == null){
				$.getJSON('/api/challenge/active/recent/'+options.page, function(data){
					that.set('numberActiveChallenges',data.numberActiveChallenges);
					that.set('activeChallenges', data.results);
				});
			}else if(type == "popular"){
				$.getJSON('/api/challenge/active/popular/'+options.page, function(data){
					that.set('numberActiveChallenges',data.numberActiveChallenges);
					that.set('activeChallenges', data.results);
				});
			}else if(type == "soonest"){
				$.getJSON('/api/challenge/active/soonest/'+options.page, function(data){
					that.set('numberActiveChallenges',data.numberActiveChallenges);
					that.set('activeChallenges', data.results);
				});
			}
		},

		getPreviousChallenges: function(type,options){
			var that = this;
			var data2 = {};
			if(type == "popular"){
				$.getJSON('/api/challenge/previous/popular/'+options.page, function(data){
					that.set('numberPreviousChallenges',data.numberPreviousChallenges);
					that.set('previousChallenges', data.results);
				});
			}else if(type == "soonest" || type == null){
				$.getJSON('/api/challenge/previous/soonest/'+options.page, function(data){
					that.set('numberPreviousChallenges',data.numberPreviousChallenges);
					that.set('previousChallenges', data.results);
				});
			}
		},

		getMyChallenges: function(type,options){
			var that = this;
			var data2 = {};
			if(type == "popular"){
				$.getJSON('/api/challenge/mine/popular/'+options.page, function(data){
					that.set('numberMyChallenges',data.numberMyChallenges);
					that.set('myChallenges', data.results);
				});
			}else if(type == "soonest" || type == null){
				$.getJSON('/api/challenge/mine/soonest/'+options.page, function(data){
					that.set('numberMyChallenges',data.numberMyChallenges);
					that.set('myChallenges', data.results);
				});
			}
		},

		getChallengeWinners: function(type,options){
			var that = this;
			var data2 = {};
			if(type == "popular"){
				$.getJSON('/api/challenge/winners/popular/'+options.page, function(data){
					that.set('numberChallengeWinners',data.numberChallengeWinners);
					that.set('challengeWinners', data.results);
				});
			}else if(type == "soonest" || type == null){
				$.getJSON('/api/challenge/winners/soonest/'+options.page, function(data){
					that.set('numberChallengeWinners',data.numberChallengeWinners);
					that.set('challengeWinners', data.results);
				});
			}
		},

		getChallenge : function(options){
			var that = this;
			var data2 = {};
			$.getJSON('/api/challenge/get/'+options.id, function(data){
				that.set('challenge',data.results);
			});
		},

		submitChallenge : function(options){
			var that = this;
			var data2 = {};
			console.log(options)
			$.ajax({
	        type: 'POST',
	        async: false,
	        url: '/api/challenge/submit',
            dataType: "json",
            data: { "title": options.story_name, "description": options.story_description, "type": options.story_type, "categories": options.categories, "tags": options.story_tags, "chapter_title": options.chapter_title, "chapter_text": options.chapter_text, "challenge_id": options.challenge_id},
            success: function(data){
            	data2 = data;
            }
	      });
			return data2;
		},

		challengeComment : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        async: false,
		        url: '/api/challenge/comment',
	            dataType: "json",
	            data: { "challenge_id": options.challenge_id, "comment": options.comment},
	            success: function(data){
	            	data2 = data;
	            }
	      	});
			return data2;
		},

		getChallengeDetails : function(options){
			var that = this;
			$.ajax({
		        type: 'POST',
		        async: false,
		        url: '/api/challenge/details',
	            dataType: "json",
	            data: { "challenge_id": options.id},
	            success: function(data){
	            	console.log(data.results)
	            	that.set('challengeDetails',data.results);
	            }
	      	});
		},

		editChallenge : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        async: false,
		        url: '/api/challenge/edit',
		            dataType: "json",
		            data: { "challenge_name": options.challenge_name, "challenge_description": options.challenge_description, "challenge_start_date": options.challenge_start_date, "challenge_end_date": options.challenge_end_date, "challenge_id": options.challenge_id},
		            success: function(data){
		            	data2 = data.results;
		            }
		      });
			return data2;
		},

		subscribeChallenge : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/challenge/subscribe',
		        async: false,
		        data: {"challenge_id": options.challenge_id},
		            dataType: "json",
		            success: function(data){

		            }

		    });
		},

		unsubscribeChallenge : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/challenge/unsubscribe',
		        async: false,
		        data: {"challenge_id": options.challenge_id},
		            dataType: "json",
		            success: function(data){

		            }

		    });
		},

		getLeaderboard : function(time){
			var that = this;
			var data2 = {};
			if(typeof time == "undefined" || time == null){
				time = "7days";
			}
			$.ajax({
		        type: 'POST',
		        url: '/api/challenge/leaderboard/'+time,
		        async: false,
	            dataType: "json",
	            success: function(data){
	            	that.set('challengeLeaderboard',data.results);
	            	if(typeof window.challengeRightContentView != "undefined"){
	            		views.challengeRightContentView.leaderboardFilter = time;
	            	}
	            }

		    });
		}

	});

	var ChallengeCollection = Backbone.Collection.extend({
		model: ChallengeModel,

	});

	return {
		ChallengeModel : ChallengeModel,
		ChallengeCollection : ChallengeCollection
	};
});