define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var CircleModel = Backbone.Model.extend({
		
		getMyCircles : function(){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
		        async: false
		    });
			$.getJSON('/api/circles/mycircles', function(data){
          		that.set('myCircles',data.results);
        	});
		},

		newCircle : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/new',
		        dataType: "json",
		        async: false,
				data: { "circle_category": options.circle_category,  "circle_name": options.circle_name, "circle_description": options.circle_description, "circle_type": options.circle_type, "circle_album": options.circle_album},
		        success: function(data){
		            data2 = data;
		        }
		    });
		    return data2;
		},

		newTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/new',
		        dataType: "json",
		        async: false,
				data: { "circle_id": options.circle_id,  "topic_name": options.topic_name, "topic_body": options.topic_body},
		        success: function(data){
		            data2 = data;
		        }
		    });
		    return data2;
		},

		editTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/edit',
		        dataType: "json",
		        async: false,
				data: { "circle_id": options.circle_id,  "topic_name": options.topic_name, "old_topic_id": options.old_topic_id},
		        success: function(data){
		            data2 = data;
		        }
		    });
		    return data2;
		},

		editCircle : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/edit',
		        dataType: "json",
		        async: false,
				data: { "circle_category": options.circle_category, "circle_id": options.circle_id, "circle_name": options.circle_name, "circle_description": options.circle_description, "circle_type": options.circle_type, "circle_album": options.circle_album},
		        success: function(data){
		            data2 = data;
		        }
		    });
		    return data2;
		},

		getCircleDetails : function(id){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/details',
		        dataType: "json",
		        async: false,
				data: { "circle_id": id},
		        success: function(data){
		            that.set('circleDetails',data.results);
		            return true;
		        }
		    });
		},

		getTopicDetails : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic',
		        dataType: "json",
		        async: false,
				data: { "topic_id": options.topicid, "circle_id": options.id},
		        success: function(data){
		            that.set('topicDetails',data.results);
		        }
		    });
		},

		getCategories : function(){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'GET',
		        url: '/api/circles/categories',
		        dataType: "json",
		        async: false,
		        success: function(data){
		            that.set('categories',data.results);
		        }
		    });
		},

		browseCircles : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/browseCircles',
		        dataType: "json",
		        async: false,
		        data: {'type':options.type, 'order': options.order},
		        success: function(data){
		        	if(options.type == "overview"){
		        		that.unset('browseCirclesOfficial');
		        		that.unset('browseCirclesPopular');
		        		that.unset('browseCirclesActive');
			        	that.set('browseCirclesOfficial',data.official);
			            that.set('browseCirclesPopular',data.popular);
			            that.set('browseCirclesActive',data.active);
			        }else{
			        	that.unset('browseCircles');
			        	that.set('browseCircles',data.results);
			        }
		        }
		    });
		},

		searchCircles : function(term){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/searchCircles',
		        dataType: "json",
		        async: false,
		        data: {'term':term},
		        success: function(data){
			        that.set('searchCircles',data.results);
		        }
		    });
		},

		joinCircle : function(id){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/join',
		        dataType: "json",
		        async: false,
		        data: {'id':id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		unjoinCircle : function(id){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/unjoin',
		        dataType: "json",
		        async: false,
		        data: {'id':id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		requestJoinCircle : function(id){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/request',
		        dataType: "json",
		        async: false,
		        data: {'id':id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		unrequestJoinCircle : function(id){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/unrequest',
		        dataType: "json",
		        async: false,
		        data: {'id':id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		editComment : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/post/edit',
		        dataType: "json",
		        async: false,
		        data: {'post_id':options.comment_id, "edited_comment":options.edited_comment},
		        success: function(data){
			        return true;
		        }
		    });
		},

		deleteComment : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/post/delete',
		        dataType: "json",
		        async: false,
		        data: {'post_id':options.comment_id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		replyTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/post/reply',
		        dataType: "json",
		        async: false,
		        data: {'topic_id':options.topic_id, "post":options.reply},
		        success: function(data){
			        var data2 = data.id;
		        }
		    });
			return data2;
		},

		subscribeTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/subscribe',
		        dataType: "json",
		        async: false,
		        data: {'topic_id':options.topic_id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		unsubscribeTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/unsubscribe',
		        dataType: "json",
		        async: false,
		        data: {'topic_id':options.topic_id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		pinTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/pin',
		        dataType: "json",
		        async: false,
		        data: {'topic_id':options.topic_id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		unpinTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/topic/unpin',
		        dataType: "json",
		        async: false,
		        data: {'topic_id':options.topic_id},
		        success: function(data){
			        return true;
		        }
		    });
		},

		getCircleMembers : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/members',
		        dataType: "json",
		        async: false,
		        data: {'circle_id':options.id},
		        success: function(data){
			        that.set('circleMembers',data.results);
		        }
		    });
		},

		makeAdmin : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/members/makeAdmin',
		        dataType: "json",
		        async: false,
		        data: {'circle_id':options.circle_id,'author_id':options.author_id},
		        success: function(data){
			        
		        }
		    });
		},

		revokeAdmin : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/members/revokeAdmin',
		        dataType: "json",
		        async: false,
		        data: {'circle_id':options.circle_id,'author_id':options.author_id},
		        success: function(data){
			        
		        }
		    });
		},

		approveMember : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/members/approveMember',
		        dataType: "json",
		        async: false,
		        data: {'circle_id':options.circle_id,'author_id':options.author_id},
		        success: function(data){
			        
		        }
		    });
		},

		removeMember : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/circles/members/removeMember',
		        dataType: "json",
		        async: false,
		        data: {'circle_id':options.circle_id,'author_id':options.author_id},
		        success: function(data){
			        
		        }
		    });
		},

	});

	var CircleCollection = Backbone.Collection.extend({
		model: CircleModel,

	});

	return {
		CircleModel : CircleModel,
		CircleCollection : CircleCollection
	};
});