define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var StoryModel = Backbone.Model.extend({

		defaults : {
			id: null,
			story_name: null,
			story_description: null,
			story_type: null,
			story_category: null,
			written_by: null,
			is_public: null,
			tags: null,
			chapters: []
		},

		subscribeToStory : function(id){
			var that = this;
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/story/actions/subscribe',
		        dataType: "json",
		        data: {"id": id},
		        success: function(data){
		        	data2 = data;
		        }
		    });

		},

		unsubscribeToStory : function(id){
			var that = this;
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/story/actions/unsubscribe',
		        dataType: "json",
		        data: {"id": id},
		        success: function(data){
		        	data2 = data;
		        }
		    });

		},

		saveNewDraft : function(){
			var that = this;
			//console.log(this.get('chapters'));
			$.ajax({
				type: 'POST',
				url: '/api/story/new/draft/save',
		        dataType: "json",
		        async: "false",
		        data: {	"id": that.get('id'),
		        		"story_name": that.get('story_name'), 
		        		"story_description": that.get('story_description'),
		        		"story_type": that.get('story_type'),
		        		"story_category": that.get('story_category'),
		        		"written_by": that.get('written_by'),
		        		"is_public": 0,
		        		"tags": that.get('tags'),
		        		"album": that.get('album'),
		        		"chapters": { 	"id" : that.get("chapters").id,
		        						"chapter_name": that.get("chapters").chapter_name,
		        						"written_by": that.get("chapters").written_by,
		        						"chapter_body": that.get("chapters").chapter_body,
		        						"branch": that.get("chapters").branch
		        						}

		        	},
		        success: function(data, textStatus){
		          that.id = data.id;
		          if(data.success == 1){
		          	if(data.type == "new"){
		          		that.set('id',data.story_id);
		          		var thisChapter = that.get('chapters');
		          		thisChapter.id = data.chapter_id;
		          		that.set('chapters',thisChapter);
		          	}
		          	window.app.vent.trigger('newStory:saveDraftSuccess');
		          }else{
		          	window.app.vent.trigger('newStory:saveDraftFailure');
		          }
		        },
		        error: function(error){
		        	window.app.vent.trigger('newStory:saveDraftFailure');
		        }
			});
		},

		publishNewStory : function(){
			var that = this;
			$.ajax({
				type: 'POST',
				url: '/api/story/new/publish',
		        dataType: "json",
		        data: {	"id": that.get('id'),
		        		"story_name": that.get('story_name'), 
		        		"story_description": that.get('story_description'),
		        		"story_type": that.get('story_type'),
		        		"story_category": that.get('story_category'),
		        		"written_by": that.get('written_by'),
		        		"is_public": 1,
		        		"tags": that.get('tags'),
		        		"album": that.get('album'),
		        		"chapters": { 	"id" : that.get("chapters").id,
		        						"chapter_name": that.get("chapters").chapter_name,
		        						"written_by": that.get("chapters").written_by,
		        						"chapter_body": that.get("chapters").chapter_body,
		        						"branch": that.get("chapters").branch
		        						}
		        	},
		        success: function(data){
		          that.id = data.id;
		          if(data.success == 1){
		          	if(data.type == "new"){
		          		that.set('id',data.story_id);
		          		var thisChapter = that.get('chapters');
		          		thisChapter.id = data.chapter_id;
		          		that.set('chapters',thisChapter);
		          	}
		          	var options = {};
		          	options.story_id = data.story_id;
		          	options.story_slug = data.story_slug;
		          	options.chapter_slug = data.chapter_slug;
		          	window.app.vent.trigger('newStory:publishSuccess',options);
		          }else{
		          	window.app.vent.trigger('newStory:publishFailure');
		          }
		        },
		        error: function(error){
		        	window.app.vent.trigger('newStory:publishFailure');
		        }
			});
		},

		getStories : function(author,page){
			var that = this;
			data2 = {};
			$.ajaxSetup({
		        async: false
		    });
			$.getJSON('/api/story/author/get/'+author+'/'+page, function(data){
				that.set('numberOfAuthorStories',data.numberOfAuthorStories);
				that.set('authorStories',data.results);
				data2 = data;
			});
			return data2;
		},

		getChaptersFromAuthor : function(author,page){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.getJSON('/api/chapters/author/get/'+author+'/'+page,function(data){
				that.set('numberOfAuthorChapters',data.numberOfAuthorChapters);
		        that.set("authorChapters",data.response);
		    });
		},

		getStory : function(id){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
		        async: false
		    });
			$.getJSON('/api/story/id/get/'+id, function(data){
				
				console.log(data);
				//window.app.vent.trigger('stories:loadingComplete', data);
				data2 = data;
			});
			return data2;
		},

		getChapter : function(id){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.getJSON('/api/chapter/get/'+id, function(data){
				data2 = data;
			});
			return data2;
		},

		saveChapterDraft : function(options){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/save',
		        dataType: "json",
		        data: {	"parent_id": options.parent_id, "story_id": options.story_id, "chapter_name": options.chapter_name, "chapter_body": options.chapter_body, "new_chapter_id": options.new_chapter_id},
		        success: function(data){
		        	data2 = data;
		        }
		    });
			return data2;
		},

		publishChapter : function(options){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/publish',
		        dataType: "json",
		        data: {	"parent_id": options.parent_id, "story_id": options.story_id, "chapter_name": options.chapter_name, "chapter_body": options.chapter_body, "new_chapter_id": options.new_chapter_id},
		        success: function(data){
		        	data2 = data;
		        }
		    });
			return data2;
		},

		editChapter : function(options){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/edit',
		        dataType: "json",
		        data: {	"chapter_name": options.chapter_name, "chapter_body": options.chapter_body, "chapter_id": options.chapter_id},
		        success: function(data){
		        	data2 = data;
		        }
		    });
			return data2;
		},

		editStory : function(options){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/story/actions/edit',
		        dataType: "json",
		        data: { "album": options.album, "story_id": options.story_id, "story_name": options.story_name, "story_description": options.story_description, "story_type": options.story_type, "categories" : options.categories, "is_public": options.is_public, "story_tags": options.story_tags },
		        success: function(data){
		        	data2 = data.slug;
		        }
		    });
			return data2;
		},

		deleteChapter : function(options){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/delete',
		        dataType: "json",
		        data: {	"chapter_id": options.chapter_id},
		        success: function(data){
		        	data2 = data;
		        }
		    });
			return data2;
		},

		getAllStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category == null){
				$.getJSON('/api/story/get-all/'+period+'/'+page, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-all/'+period+'/'+page+'/'+category, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}
		},

		getPopularStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-popular/'+period+'/'+page+'/'+category, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-popular/'+period+'/'+page, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}
		},

		getModulePopularStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-popular/'+period+'/'+page+'/'+category, function(data){
					that.set('modulePopularStorySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-popular/'+period+'/'+page, function(data){
					that.set('modulePopularStorySearch', data.results);
				});
			}
		},

		getHomepagePopularStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-homepage-popular/'+period+'/'+page+'/'+category, function(data){
					that.set('storySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-homepage-popular/'+period+'/'+page, function(data){
					that.set('storySearch', data.results);
				});
			}
		},

		getRecentStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-recent/'+period+'/'+page+'/'+category, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-recent/'+period+'/'+page, function(data){
					that.set('numberOfStories',data.numberOfStories);
					that.set('storySearch', data.results);
				});
			}
		},

		getModuleRecentStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-recent/'+period+'/'+page+'/'+category, function(data){
					that.set('moduleRecentStorySearch', data.results);
				});
			}else{
				$.getJSON('/api/story/get-recent/'+period+'/'+page, function(data){
					that.set('moduleRecentStorySearch', data.results);
				});
			}
		},

		getHomepageRecentStories : function(period, page, category){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
		    if(category != "" && category != null){
				$.getJSON('/api/story/get-homepage-recent/'+period+'/'+page+'/'+category, function(data){
					that.set('recentStories', data.results);
				});
			}else{
				$.getJSON('/api/story/get-homepage-recent/'+period+'/'+page, function(data){
					that.set('recentStories', data.results);
				});
			}
		},

		getCategories : function(){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.getJSON('/api/categories', function(categories){
				data2 = categories;
				that.set('categories',categories);
			});
			return data2;
		},

		getStoryTypes : function(){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
				async: false
			});
			$.getJSON('/api/storyTypes', function(storyTypes){
				data2 = storyTypes;
			});
			return data2;
		},

		readChapter : function(story_id, chapter_slug){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/read',
		        dataType: "json",
		        data: { "story_id": story_id, "chapter_slug": chapter_slug},
		        success: function(data){

		        }
		    });
		},

		wasHelpful : function(story_comment_id){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/helpful',
		        dataType: "json",
		        data: { "story_comment_id": story_comment_id },
		        success: function(data){

		        }
		    });
		},

		notHelpful : function(story_comment_id){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: '/api/chapter/actions/notHelpful',
		        dataType: "json",
		        data: { "story_comment_id": story_comment_id },
		        success: function(data){

		        }
		    });
		},

		getRecentComments: function(){
			var that = this;
			var data2 = {};
			$.getJSON('/api/chapters/comment/getLatestComments',function(data){
		        that.set("recentComments",data.results);
		    });
		}
	});

	var StoryCollection = Backbone.Collection.extend({
		model: StoryModel,
		url: '/',
	});

	return {
		StoryModel : StoryModel,
		StoryCollection : StoryCollection
	};
});
