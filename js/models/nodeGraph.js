define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var nodeGraph = Backbone.Model.extend({
		nodeNumber: 0,
	    height: 718,
	    root: [],
	    xArray: [],
	    yArray: [],
	    random: [],
	    numberOfNodes: 0,
	    force: [],
	    svg: [],
	    link: [],
	    node: [],
	    nodes: [],

		init: function(){
			var that = this;
			$("svg").remove();
		    var width = $('#chapterNodes').width();
		    var height = 400;
		    that.force = d3.layout.force()
		    .linkDistance(60)
		    .charge(-1500)
		    .gravity(.2)
		    .size([width, height])
		    .on("tick", this.tick);

		    svg = d3.select("#chapterNodes")
		    .append("svg")
		    .attr("width", width)
		    .attr("height", height);
		    $('.legend-container-master').css('display','block');
		    link = svg.selectAll(".link"),
		    node = svg.selectAll(".node");
		},

		tick: function(){
			var that = this;
			var targetX;
			var targetY;
		  //node.attr("cx", function(d) { return d.x = Math.max(50, Math.min(width - 50, d.x)); })
		    //.attr("cy", function(d) { return d.y = Math.max(50, Math.min(height - 50, d.y)); });

		link.attr("x1", function(d) { return d.source.x; })
		    .attr("y1", function(d) { return d.source.y; })
		    .attr("x2", function(d) { return d.target.x; })
		    .attr("y2", function(d) { return d.target.y; });

		node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
			var oddEven = 1;
			var highestPoint = 0;
			var lowestPoint = 0;
		   $.each(node[0],function(i,j){
			if(node[0][i].__data__.py >= highestPoint){
				highestPoint = node[0][i].__data__.py;
			}
			if(node[0][i].__data__.py <= lowestPoint){
				lowestPoint = node[0][i].__data__.py;
			}
			currentBranch = node[0][i].__data__.branch;

			if(node[0][i].__data__.level == 2 && oddEven == 1 && currentBranch == node[0][i+1].__data__.branch){
		    	node[0][i].childNodes[1].attributes[0].nodeValue = "20";
			oddEven = 0;
		     }else if(node[0][i].__data__.level == 2 && oddEven == 0 && currentBranch == node[0][i+1].__data__.branch){
		    	node[0][i].childNodes[1].attributes[0].nodeValue = "-15";
			oddEven = 1;
		     }else{
		    	node[0][i].childNodes[1].attributes[0].nodeValue = "-15";
		     }
		     });
			//console.log(highestPoint + " " + Math.abs(lowestPoint) + " " + height);
			svgSize = highestPoint + Math.abs(lowestPoint);
			width = $("#chapterNodes").width();
			/**
			if(svgSize > 400){
				svg.attr("height",svgSize + 20);
				svg.attr("width", width);
				that.force.size([width,svgSize]);
			}else{
				svg.attr("height","400");
				svg.attr("width", width);
				that.force.size([width,400]);
			}**/
		},

		activateNodeGraph: function(chapters){

			var chapterNodes = [];
			var thisNode = {
				"id": chapters[0].id,
				"name": chapters[0].chapter_name,
				"branch": 1,
				"children": [
				]
			}
			chapterNodes.push(thisNode);
			chapters.shift();

			_.each(chapters, function(chapter){
				var thisNode = {
					"id": chapter.id,
					"name": chapter.chapter_name,
					"branch": 1,
					"children": [

					]
				}
				chapterNodes[0].children.push(thisNode);
			});
			this.root = "";
			this.update();
			this.root = chapterNodes;
			this.update();
		},

		update: function(){
			var nodes = this.flatten(this.root),
		      links = d3.layout.tree().links(nodes);
		  var nodeNumber = 0;
		  var that = this;
		  // Restart the force layout.
		  that.force
		      .nodes(nodes)
		      .links(links)
		      .start();

		  // Update links.
		  link = link.data(links, function(d) { return d.target.id; });

		  link.exit().remove();

		  link.enter().insert("line", ".node")
		      .attr("class", "link");

		  // Update nodes.
		  node = node.data(nodes, function(d) { return d.id; });

		  node.exit().remove();

		  //var nodeEnter = new Object();

		  var nodeEnter = node.enter().append("g")
		      .attr("class", "node")
		      //.attr("id","1")
		      .on("click", this.click)
		      .on("mouseover", function(d) {
			
			    //svg.selectAll("g").sort(function (a, d) { // select the parent and sort the path's
			      //if (a.id && d.id && a.id != d.id) return -1;               // a is not the hovered element, send "a" to the back
			      //else return 1;                             // a is the hovered element, bring "a" to the front
			  //});
			}).call(this.force.drag);

			  nodeEnter.append("circle")
			      .attr("r", function(d) { return "8"; })
			      .style("fill", function(d) {if(d.type == "id"){ return "rgb(232, 75, 62)";}else if(d.type == "defendant"){return "rgb(81,149,66)";}else if(d.type == "solicitor"){return "rgb(34,54,86)";}else if(d.type == "court"){return "grey";}else if(d.type == "magistrate"){return "rgb(54,108,133)";}else if(d.type == "company"){return "rgb(45,156,84)";}else if(d.type == "director"){return "rgb(238,130,0)";}})
				.style("fill-opacity","100%")
				.style("stroke", function(d){if(d.type == "id"){ return "rgb(238,116,48)";}else if(d.type == "defendant"){return "rgb(114,191,67)";}else if(d.type == "solicitor"){return "rgb(42,98,140)";}else if(d.type == "court"){return "grey";}else if(d.type == "magistrate"){return "rgb(33,150,178)";}else if(d.type == "company"){return "rgb(35,153,49)";}else if(d.type == "director"){return "rgb(201,119,57)";}})
				.style("stroke-width","5px")
			      .on("mouseover",this.mouseover1)
			    .on("mouseout",this.mouseout);

			  nodeEnter.append("text")
			      .attr("dy", "-10")
			      .attr("dx", "0")
			      .attr("cx","0")
			      .attr("cy","0")
			      .text(function(d) {  return d.name; });
			  //$( "g" ).last().attr( "class", "node rootNode" );
			 	numberOfNodes = $("g").length;

		},

		flatten: function(root) {
		  var nodes = [], i = 0;

		  function recurse(node) {
		    if (node.children) node.children.forEach(recurse);
		    if (!node.id) node.id = ++i;
		    nodes.push(node);
		  }

		  recurse(root);
		  console.log(nodes);
		  return nodes;
		},

		click : function(d) {
			console.log(d)
			var that = this;
			if (d3.event.defaultPrevented) return; // ignore drag
			  if (d.children) {
			    d._children = d.children;
			    d.children = null;
			  } else {
			    d.children = d._children;
			    d._children = null;
			  }
			  window.views.storyView.models.nodeGraph.update();
		},

		mouseover1: function(){

		},

		mouseover: function(){

		}
	});

	return nodeGraph;

});