define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var MessageModel = Backbone.Model.extend({
		
		startNewMessageThread : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: '/api/messages/thread/new',
	            dataType: "json",
	            async: false,
	            data: {"sender":window.storage.get('profile').id,"recipient":options.recipient,"thread_title":options.thread_title,"message":options.message},
	            success: function(data){
	            	data2 = data;
	            }
			});
			return data2;
		},

		startNewMessage : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: '/api/messages/message/send',
	            dataType: "json",
	            async: false,
	            data: {"thread_id":options.thread_id,"message":options.message},
	            success: function(data){
	            	
	            }
			});
		},

		getMessage : function(options){
			var that = this;
			var data2 = {};
			console.log(options)
			$.ajax({
				type: 'POST',
				url: '/api/messages/thread/get',
	            dataType: "json",
	            async: false,
	            data : {"slug":options.slug,"id":options.id},
	            success: function(data){
	            	that.set('message',data.result);
	            }
			});
		},

		getAllOfAuthorsInbox : function(){
			var that = this;
			var data2 = {};
			$.getJSON('/api/messages/message/inbox', function(data){
				that.set('inbox',data.result);
				console.log(data.result);
				if(data.result.length < 1){
            		window.controllers.messagesController.postRenderInbox(that);
            	}
			});
		},

		getAllOfAuthorsSent : function(){
			var that = this;
			var data2 = {};
			$.getJSON('/api/messages/message/sent', function(data){
				that.set('sent',data.result);
				if(data.result.length < 1){
            		window.controllers.messagesController.postRenderSent(that);
            	}
			});
		}

	});

	var MessageCollection = Backbone.Collection.extend({
		model: MessageModel,

	});

	return {
		MessageModel : MessageModel,
		MessageCollection : MessageCollection
	};
});