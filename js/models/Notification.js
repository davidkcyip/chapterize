define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var NotificationModel = Backbone.Model.extend({

		getNewNotifications : function(last_checked){
			var that = this;
			var now = Math.round(new Date().getTime()/1000);
	        storage.set('last_checked',now);
			$.ajax({
		        type: 'GET',
		        async: false,
		        url: '/api/notifications/new',
	            dataType: "json",
	            data: { "last_checked": last_checked},
	            success: function(data){
	            	that.set('newNotifications',data.results);
	            }
		   	});
		},

		getAllUnreadNotifications : function(){
			var that = this;
			$.ajax({
		        type: 'GET',
		        async: false,
		        url: '/api/notifications/unread',
	            dataType: "json",
	            success: function(data){
	            	that.set('unreadNotifications',data.results);
	            }
		   	});
		},

		getAllNotifications : function(){
			this.set('allNotifications');
			var that = this;
			$.ajax({
		        type: 'GET',
		        async: false,
		        url: '/api/notifications/all',
	            dataType: "json",
	            success: function(data){
	            	that.set('allNotifications',data.results);
	            }
		   	});
		},

		readNotification : function(id){
			var that = this;
			$.ajax({
		        type: 'GET',
		        async: false,
		        url: '/api/notifications/read',
	            dataType: "json",
	            data: { "id": id},
	            success: function(data){
	            	that.set('unreadNotifications',data.results);
	            }
		   	});
		}
	});

	var NotificationCollection = Backbone.Collection.extend({
		model: NotificationModel,

	});

	return {
		NotificationModel : NotificationModel,
		NotificationCollection : NotificationCollection
	};
});