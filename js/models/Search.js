define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var SearchModel = Backbone.Model.extend({
		
		getAllStories : function(){
			var that = this;
			var data2 = {};
			$.ajaxSetup({
		        async: false
		    });
			$.getJSON('/api/story/get-all', function(data){
				that.set('modulePopularStorySearch',data.numberOfStories);
				that.set('storySearch', data.results);
			});
		},

		searchResults : function(query,page){
			var that = this;
			var data2 = {};
			$.getJSON('/api/search/search-all?query='+query+'&page='+page, function(data){
				that.set('searchCount', data.searchCount);
				that.set('searchResults', data.results);
			});
		},

		getAllTags : function(){
			var that = this;
			var data2 = {};
			$.getJSON('/api/search/all-tags', function(data){
				that.set('searchTags', data.results);
			});
		},

		getSearchTagStories : function(settings,page){
			var that = this;
			var data2 = {};
			$.getJSON('/api/search/tag-stories/'+page+'?query='+settings, function(data){
				that.set('searchTagStoriesNumberOfStories',data.numberOfStories);
				that.set('searchTagStories', data.results);
			});
		}
	});

	var SearchCollection = Backbone.Collection.extend({
		model: SearchModel,

	});

	return {
		SearchModel : SearchModel,
		SearchCollection : SearchCollection
	};
});