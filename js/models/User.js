define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var UserModel = Backbone.Model.extend({
		
		getUser : function(author){
			var that = this;
			var data2 = {};
			$.getJSON('/api/getProfile/'+author, function(user){
				that.set('profileUser',user);
			});
		},
		
		getAllAuthors : function(period,page){
			var that = this;
			$.getJSON('/api/user/get-all/'+period+'/'+page, function(data){
				that.set('numberOfAuthors',data.numberOfAuthors);
				that.set('userSearch', data.results);
			});
		},

		getRecentAuthors : function(period,page){
			var that = this;
			$.getJSON('/api/user/get-recent/'+period+'/'+page, function(data){
				that.set('numberOfAuthors',data.numberOfAuthors);
				that.set('userSearch', data.results);
			});
		},

		getPopularAuthors : function(period,page){
			var that = this;
			$.getJSON('/api/user/get-popular/'+period+'/'+page, function(data){	
				that.set('numberOfAuthors',data.numberOfAuthors);
				that.set('userSearch', data.results);
			});
		},

		sendReport : function(options){
			var that = this;
			$.ajax({
				type: 'POST',
				url: '/api/user/report',
	            dataType: "json",
	            async: false,
	            data: {"id": options.id,"message":options.message},
	            success: function(data){
	            	
	            }
			});
		},

		getFollowSuggestions : function(){
			var that = this;
			$.ajax({
				type: 'GET',
				url: '/api/user/suggestions',
	            dataType: "json",
	            async: false,
	            success: function(data){
	            	console.log('hi')
	            	that.set('suggestions',data);
	            }
			});
		}
	});

	var UserCollection = Backbone.Collection.extend({
		model: UserModel,

	});

	return {
		UserModel : UserModel,
		UserCollection : UserCollection
	};
});