define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var SettingsModel = Backbone.Model.extend({
		
		getEmailSettings : function(){
			var that = this;
			$.getJSON('/api/settings/email/get', function(data){
				that.set('emailSettings', data.results);
			});
		},

		saveEmailSettings : function(checked){
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        dataType: 'json',
		        url: '/api/settings/email/save',
		        async: false,
		        data: {"emailSettings": checked},
		        success: function(data){
		          data2 = data.success;
		        }

		    });
		    return data2;
		}
	});

	var SettingsCollection = Backbone.Collection.extend({
		model: SettingsModel,

	});

	return {
		SettingsModel : SettingsModel,
		SettingsCollection : SettingsCollection
	};
});