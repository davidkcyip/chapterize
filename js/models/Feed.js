define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var FeedModel = Backbone.Model.extend({
		
		getInitialFeed : function(type, username, updates){
			var data2 = {};
			var that = this;
			if(updates == null){
				updates = "all";
			}
			$.ajax({
				type: 'GET',
				url: "/api/feed/getInitial/"+username+"/"+type+"/"+updates,
	            dataType: "json",
	            async: false,
	            success: function(feed){
	            	if(updates == "all"){
	            		that.set('initialFeed', feed);
	            	}else{
	            		that.set('previousFeed', feed);
	            	}
	            }
			});
		},

		getHomepageFeed : function(type){
			var data2 = {};
			var that = this;
			if(type == null){
				type = "all";
			}
			$.ajax({
				type: 'GET',
				url: "/api/feed/getInitial/all/homepageStoryUpdates/"+type,
	            dataType: "json",
	            success: function(feed){
	            	if(feed.length < 10){
				    	$('.homepageLoadMore').remove();
				    }
	            	that.set('homepageFeed', feed);
	            	$('.homepageLoadMore').html("LOAD PREVIOUS UPDATES");
	            }
			});
		},

		getSubscriptionList : function(id){
			var data2 = {};
			var that = this;
			$.getJSON("/api/feed/getSubscriptionsList/"+id,function(subscriptions){
				if(id == window.storage.get('profile').id){
					that.set('mySubscriptionList', subscriptions);
				}else{
					that.set('profileSubscriptionList', subscriptions);
				}
			});
		},

		getBasicStats : function(id){
			var data2 = {};
			var that = this;
			$.getJSON("/api/getBasicStats/"+id, function(basicStats){
				that.set('basicStats', basicStats);
			});
		},

		getProfile : function(username){
			var that = this;
			$.getJSON("/api/getProfile/"+username, function(profile){
				that.set('profile',profile);
			});
		},

		getStatus : function(id){
			var that = this;
			$.ajax({
				type: 'GET',
				url: "/api/feed/status",
	            dataType: "json",
	            data: {"id":id},
	            async: false,
	            success: function(data){
	            	that.set('initialFeed',data.results);
	            }
			});
		},

		repost : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
				type: 'POST',
				url: "/api/feed/repost",
	            dataType: "json",
	            data: {"id":options.repost_id, "shareText": options.share_text},
	            async: false,
	            success: function(data){
	            	data2 = data.results;
	            }
			});
			return data2;
		}
	});

	var FeedCollection = Backbone.Collection.extend({
		model: FeedModel,

	});

	return {
		FeedModel : FeedModel,
		FeedCollection : FeedCollection
	};
});