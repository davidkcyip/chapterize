define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var ChapterModel = Backbone.Model.extend({
		
		getAllChapters : function(period,page){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
			$.getJSON('/api/chapter/get-all/'+period+'/'+page, function(data){
				that.set('numberOfChapters',data.numberOfChapters);
				that.set('chapterSearch', data.results);
			});
		},

		getRecentChapters : function(period,page){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
			$.getJSON('/api/chapter/get-recent/'+period+'/'+page, function(data){
				that.set('numberOfChapters',data.numberOfChapters);
				that.set('chapterSearch', data.results);
			});
		},

		getPopularChapters : function(period,page){
			var that = this;
			var data2 = {};
			if(page == null){
				page = 1;
			}
			$.getJSON('/api/chapter/get-popular/'+period+'/'+page, function(data){
				that.set('numberOfChapters',data.numberOfChapters);
				that.set('chapterSearch', data.results);
			});
		},
	});

	var ChapterCollection = Backbone.Collection.extend({
		model: ChapterModel,

	});

	return {
		ChapterModel : ChapterModel,
		ChapterCollection : ChapterCollection
	};
});