define([
	'underscore',
	'backbone',
], function(_, Backbone){

	var AppRouter = Backbone.Router.extend({
		controllers: {},
		home: {},
		profile: {},

		initialize : function(){
			
		},

		routes : {
			'' : 'homepage',
			'about' : 'about',
			'help' : 'help',
			'contact' : 'contact',
			'home' : 'home',
			'profile/:username' : 'profile',
			'profile/:username/status/:id' : 'profile',
			'profile' : 'profile',
			'verify' : 'verify',
			'activate/:id/:activation' : 'activateAccount',
			'story/new' : 'newStory',
			'story/edit/:slug/:id' : 'editStory',
			'stories/:author' : 'storyList',
			'stories/:author/:page' : 'storyList',
			'chapters/:author' : 'chapterList',
			'chapters/:author/:page' : 'chapterList',
			'story/:id/:stori' : 'story',
			'story/:id/:stori/:chapter' : 'story',
			'chapter/:story/:chapterId/:action' : 'chapter',
			'search' : 'search',
			'search/:type' : 'search',
			'search/:type/:settings' : 'search',
			'search/:type/:settings/:page' : 'search',
			'mobileLogin' : 'mobileLogin',
			'mobileRegister' : 'mobileRegister',
			'mobileForgotPassword' : 'mobileForgotPassword',
			'messages/:action' : 'messages',
			'messages/:action/:id/:slug' : 'messages',
			'brainstorm' : 'brainstorm',
			'brainstorm/:story' : 'brainstorm',
			'brainstorm/:id/:story' :'brainstorm',
			'brainstorm/:id/:story/:action' :'brainstorm',
			'brainstorm/:id/:story/:action/:thread_id/:topic' :'brainstorm',
			'challenge' : 'challenge',
			'challenge/new' : 'newChallenge',
			'challenge/browse' : 'browseChallenge',
			'challenge/winners' : 'challengeWinners',
			'challenge/winners/:page' : 'challengeWinners',
			'challenge/active' : 'activeChallenge',
			'challenge/active/:page' : 'activeChallenge',
			'challenge/previous' : 'previousChallenge',
			'challenge/previous/:page' : 'previousChallenge',
			'challenge/leaderboard' : 'challengeLeaderboard',
			'challenge/:id' : 'challenge',
			'challenge/:id/:slug' : 'challenge',
			'challenge/:id/:slug/:action' : 'challenge',
			'settings/:type' : 'settings',
			'notifications' : 'notifications',
			'circle/browse' : 'browseCircle', //done
			'circle/browse/:type/:order/:page' : 'browseCircle', //done
			'circle/new' : 'newCircle', //done
			'circle/search/:term/:page' : 'searchCircle', //done
			'circle/:id/:groupSlug/topics' : 'circle',
			'circle/:id/:groupSlug/topics/:page' : 'circle',
			'circle/:id/:groupSlug/topic/:topicId/:topicSlug' : 'circleTopic',
			'circle/:id/:groupSlug/topic/:topicId/:topicSlug/:topicAction' : 'circleTopic',
			'circle/:id/:groupSlug/:groupAction' : 'circle',
			'*notFound': 'notFound'
		},

		browseCircle : function(type, order, page){
			var options = {};
			if(type == null){
				options.type = "overview";
			}else{
				options.type = type;
			}
			options.order = order;
			options.page = page;
			options.action = "browseCircle";
			console.log(options)
			require(['js/controllers/circle'], function(CircleController){
				window.controllers.circleController = new CircleController(options);
			});	
		},

		circle : function(groupId, groupSlug, groupAction){
			var that = this;
			var options = {};
			if(groupSlug == null && groupAction == null){
				options.action = "viewCircles";
				options.id = groupId;
			}else if(groupSlug != null && groupAction == null){
				options.action = "viewCircleTopics";
				options.groupslug = groupSlug;
				options.id = groupId;
				options.page = 1;
				$('head').append('<meta name="fragment" content="!">');
			}else if(groupSlug != null && groupAction == "edit"){
				options.action = "editCircle";
				options.groupslug = groupSlug;
				options.id = groupId;
			}else if(groupSlug != null && groupAction == "manage-users"){
				options.action = "manageUsers";
				options.groupslug = groupSlug;
				options.id = groupId;
			}else if(groupSlug != null && groupAction == "new-topic"){
				options.action = "newTopic";
				options.groupslug = groupSlug;
				options.id = groupId;
			}else if(groupSlug != null && app.isNumber(groupAction)){
				options.action = "viewCircleTopics";
				options.groupslug = groupSlug;
				options.id = groupId;
				options.page = groupAction;
				$('head').append('<meta name="fragment" content="!">');
			}
			console.log(options)
			require(['js/controllers/circle'], function(CircleController){
				window.controllers.circleController = new CircleController(options);
			});	
		},

		searchCircle : function(term, page){
			var that = this;
			var options = {};
			options.action = "searchCircles";
			options.term = term;
			options.page = page;
			require(['js/controllers/circle'], function(CircleController){
				window.controllers.circleController = new CircleController(options);
			});	
		},

		circleTopic : function(groupId, groupSlug, topicId, topicSlug, topicAction){
			var that = this;
			var options = {};
			if(groupSlug != null && topicSlug != null && topicAction == null){
				options.groupslug = groupSlug;
				options.topicslug = topicSlug;
				options.topicid = topicId;
				options.action = "viewTopic";
				options.id = groupId;
				options.page = 1;
				$('head').append('<meta name="fragment" content="!">');
			}else if(groupSlug != null && topicSlug != null && topicAction == "edit"){
				options.groupslug = groupSlug;
				options.topicslug = topicSlug;
				options.topicid = topicId;
				options.action = "editTopic";
				options.id = groupId;
			}else if(groupSlug != null && topicSlug != null && !isNaN(topicAction)){
				options.groupslug = groupSlug;
				options.topicslug = topicSlug;
				options.topicid = topicId;
				options.action = "viewTopic";
				options.id = groupId;
				options.page = topicAction;
				$('head').append('<meta name="fragment" content="!">');
			}
			require(['js/controllers/circle'], function(CircleController){
				window.controllers.circleController = new CircleController(options);
			});	
		},

		newCircle : function(){
			var that = this;
			var options = {};
			options.action = "newCircle";
			require(['js/controllers/circle'], function(CircleController){
				window.controllers.circleController = new CircleController(options);
			});	
		},

		homepage : function(){
			var that = this;
			$('head').append('<meta name="fragment" content="!">');
			require(['js/controllers/homepage'], function(HomeController){
		        window.controllers.homepage = new HomeController();
		    });
		},

		about : function(){
			$('head').append('<meta name="fragment" content="!">');
			require(['js/controllers/about'], function(AboutController){
		        window.controllers.about = new AboutController();
		    });
		},

		help : function(){
			require(['js/controllers/help'], function(HelpController){
		        window.controllers.help = new HelpController();
		    });
		},

		contact : function(){
			require(['js/controllers/contact'], function(ContactController){
		        window.controllers.contact = new ContactController();
		    });
		},

		home : function(){
			if(typeof window.controllers.home != "undefined"){
				window.controllers.home.remove();
			}
			require(['js/controllers/home'], function(HomeController){
		        window.controllers.home = new HomeController();
		    });
		},

		profile : function(username, id){
			var that = this;
			$('head').append('<meta name="fragment" content="!">');
			if(username == null || typeof username == "undefined"){
				username = windows.storage.get('profile').pen_name;
			}
			require(['js/controllers/profile'], function(ProfileController){
		        window.controllers.profile = new ProfileController({username: username, id: id});
		    });
		},

		verify : function(){
			require(['js/controllers/home','js/controllers/verify'], function(HomeController,VerifyController){
		        window.controllers.home = new HomeController();
		        window.controllers.verify = new VerifyController();
		    });
		},

		activateAccount : function(id, activation_code){
			$.ajax({
				type: "POST",
	            url: '/api/activate',
	            dataType: "json",
	            data: {"id": id, "activation_code": activation_code},
	            success: function(data){
	            	console.log(data);
	            	if(data.error == 0){
	            		require(['js/controllers/homepage','js/controllers/activationSuccess'], function(HomePageController,ActivationSuccessController){
	            			window.controllers.homepage = new HomePageController();
							window.controllers.activationSuccess = new ActivationSuccessController();
						});
	            	}else{
	            		require(['js/controllers/homepage','js/controllers/activationError'], function(HomePageController,ActivationErrorController){
	            			window.controllers.homepage = new HomePageController();
							window.controllers.activationError = new ActivationErrorController();
						});
	            	}
	            }
			});
		},

		newStory : function(){
			require(['js/controllers/newStory'], function(NewStoryController){
				window.controllers.newStoryController = new NewStoryController();
			});
		},

		chapter : function(story, chapterId, action){
			if(action == "new"){
				require(['js/controllers/newChapter'], function(NewChapterController){
					var options = {};
					options.story = story;
					options.chapterId = chapterId;
					options.action = action;
					window.controllers.newChapterController = new NewChapterController(options);
				});
			}else if(action == "edit"){
				require(['js/controllers/editChapter'], function(EditChapterController){
					var options = {};
					options.story = story;
					options.chapterId = chapterId;
					options.action = action;
					window.controllers.editChapterController = new EditChapterController(options);
				});
			}
		},

		storyList : function(author,page){
			$('head').append('<meta name="fragment" content="!">');
			var options = {};
			options.author = author;
			if(page == null){
				options.page = 1;
			}else{
				options.page = page;
			}
			$('head').append('<meta name="fragment" content="!">');
			if(typeof window.controllers.storyListController != "undefined"){
				window.controllers.storyListController.unbind();
				window.controllers.storyListController.remove();
			}
			require(['js/controllers/storyList'], function(StoryListController){
				window.controllers.storyListController = new StoryListController(options);
			});
		},

		chapterList : function(author,page){
			$('head').append('<meta name="fragment" content="!">');
			var options = {};
			options.author = author;
			if(page == null){
				options.page = 1;
			}else{
				options.page = page;
			}
			$('head').append('<meta name="fragment" content="!">');
			if(typeof window.controllers.chapterListController != "undefined"){
				window.controllers.chapterListController.unbind();
				window.controllers.chapterListController.remove();
			}
			require(['js/controllers/chapterList'], function(ChapterListController){
				window.controllers.chapterListController = new ChapterListController(options);
			});
		},

		story : function(id, stori, chapter){
			var options = {};
			options.id = id;
			options.story = stori;
			options.chapter = chapter;
			if(typeof window.controllers.storyController != "undefined"){
				window.controllers.storyController.unbind();
				window.controllers.storyController.remove();
				console.log('zombie controller')
			}
			$('head').append('<meta name="fragment" content="!">');
			require(['js/controllers/story'], function(StoryController){
				console.log(id)
				window.controllers.storyController = new StoryController(options);
			});
		},

		editStory : function(slug, id){
			var options = {};
			options.slug = slug;
			options.id = id;
			if(typeof window.controllers.editStoryController != "undefined"){
				window.controllers.editStoryController.unbind();
				window.controllers.editStoryController.remove();
			}
			require(['js/controllers/editStory'], function(EditStoryController){
				window.controllers.editStoryController = new EditStoryController(options);
			});
		},

		search : function(type, settings, page){
			var options = {};
			if( !isNaN(type)){
				options.page = type;
				options.type = null;
			}else{
				options.page = page;
				options.type = type;
			}
			options.settings = settings;
			if(typeof window.controllers.searchController != "undefined"){
				window.controllers.searchController.remove();
			}
			require(['js/controllers/search'], function(SearchController){
				window.controllers.searchController = new SearchController(options);
			});
		},

		mobileLogin : function(){
			if(typeof window.controllers.mobileLoginController != "undefined"){
				window.controllers.mobileLoginController.remove();
			}
			require(['js/controllers/mobileLogin'], function(MobileLoginController){
				window.controllers.mobileLoginController = new MobileLoginController();
			});
		},

		mobileRegister : function(){
			if(typeof window.controllers.mobileRegisterController != "undefined"){
				window.controllers.mobileRegisterController.remove();
			}
			require(['js/controllers/mobileRegister'], function(MobileRegisterController){
				window.controllers.mobileRegisterController = new MobileRegisterController();
			});
		},

		mobileForgotPassword : function(){
			if(typeof window.controllers.mobileForgotPasswordController != "undefined"){
				window.controllers.mobileForgotPasswordController.remove();
			}
			require(['js/controllers/mobileForgotPassword'], function(MobileForgotPasswordController){
				window.controllers.mobileForgotPasswordController = new MobileForgotPasswordController();
			});
		},

		messages : function(action, id, slug){
			if(typeof window.controllers.messagesController != "undefined"){
				window.controllers.messagesController.remove();
			}
			var options = {};
			options.action = action;
			options.slug = slug;
			options.id = id;
			require(['js/controllers/messages'], function(MessagesController){
				window.controllers.messagesController = new MessagesController(options);
			});
		},

		brainstorm : function(id, story, action, thread_id, topic){
			if(typeof window.controllers.brainstormController != "undefined"){
				window.controllers.brainstormController.remove();
			}
			var options = {};
			options.id = id;
			options.story = story;
			options.topic = topic;
			options.action = action;
			options.thread_id = thread_id;
			require(['js/controllers/brainstorm'], function(BrainstormController){
				window.controllers.brainstormController = new BrainstormController(options);
			});
		},

		challenge : function(id,slug,action){
			var options = {};
			if(id == null && slug == null){
				var action = "myChallenges";
				options.page = 1;
			}else if(id != null && !isNaN(id) && slug == null){
				var action = "myChallenges";
				options.page = id;
			}else if(id != null && slug != null && action == null){
				var action = "readChallenge";
				$('head').append('<meta name="fragment" content="!">');
			}else{
				if(action != null && action == "new"){
					var action = "newChallenge";
				}else if(action != null && action == "edit"){
					var action = "editChallenge";
				}else if(action != null && action == "delete"){
					var action = "deleteChallenge";
				}else if(action != null && action == "submit"){
					var action = "submitChallenge"
				}
			}
			options.id = id;
			options.slug = slug;
			options.action = action;
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		newChallenge : function (){
			var options = {};
			options.action = "newChallenge";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		browseChallenge : function (){
			var options = {};
			options.action = "browseChallenges";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		challengeWinners : function (page){
			var options = {};
			if(page == null){
				options.page = 1;
			}else{
				options.page = page;
			}
			options.action = "challengeWinners";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		activeChallenge : function(page){
			var options = {};
			if(page == null){
				options.page = 1;
			}else{
				options.page = page;
			}
			options.action = "activeChallenges";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		previousChallenge : function(page){
			var options = {};
			if(page == null){
				options.page = 1;
			}else{
				options.page = page;
			}
			options.action = "previousChallenges";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		challengeLeaderboard : function(){
			var options = {};
			options.action = "challengeLeaderboard";
			require(['js/controllers/challenge'], function(ChallengeController){
				window.controllers.challengeController = new ChallengeController(options);
			});
		},

		settings : function(type){
			var options = {};
			options.type = type;
			require(['js/controllers/settings'], function(SettingsController){
				window.controllers.settingsController = new SettingsController(options);
			});
		},

		notifications : function(){
			if(typeof window.controllers.notificationsController != "undefined"){
				window.controllers.notificationsController.remove();
			}
			require(['js/controllers/notifications'], function(NotificationsController){
				window.controllers.notificationsController = new NotificationsController();
			});
		},

		notFound : function(){
			require(['js/controllers/notFound'], function(notFoundController){
		        window.controllers.notFound = new notFoundController();
		    });
		}

	});
    

	return AppRouter;
});