define([

], function(){

	var nodeGraph = function(){
		console.log('graph')
		var init = function(){
			$("svg").remove();
		    var width = $('#dialog').width();
		    var height = 718;
		    force = d3.layout.force()
		    .linkDistance(60)
		    .charge(-1500)
		    .gravity(.2)
		    .size([width, height])
		    .on("tick", tick);
		    
		    svg = d3.select("#chapterNodes")
		    .append("svg")
		    .attr("width", width)
		    .attr("height", height);
		    $('.legend-container-master').css('display','block');
		    link = svg.selectAll(".link"),
		    node = svg.selectAll(".node");
		}
	}

	return {
		nodeGraph: nodeGraph
	}
});