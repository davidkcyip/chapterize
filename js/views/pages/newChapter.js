define([
  'underscore',
  'backbone',
  'text!templates/pages/newChapter.html',
  'text!templates/items/newChapterRight.html',
  'nicEdit'
], function( _, Backbone, NewChapterTemplate, NewChapterRightTemplate, nicEdit ){
  var newChapterView = Backbone.View.extend({
    newChapterTemplate: _.template(NewChapterTemplate),
    newChapterRightTemplate: _.template(NewChapterRightTemplate),

    el : '.mainContent',
    model: {},
    chapter: {},
    new_chapter_id: 0,

    events : {
      'click #saveChapterDraft' : 'saveChapterDraft',
      'click #publishChapter' : 'publishChapter'
    },

    initialize: function(model,chapter, action){
      $('.breadcrumbs #second').html("<a href='/profile/"+ chapter.pen_name +"'>" + chapter.real_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("<a href='/story/" + chapter.pen_name + "/" + chapter.story_id + "/" + chapter.story_slug + "'>" + chapter.story_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fourth').html("<a href='/story/" + chapter.pen_name + "/" + chapter.story_id + "/" + chapter.story_slug + "/" + chapter.slug + "'>" + chapter.chapter_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fifth').html("New Chapter<i class='fa fa-angle-double-right'></i>");

      document.title = "New Chapter";
      this.model = model;
      this.chapter = chapter;

      this.render(chapter, action);
    },

    render: function(chapter, action){
      var newChapterRightTemplate2 = this.newChapterRightTemplate({chapters: chapter});
      this.$el.html(this.newChapterTemplate({rightContent: newChapterRightTemplate2}));
      nicEditors.allTextAreas();
      return this.el;
    },

    saveChapterDraft : function(){
      if(typeof storage.get('profile') == "object"){
        var nicE = new nicEditors.findEditor('storyContent').getContent();
        var chapterName = $('#chapterName').val();
        if($('#chapterName').val() != "" && nicE != "<br>" && nicE.length > 0){
          var options = [];
          var that = this;
          options.parent_id = this.chapter.id;
          options.story_id = this.chapter.story_id;
          options.chapter_name = chapterName;
          options.chapter_body = nicE;
          options.new_chapter_id = this.new_chapter_id;
          var saveDraft = this.model.saveChapterDraft(options);
          $.when(saveDraft).done(that.checkIfSuccessful(saveDraft,"draft"));
        }else{
          $('.step2Error').text('Title and chapter body must not be empty');
          setTimeout(function(){
            $('.step2Error').text('').css('color','#ae1819');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    checkIfSuccessful : function(saveDraft,action){
      var that = this;
      if(saveDraft.success == 1){
        if(action == "publish"){
          that.publishChapterSuccess(saveDraft.response);
        }else{
          that.new_chapter_id = saveDraft.response.id;
          that.saveDraftSuccess(); 
        }
      }else{
        if(typeof saveDraft.response != "undefined"){
          if(typeof saveDraft.response.id != "undefined"){
            that.new_chapter_id = saveDraft.response.id;
          } 
        }
        if(action == "draft"){
          that.saveDraftError();
        }else{
          that.publishChapterError();
        }
      }
    },

    saveDraftError : function(){
      $('.step2Error').text('There was a problem in saving your draft, please try again or save your draft onto your computer and contact support for help.').css('color','#ae1819');
      setTimeout(function(){
        $('.step2Error').text('');
      },2000);
    },

    publishChapterError : function(){
      $('.step2Error').text('There was a problem publishing your story, please try again or save your draft onto your computer and contact support for help.').css('color','#ae1819');
      setTimeout(function(){
        $('.step2Error').text('');
      },2000);
    },

    saveDraftSuccess : function(){
      $('.step2Error').text('Your chapter has been saved!').css('color','#648e2a');
      setTimeout(function(){
        $('.step2Error').text('').css('color','#ae1819');
      },2000);
    },  

    publishChapterSuccess : function(response){
      appRouter.navigate('/story/'+this.chapter.story_id+'/'+this.chapter.story_slug+'/'+response.slug, {trigger: true});
    },

    publishChapter : function(){
      var nicE = new nicEditors.findEditor('storyContent').getContent();
      var chapterName = $('#chapterName').val();
      if($('#chapterName').val() != "" && nicE != "<br>" && nicE.length > 0){
        var options = [];
        var that = this;
        options.parent_id = this.chapter.id;
        options.story_id = this.chapter.story_id;
        options.chapter_name = chapterName;
        options.chapter_body = nicE;
        options.new_chapter_id = this.new_chapter_id;
        var saveDraft = this.model.publishChapter(options);
        $.when(saveDraft).done(
          that.checkIfSuccessful(saveDraft,"publish")
        )
      }else{
        $('.step2Error').text('Title and chapter body must not be empty');
        setTimeout(function(){
          $('.step2Error').text('').css('color','#ae1819');
        },2000);
      }
    }

  });

  return newChapterView;
});