define([
  'underscore',
  'backbone',
  'text!templates/pages/contact.html',
  'nicEdit'
], function( _, Backbone, ContactTemplate, nicEdit ){
  var contactView = Backbone.View.extend({
    contactTemplate: _.template(ContactTemplate),

    el : '.mainContent',

    events : {
      'click .sendMessage' : 'sendMessage'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.contactTemplate());
      nicEditors.allTextAreas();
      return this.el;
    },

    sendMessage : function(){
      var personname = $('#yourName').val();
      var personemail = $('#yourEmail').val();
      var nicE = new nicEditors.findEditor('message');
      var personmessage = nicE.getContent();
      $.ajax({
        type: 'POST',
        url: '/api/send-message',
            dataType: "json",
            data: { "name": personname, "message": personmessage, "email": personemail },
            success: function(data){
              $('#yourName').val('');
              $('#yourEmail').val('');
              nicE.setContent( '' );
              $('.sendMessageError').html("Thank you for your message! We will contact you within 24 hours.");
              setTimeout( function(){
                $('.sendMessageError').html('');
              },2000);
            }
      });
    }

  });

  return contactView;
});