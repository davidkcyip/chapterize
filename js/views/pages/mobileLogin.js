define([
  'underscore',
  'backbone',
  'text!templates/pages/mobileLogin.html'
], function( _, Backbone, MobileLoginTemplate ){
  var mobileLoginView = Backbone.View.extend({
    mobileLoginTemplate: _.template(MobileLoginTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.mobileLoginTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return mobileLoginView;
});