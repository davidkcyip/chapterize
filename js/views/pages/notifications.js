define([
  'underscore',
  'backbone',
  'text!templates/pages/notifications.html'
], function( _, Backbone, NotificationsTemplate ){
  var notificationsView = Backbone.View.extend({
    notificationsTemplate: _.template(NotificationsTemplate),

    el : '.mainContent',
    notifications: {},

    events : {  
      'click .notificationRectangle' : 'readNotification'
    },

    initialize: function(results){
      this.notifications = results;
      //this.el = $('.mainContainer');
      this.render(results);
    },

    render: function(results){
      this.$el.html(this.notificationsTemplate({notifications:results}));
      //this.delegateEvents();
      return this.el;
    },

    readNotification : function(e){
      var notification_id = $(e.currentTarget).attr('id');
      var is_read = $(e.currentTarget).attr('read');
      if(is_read == 0){
        window.controllers.mainContentController.models.notificationModel.readNotification(notification_id);
      }
    }

  });

  return notificationsView;
});