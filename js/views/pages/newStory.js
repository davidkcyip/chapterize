define([
  'underscore',
  'backbone',
  'text!templates/pages/newStory.html',
  'text!templates/items/newStoryStep1.html',
  'text!templates/items/newStoryStep2.html',
  'js/jquery.tagsinput',
  'chosen',
  'nicEdit'
], function( _, Backbone, NewStoryTemplate, NewStoryStep1Template, NewStoryStep2Template, TagsInput, Chosen, nicEdit ){
  var newStoryView = Backbone.View.extend({
    newStoryTemplate: _.template(NewStoryTemplate),
    newStoryStep1Template: _.template(NewStoryStep1Template),
    newStoryStep2Template: _.template(NewStoryStep2Template),
    categories: {},
    storyTypes: {},
    title: 'New Story',

    el : '.mainContent',

    events : {
      'click #goToStep2' : 'saveStep1',
      'click #goToStep1' : 'saveStep2',
      'click #saveDraft' : 'saveDraft',
      'keyup #storyName' : 'setPageTitle',
      'click #publish' : 'publish',
      'click .updateAlbum' : 'updateAlbum'
    },

    initialize: function(categories, storyTypes, storyModel, storyList){
      $('.breadcrumbs #second').html("<a href='/profile/"+ window.storage.get('profile').pen_name +"'>" + window.storage.get('profile').real_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("<a href='/story/" + window.storage.get('profile').pen_name + "'>Stories</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fourth').html("<a href='/story/new'>New</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fifth').html("");

      window.app.vent.on('newStory:step1Saved',this.goToStep2, this);
      window.app.vent.on('newStory:step2Saved',this.goToStep1, this);
      window.app.vent.on('newStory:saveDraftSuccess',this.saveDraftSuccess, this);
      window.app.vent.on('newStory:saveDraftFailure',this.saveDraftFailure, this);
      window.app.vent.on('newStory:publishSuccess',this.publishSuccess, this);
      window.app.vent.on('newStory:publishFailure',this.publishFailure, this);

      document.title = this.title;

      this.categories = categories;
      this.storyTypes = storyTypes;

      this.render(categories, storyTypes, storyModel, storyList);
    },

    render: function(categories, storyTypes, storyModel, storyList){
      var newStoryStep1Template2 = this.newStoryStep1Template({categories: categories, storyTypes: storyTypes, story: storyModel});
      this.$el.html(this.newStoryTemplate({rightContent: newStoryStep1Template2, story: storyModel, stories: storyList}));
      $('.rightContent #tags').tagsInput({
        width: '100%',
        height: '100%'
      });
      $('#category').chosen({max_selected_options: 3});
      return this.el;
    },

    saveStep2 : function(){
      if(typeof storage.get('profile') == "object"){
        var nicE = new nicEditors.findEditor('storyContent').getContent();
        console.log(nicE);
        if($('#chapterName').val() != "" && nicE != "<br>" && nicE.length > 0){
          window.app.vent.trigger('newStory:saveStep2');
        }else{
          $('.step2Error').text('Please enter a title and content for your chapter!');
        }
      }else{
        views.mainContentView.login();
      }
    },

    saveStep1 : function(){
      if(typeof storage.get('profile') == "object"){
        if($('#storyName').val() != ""){
          window.app.vent.trigger('newStory:saveStep1');
        }else{
          $('.step1Error').text('Please enter a title for your story!');
        }
      }else{
        views.mainContentView.login();
      }
    },

    goToStep2 : function(e){
      if(typeof storage.get('profile') == "object"){
        $('.rightContent').html(this.newStoryStep2Template({chapters: e}));
        nicEditors.allTextAreas();
        $('.nicEdit-main').parent().css('background','#ffffff');
      }else{
        views.mainContentView.login();
      }
    },

    goToStep1 : function(e){
      $('.rightContent').html(this.newStoryStep1Template({categories: this.categories, storyTypes: this.storyTypes, story: e.attributes }));
     $('.rightContent #tags').tagsInput({
        width: '100%',
        height: '100%'
      });
      $('#category').chosen({max_selected_options: 3});
    },

    saveDraft : function(){
      if(typeof storage.get('profile') == "object"){
        window.app.vent.trigger('newStory:saveDraft');
      }else{
        views.mainContentView.login();
      }
    },

    saveDraftSuccess : function(){
      $('.step2Error').text('Draft saved!').css('color','#648e2a');
      setTimeout(function(){
        $('.step2Error').text('').css('color','#ae1819');
      },2000);
    },

    saveDraftFailure : function(){
      $('.step2Error').text('There was a problem in saving your draft, please try again or save your draft onto your computer and contact support for help.').css('color','#ae1819');
      setTimeout(function(){
        $('.step2Error').text('');
      },2000);
    },

    publish : function(){
      if(typeof storage.get('profile') == "object"){
        window.app.vent.trigger('newStory:publish');
      }else{
        views.mainContentView.login();
      }
    },

    publishSuccess : function(options){
      appRouter.navigate('/story/'+options.story_id+'/'+options.story_slug+'/'+options.chapter_slug, {trigger: true});
    },

    publishFailure : function(){
      $('.step2Error').text('There was a problem publishing your story, please try again or save your draft onto your computer and contact support for help.').css('color','#ae1819');
      setTimeout(function(){
        $('.step2Error').text('');
      },2000);
    },

    setPageTitle : function(){
      var thisTitle = $('#storyName').val();
      $('.breadcrumbs #fifth').html(thisTitle + " <i class='fa fa-angle-double-right'></i>");
      if(thisTitle.length > 0){
        document.title = this.title + ' - ' + thisTitle;
      }else{
        document.title = this.title;
        $('.breadcrumbs #fifth').html("");
      }
    },

    updateAlbum : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        require(['text!templates/items/modal.html','text!templates/items/updateAvatar.html', 'js/croppic.min'], function(ModalTemplate, UpdateAvatarTemplate, Crop){
          var modalTemplate = _.template(ModalTemplate);
          var updateAvatarTemplate = _.template(UpdateAvatarTemplate);
          that.$el.append(modalTemplate());
          $('.modal').append(updateAvatarTemplate());
          $('.modalBox').css('width','350px').css('height','350px');
          var cropperOptions = {
            uploadUrl:'/api/uploadAlbumInitial',
            cropUrl:'/api/cropAlbumInitial',
            imgEyecandy:true,
            imgEyecandyOpacity:0,
            outputUrlId:'outputUrlId',
            customUploadButtonId:'updateAvatarButton',
            onAfterImgCrop: function(){
              var newAvatar = that.cropperHeader.croppedImg.valueOf().attr('src');
              
              $('img[class="album"]').attr('src',newAvatar);
              that.cropperHeader.destroy();
              $('.modalBox').remove();
              $('.modal').remove();
            }
          }
          //var crop = Crop;
          //var cropHead = new crop('uploadAvatar', cropperOptions);
          that.cropperHeader = new Croppic('uploadAvatar', cropperOptions);
          //cropperHeader.form.find('input[type="file"]').trigger('click');
        });
      }else{
        views.mainContentView.login();
      }
    }

  });

  return newStoryView;
});