define([
  'underscore',
  'backbone',
  'text!templates/pages/mobileForgotPassword.html'
], function( _, Backbone, MobileForgotPasswordTemplate ){
  var mobileForgotPasswordView = Backbone.View.extend({
    mobileForgotPasswordTemplate: _.template(MobileForgotPasswordTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.mobileForgotPasswordTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return mobileForgotPasswordView;
});