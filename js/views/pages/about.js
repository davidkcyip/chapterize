define([
  'underscore',
  'backbone',
  'text!templates/pages/about.html'
], function( _, Backbone, AboutTemplate ){
  var aboutView = Backbone.View.extend({
    aboutTemplate: _.template(AboutTemplate),

    el : '.mainContent',

    events : {
      
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.aboutTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return aboutView;
});