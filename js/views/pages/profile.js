define([
  'underscore',
  'backbone',
  'text!templates/pages/profile.html',
  'text!templates/items/homeRightContent.html',
  'text!templates/items/homeNewsfeed.html',
  'nicEdit'
], function( _, Backbone, ProfileTemplate, ProfileRightContentTemplate, ProfileNewsfeedTemplate, nicEdit ){
  var profileView = Backbone.View.extend({
    profileTemplate: _.template(ProfileTemplate),
    profileRightContentTemplate : _.template(ProfileRightContentTemplate),
    profileNewsfeedTemplate : _.template(ProfileNewsfeedTemplate),
    cropperHeader : {},
    thisProfile : {},
    type: 'profile',
    thisFeed: {},
    shareId: 0,
    sharePost: {},

    el : '.mainContent',

    events : {
      'click .submitReplyButton' : 'submitReply',
      'mouseover .following' : 'showUnfollow',
      'mouseleave .unfollow' : 'unShowUnfollow',
      'click .updateAvatar' : 'updateAvatar',
      'click .profileInfo .unfollow' : 'unfollowUser',
      'click .profileInfo .follow' : 'followUser',
      'click .profileEditButton' : 'showEditProfileModal',
      'click #editProfileButton' : 'submitEditProfile',
      'click .sendMessageButton' : 'sendMessageDialog',
      'click .previousItemsProfile' : 'loadPreviousProfile',
      'click .profileAllActivities' : 'profileAllActivities',
      'click .profileStatuses' : 'profileStatuses',
      'click .profileStoryUpdates' : 'profileStoryUpdates',
      'click .profileChallenges' : 'profileChallenges',
      'click #noFollowers button' : 'noFollowerModal',
      'click #noFollowing button' : 'noFollowingModal',
      'click .reportUserButton' : 'reportUser',
      'click .reportUserSubmit' : 'reportUserSend',
      'click .shareButton' : 'sharePost',
      'click .repostConfirm' : 'repostConfirm'
    },

    initialize: function(profile, feed, subscriptions, mySubscriptions, basicStats){
      //this.el = $('.mainContainer');
      var that = this;
      amISubscribed = 0;
      this.thisProfile = profile;
      this.thisFeed = feed;

      document.title = profile.real_name;
      $('.breadcrumbs #second').html("<a href='/profile/" + profile.pen_name + "'>" +profile.real_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("");
      $('.breadcrumbs #fourth').html("");
      $('.breadcrumbs #fifth').html("");

      window.app.vent.on('profile:unfollowSuccess',this.unfollowUserSuccess, this);
      window.app.vent.on('profile:followSuccess',this.followUserSuccess, this);
      window.app.vent.on('profile:editProfileSuccess',this.editProfileSuccess, this);

      _.each(mySubscriptions, function(subscription){
        if(subscription.item_subscribed_to == profile.id && subscription.subscription_type == "user"){
          amISubscribed = 1;
        }
      });
      this.render(profile, feed, subscriptions, amISubscribed, basicStats);
    },

    renderChallengePosition: function(results){
      var i = 1;
      var that = this;
      var me = _.where(results, {author_id: that.thisProfile.id});
      if(typeof me != "undefined"){
        $('#challengePosition').html('<i class="fa fa-trophy"></i> Currently placed '+ app.getOrdinal(me[0].position) + " overall in Challenge Leaderboard")
      }
    },

    render: function(profile, feed, subscriptions, amISubscribed, basicStats){
      $('meta[property="og:title"]').attr('content',"Bloqly - " + profile.real_name);
      $('meta[property="og:description"]').attr('content',profile.bio);
      var profileNewsfeedTemplate2 = this.profileNewsfeedTemplate({feed: feed});
      var profileRightContentTemplate2 = this.profileRightContentTemplate({newsfeed: profileNewsfeedTemplate2, to: profile.id});
      this.$el.html(this.profileTemplate({profile: profile, subscriptions: subscriptions, rightContent : profileRightContentTemplate2, amISubscribed : amISubscribed, basicStats: basicStats}));
      Tipped.create(".likesPopup");
      return this.el;
    },

    showUnfollow : function(){
      $('.profileInfo .following').removeClass('following').addClass('unfollow').text('Unfollow');
    },

    unShowUnfollow : function(){
      $('.profileInfo .unfollow').removeClass('unfollow').addClass('following').text('Following');
    },

    unfollowUser : function(){
      var noFollowers = parseInt($('#noFollowers').text()) - 1;
      $('#noFollowers').html("<button>"+ noFollowers + "<br/>followers</button>");
      window.app.vent.trigger('profile:unfollow',this.thisProfile.id);
    },

    unfollowUserSuccess : function(){
      $('.profileInfo .unfollow').removeClass('unfollow').addClass('follow').text('Follow');
    },

    followUser : function(){
      if(typeof storage.get('profile') == "object"){
        var noFollowers = parseInt($('#noFollowers').text()) + 1;
        $('#noFollowers').html("<button>"+ noFollowers + "<br/>followers</button>");
        window.app.vent.trigger('profile:follow',this.thisProfile.id);
      }else{
        views.mainContentView.login();
      }
    },

    followUserSuccess : function(){
      $('.profileInfo .follow').removeClass('follow').addClass('following').text('Following');
    },

    updateAvatar : function(){
      var that = this;
      require(['text!templates/items/modal.html','text!templates/items/updateAvatar.html', 'js/croppic.min'], function(ModalTemplate, UpdateAvatarTemplate, Crop){
        var modalTemplate = _.template(ModalTemplate);
        var updateAvatarTemplate = _.template(UpdateAvatarTemplate);
        that.$el.append(modalTemplate());
        $('.modal').append(updateAvatarTemplate());
        $('.modalBox').css('width','350px').css('height','350px');
        var cropperOptions = {
          uploadUrl:'/api/uploadAvatar',
          cropUrl:'/api/cropAvatar',
          imgEyecandy:true,
          imgEyecandyOpacity:0,
          outputUrlId:'outputUrlId',
          customUploadButtonId:'updateAvatarButton',
          onAfterImgCrop: function(){
            var newAvatar = that.cropperHeader.croppedImg.valueOf().attr('src');
            var profile = window.storage.get('profile');
            
            $('img[src="'+profile.avatar+'"]').attr('src',newAvatar);
            $('a[href="'+profile.avatar+'"]').attr('href',newAvatar);
            profile.avatar = newAvatar;
            window.storage.set('profile',profile);
            that.cropperHeader.destroy();
            $('.modalBox').remove();
            $('.modal').remove();
          }
        }
        //var crop = Crop;
        //var cropHead = new crop('uploadAvatar', cropperOptions);
        that.cropperHeader = new Croppic('uploadAvatar', cropperOptions);
        //cropperHeader.form.find('input[type="file"]').trigger('click');
      });
    },

    showEditProfileModal : function(){
      var that = this;
      require(['text!templates/items/modal.html','text!templates/items/editProfile.html'], function(ModalTemplate, EditProfileTemplate){
        var modalTemplate = _.template(ModalTemplate);
        var editProfileTemplate = _.template(EditProfileTemplate);

        that.$el.append(modalTemplate());
        $('.modal').append(editProfileTemplate());
        var nic = new nicEditor();
        nic.addInstance('bio');
        nic.setPanel('bio');
      });
    },

    submitEditProfile : function(){
      var realName = $('#realName').val();
      var bio = new nicEditors.findEditor('bio').getContent();
      var country = $('#country').val();
      var website = $('#website').val();
      var facebook = $('#facebook').val();
      var twitter = $('#twitter').val();
      var google = $('#googleplus').val();

      var user = window.storage.get('profile');
      user.real_name = realName;
      user.bio = bio;
      user.country = country;
      user.website_link = website;
      user.facebook_link = facebook;
      user.twitter_link = twitter;
      user.google_link = google;
      window.storage.set('profile',user);

      require(['text!templates/items/profileNameCountry.html', 'text!templates/items/profileSocialLinks.html', 'text!templates/items/profileBio.html', 'text!templates/items/profileWebsite.html'], function(ProfileNameCountryTemplate, ProfileSocialLinksTemplate, ProfileBioTemplate, ProfileWebsiteTemplate){
        var nameCountry = _.template(ProfileNameCountryTemplate);
        var social = _.template(ProfileSocialLinksTemplate);
        var websiteTemplate = _.template(ProfileWebsiteTemplate);
        var bioTemplate = _.template(ProfileBioTemplate);
        $('.profileName .floatLeft').html(nameCountry({name: realName, country: country}));
        $('.profileBio .social').html(social({facebook: facebook, twitter: twitter, google: google}));
        $('.profileBio .bio').html(bioTemplate({bio: bio}));
        $('.profileBio .website').html(websiteTemplate({website: website}));
      });

      window.app.vent.trigger('profile:editProfile');
    },

    editProfileSuccess : function(){
      $('.modal').remove();
    },

    sendMessageDialog : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        require(['text!templates/items/modal.html','text!templates/items/sendMessageModal.html'], function(ModalTemplate,SendMessageModal){
          var modalTemplate = _.template(ModalTemplate);
          var sendMessageModal = _.template(SendMessageModal);
          $('.mainContainer').append(modalTemplate());
          $('.modal').append(sendMessageModal({real_name: that.thisProfile.real_name}));
          var nic = new nicEditor();
          nic.addInstance('message');
        });
      }else{
        views.mainContentView.login();
      }
    },

    loadPreviousProfile : function(){
      var that = this;
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/" + that.thisProfile.pen_name + "/"+that.type+"/previous",
              dataType: "json",
              async: false,
              success: function(feed){
                var profileNewsfeedTemplate2 = that.profileNewsfeedTemplate({feed: feed});
                $('.postsList').append(profileNewsfeedTemplate2);
                
              }
      });
    },

    getAllActivities : function(){
      var that = this;
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/" + that.thisProfile.pen_name + "/"+that.type+"/all",
              dataType: "json",
              async: false,
              success: function(feed){
                if(feed.length){
                  var profileNewsfeedTemplate2 = that.profileNewsfeedTemplate({feed: feed});
                  $('.postsList').html(profileNewsfeedTemplate2);
                }else{
                  $('.postsList').html('<div style="margin-top:10px;margin-bottom:10px;">No updates found for this filter.</div>');
                }
              }
      });
    },

    profileAllActivities : function(){
      var that = this;
      this.type = "profileAll";
      this.getAllActivities();
    },

    profileStatuses : function(){
      var that = this;
      this.type = "profile";
      this.getAllActivities();
    },

    profileStoryUpdates : function(){
      var that = this;
      this.type = "profileStoryUpdates";
      this.getAllActivities();
    },

    profileChallenges : function(){
      var that = this;
      this.type = "profileChallenges";
      this.getAllActivities();
    },

    noFollowingModal : function(){
      var that = this;
      $.getJSON('/api/user/following/'+this.thisProfile.id, function(following){
        require(['text!templates/items/modal.html','text!templates/items/followerFollowingModal.html'], function(ModalTemplate, FollowerFollowingModalTemplate){
          var modalTemplate = _.template(ModalTemplate);
          var followerFollowingModalTemplate = _.template(FollowerFollowingModalTemplate);
          $('.mainContent').append(modalTemplate());
          $('.modal').append(followerFollowingModalTemplate({results: following.results, type: 'Authors following ' + that.thisProfile.real_name}));
        });
      });
    },

    noFollowerModal : function(){
      var that = this;
      $.getJSON('/api/user/followers/'+this.thisProfile.id, function(follower){
        require(['text!templates/items/modal.html','text!templates/items/followerFollowingModal.html'], function(ModalTemplate, FollowerFollowingModalTemplate){
          var modalTemplate = _.template(ModalTemplate);
          var followerFollowingModalTemplate = _.template(FollowerFollowingModalTemplate);
          $('.mainContent').append(modalTemplate());
          $('.modal').append(followerFollowingModalTemplate({results: follower.results, type: 'Authors followed by ' + that.thisProfile.real_name}));
        });
      });
    },

    reportUser : function(){
      if(typeof storage.get('profile') == "object"){
        require(['text!templates/items/modal.html','text!templates/items/reportUserModal.html'], function(ModalTemplate, ReportUserModalTemplate){
          var modalTemplate = _.template(ModalTemplate);
          var reportUserModalTemplate = _.template(ReportUserModalTemplate);

          $('.mainContent').append(modalTemplate());
          $('.modal').append(reportUserModalTemplate());
          var nic = new nicEditor();
          nic.addInstance('reportUserMessage');
        });
      }else{
        views.mainContentView.login();
      }
    },

    reportUserSend : function(){
      var options = {};
      options.message = new nicEditors.findEditor('reportUserMessage').getContent();
      options.id = this.thisProfile.id;
      require(['js/models/User'], function(UserModel){
        window.controllers.profile.models.userModel = new UserModel.UserModel();
        window.controllers.profile.models.userModel.sendReport(options);
        $('.modalBox').html("Your report has been sent. We will email you if and when action is taken.").css('height','45px');
      });
    },

    sharePost : function(e){
      if(typeof storage.get('profile') == "object"){
        var shareid = $(e.currentTarget).attr('id');
        id = shareid.split('_');
        id = id[1];
        this.shareId = id;
        var that = this;
        _.each(this.thisFeed, function(feed){
          if(feed.id == id){
            console.log(feed);
            that.sharePost = feed;
            require(['text!templates/items/modal.html','text!templates/items/sharePost.html'], function(ModalTemplate, SharePostTemplate){
              var modalTemplate = _.template(ModalTemplate);
              var sharePostTemplate = _.template(SharePostTemplate);
              that.$el.append(modalTemplate());
              $('.modal').append(sharePostTemplate({post: feed}));
              var modalHeight = $('.modalBox .singlePost').height()+10;
              $('.modalBox').height(modalHeight);
            });
          }
        });
      }else{
        views.mainContentView.login();
      }
    },

    repostConfirm : function(){
      var repostText = $('#repostText').val();
      var postId = this.shareId;
      var options = {};
      options.repost_id = postId;
      options.share_text = repostText;
      var that = this;
      $('.modal').remove();
      var id = window.controllers.home.models.feedModel.repost(options);
      $.when(id).done(
        Backbone.history.loadUrl(Backbone.history.fragment)
      )
    }

  });

  return profileView;
});