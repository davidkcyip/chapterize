define([
  'underscore',
  'backbone',
  'text!templates/pages/messages.html'
], function( _, Backbone, MessagesTemplate ){
  var MessagesView = Backbone.View.extend({
    messagesTemplate: _.template(MessagesTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.messagesTemplate());
      $('.leftMenuUsernameHolder .menuMessagesNumber').text($('.menuMessagesNumber').html());
      //this.delegateEvents();
      return this.el;
    }

  });

  return MessagesView;
});