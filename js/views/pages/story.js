define([
  'underscore',
  'backbone',
  'text!templates/pages/story.html',
  'text!templates/items/storyList.html',
  'text!templates/pages/chapter.html',
  'text!templates/items/singleChapter.html',
  'rating',
  'tipi',
  'text!templates/items/modal.html',
  'text!templates/items/rate.html',
  'jqueryui',
  'tree',
  'text!templates/items/chaptersList.html',
  'text!templates/items/verifyDeleteChapter.html',
  'text!templates/items/chaptersInStory.html',
  'text!templates/items/chaptersInStoryBrainstorm.html',
  'bootpag'
], function( _, Backbone, StoryTemplate, StoryListTemplate, ChapterTemplate, SingleChapterTemplate, Rating, Tipi, ModalTemplate, RateTemplate, jqueryui, tree, ChaptersListTemplate, VerifyDeleteChapterTemplate, ChaptersInStoryTemplate, BrainstormListTemplate, bootpag ){
  var storyView = Backbone.View.extend({
    storyTemplate: _.template(StoryTemplate),
    storyListTemplate: _.template(StoryListTemplate),
    chapterTemplate: _.template(ChapterTemplate),
    singleChapterTemplate: _.template(SingleChapterTemplate),
    chaptersListTemplate: _.template(ChaptersListTemplate),
    chaptersInStoryTemplate : _.template(ChaptersInStoryTemplate),
    brainstormListTemplate : _.template(BrainstormListTemplate),
    storyId: 0,
    chapterId: 0,
    rating: 0,
    chapterRatingComment: 0,
    newRating: {},
    models: {},
    author: "",
    storyOptions:{},
    chapterOfThisStory:{},

    el : '.mainContent',

    events : {
      'click .rate' : 'rate',
      'click .rateChapterButton' : 'submitRating',
      'click .chapterCommentButton' : 'sendComment',
      'click .mapOpenClose' : 'mapOpenClose',
      'click .continueStory' : 'newChapter',
      'click .editChapterButton' : 'editChapter',
      'click .deleteChapterButton' : 'openDeleteBox',
      'click .verifyDeleteChapterButton' : 'deleteChapter',
      'click .favourite' : 'favouriteChapter',
      'click .unfavourite' : 'unfavouriteChapter',
      'click .bookmark' : 'bookmarkChapter',
      'click .unbookmark' : 'unbookmarkChapter',
      'click .editStoryButton' : 'editStoryDetails',
      'click .subscribeToStory' : 'subscribeToStory',
      'click .unsubscribeToStory' : 'unsubscribeToStory',
      'mouseover .subscribedToStory' : 'changeToUnsubscribe',
      'mouseout .unsubscribeToStory' : 'changeTosubscribed',
      'click .wasHelpful' : 'wasHelpful',
      'click .notHelpful' : 'notHelpful',
      'click .share' : 'shareChapter'
    },

    initialize: function(options){
      this.storyOptions = options;
      this.storyId = options.id;
      this.thisStory = options.stories[0];
      this.author = options.author;
      console.log(options)
      if( options.author != null &&  options.id == null &&  options.story == null &&  options.chapter == null){
        this.render(options.stories, options.users, options.author, "storylist", options.id, options.chapter);
      }else if( options.id != null &&  options.story != null &&  options.chapter == null){
        this.render(options.stories, options.users, options.author, "story", options.id, options.chapter);
      }else if( options.id != null &&  options.story != null &&  options.chapter != null){
        this.render(options.stories, options.users, options.author, "chapter", options.id, options.chapter);
      }
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModuleRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    render: function(stories, users, author, type, id, chapter){

      var that = this;
      if(type == "storylist"){
        $('.breadcrumbs #second').html("<a href='/profile/"+ author +"'>" + users[0].get('real_name') + "</a> <i class='fa fa-angle-double-right'></i>");
        $('.breadcrumbs #third').html("<a href='/story/" + author + "'>Stories</a> <i class='fa fa-angle-double-right'></i>");
        $('.breadcrumbs #fourth').html("");
        $('.breadcrumbs #fifth').html("");
        document.title = users[0].get('real_name') + " - Stories";
        $('meta[property="og:title"]').attr('content',users[0].get('real_name') + " - Stories");
        var storyListTemplate2 = this.storyListTemplate({stories: stories, users: users, author: author});
        this.$el.html(this.storyTemplate({rightContent: storyListTemplate2, stories: stories, users: users, author: author}));
      }else if(type == "story"){

        this.chapterId = stories[0].get('chapters')[0].id;
        document.title = stories[0].get('story_name') + " - " + stories[0].get('chapters')[0].chapter_name;
        $('meta[property="og:title"]').attr('content',"Bloqly - " + stories[0].get('story_name') + " - " + stories[0].get('chapters')[0].chapter_name);
        $('meta[property="og:description"]').attr('content',stories[0].get('chapters')[0].chapter_body);
        

        var singleChapterTemplate2 = this.chaptersInStoryTemplate({chapter: stories[0].get('chapters')[0], users: users, author: author, chapterNumber: 0,story: stories[0]});
        this.$el.html(this.chapterTemplate({rightContent: singleChapterTemplate2, stories: stories, users: users, author: author, chapter: stories[0].get('chapters')[0]}));
        
        this.startChart(stories[0].get('slug'),stories[0].get('id'), stories[0].get('chapters'), stories[0].get('story_type'), null, "story");

        
        $.ajax({
            type: 'GET',
            url: '/api/brainstorm/get',
            data: {"id": that.storyOptions.id, "slug": that.storyOptions.story},
            dataType: "json",
            success: function(data){
              $('.brainstormList').html(that.brainstormListTemplate({brainstorms:data.results,story:stories[0]}))
            }
        });
      }else if(type == "chapter"){
        var that = this;
        var chapterName = '';
        var thisChapter = {};
        var chapterNumber = 0;
        var i = 0;
        _.each(stories[0].get('chapters'), function(chapter1){
          if(chapter1.slug == chapter){
            document.title = stories[0].get('story_name') + " - " + chapter1.chapter_name;
            chapterName = chapter1.chapter_name;
            thisChapter = chapter1;
            that.chapterOfThisStory = chapter1;
            chapterNumber = i;
            that.chapterId = chapter1.id;
          }
          i++;
        });
        console.log(stories[0].get('chapters'))
        $('meta[property="og:title"]').attr('content',stories[0].get('story_name') + " - " + thisChapter.chapter_name);
        $('meta[property="og:description"]').attr('content',thisChapter.chapter_body);

        var singleChapterTemplate2 = this.singleChapterTemplate({chapter: thisChapter, users: users, author: author, story: stories[0], chapterNumber: chapterNumber});
        
        this.$el.html(this.chapterTemplate({rightContent: singleChapterTemplate2, stories: stories, users: users, author: author, chapter: thisChapter}));
        
        this.startChart(stories[0].get('slug'),stories[0].get('id'), stories[0].get('chapters'), stories[0].get('story_type'), thisChapter, "chapter");
        
      }
      $(".chapterStars").rating({
        messages: [],
        icon: 'fa-star',
        color: '#372847',
        colorHover: '#CF4656',
      });
      var title = $('.chapterStars').attr('title');
      $('.ratingDiv .rating-container').attr('title',title);
      Tipped.create(".ratingDiv .rating-container");
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto')
      return this.el;
    },

    renderChapters : function(response){
      var num_chapters = 100;
      var num_stories = 100;
      var chaptersListTemplate2 = this.chaptersListTemplate({chapters: response});
      $('.chapters .chapterListContainer').html(chaptersListTemplate2);
      var url = window.location.pathname;
        url = url.split("/");
        var thisUrl = "";
        if(!isNaN(url[url.length-1])){
          for(var i = 1; i < url.length-1; i++){
            thisUrl = thisUrl + "/" + url[i];
          }
        }else{
          thisUrl = window.location.pathname;
        }
      $( ".storyPage" ).ready(function() {
          $('.storyPage').bootpag({
             total: Math.ceil(parseInt(num_stories)/10),
             page: parseInt(page),
             maxVisible: 10,
             leaps: true,
             next: '>',
             prev: '<'
          }).on('page', function(event, num){
            appRouter.navigate(thisUrl+"/"+num, {trigger: true});
          });;
        });
      $( ".chapterPage" ).ready(function() {
          $('.chapterPage').bootpag({
             total: Math.ceil(parseInt(num_chapters)/10),
             page: parseInt(page),
             maxVisible: 10,
             leaps: true,
             next: '>',
             prev: '<'
          }).on('page', function(event, num){
            appRouter.navigate(thisUrl+"/"+num, {trigger: true});
          });;
        });
    },

    startChart : function(storySlug, storyId, chapters, story_type, thisChapter, type){
      var that = this;
      if(story_type == "Story" || story_type == "Challenge"){
        //root
        console.log(chapters)
        that.$el.find('.tree').append("<li id='0'><div><a href='/story/" + storyId + "/" + storySlug + "/" + chapters[0].slug + "'>" + window.app.shortenTitle(chapters[0].chapter_name) + "</a></div><ul id='" + chapters[0].id +"'></ul></li>");
        that.$el.find('li[id=0]').css('margin','0').css('width','100%');
        chapters.shift();
        
        _.each(chapters, function(chapter){
          that.$el.find('ul[id="'+chapter.parent_chapter_id+'"]').append("<li><div><a href='/story/" + storyId + "/" + storySlug + "/" + chapter.slug + "'>" + window.app.shortenTitle(chapter.chapter_name) + "</a></div><ul id='" + chapter.id +"'></ul></li>")
        })

        $('.tree').tree_structure({
          'add_option': false,
          'edit_option': false,
          'delete_option': false,
          'confirm_before_delete' : false,
          'animate_option': [true, 10],
          'fullwidth_option': false,
          'align_option': 'center',
          'draggable_option': false
        });
      }else if(story_type == "Adventure" && type == "chapter"){
        //root
        console.log(thisChapter)
        that.$el.find('.tree').append("<li id='0'><div><a href='/story/" + storyId + "/" + storySlug + "/" + thisChapter.slug + "'>" + window.app.shortenTitle(thisChapter.chapter_name) + "</a></div><ul id='" + thisChapter.id +"'></ul></li>");
        that.$el.find('li[id=0]').css('margin','0').css('width','100%');

        _.each(chapters, function(chapter){
          if(thisChapter.id == chapter.parent_chapter_id){
            that.$el.find('ul[id="'+chapter.parent_chapter_id+'"]').append("<li><div><a href='/story/" + storyId + "/" + storySlug + "/" + chapter.slug + "'>" + window.app.shortenTitle(chapter.chapter_name) + "</a></div><ul id='" + chapter.id +"'></ul></li>")
          }
        })

        $('.tree').tree_structure({
          'add_option': false,
          'edit_option': false,
          'delete_option': false,
          'confirm_before_delete' : false,
          'animate_option': [true, 10],
          'fullwidth_option': false,
          'align_option': 'center',
          'draggable_option': false
        });
      }else if(story_type == "Adventure" && type == "story"){
        //root
        that.$el.find('.tree').append("<li id='0'><div><a href='/story/" + storyId + "/" + storySlug + "/" + chapters[0].slug + "'>" + window.app.shortenTitle(chapters[0].chapter_name) + "</a></div><ul id='" + chapters[0].id +"'></ul></li>");
        that.$el.find('li[id=0]').css('margin','0').css('width','100%');

        $('.tree').tree_structure({
          'add_option': false,
          'edit_option': false,
          'delete_option': false,
          'confirm_before_delete' : false,
          'animate_option': [true, 10],
          'fullwidth_option': false,
          'align_option': 'center',
          'draggable_option': false
        });
      }
    },

    rate : function(){
      if(typeof storage.get('profile') == "object"){
        $('.modal').remove();
        var modalTemplate = _.template(ModalTemplate);
        var rateTemplate = _.template(RateTemplate);

        $('.mainContent').append(modalTemplate());
        $('.modal').append(rateTemplate());
        $(".rateChapter").rating({
          messages: [],
          icon: 'fa-star',
          colorHover: '#CF4656',
        });
        $(".rating-container").css('display','block').css('width','100%').css('text-align','center');
      }else{
        views.mainContentView.login();
      }
    },

    submitRating : function(){
      this.rating = $('.rateChapter').rating('val');
      this.chapterRatingComment = $('#rateComment').val();
      var options = {};
      options.chapterId = this.chapterId;
      options.storyId = this.storyId;
      options.rating = this.rating;
      options.chapterRatingComment = this.chapterRatingComment;
      $.ajax({
        type: 'POST',
        url: '/api/chapter/rate',
        dataType: 'json',
        data: {'chapterId': options.chapterId, 'storyId': options.storyId, 'chapterRatingComment': options.chapterRatingComment, 'rating': options.rating},
        success: function(data){
          window.app.vent.trigger('chapter:rateChapterSuccess', data.id);
        }
      });
      this.rateChapterSuccess();
    },

    rateChapterSuccess : function(){
      Backbone.history.loadUrl(Backbone.history.fragment);
    },

    sendComment : function(){
      if(typeof storage.get('profile') == "object"){
        var comment = $('.chapterComment').val();
        var options = {};
        options.chapterId = this.chapterId;
        options.storyId = this.storyId;
        options.comment = comment;
        var that = this;
        this.newRating = {
          'avatar': window.storage.get('profile').avatar,
          'comment_body': comment,
          'commented_at': Date(),
          'commented_by': window.storage.get('profile').id,
          'id': 0,
          'pen_name': window.storage.get('profile').pen_name,
          'rating': null,
          'real_name': window.storage.get('profile').real_name
        }
        $.ajax({
          type: 'POST',
          url: '/api/chapter/comment',
          dataType: 'json',
          data: {'chapterId': options.chapterId, 'storyId': options.storyId, 'comment': options.comment},
          success: function(data){
            that.commentChapterSuccess(data.id);
          }
        });
      }else{
        views.mainContentView.login();
      }
    },

    commentChapterSuccess : function(commentId){
      var that = this;
      this.newRating.id = commentId;
      if($('.commentsContainer').text().indexOf("Be the first to comment or rate this chapter!") != -1){
        $('.commentsContainer').html('');
      }
      require(['text!templates/items/newRating.html'], function(NewRatingTemplate){
        var newRatingTemplate = _.template(NewRatingTemplate);
        $('.commentsContainer').prepend(newRatingTemplate({reply: that.newRating}));
      });
      $('.chapterComment').val('');
      console.log('sendCommentSuccess');
    },

    mapOpenClose : function(e){
      $('.tree').toggle();
      if($('.mapOpenClose .fa').hasClass("fa-minus-circle")){
        $('.mapOpenClose .fa').removeClass('fa-minus-circle').addClass('fa-plus-circle');
        $(e.currentTarget).parent().css('border-bottom','1px dotted #cccccc');
      }else{
        $('.mapOpenClose .fa').addClass('fa-minus-circle').removeClass('fa-plus-circle');
        $(e.currentTarget).parent().css('border-bottom','0');
      }
    },

    newChapter : function(){
      if(typeof storage.get('profile') == "object"){
        appRouter.navigate('/chapter/'+this.thisStory.get('slug')+'/'+this.chapterId+'/new', {trigger: true});
      }else{
        views.mainContentView.login();
      }
    },

    editChapter : function(){
      if(typeof storage.get('profile') == "object"){
        appRouter.navigate('/chapter/'+this.thisStory.get('slug')+'/'+this.chapterId+'/edit', {trigger: true});
      }else{
        views.mainContentView.login();
      }
    },

    openDeleteBox : function(){
      if(typeof storage.get('profile') == "object"){
        var modalTemplate = _.template(ModalTemplate);
        var verifyDeleteTemplate2 = _.template(VerifyDeleteChapterTemplate);
        $('.mainContent').append(modalTemplate);
        $('.modal').append(verifyDeleteTemplate2);
      }else{
        views.mainContentView.login();
      }
    },

    deleteChapter : function(){
      var chapterId = this.chapterId;
      var options = [];
      options.chapter_id = chapterId;
      $.ajax({
        type: 'POST',
        url: '/api/chapter/actions/delete',
            dataType: "json",
            data: { "chapter_id": options.chapter_id},
            success: function(data){
              data2 = data;
            }
        });
      appRouter.navigate('/story/'+this.author+'/'+this.thisStory.get('id')+'/'+this.thisStory.get('slug'), {trigger: true});
    },

    favouriteChapter : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        window.controllers.mainContentController.models.favouriteModel.favouriteChapter(that.chapterId, that.storyId);
        $('.favourite').addClass('unfavourite').removeClass('favourite').html("<i class='fa fa-heart' style='color: #ff3333'></i> Unfavourite")
        var favourites = parseInt($('.favouriteNumber').text()) + 1;
        $('.favouriteNumber').text(favourites);
      }else{
        views.mainContentView.login();
      }
    },

    unfavouriteChapter : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        window.controllers.mainContentController.models.favouriteModel.unfavouriteChapter(that.chapterId, that.storyId);
        $('.unfavourite').addClass('favourite').removeClass('unfavourite').html("<i class='fa fa-heart'></i> Favourite");
        var favourites = parseInt($('.favouriteNumber').text()) - 1;
        $('.favouriteNumber').text(favourites);
      }else{
        views.mainContentView.login();
      }
    },

    bookmarkChapter : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        window.controllers.mainContentController.models.bookmarkModel.bookmarkChapter(that.chapterId, that.storyId);
        $('.bookmark').addClass('unbookmark').removeClass('bookmark').html("<i class='fa fa-bookmark' style='color: #8dc63f'></i> Unbookmark")
        var bookmarks = parseInt($('.bookmarkNumber').text()) + 1;
        $('.bookmarkNumber').text(bookmarks);
      }else{
        views.mainContentView.login();
      }
    },

    unbookmarkChapter : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        window.controllers.mainContentController.models.bookmarkModel.unbookmarkChapter(that.chapterId, that.storyId);
        $('.unbookmark').addClass('bookmark').removeClass('unbookmark').html("<i class='fa fa-bookmark'></i> Unbookmark")
        var bookmarks = parseInt($('.bookmarkNumber').text()) - 1;
        $('.bookmarkNumber').text(bookmarks);
      }else{
        views.mainContentView.login();
      }
    },

    editStoryDetails : function(){
      appRouter.navigate('/story/edit/'+this.thisStory.get('slug')+'/'+this.thisStory.get('id'), {trigger: true});
    },

    subscribeToStory : function(){
      if(typeof storage.get('profile') == "object"){
        window.controllers.storyController.models.story.subscribeToStory(this.storyId);
        $('.subscribeToStory').html('<i class="fa fa-bookmark" style="color: #8dc63f;"></i> Subscribed').addClass('subscribedToStory').removeClass('subscribeToStory');
      }else{
        views.mainContentView.login();
      }
    },

    unsubscribeToStory : function(){
      if(typeof storage.get('profile') == "object"){
        window.controllers.storyController.models.story.unsubscribeToStory(this.storyId);
        $('.unsubscribeToStory').html('<i class="fa fa-bookmark"></i> Subscribe').addClass('subscribeToStory').removeClass('unsubscribeToStory').removeClass('subscribedToStory');
      }else{
        views.mainContentView.login();
      }
    },

    changeToUnsubscribe : function(){
      if(typeof storage.get('profile') == "object"){
        $('.subscribedToStory').html('<i class="fa fa-bookmark"></i> Unsubscribe').addClass('unsubscribeToStory').removeClass('subscribedToStory').removeClass('subscribeToStory');
      }else{
        views.mainContentView.login();
      }
    },

    changeTosubscribed : function(){
      if(typeof storage.get('profile') == "object"){
        $('.unsubscribeToStory').html('<i class="fa fa-bookmark" style="color: #8dc63f;"></i> Subscribed').addClass('subscribedToStory').removeClass('unsubscribeToStory').removeClass('subscribeToStory');
      }else{
        views.mainContentView.login();
      }
    },

    wasHelpful : function(e){
      if(typeof storage.get('profile') == "object"){
        var story_comment_id = $(e.currentTarget).attr('id');
        var numberOfHelpfulReviews = parseInt($('#reply_'+story_comment_id+' .numberOfHelpfulRatingReviews').text() + 1);
        var totalNumberOfRatingReviews = parseInt($('#reply_'+story_comment_id+' .totalNumberOfRatingReviews').text() + 1);
        window.controllers.storyController.models.story.wasHelpful(story_comment_id);
        $('#reply_'+story_comment_id+' #helpfulSpan').remove();
        $('#reply_'+story_comment_id+' .numberOfHelpfulRatingReviews').text(numberOfHelpfulReviews);
        $('#reply_'+story_comment_id+' .totalNumberOfRatingReviews').text(totalNumberOfRatingReviews);
      }else{
        views.mainContentView.login();
      }
    },

    notHelpful : function(e){
      if(typeof storage.get('profile') == "object"){
        var story_comment_id = $(e.currentTarget).attr('id');
        var numberOfHelpfulReviews = parseInt($('#reply_'+story_comment_id+' .numberOfHelpfulRatingReviews').text());
        var totalNumberOfRatingReviews = parseInt($('#reply_'+story_comment_id+' .totalNumberOfRatingReviews').text() + 1);
        window.controllers.storyController.models.story.notHelpful(story_comment_id);
        $('#reply_'+story_comment_id+' #helpfulSpan').remove();
        $('#reply_'+story_comment_id+' .numberOfHelpfulRatingReviews').text(numberOfHelpfulReviews);
        $('#reply_'+story_comment_id+' .totalNumberOfRatingReviews').text(totalNumberOfRatingReviews);
      }else{
        views.mainContentView.login();
      }
    },

    shareChapter : function(){
      var that = this;
      require(['text!templates/items/share.html','text!templates/items/modal.html'], function(ShareModalTemplate, ModalTemplate){
        var url = document.URL;
        var shareModalTemplate = _.template(ShareModalTemplate);
        var modalTemplate = _.template(ModalTemplate);
        $('.mainContent').append(modalTemplate());
        $('.modal').append(shareModalTemplate({chapter: that.chapterOfThisStory}));
      });
    }

  });

  return storyView;
});
