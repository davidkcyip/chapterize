define([
  'underscore',
  'backbone',
  'text!templates/pages/mobileRegister.html'
], function( _, Backbone, MobileRegisterTemplate ){
  var mobileRegisterView = Backbone.View.extend({
    mobileRegisterTemplate: _.template(MobileRegisterTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.mobileRegisterTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return mobileRegisterView;
});