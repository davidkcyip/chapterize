define([
  'underscore',
  'backbone',
  'text!templates/pages/circle.html'
], function( _, Backbone, CircleTemplate ){
  var circleView = Backbone.View.extend({
    circleTemplate: _.template(CircleTemplate),

    el : '.mainContent',

    events: {
      'keyup #searchCirclesInput' : 'searchCircles'
    },

    initialize: function(options){
      //this.el = $('.mainContainer');
      this.render(options);
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getHomepageRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModuleRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    render: function(options){
      if(typeof options.term != "undefined"){
        this.$el.html(this.circleTemplate({term: options.term}));
      }else{
        this.$el.html(this.circleTemplate({term: null}));
      }
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto')
      return this.el;
    },

    searchCircles : function(e){
      var searchTerm = $('#searchCirclesInput').val();
      var code = e.keyCode || e.which;
      if(code == 13 && searchTerm.length > 3) { 
        appRouter.navigate("/circle/search/"+searchTerm+"/1", {trigger: true});
      }
    }

  });

  return circleView;
});