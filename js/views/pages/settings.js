define([
  'underscore',
  'backbone',
  'text!templates/pages/settings.html'
], function( _, Backbone, SettingsTemplate ){
  var settingsView = Backbone.View.extend({
    settingsTemplate: _.template(SettingsTemplate),

    el : '.mainContent',

    events : {
      'click .test' : 'test'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.settingsTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return settingsView;
});