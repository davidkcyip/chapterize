define([
  'underscore',
  'backbone',
  'text!templates/pages/homepage.html',
  'js/models/Feed',
  'timeline',
  'zozo'
], function( _, Backbone, HomeTemplate, FeedModel, timeline, zozo ){
  var homeView = Backbone.View.extend({
    homeTemplate: _.template(HomeTemplate),

    el : '.mainContent',
    models: {},
    type: 'all',
    timeline_data: [],
    timeline: {},
    number: 0,

    events : {
      'click .homepageLoadMore' : 'loadPrevious',
      'click .learnAbout' : 'closeAboutUs'
    },

    initialize: function(){
      var that = this;
      this.render();
      this.models.feedModel = new FeedModel.FeedModel();
      this.models.feedModel.on('change:homepageFeed',this.renderResults,this);
      this.models.feedModel.getHomepageFeed('all');
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModuleRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    renderResults: function(results){
      var that = this;
      results = results.get('homepageFeed');
      for(var i = results.length - 1; i >= 0; i-- ){
        if(results[i].type == "new_story"){
          var title = '<a href="/profile/'+results[i].status_by_pen_name+'">'+results[i].status_by_real_name+'</a> published <a href="/story/'+results[i].story_id+'/'+results[i].story_slug+'">'+results[i].story_name+'</a>';
          var content = results[i].status_text;
          var readmore = '/story/'+results[i].story_id+'/'+results[i].story_slug;
        }else if(results[i].type == "new_chapter"){
          var title = '<a href="/profile/'+results[i].status_by_pen_name+'">'+results[i].status_by_real_name+'</a> published <a href="/story/'+results[i].story_id+'/'+results[i].story_slug+'/'
          +results[i].chapter_slug+'">'+results[i].chapter_name+'</a> in <a href="/story/'+results[i].story_id+'/'+results[i].story_slug+'">'+results[i].story_name+'</a>';
          var content = results[i].status_text;
          var readmore = '/story/'+results[i].story_id+'/'+results[i].story_slug+'/'+results[i].chapter_slug;
        }else if(results[i].type == "chapter_comment"){
          var title = '<a href="/profile/'+results[i].status_by_pen_name+'">'+results[i].status_by_real_name+'</a> commented in <a href="/story/'+results[i].story_id+'/'+results[i].story_slug+'/'+results[i].chapter_slug+'">'+results[i].chapter_name+'</a>';
          var content = results[i].status_text;
          var readmore = '/story/'+results[i].story_id+'/'+results[i].story_slug+'/'+results[i].chapter_slug;
        }
        var array = {
          type: 'blog_post',
          date: results[i].status_time,
          title: title,
          content: content,
          readmore: readmore
        }
        that.timeline_data.push(array);
      }
        that.timeline = new Timeline($('#timeline'), that.timeline_data);
        that.timeline.setOptions({
            animation:   false,
            lightbox:    true,
            showMonth:   true,
            responsive_width: 590,
            separator:   'month_year',
            first_separator: true,
            columnMode:  'dual',
            order:       'desc'
        });
        that.timeline.display();
      that.timeline_data = [];
    },

    render: function(){
      this.$el.html(this.homeTemplate());
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto')
      return this.el;
    },

    loadPrevious : function(){
      this.type = "previous";
      $('.homepageLoadMore').html("LOADING <i class='fa fa-spinner fa-spin' style='font-size:18px;'></i>");
      this.models.feedModel.getHomepageFeed('previous');
    },

    closeAboutUs : function(){
      $('.learnAbout').remove();
      storage.set('closedAboutUs',1);
    }

  });

  return homeView;
});