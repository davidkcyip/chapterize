define([
  'underscore',
  'backbone',
  'text!templates/pages/search.html',
  'text!templates/pages/searchMobile.html'
], function( _, Backbone, SearchTemplate, SearchMobileTemplate ){
  var searchView = Backbone.View.extend({
    searchTemplate: _.template(SearchTemplate),
    searchMobileTemplate: _.template(SearchMobileTemplate),

    el : '.mainContent',

    events : {
      'keyup #searchInput' : 'searchResults'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || new Date().getTime() - storage.get('moduleLastChecked') <= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || new Date().getTime() - storage.get('moduleLastChecked') <= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || new Date().getTime() - storage.get('moduleLastChecked') <= 3600){
        views.mainContentView.models.storyModel.getModuleRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || new Date().getTime() - storage.get('moduleLastChecked') <= 3600){
        views.mainContentView.models.storyModel.getRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    render: function(){
      if( !navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
        this.$el.html(this.searchTemplate());
      }else{
        this.$el.html(this.searchMobileTemplate());
      }
      $('.breadcrumbs #second').html("<a href='/search'>Search</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("");
      $('.breadcrumbs #fourth').html("");
      $('.breadcrumbs #fifth').html("");
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto');
      return this.el;
    },

    searchResults : function(e){
      var code = e.keyCode || e.which;
      var value = $(e.currentTarget).val();
      if(code == 13 && value.length > 3) { 
        appRouter.navigate("/search/"+value+"/all/1", {trigger: true});
      }
    }

  });

  return searchView;
});