define([
  'underscore',
  'backbone',
  'text!templates/pages/newCircleLeft.html'
], function( _, Backbone, NewCircleLeftTemplate ){
  var newCircleLeftView = Backbone.View.extend({
    newCircleLeftTemplate: _.template(NewCircleLeftTemplate),

    el : '.mainContent',

    events: {
      'click .updateAlbum' : 'updateAlbum'
    },

    initialize: function(options){
      //this.el = $('.mainContainer');
      this.render(options);
    },

    render: function(options){
      this.$el.html(this.newCircleLeftTemplate());
      //this.delegateEvents();
      return this.el;
    },

    updateAlbum : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        require(['text!templates/items/modal.html','text!templates/items/updateAvatar.html', 'js/croppic.min'], function(ModalTemplate, UpdateAvatarTemplate, Crop){
          var modalTemplate = _.template(ModalTemplate);
          var updateAvatarTemplate = _.template(UpdateAvatarTemplate);
          that.$el.append(modalTemplate());
          $('.modal').append(updateAvatarTemplate());
          $('.modalBox').css('width','350px').css('height','350px');
          var cropperOptions = {
            uploadUrl:'/api/uploadAlbumInitial',
            cropUrl:'/api/cropAlbumInitial',
            imgEyecandy:true,
            imgEyecandyOpacity:0,
            outputUrlId:'outputUrlId',
            customUploadButtonId:'updateAvatarButton',
            onAfterImgCrop: function(){
              var newAvatar = that.cropperHeader.croppedImg.valueOf().attr('src');
              
              $('img[class="album"]').attr('src',newAvatar);
              that.cropperHeader.destroy();
              $('.modalBox').remove();
              $('.modal').remove();
            }
          }
          //var crop = Crop;
          //var cropHead = new crop('uploadAvatar', cropperOptions);
          that.cropperHeader = new Croppic('uploadAvatar', cropperOptions);
          //cropperHeader.form.find('input[type="file"]').trigger('click');
        });
      }else{
        views.mainContentView.login();
      }
    }

  });

  return newCircleLeftView;
});