define([
  'underscore',
  'backbone',
  'text!templates/items/headerContainer.html',
  'text!templates/items/footer.html',
  'text!templates/items/header-loggedin.html',
  'text!templates/items/header.html',
  'text!templates/items/headerMenu.html',
  'text!templates/items/headerMenu-loggedin.html',
  'text!templates/items/headerMenu-loggedin-mobile.html',
  'text!templates/items/registerSuccess.html',
  'text!templates/items/loginModal.html',
  'text!templates/items/modal.html',
  'text!templates/items/bookmarks.html',
  'text!templates/items/favourites.html',
  'js/models/Search',
  'js/models/Story',
  'nicEdit',
  'zozo'
], function( _, Backbone, HeaderContainerTemplate, FooterTemplate, HeaderLoggedInTemplate, HeaderTemplate, HeaderMenuTemplate, HeaderMenuLoggedInTemplate, HeaderMenuLoggedInMobileTemplate, RegisterSuccess, LoginModalTemplate, ModalTemplate, BookmarksTemplate, FavouritesTemplate, SearchModel, StoryModel, nicEdit, zozo ){
  var mainContentView = Backbone.View.extend({
    mainContentTemplate: _.template(HeaderContainerTemplate + FooterTemplate),

    el : 'body',
    models : {},

    events : {
      'click #wrapper #headerMenu button' : 'activeButton',
      'click #wrapper .footer a' : 'deactivateButton',
      'click #wrapper #headerMenu a[name="about"]' : 'goToAbout',
      'click #wrapper #headerMenu a[name="register"]' : 'goToRegister',
      'click #wrapper #headerMenu a[name="features"]' : 'goToFeatures',
      'click #arrowUp' : 'scrollToTop',
      'click a' : 'dontRefresh',
      'click #wrapper .bg' : 'closeDialog',
      'click #closeModal span i' : 'closeDialog',
      'click #signUpButton' : 'signUp',
      'keyup #signUpBox' : 'signUp',
      'keyup #penname' : 'checkPenname',
      'keyup #email' : 'checkEmail',
      'keyup #password2' : 'checkPasswords',
      'keyup #signUpName' : 'checkName',
      'click #login' : 'login',
      'keyup #signUpBody input' : 'loginSubmit',
      'click #loginButton' : 'loginSubmit',
      'click #logout' : 'logout',
      'click .forgotPassword' : 'forgotPassword',
      'click #forgotPasswordButton' : 'sendNewPassword',
      'click .likeStatusButton' : 'likeStatus',
      'click .unlikeStatusButton' : 'unlikeStatus',
      'click .likeReplyButton' : 'likeReply',
      'click .unlikeReplyButton' : 'unlikeReply',
      'click .replyButton' : 'showHideReplyInput',
      'click .submitReplyButton' : 'submitReply',
      'click #submitStatusButton' : 'submitStatus',
      'click .deleteStatusButton' : 'deleteStatus',
      'click .deleteReplyButton' : 'deleteReply',
      'click .mobileMenuStories' : 'toggleMobileStories',
      'click .mobileMenuSearch' : 'toggleMobileSearch',
      'click .mobileMenuChallenges' : 'toggleMobileChallenges',
      'click .mobileMenuMessages' : 'toggleMobileMessages',
      'click .mobileMenuBookmarks' : 'toggleMobileBookmarks',
      'click .mobileMenuFavourites' : 'toggleMobileFavourites',
      'click .mobileMenuNotifications' : 'toggleMobileNotifications',
      'click .sendMessageConfirm' : 'sendMessageConfirm',
      'keyup #menuBarSearch' : 'search',
      'click .notificationModal' : 'readMessage',
      'click #registerButton' : 'openRegisterForm',
      'click #shareURL' : 'highlightInput'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
      window.app.vent.on('site:registerSuccess', this.registerSuccess, this);
      window.app.vent.on('site:forgotPasswordSuccess', this.forgotPasswordSuccess, this);
      window.app.vent.on('home:submitStatusSuccess', this.submitStatusSuccess, this);
      window.app.vent.on('home:submitReplySuccess', this.submitReplySuccess, this);
      window.app.vent.on('home:deleteReplySuccess', this.deleteReplySuccess, this);
      window.app.vent.on('home:deleteStatusSuccess', this.deleteStatusSuccess, this);
      window.app.vent.on('home:likeStatusSuccess', this.likeUnlikeStatusSuccess, this);
      window.app.vent.on('home:unlikeStatusSuccess', this.likeUnlikeStatusSuccess, this);
      window.app.vent.on('home:likeReplySuccess', this.likeUnlikeReplySuccess, this);
      window.app.vent.on('home:unlikeReplySuccess', this.likeUnlikeReplySuccess, this);
      this.models.searchModel = new SearchModel.SearchModel();
      this.models.storyModel = new StoryModel.StoryModel();
      
      this.models.searchModel.on('change:searchTags',this.renderTags,this);
      this.models.storyModel.on('change:modulePopularStorySearch',this.renderPopularStories,this);
      this.models.storyModel.on('change:moduleRecentStorySearch',this.renderRecentStories,this);
      this.models.storyModel.on('change:recentComments',this.renderRecentComments,this);
    },

    render: function(){
      if(window.storage.get('profile')){
        var headerTemplate = _.template(HeaderLoggedInTemplate);
        if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
          var headerMenuTemplate = _.template(HeaderMenuLoggedInTemplate);
        }else{
          var headerMenuTemplate = _.template(HeaderMenuLoggedInMobileTemplate);
          //var HeaderMenuTemplate = _.template("<div></div>");
        }
        this.$el.html(this.mainContentTemplate({header: headerTemplate(), headerMenu: headerMenuTemplate()}));
      }else{
        var headerTemplate = _.template(HeaderTemplate);
        if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
          var headerMenuTemplate = _.template(HeaderMenuTemplate);
        }else{
          //var headerMenuTemplate = _.template("<div></div>");
          var headerMenuTemplate = _.template(HeaderMenuTemplate);
        }
        this.$el.html(this.mainContentTemplate({header: headerTemplate(), headerMenu: headerMenuTemplate()}));
      }
      var touch   = $('#touch-menu');
      var menu  = $('.menu');
      $(touch).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
      });
      
      $(window).resize(function(){
        var w = $(window).width();
        if(w > 767 && menu.is(':hidden')) {
          menu.removeAttr('style');
        }
      });
      return this.el;
    },

    highlightInput : function(e){
      e.currentTarget.select();
    },

    readMessage : function(e){
      var notification_id = $(e.currentTarget).attr('id');
      window.controllers.mainContentController.models.notificationModel.readNotification(notification_id);
    },

    search : function(e){
      var code = e.keyCode || e.which;
      var value = $(e.currentTarget).val();
      if(code == 13 && value.length > 3) { 
        appRouter.navigate("/search/"+value+"/all/1", {trigger: true});
      }
    },

    toggleMobileBookmarks : function(){
      if($('.mobileMenuBookmarks .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuBookmarks .sub-menu').slideUp();
      }else{
        $('.mobileMenuBookmarks .sub-menu').slideDown();
      }
    },

    toggleMobileMessages : function(){
      if($('.mobileMenuMessages .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuMessages .sub-menu').slideUp();
      }else{
        $('.mobileMenuMessages .sub-menu').slideDown();
      }
    },

    toggleMobileChallenges : function(){
      if($('.mobileMenuChallenges .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuChallenges .sub-menu').slideUp();
      }else{
        $('.mobileMenuChallenges .sub-menu').slideDown();
      }
    },

    toggleMobileSearch : function(){
      if($('.mobileMenuSearch .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuSearch .sub-menu').slideUp();
      }else{
        $('.mobileMenuSearch .sub-menu').slideDown();
      }
    },

    toggleMobileStories : function(){
      if($('.mobileMenuStories .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuStories .sub-menu').slideUp();
      }else{
        $('.mobileMenuStories .sub-menu').slideDown();
      }
    },

    toggleMobileFavourites : function(){
      if($('.mobileMenuFavourites .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuFavourites .sub-menu').slideUp();
      }else{
        $('.mobileMenuFavourites .sub-menu').slideDown();
      }
    },

    toggleMobileNotifications : function(){
      if($('.mobileMenuNotifications .sub-menu').css('display') == 'block'){
        var action = "hide";
      }else{
        var action = "show";
      }
      $('.sub-menu').slideUp();
      if(action == "hide"){
        $('.mobileMenuNotifications .sub-menu').slideUp();
      }else{
        $('.mobileMenuNotifications .sub-menu').slideDown();
      }
    },

    postRenderBookmark : function(results){
      var bookmarksTemplate = _.template(BookmarksTemplate);
      if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
        $('.bookmarksMenu').html(bookmarksTemplate({bookmarks:results.get('bookmarks')}));
      }else{
        $('.bookmarksMenu .sub-menu').html(bookmarksTemplate({bookmarks:results.get('bookmarks')}));
      }
    },

    postRenderFavourite : function(results){
      var favouritesTemplate = _.template(FavouritesTemplate);
      if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
        $('.favouritesMenu').html(favouritesTemplate({favourites:results.get('favourites')}));
      }else{
        $('.favouritesMenu .sub-menu').html(favouritesTemplate({favourites:results.get('favourites')}));
      }
    },

    showHideReplyInput : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");

      window.app.vent.trigger('home:openCloseReplyArea',thisId[1], thisId[0]);
    },

    submitStatus : function(e){
      window.app.vent.trigger('home:submitStatus',$('#submitStatus').attr('to'));
    },

    submitStatusSuccess : function(id,to_id,to_real_name,to_pen_name){
      var status = $('#submitStatus').val();
      $('#submitStatus').val('');
      if(typeof window.views.profileView != "undefined"){
        var to_real_name = window.views.profileView.thisProfile.real_name;
        var to_pen_name = window.views.profileView.thisProfile.pen_name;
      }else{
        var to_real_name = window.storage.get('profile').real_name;
        var to_pen_name = window.storage.get('profile').pen_name;
      }
      var feed = [{
        "avatar" : window.storage.get('profile').avatar,
        "id" : id,
        "status_by_id" : window.storage.get('profile').id,
        "status_to_id" : to_id,
        "by_real_name" : window.storage.get('profile').real_name,
        "by_pen_name" : window.storage.get('profile').pen_name,
        "status_to_real_name" : to_real_name,
        "status_to_pen_name" : to_pen_name,
        "status" : status,
        "status_time" : new Date().getTime(),
        "replies" : [],
        "type": "status"
      }];
      require(['text!templates/items/homeNewsfeed.html'], function(HomeNewsfeedTemplate){
        var homeNewsfeedTemplate = _.template(HomeNewsfeedTemplate);
        var homeNewsfeedTemplate2 = homeNewsfeedTemplate({feed: feed});
        $('.postsList').prepend(homeNewsfeedTemplate2);
      });
    },

    submitReplySuccess : function(id,post_id,reply){
      $('#replyAreaInput_'+post_id).val('');
      var replies = [{
        "avatar" : window.storage.get('profile').avatar,
        "id" : id,
        "reply_by_pen_name" : window.storage.get('profile').pen_name,
        "reply_by_real_name" : window.storage.get('profile').real_name,
        "reply_by_id" : window.storage.get('profile').id,
        "replied_at" : new Date().getTime(),
        "reply" : reply,
        "likes" : []
      }];
      require(['text!templates/items/postReply.html'], function(PostReplyTemplate){
        var postReplyTemplate = _.template(PostReplyTemplate);
        var postReplyTemplate2 = postReplyTemplate({replies: replies,postid: post_id});
        $('#post_'+post_id+' .repliesContainer').prepend('<div class="replies" id="reply_'+id+'">'+postReplyTemplate2+"</div>");
      });
    },

    submitReply : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      var reply = $('#replyAreaInput_'+thisId[1]).val();
      var options = [];
      options['reply'] = reply;
      options['id'] = thisId[1];
      if(reply != "" && reply){
        window.app.vent.trigger('home:sendReplyStatus',options);
      }
    },

    activeButton : function(){
      window.app.vent.trigger('menu:activeButton',window.app.getActiveElement(this));
    },

    deactivateButton : function(){
      window.app.vent.trigger('menu:deactivateButton');
    },

    goToAbout : function(){
      window.location = "http://beta.bloqly.com#description";
    },

    goToFeatures : function(){
      window.location = "http://beta.bloqly.com#features";
    },

    goToRegister : function(){
      window.location = "http://beta.bloqly.com#register";
    },

    scrollToTop : function(){
      window.app.vent.trigger('site:scrollToTop');
    },

    dontRefresh : function(e){
      if(e.currentTarget.href.indexOf('javascript:void(0);') == -1 &&e.currentTarget.href.indexOf('reddit.com/submit') == -1 && e.currentTarget.href.indexOf('tumblr.com/share') == -1 && e.currentTarget.href.indexOf('plus.google.com/share') == -1 && e.currentTarget.href.indexOf('facebook.com/sharer') == -1 && e.currentTarget.href.indexOf('twitter.com/intent/tweet') == -1 && e.currentTarget.href.indexOf('/api/login/facebook') == -1 && e.currentTarget.href.indexOf('/avatars') == -1 && e.currentTarget.href.indexOf('/blog') == -1 && window.location.href.indexOf('/blog') == -1 && e.currentTarget.href != "#popular" && e.currentTarget.href != "#tags" && e.currentTarget.href != "#latest" && e.currentTarget.href != "#comments" && e.currentTarget.href != ""){
        e.preventDefault();
        if((e.currentTarget.href.indexOf('bloqly.com') > -1 || e.currentTarget.href.indexOf('127.0.0.1') > -1)){
          if(e.currentTarget.href.indexOf('#') == -1){
            appRouter.navigate(e.currentTarget.pathname, {trigger: true});
          }
        }else{
          window.open(e.currentTarget.href,'_blank');
        }
      }else if(e.currentTarget.href == "" || e.currentTarget.href.indexOf('/api/login/facebook') > -1 || e.currentTarget.href.indexOf('javascript:void(0);') > -1){
      
      }else{
        window.open(e.currentTarget.href,'_blank');
      }
    },

    closeDialog : function(e){
      window.app.vent.trigger('site:closeModal',e);
    },

    checkPenname : function(){
      window.app.vent.trigger('site:checkPenname');
    },

    checkEmail : function(){
      window.app.vent.trigger('site:checkEmail');
    },

    checkPasswords : function(){
      window.app.vent.trigger('site:checkPasswords');
    },

    checkName : function(){
      window.app.vent.trigger('site:checkName');
    },

    signUp : function(e){
      if((typeof e.keyCode != "undefined" && e.keyCode == 13) || e.type == "click"){
        window.app.vent.trigger('site:signUp');
      }
    },

    login : function(){
      $('.mainContainer').append(_.template(ModalTemplate));
      $('.modal').append(_.template(LoginModalTemplate));
      $('.modalBox').css('height','440px');
    },

    openRegisterForm : function(){
      window.app.vent.trigger('home:openRegistrationBox');
    },

    loginSubmit : function(e){
      if((typeof e.keyCode != "undefined" && e.keyCode == 13) || e.type == "click"){
        window.app.vent.trigger('site:loginSubmit'); 
      }
    },

    logout : function(){
      window.app.vent.trigger('site:logout');
    },

    forgotPassword : function(){
      require(['text!templates/items/forgotPassword.html'], function(ForgotPasswordTemplate){
        var forgotPasswordTemplate = _.template(ForgotPasswordTemplate);
        $('.modalBox').remove();
        $('.modal').append(forgotPasswordTemplate);
        $('.modalBox').css('height','210px');
      });
    },

    forgotPasswordSuccess : function(){
      require(['text!templates/items/forgotPasswordSuccess.html'], function(ForgotPasswordSuccessTemplate){
        var forgotPasswordSuccessTemplate = _.template(ForgotPasswordSuccessTemplate);
        $('.modalBox').remove();
        $('.modal').append(forgotPasswordSuccessTemplate);
        $('.modalBox').css('height','80px');
      });
    },

    sendNewPassword : function(){
      window.app.vent.trigger('site:sendNewPassword');
    },

    registerSuccess : function(){
      $('.modalBox').html(_.template(RegisterSuccess)).css('height','65px');
    },

    likeStatus : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        window.app.vent.trigger('home:likeStatus',thisId[1]);
      }else{
        this.login();
      }
    },

    likeReply : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        thisPostId = $(e.currentTarget).attr('data-parent');
        var options = [];
        options['id'] = thisId[1];
        options['thisPostId'] = thisPostId;
        window.app.vent.trigger('home:likeReply',options);
      }else{
        this.login();
      }
    },

    likeUnlikeStatusSuccess : function(feed){
      require(['text!templates/items/postStatus.html','text!templates/items/repostStatus.html'], function(PostStatusTemplate, RepostStatusTemplate){
        var postStatusTemplate = _.template(PostStatusTemplate);
        var repostStatusTemplate = _.template(RepostStatusTemplate);
        if(typeof feed[0].shared_status == "undefined"){
          $('#post_'+feed[0].id+ " .statusContainer").html(postStatusTemplate({feed: feed}));
        }else{
          $('#post_'+feed[0].id).html(repostStatusTemplate({post: feed[0]}));
        }
        Tipped.create(".likesPopup");
        $('.repliesContainer').hide();
      });
    },

    unlikeStatus : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        window.app.vent.trigger('home:unlikeStatus',thisId[1]);
      }else{
        this.login();
      }
    },

    unlikeReply : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        thisPostId = $(e.currentTarget).attr('data-parent');
        var options = [];
        options['id'] = thisId[1];
        options['thisPostId'] = thisPostId;
        window.app.vent.trigger('home:unlikeReply',options);
      }else{
        this.login();
      }
    },

    likeUnlikeReplySuccess : function(options){
      require(['text!templates/items/postReply.html'], function(PostReplyTemplate){
        var postReplyTemplate = _.template(PostReplyTemplate);
        $('#reply_'+options.reply[0].id).html(postReplyTemplate({replies: options.reply, postid: options.thisPostId}));
        Tipped.create(".likesPopup");
      });
    },

    deleteStatus : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        window.app.vent.trigger('home:deleteStatus',thisId[1]);
      }else{
        this.login();
      }
    },

    deleteStatusSuccess : function(id){
      $('#post_'+id).remove();
    },

    deleteReply : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = e.currentTarget.id;
        thisId = thisId.split("_");
        window.app.vent.trigger('home:deleteReply',thisId[1]);
      }else{
        this.login();
      }
    },

    deleteReplySuccess : function(id){
      $('#reply_'+id).remove();
    },

    sendMessageConfirm : function(){
      var title = $('#messageTitle').val(), message = new nicEditors.findEditor('message').getContent();
      console.log('hi')
      if(title != null && message != null && title != "" && message != ""){
        var options = {};
        options.recipient = window.views.profileView.thisProfile.id;
        options.thread_title = title;
        options.message = message;
        var result = window.controllers.mainContentController.models.messagesModel.startNewMessageThread(options);

        $.when(result).done($('.modal').remove());
      }else{
        $('.sendMessageModalError').text('Please enter a title and message.');
        $('.modalBox').css('height','360px');
        setTimeout( function(){
          $('.sendMessageModalError').text('').css('height','340px');
          $('.modalBox').css('height','340px');
        },2000);
      }
    },

    renderTags : function(results){
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        storage.set('moduleTags',results);
        storage.set('moduleLastChecked',new Date().getTime());
      }
      var results = _.first(_.sortBy(storage.get('moduleTags')['searchTags'], function(o) { return o.tag_percentage; }).reverse(),10);
        require(['text!templates/items/homepageTags.html'], function(HomepageTagsTemplate){
          var homepageTagsTemplate = _.template(HomepageTagsTemplate);
          $( "#tabbed-nav #tags" ).ready(function() {
            $('#tabbed-nav .z-container #tags .z-content-inner .spinner').remove();
            $('#tabbed-nav #tags').append(homepageTagsTemplate({results:results}));
          });
        });
    },

    renderPopularStories : function(results){
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        storage.set('modulePopular',results);
        storage.set('moduleLastChecked',new Date().getTime());
      }
      var results = _.first(storage.get('modulePopular')['modulePopularStorySearch'], 5);
      var ratingArray = [];
        _.each(results, function(result){
          var bookmarks = _.pluck(result.chapters, 'bookmarks');
          var bookmarkCount = _.pluck(bookmarks,'bookmark_count');
          var bookmarkCount = _.reduce(bookmarkCount, function(a,b){ return parseInt(a) + parseInt(b); },0);
          var favourites = _.pluck(result.chapters, 'favourites');
          var favouriteCount = _.pluck(favourites, 'favourite_count');
          var favouriteCount = _.reduce(favouriteCount, function(a,b){ return parseInt(a) + parseInt(b); },0);
          var ratings = _.pluck(result.chapters, 'comments');
          var ratingsCount = _.pluck(ratings[0],'rating');
          _.each(ratingsCount, function(rating){
            if(rating != null){
              ratingArray.push(rating);
            }
          });
          var ratingsCount = _.reduce(ratingArray, function(a,b){ return parseInt(a) + parseInt(b); },0);
          result.bookmark = bookmarkCount;
          result.favourite = favouriteCount;
          if(ratingArray.length > 0){
            result.average_rating = Math.round(ratingsCount/ratingArray.length * 100)/100;
          }else{
            result.average_rating = "NA";
          }
        });
        require(['text!templates/items/homepagePopularStories.html'], function(HomepagePopularStoriesTemplate){
          var homepagePopularStoriesTemplate = _.template(HomepagePopularStoriesTemplate);
          $( "#popular .z-content-inner" ).ready(function() {
            $('#popular .z-content-inner .spinner').remove();
            $('#popular .z-content-inner').append(homepagePopularStoriesTemplate({results:results}));
          });
        });
    },

    renderRecentStories : function(results){
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        storage.set('moduleRecent',results);
        storage.set('moduleLastChecked',new Date().getTime());
      }
      var results = _.first(storage.get('moduleRecent')['moduleRecentStorySearch'], 5);
        require(['text!templates/items/homepageRecentStories.html'], function(HomepageRecentStoriesTemplate){
          var homepageRecentStoriesTemplate = _.template(HomepageRecentStoriesTemplate);
          $( "#latest .z-content-inner" ).ready(function() {
            $('#latest .z-content-inner .spinner').remove();
            $('#latest .z-content-inner').append(homepageRecentStoriesTemplate({results:results}));
          });
        });
    },

    renderRecentComments : function(results){
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        storage.set('moduleComments',results);
        storage.set('moduleLastChecked',new Date().getTime());
      }
      var results = storage.get('moduleComments')['recentComments'];
        require(['text!templates/items/homepageRecentComments.html'], function(HomepageRecentComments){
          var homepageRecentComments = _.template(HomepageRecentComments);
          $( "#comments .z-content-inner" ).ready(function() {
            $('#comments .z-content-inner .spinner').remove();
            $('#comments .z-content-inner').append(homepageRecentComments({results:results}));
          });
        });
    },

  });

  return mainContentView;
});