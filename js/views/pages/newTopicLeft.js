define([
  'underscore',
  'backbone',
  'text!templates/pages/newTopicLeft.html'
], function( _, Backbone, NewTopicLeftTemplate ){
  var newTopicLeftView = Backbone.View.extend({
    newTopicLeftTemplate: _.template(NewTopicLeftTemplate),

    el : '.mainContent',
    circleDetails : {},

    events : {

    },

    initialize: function(options, results){
      var that = this;
      console.log(results)
      setTimeout( function(){
        that.render(results);
      },40);
        

        //console.log(that.circleDetails)
        //that.render();
    },

    render: function(results){
      this.$el.html(this.newTopicLeftTemplate({circle: results}));
      //this.delegateEvents();
      return this.el;
    }

  });

  return newTopicLeftView;
});