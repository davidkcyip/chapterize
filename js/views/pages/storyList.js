define([
  'underscore',
  'backbone',
  'text!templates/pages/storyChapterLeft.html',
  'text!templates/items/storyList.html',
  'bootpag'
], function( _, Backbone, StoryChapterLeftTemplate, StoryListTemplate, bootpag ){
  var storyListView = Backbone.View.extend({
    storyChapterLeftTemplate: _.template(StoryChapterLeftTemplate),
    storyListTemplate: _.template(StoryListTemplate),
    page: 1,

    el : '.mainContent',

    events : {
      
    },

    initialize: function(page){
      this.page = page;
      this.render();
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModuleRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    render: function(){
      this.$el.html(this.storyChapterLeftTemplate());
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto');
      return this.el;
    },

    populateLeft : function(user){
      var html = '<a href="/profile/'+user.pen_name+'"><img src="'+user.avatar+'" width="24" class="avatar"></a> <a href="/profile/'+user.pen_name+'">'+user.real_name+'</a>';
      $('.profileHeader').html(html);
    },

    renderStories : function(results){
      var that = this;
      $('meta[property="og:title"]').attr('content',"Bloqly - " + results[0].authors[0].real_name + " - Stories");
      $('meta[property="og:description"]').attr('content',results[0].authors[0].real_name+"'s stories on Bloqly");
      $('.rightContent').html(this.storyListTemplate({results: results}));
      setTimeout( function(){
        that.renderPagination();
      },10);
    },

    renderPagination : function(){
      var that = this;
      var num_stories = controllers.storyListController.models.storyModel.get('numberOfAuthorStories');
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_stories)/10),
           page: parseInt(that.page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
    }

  });

  return storyListView;
});