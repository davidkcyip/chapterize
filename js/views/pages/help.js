define([
  'underscore',
  'backbone',
  'text!templates/pages/help.html'
], function( _, Backbone, HelpTemplate ){
  var helpView = Backbone.View.extend({
    helpTemplate: _.template(HelpTemplate),

    el : '.mainContent',

    events : {
      'click .test' : 'test'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.helpTemplate());
      //this.delegateEvents();
      return this.el;
    }

  });

  return helpView;
});