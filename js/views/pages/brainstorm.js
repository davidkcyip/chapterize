define([
  'underscore',
  'backbone',
  'text!templates/pages/brainstorm.html'
], function( _, Backbone, BrainstormTemplate ){
  var brainstormView = Backbone.View.extend({
    brainstormTemplate: _.template(BrainstormTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.brainstormTemplate());
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto')
      return this.el;
    },

  });

  return brainstormView;
});