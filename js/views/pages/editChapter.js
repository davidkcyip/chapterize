define([
  'underscore',
  'backbone',
  'text!templates/pages/newChapter.html',
  'text!templates/items/editChapterRight.html',
  'nicEdit'
], function( _, Backbone, NewChapterTemplate, EditChapterRightTemplate, nicEdit ){
  var editChapterView = Backbone.View.extend({
    newChapterTemplate: _.template(NewChapterTemplate),
    editChapterRightTemplate: _.template(EditChapterRightTemplate),

    el : '.mainContent',
    model: {},
    chapter: {},
    new_chapter_id: 0,

    events : {
      'click #editChapterButton' : 'editChapter'
    },

    initialize: function(model,chapter, action){
      $('.breadcrumbs #second').html("<a href='/profile/"+ chapter.pen_name +"'>" + chapter.real_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("<a href='/story/" + chapter.pen_name + "/" + chapter.story_id + "/" + chapter.story_slug + "'>" + chapter.story_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fourth').html("<a href='/story/" + chapter.pen_name + "/" + chapter.story_id + "/" + chapter.story_slug + "/" + chapter.slug + "'>" + chapter.chapter_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fifth').html("Edit<i class='fa fa-angle-double-right'></i>");

      document.title = "Edit Chapter - "+chapter.story_name;
      this.model = model;
      this.chapter = chapter;

      this.render(chapter, action);
    },

    render: function(chapter, action){
      var editChapterRightTemplate2 = this.editChapterRightTemplate({chapters: chapter});
      this.$el.html(this.newChapterTemplate({rightContent: editChapterRightTemplate2}));
      nicEditors.allTextAreas();
      return this.el;
    },

    editChapter : function(){
      if(typeof storage.get('profile') == "object"){
        var nicE = new nicEditors.findEditor('storyContent').getContent();
        var chapterName = $('#chapterName').val();
        if($('#chapterName').val() != "" && nicE != "<br>" && nicE.length > 0){
          var options = [];
          var that = this;
          options.story_id = this.chapter.story_id;
          options.chapter_name = chapterName;
          options.chapter_body = nicE;
          options.chapter_id = this.chapter.id;
          var saveDraft = this.model.editChapter(options);
          $.when(saveDraft).done(that.checkIfSuccessful(saveDraft));
        }else{
          $('.step2Error').text('Title and chapter body must not be empty');
          setTimeout(function(){
            $('.step2Error').text('').css('color','#ae1819');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    checkIfSuccessful : function(saveDraft){
      var that = this;
      if(saveDraft.success == 1){
        that.editChapterSuccess(saveDraft);
      }else{
        that.editChapterError();
      }
    },

    editChapterError : function(){
      $('.step2Error').text('There was a problem in saving your chapter, please try again or save your draft onto your computer and contact support for help.').css('color','#ae1819');
      setTimeout(function(){
        $('.step2Error').text('');
      },2000);
    },

    editChapterSuccess : function(response){
      appRouter.navigate('/story/'+this.chapter.story_id+'/'+this.chapter.story_slug+'/'+response.slug, {trigger: true});
    }

  });

  return editChapterView;
});