define([
  'underscore',
  'backbone',
  'text!templates/pages/editStory.html',
  'text!templates/items/editStory.html',
  'js/jquery.tagsinput',
  'chosen'
], function( _, Backbone, EditStoryTemplate, EditStoryRightTemplate, TagsInput, Chosen ){
  var editStoryView = Backbone.View.extend({
    editStoryTemplate: _.template(EditStoryTemplate),
    editStoryRightTemplate: _.template(EditStoryRightTemplate),

    el : '.mainContent',
    model: {},
    chapter: {},
    storyDetails: {},
    new_chapter_id: 0,

    events : {
      'click #saveStoryDetails' : 'editStory',
      'click .updateAlbum' : 'updateAlbum'
    },

    initialize: function(model, story, categories, storyTypes){
      console.log(story)
      $('.breadcrumbs #second').html("<a href='/profile/"+ story[0].pen_name +"'>" + story[0].real_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("<a href='/story/" + story[0].pen_name + "/" + story[0].id + "/" + story[0].slug + "'>" + story[0].story_name + "</a> <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fourth').html("Edit <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #fifth').html("");

      window.app.vent.on('editStory:done',this.editStoryDone, this);

      document.title = "Edit Story - "+story[0].story_name;
      this.model = model;
      this.storyDetails = story[0];

      this.render(story, categories, storyTypes);
    },

    render: function(story, categories, storyTypes){
      var editStoryRightTemplate2 = this.editStoryRightTemplate({story: story[0], categories: categories, storyTypes: storyTypes});
      this.$el.html(this.editStoryTemplate({rightContent: editStoryRightTemplate2, story: story[0]}));
      $('.rightContent #tags').tagsInput({
        width: '100%',
        height: '100%'
      });
      $('#category').chosen({max_selected_options: 3});
      _.each(story[0].tags, function(tag){
          $('input[name=tags]').addTag(tag.tag);
      });
      return this.el;
    },

    editStory : function(){
      if(typeof storage.get('profile') == "object"){
        if($('#storyName').val() != ""){
          var options = {};
          options.story_name = $('#storyName').val();
          options.story_description = $('#storyDescription').val();
          options.story_type = $('#storyType').val();
          options.categories = $('#category').val();
          options.is_public = $('#isPublic').val();
          options.story_tags = $('input[name=tags]').val();
          options.story_name = $('#storyName').val();
          options.album = $('.album').attr('src');
          options.story_id = this.storyDetails.id;
          console.log(options)
          window.app.vent.trigger('editStory:sendEdit',options);
        }else{
          $('.step1Error').text('Story title must not be empty.');
          setTimeout(function(){
            $('.step1Error').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    editStoryDone : function(slug){
      console.log(slug);
      appRouter.navigate('/story/'+this.storyDetails.id+'/'+slug, {trigger: true});
    },

    updateAlbum : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        require(['text!templates/items/modal.html','text!templates/items/updateAvatar.html', 'js/croppic.min'], function(ModalTemplate, UpdateAvatarTemplate, Crop){
          var modalTemplate = _.template(ModalTemplate);
          var updateAvatarTemplate = _.template(UpdateAvatarTemplate);
          that.$el.append(modalTemplate());
          $('.modal').append(updateAvatarTemplate());
          $('.modalBox').css('width','350px').css('height','350px');
          var cropperOptions = {
            uploadUrl:'/api/uploadAlbumInitial',
            cropUrl:'/api/cropAlbumInitial',
            imgEyecandy:true,
            imgEyecandyOpacity:0,
            outputUrlId:'outputUrlId',
            customUploadButtonId:'updateAvatarButton',
            onAfterImgCrop: function(){
              var newAvatar = that.cropperHeader.croppedImg.valueOf().attr('src');
              
              $('img[class="album"]').attr('src',newAvatar);
              that.cropperHeader.destroy();
              $('.modalBox').remove();
              $('.modal').remove();
            }
          }
          //var crop = Crop;
          //var cropHead = new crop('uploadAvatar', cropperOptions);
          that.cropperHeader = new Croppic('uploadAvatar', cropperOptions);
          //cropperHeader.form.find('input[type="file"]').trigger('click');
        });
      }else{
        views.mainContentView.login();
      }
    }

  });

  return editStoryView;
});