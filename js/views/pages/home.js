define([
  'underscore',
  'backbone',
  'jquery',
  'text!templates/pages/home.html',
  'text!templates/items/homeRightContent.html',
  'text!templates/items/homeNewsfeed.html',
  'text!templates/items/repost.html'
], function( _, Backbone, $, HomeTemplate, HomeRightContentTemplate, HomeNewsfeedTemplate, RepostTemplate){
  var homeView = Backbone.View.extend({
    homeTemplate: _.template(HomeTemplate),
    homeRightContentTemplate : _.template(HomeRightContentTemplate),
    homeNewsfeedTemplate : _.template(HomeNewsfeedTemplate),
    repostTemplate: _.template(RepostTemplate),
    thisProfile: {},
    type: 'home',
    followerName: '',
    thisFeed: {},
    shareId: 0,
    sharePost: {},

    el : '.mainContent',

    events : {
      'keyup .newsfeedSearch' : 'searchNewsfeed',
      'click .newsfeedFilter .unfollow' : 'unfollowHome',
      'click .previousItems' : 'loadPrevious',
      'click .homeAll' : 'homeAll',
      'click .homeStatuses' : 'homeStatuses',
      'click .homeStoryUpdates': 'homeStoryUpdates',
      'click .homeChallengeUpdates' : 'homeChallengeUpdates',
      'click .homeMyFollowers' : 'homeMyFollowers',
      'click .whoIFollow li' : 'whoIFollow',
      'click .previousItemsFollower' : 'previousItemsFollower',
      'click .shareButton' : 'sharePost',
      'click .repostConfirm' : 'repostConfirm'
    },

    initialize: function(profile, feed, subscriptions, basicStats){
      window.app.vent.on('home:sendReplyStatusFailure', this.sendReplyStatusFailure,this);

      document.title = "Home";
      this.thisProfile = profile;
      this.thisFeed = feed;

      this.render(profile, feed, subscriptions, basicStats);
      if(typeof storage.get('moduleTags') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.searchModel.getAllTags();
      }else{
        views.mainContentView.renderTags(storage.get('moduleTags'));
      }
      if(typeof storage.get('modulePopular') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModulePopularStories('allTime');
      }else{
        views.mainContentView.renderPopularStories(storage.get('modulePopular'));
      }
      if(typeof storage.get('moduleRecent') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getModuleRecentStories('allTime');
      }else{
        views.mainContentView.renderRecentStories(storage.get('moduleRecent'));
      }
      if(typeof storage.get('moduleComments') == "undefined" || typeof storage.get('moduleLastChecked') == "undefined" || (new Date().getTime() - storage.get('moduleLastChecked'))/1000 >= 3600){
        views.mainContentView.models.storyModel.getRecentComments();
      }else{
        views.mainContentView.renderRecentComments(storage.get('moduleComments'));
      }
    },

    render: function(profile, feed, subscriptions, basicStats){
      //window.app.vent.trigger('site:removeDivs');
      $('.breadcrumbs #second').html("");
      $('.breadcrumbs #third').html("");
      $('.breadcrumbs #fourth').html("");
      $('.breadcrumbs #fifth').html("");
      var homeNewsfeedTemplate2 = this.homeNewsfeedTemplate({feed: feed});
      var homeRightContentTemplate2 = this.homeRightContentTemplate({newsfeed: homeNewsfeedTemplate2, to: window.storage.get('profile').id});
      this.$el.html(this.homeTemplate({subscriptions: subscriptions, rightContent : homeRightContentTemplate2, basicStats: basicStats}));
      Tipped.create(".likesPopup");
      $("#tabbed-nav").zozoTabs({
          position: "top-left",
          style: "clean",
          theme: "flat-alizarin",
          spaced: true,
          rounded: true,
          animation: {
              easing: "easeInOutExpo",
              duration: 600,
              effects: "slideH"
          },
          size:"large"
      });
      $('.z-tab').css('width','auto')
      return this.el;
    },

    searchNewsfeed : function(e){
      window.app.vent.trigger('home:searchNewsfeed',e.currentTarget);
    },

    unfollowHome : function(e){
      var id = $(e.currentTarget).attr('data-id');
      //remove div from list
      $('li[user="'+id+'"]').remove();
      if($('.whoIFollow li').length == 0){
        $('.whoIFollow').text('YOU ARE NOT SUBSCRIBED TO ANY AUTHOR OR STORY.');
      }
      //subtract one from subscriptions below the avatar and re-render that piece of html. Should probably do this in models
      var subscriptionsNumber = $('.homeSubscriptions').text().split(" ");
      subscriptionsNumber = subscriptionsNumber[0] - 1;
      $('.homeSubscriptions').text(subscriptionsNumber + " subscriptions");
      //remove unfollowed person's posts from wall, unless it's a post to you.
      $('.singlePost[by="'+id+'"][to="'+id+'"]').remove();
      //tell controller/model to unfollow this person
      window.app.vent.trigger('profile:unfollow',id);
    },

    loadPrevious : function(){
      var that = this;
      $('.previousItems').html('LOADING <i class="fa fa-spinner fa-spin"></i>');
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/"+window.storage.get('profile').pen_name+"/"+that.type+"/previous",
              dataType: "json",
              success: function(feed){
                var homeNewsfeedTemplate2 = that.homeNewsfeedTemplate({feed: feed});
                $('.postsList').append(homeNewsfeedTemplate2);
                $('.previousItemsFollower').remove();
                $('.previousItems').remove();
                if(feed.length == 10){
                  $('.rightContent').append('<button class="previousItems" style="width:100%;padding:10px;font-size:14px;">LOAD MORE UPDATES</button>');
                }
              }
      });
    },

    getAllActivities : function(){
      var that = this;
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/" + window.storage.get('profile').pen_name + "/"+that.type+"/all",
              dataType: "json",
              async: false,
              success: function(feed){
                console.log(feed)
                var homeNewsfeedTemplate2 = that.homeNewsfeedTemplate({feed: feed});
                if(feed.length){
                  $('.postsList').html(homeNewsfeedTemplate2);
                }else{
                  $('.postsList').html('<div style="margin-top:10px;margin-bottom:10px;">No updates found for this filter.</div>');
                }
                $('.previousItemsFollower').remove();
                $('.previousItems').remove();
                if(feed.length == 10){
                  $('.rightContent').append('<button class="previousItems" style="width:100%;padding:10px;font-size:14px;">LOAD MORE UPDATES</button>');
                }
              }
      });
    },

    homeAll : function(){
      this.type = "homeAll";
      this.getAllActivities();
    },

    homeStatuses : function(){
      this.type = "home";
      this.getAllActivities();
    },

    homeStoryUpdates : function(){
      this.type = "homeStoryUpdates";
      this.getAllActivities();
    },

    homeChallengeUpdates : function(){
      this.type = "homeChallengeUpdates";
      this.getAllActivities();
    },

    homeMyFollowers : function(){
      this.type = "homeMyFollowers";
      this.getAllActivities();
    },

    whoIFollow : function(e){
      var that = this;
      this.followerName = $(e.currentTarget).attr('user');
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/" + that.followerName + "/singleFollowerAll/all",
              dataType: "json",
              async: false,
              success: function(feed){
                var homeNewsfeedTemplate2 = that.homeNewsfeedTemplate({feed: feed});
                if(feed.length){
                  $('.postsList').html(homeNewsfeedTemplate2);
                }else{
                  $('.postsList').html('<div style="margin-top:10px;margin-bottom:10px;">No updates found for this filter.</div>');
                }
                $('.previousItemsFollower').remove();
                $('.previousItems').remove();
                if(feed.length == 10){
                  $('.rightContent').append('<button class="previousItemsFollower" style="width:100%;padding:10px;font-size:14px;">LOAD MORE UPDATES</button>');
                }
              }
      });
    },

    previousItemsFollower : function(){
      var that = this;
      $.ajax({
        type: 'GET',
        url: "/api/feed/getInitial/"+that.followerName+"/singleFollowerAll/previous",
              dataType: "json",
              async: false,
              success: function(feed){
                var homeNewsfeedTemplate2 = that.homeNewsfeedTemplate({feed: feed});
                $('.postsList').html(homeNewsfeedTemplate2);
                $('.previousItemsFollower').remove();
                $('.previousItems').remove();
                if(feed.length == 10){
                  $('.rightContent').append('<button class="previousItemsFollower" style="width:100%;padding:10px;font-size:14px;">LOAD MORE UPDATES</button>');
                }
              }
      });
    },

    sharePost : function(e){
      var shareid = $(e.currentTarget).attr('id');
      id = shareid.split('_');
      id = id[1];
      this.shareId = id;
      var that = this;
      _.each(this.thisFeed, function(feed){
        if(feed.id == id){
          console.log(feed);
          that.sharePost = feed;
          require(['text!templates/items/modal.html','text!templates/items/sharePost.html'], function(ModalTemplate, SharePostTemplate){
            var modalTemplate = _.template(ModalTemplate);
            var sharePostTemplate = _.template(SharePostTemplate);
            that.$el.append(modalTemplate());
            $('.modal').append(sharePostTemplate({post: feed}));
            var modalHeight = $('.modalBox .singlePost').height()+10;
            $('.modalBox').height(modalHeight);
          });
        }
      });
    },

    repostConfirm : function(){
      var repostText = $('#repostText').val();
      var postId = this.shareId;
      var options = {};
      options.repost_id = postId;
      options.share_text = repostText;
      var that = this;
      $('.modal').remove();
      var id = window.controllers.home.models.feedModel.repost(options);
      $.when(id).done(
        Backbone.history.loadUrl(Backbone.history.fragment)
      )
    }

  });

  return homeView;
});
