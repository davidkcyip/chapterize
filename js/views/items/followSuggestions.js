define([
  'underscore',
  'backbone',
  'text!templates/items/followSuggestions.html'
], function( _, Backbone, FollowSuggestionsTemplate){
  var followSuggestionsView = Backbone.View.extend({
    followSuggestionsTemplate: _.template(FollowSuggestionsTemplate),

    el : '#suggestions',

    events : {

    },

    initialize: function(suggestions){
      this.render(suggestions);
    },

    render: function(suggestions){
      this.$el.html(this.followSuggestionsTemplate({suggestions: suggestions}));
      return this.el;
    }
  });

  return followSuggestionsView;
});