define([
  'underscore',
  'backbone',
  'text!templates/items/activationSuccess.html',
  'text!templates/items/modal.html'
], function( _, Backbone, ActivationSuccessTemplate, ModalTemplate){
  var activationSuccessView = Backbone.View.extend({
    activationSuccessTemplate: _.template(ActivationSuccessTemplate),
    modalTemplate: _.template(ModalTemplate),

    el : '.mainContainer',

    events : {

    },

    initialize: function(){
      this.render();
    },

    render: function(){
      //window.app.vent.trigger('site:removeDivs');
      $('.mainContainer').append(this.modalTemplate());
      $('.modal').append(this.activationSuccessTemplate());
      $('.modalBox').css('height','85px');
      
      return this.el;
    }
  });

  return activationSuccessView;
});