define([
  'underscore',
  'backbone',
  'text!templates/items/viewTopicRight.html',
  'nicEdit'
], function( _, Backbone, ViewTopicRightTemplate, nicEdit ){
  var viewTopicRightView = Backbone.View.extend({
    viewTopicRightTemplate: _.template(ViewTopicRightTemplate),

    el : '.rightContent',
    circleDetails : {},
    topicResults : {},
    circle: {},
    editContent : '',
    panel : {},

    events : {
      'click .replyTopicButton' : 'replyTopic',
      'click .editReplyOpen' : 'editReplyOpen',
      'click .deleteReplyOpen' : 'deleteReplyOpen',
      'click .cancelEditReplyOpen' : 'cancelEditReplyOpen',
      'click .editCommentSubmit' : 'editCommentSubmit',
      'click .editTopicButton' : 'editTopicButton',
      'click .subscribeTopicButton' : 'subscribeTopic',
      'click .unsubscribeTopicButton' : 'unsubscribeTopic',
      'click .pinTopicButton' : 'pinTopic',
      'click .unpinTopicButton' : 'unpinTopic'
    },

    initialize: function(options, results, circle){
      this.circleDetails = options;
      this.topicResults = results;
      this.circle = circle
      this.render(results);
    },

    render: function(results){
      var that = this;
      $('meta[property="og:title"]').attr('content',this.circle[0].group_name + " - " + results[0].topic_name);
      $('meta[property="og:description"]').attr('content',results[0].post_content);
      that.$el.html(that.viewTopicRightTemplate({page: that.circleDetails.page, results: results, circle: this.circle}));
      nicEditors.allTextAreas();
      return this.el;
    },

    replyTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var reply_body = new nicEditors.findEditor('replyTopic').getContent();

        if(reply_body.length > 2){
          var options = {};
          options.reply = reply_body;
          options.topic_id = this.topicResults[0].topic_id;
          var comment_id = window.controllers.circleController.models.circleModel.replyTopic(options);
          //get current page
          var thisPage = this.circleDetails.page;
          var thisPagesPost = _.where(this.topicResults[0].posts, {page:thisPage});
          if(thisPagesPost.length < 10){
            require(['text!templates/items/replyTopicBlock.html'], function(ReplyTopicBlockTemplate){
              var replyTopicBlockTemplate = _.template(ReplyTopicBlockTemplate);
              var post = {};
              post.posted_at = new Date().getTime();
              post.avatar = storage.get('profile').avatar;
              post.pen_name = storage.get('profile').pen_name;
              post.real_name = storage.get('profile').real_name;
              post.author_id = storage.get('profile').id;
              post.post_id = comment_id;
              post.post_content = reply_body;
              $('.messageContainer').append(replyTopicBlockTemplate({post: post}));
            });
          }else{
            var page = parseInt(thisPage) + 1;
            appRouter.navigate("/circle/"+this.circleDetails.id+"/"+this.circleDetails.groupslug+"/topic/"+this.circleDetails.topicslug+"/"+page, {trigger: true});
          }
        }else{
          $('.replyTopicError').html('Your reply cannot be empty.');
          setTimeout( function(){
            $('.replyTopicError').html('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    editReplyOpen : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = $(e.currentTarget).attr('id');
        var parents = $(e.currentTarget).parents();
        var parent = parents[2];
        var content = $(parent).find('.messageContent').html().trim();
        this.editContent = content;
        $(e.currentTarget).text('Cancel').removeClass('editReplyOpen').addClass('cancelEditReplyOpen');
        $(parent).find('.messageContent').html('<textarea name="editComment_'+thisId+'" id="editComment_'+thisId+'">'+content+'</textarea> <button class="editCommentSubmit" id="'+thisId+'" style="margin-bottom:10px;">Edit Post</button>')
        this.panel = new nicEditor({fullPanel : true}).panelInstance('editComment_'+thisId);
      }else{
        views.mainContentView.login();
      }
    },

    cancelEditReplyOpen : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = $(e.currentTarget).attr('id');
        var parents = $(e.currentTarget).parents();
        var parent = parents[2];
        $(parent).find('.messageContent').html(this.editContent);
        $(e.currentTarget).text('Edit').addClass('editReplyOpen').removeClass('cancelEditReplyOpen');
        this.panel.removeInstance('editComment_'+thisId );
      }else{
        views.mainContentView.login();
      }
    },

    deleteReplyOpen : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = $(e.currentTarget).attr('id');
        var parents = $(e.currentTarget).parents();
        var parent = parents[2];
        var options = {};
        options.comment_id = thisId;
        window.controllers.circleController.models.circleModel.deleteComment(options);
        parent.remove();
      }else{
        views.mainContentView.login();
      }
    },

    editCommentSubmit : function(e){
      if(typeof storage.get('profile') == "object"){
        var thisId = $(e.currentTarget).attr('id');
        var parents = $(e.currentTarget).parents();
        var parent = parents[2];
        var editedComment = new nicEditors.findEditor('editComment_'+thisId).getContent();
        var options = {};
        options.comment_id = thisId;
        options.edited_comment = editedComment;
        window.controllers.circleController.models.circleModel.editComment(options);
        $(parent).find('.messageContent').html(editedComment);
        $(e.currentTarget).text('Edit').addClass('editReplyOpen').removeClass('cancelEditReplyOpen');
        this.panel.removeInstance('editComment_'+thisId );
      }else{
        views.mainContentView.login();
      }
    },

    editTopicButton : function(){
      appRouter.navigate("/circle/"+this.circleDetails.id+"/"+this.circleDetails.groupslug+"/topic/"+this.circleDetails.topicid+"/"+this.circleDetails.topicslug+"/edit", {trigger: true});
    },

    subscribeTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.topic_id = this.topicResults[0].topic_id;
        window.controllers.circleController.models.circleModel.subscribeTopic(options);
        $('.subscribeTopicButton').addClass('unsubscribeTopicButton').removeClass('subscribeTopicButton').html('<i class="fa fa-remove"></i> Unsubscribe to Topic');
      }else{
        views.mainContentView.login();
      }
    },

    unsubscribeTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.topic_id = this.topicResults[0].topic_id;
        window.controllers.circleController.models.circleModel.unsubscribeTopic(options);
        $('.unsubscribeTopicButton').addClass('subscribeTopicButton').removeClass('unsubscribeTopicButton').html('<i class="fa fa-plus"></i> Subscribe to Topic');
      }else{
        views.mainContentView.login();
      }
    },

    pinTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.topic_id = this.topicResults[0].topic_id;
        window.controllers.circleController.models.circleModel.pinTopic(options);
        $('.pinTopicButton').addClass('unpinTopicButton').removeClass('pinTopicButton').html('<i class="fa fa-thumb-tack"></i> Unpin Topic');
      }else{
        views.mainContentView.login();
      }
    },

    unpinTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.topic_id = this.topicResults[0].topic_id;
        window.controllers.circleController.models.circleModel.unpinTopic(options);
        $('.unpinTopicButton').addClass('pinTopicButton').removeClass('unpinTopicButton').html('<i class="fa fa-thumb-tack"></i> Pin Topic');
      }else{
        views.mainContentView.login();
      }
    }

  });

  return viewTopicRightView;
});