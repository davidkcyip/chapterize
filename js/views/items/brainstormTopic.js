define([
  'underscore',
  'backbone',
  'text!templates/items/brainstormTopic.html',
  'text!templates/items/brainstormTopicList.html'
], function( _, Backbone, BrainstormTopicTemplate, BrainstormTopicListTemplate){
  var brainstormTopicView = Backbone.View.extend({
    brainstormTopicTemplate: _.template(BrainstormTopicTemplate),
    brainstormTopicListTemplate: _.template(BrainstormTopicListTemplate),

    el : '.rightContent',
    thisStory: {},
    topicOptions:{},

    events : {
      'click #replyTopicButton' : 'replyTopic',
      'mouseover .subscribedToBrainstormTopic' : 'changeToUnsubscribe',
      'mouseout .unsubscribeToBrainstormTopic' : 'changeToSubscribed',
      'click .subscribeToBrainstormTopic' : 'subscribeToTopic',
      'click .unsubscribeToBrainstormTopic' : 'unsubscribeToTopic'
    },

    initialize: function(result,brainstorms,options){
      this.topicOptions = options;
      this.render(result,brainstorms);
    },

    render: function(result,brainstorms){
      this.$el.html(this.brainstormTopicTemplate({topic:result}));
      $('.leftContent').prepend(this.brainstormTopicListTemplate({brainstorms:brainstorms}));
      var nic = new nicEditor();
      nic.addInstance('replyTopic');
      return this.el;
    },

    replyTopic: function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        var message = new nicEditors.findEditor('replyTopic').getContent();
        if(message != null && message != "" && typeof message != "undefined"){
          var options = {};
          options.message = message;
          options.story_id = this.topicOptions.id;
          options.thread_id = this.topicOptions.thread_id;
          window.controllers.brainstormController.models.brainstormModel.replyTopic(options);
          Backbone.history.loadUrl(Backbone.history.fragment, true);
        }
      }else{
        views.mainContentView.login();
      }
    },

    changeToUnsubscribe : function(e){
      $(e.currentTarget).removeClass('subscribedToBrainstormTopic').addClass('unsubscribeToBrainstormTopic').html('<i class="fa fa-bookmark"></i> Unsubscribe');
    },

    changeToSubscribed : function(e){
      $(e.currentTarget).removeClass('unsubscribeToBrainstormTopic').addClass('subscribedToBrainstormTopic').html('<i class="fa fa-bookmark"></i> Subscribed');
    },

    subscribeToTopic : function(e){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.thread_id = this.topicOptions.thread_id;
        window.controllers.brainstormController.models.brainstormModel.subscribeTopic(options);
        $(e.currentTarget).removeClass('subscribeToBrainstormTopic').addClass('subscribedToBrainstormTopic').html('<i class="fa fa-bookmark"></i> Subscribed');
      }else{
        views.mainContentView.login();
      }
    },

    unsubscribeToTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.thread_id = this.topicOptions.thread_id;
        window.controllers.brainstormController.models.brainstormModel.unsubscribeTopic(options);
        $(e.currentTarget).removeClass('unsubscribeToBrainstormTopic').addClass('subscribeToBrainstormTopic').html('<i class="fa fa-bookmark"></i> Subscribe');
      }else{
        views.mainContentView.login();
      }
    }

  });

  return brainstormTopicView;
});