define([
  'underscore',
  'backbone',
  'text!templates/items/searchChapters.html',
  'bootpag'
], function( _, Backbone, SearchChaptersTemplate, bootpag){
  var searchChaptersView = Backbone.View.extend({
    searchChaptersTemplate: _.template(SearchChaptersTemplate),

    el : '.searchDiv',
    thisPeriod : 'allTime',

    events : {
      'change select#period' : 'periodChange',
    },

    initialize: function(results, page){
      var num_chapters = controllers.searchController.models.chapterModel.get('numberOfChapters');
      this.render(results, page, num_chapters);
    },

    render: function(results, page, num_chapters){
      var that = this;
      this.$el.html(that.searchChaptersTemplate({chapters: results, page: page, period: this.thisPeriod}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_chapters)/10),
           page: parseInt(page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.$el;
    },

    periodChange : function(){
      var period = $('#period').val();
      this.thisPeriod = period;
      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split( '/' );
      var last_segment = segment_array.pop();
      if(last_segment == "all"){
        window.controllers.searchController.models.chapterModel.getAllChapters(period);
      }else if(last_segment == "recent"){
        window.controllers.searchController.models.chapterModel.getRecentChapters(period);
      }else if(last_segment == "popular"){
        window.controllers.searchController.models.chapterModel.getPopularChapters(period);
      }
    }
  });

  return searchChaptersView;
});