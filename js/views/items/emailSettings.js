define([
  'underscore',
  'backbone',
  'text!templates/items/emailSettings.html',
], function( _, Backbone, EmailSettingsTemplate){
  var emailSettingsView = Backbone.View.extend({
    emailSettingsTemplate: _.template(EmailSettingsTemplate),

    el : '.rightContent',

    events : {
      'click .saveEmailSettings' : 'saveEmailSettings'
    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      this.$el.html(this.emailSettingsTemplate());
      this.postRender(results);
      return this.el;
    },

    postRender : function(results){
      _.each(results, function(result){
        $('input[value='+result.email_settings_type_id+']').attr('checked','checked');
      });
    },

    saveEmailSettings : function(){
      if(typeof storage.get('profile') == "object"){
        var checked = [];
        var that = this;
        _.each($('input[type=checkbox]:checked'), function(checkedBox){
          checked.push($(checkedBox).val());
        });
        var didItWork = window.controllers.settingsController.models.settingsModel.saveEmailSettings(checked);
        $.when(didItWork).done(that.saveEmailPost(didItWork));
      }else{
        views.mainContentView.login();
      }
    },

    saveEmailPost : function(didItWork){
      console.log(didItWork)
      if(didItWork == 1){
        $('.subscriptionError').html("Your email subscription preferences have been successfully saved.");
      }else{
        $('.subscriptionError').html("There was an error saving your email subscription preferences, please try again later.");
      } 
    }
  });

  return emailSettingsView;
});