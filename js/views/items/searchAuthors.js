define([
  'underscore',
  'backbone',
  'text!templates/items/searchAuthors.html'
], function( _, Backbone, SearchAuthorsTemplate){
  var searchAuthorsView = Backbone.View.extend({
    searchAuthorsTemplate: _.template(SearchAuthorsTemplate),

    el : '.searchDiv',
    thisPeriod : 'allTime',

    events : {
      'change select#period' : 'periodChange',
    },

    initialize: function(results, page){
      var num_authors = controllers.searchController.models.userModel.get('numberOfAuthors');
      console.log(num_authors)
      this.render(results, page, num_authors);
    },

    render: function(results, page, num_authors){
      var that = this;
      this.$el.html(that.searchAuthorsTemplate({authors: results, page: page, period: this.thisPeriod}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_authors)/10),
           page: parseInt(page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.$el;
    },

    periodChange : function(){
      var period = $('#period').val();
      this.thisPeriod = period;
      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split( '/' );
      var last_segment = segment_array.pop();
      if(last_segment == "all"){
        window.controllers.searchController.models.userModel.getAllAuthors(period);
      }else if(last_segment == "recent"){
        window.controllers.searchController.models.userModel.getRecentAuthors(period);
      }else if(last_segment == "popular"){
        window.controllers.searchController.models.userModel.getPopularAuthors(period);
      }
    }
  });

  return searchAuthorsView;
});