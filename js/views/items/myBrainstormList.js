define([
  'underscore',
  'backbone',
  'text!templates/items/myBrainstormList.html'
], function( _, Backbone, MyBrainstormListTemplate){
  var myBrainstormListView = Backbone.View.extend({
    myBrainstormListTemplate: _.template(MyBrainstormListTemplate),

    el : '.rightContent',
    thisStory: {},

    events : {
    },

    initialize: function(result){
      this.render(result);
    },

    render: function(result){
      this.$el.html(this.myBrainstormListTemplate({brainstorms:result}));
      return this.el;
    }

  });

  return myBrainstormListView;
});