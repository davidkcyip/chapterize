define([
  'underscore',
  'backbone',
  'text!templates/items/searchResults.html',
  'bootpag'
], function( _, Backbone, SearchResultsTemplate, bootpag){
  var searchResultsView = Backbone.View.extend({
    searchResultsTemplate: _.template(SearchResultsTemplate),

    el : '.searchDiv',
    thisPeriod : 'allTime',

    events : {
      'change select' : 'periodChange'
    },

    initialize: function(results, page,query){
      var num_results = controllers.searchController.models.searchModel.get('searchCount');
      if(query == null || typeof query == "undefined" || query == "" || ($('#searchInput').val() != "" && $('#searchInput').val() != query)){
        query = $('#searchInput').val();
      }
      this.render(results, page,query, num_results);
    },

    render: function(results, page,query, num_results){
      var that = this;
      this.$el.html(that.searchResultsTemplate({results: results, page: page, period: this.thisPeriod,query:query}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_results)/10),
           page: parseInt(page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.$el;
    },
    periodChange : function(){
      var period = $('#period').val();
      var category = $('#category').val();
      this.chosenCategory = $('#category').val();
      this.thisPeriod = period;
      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split( '/' );
      var last_segment = segment_array.pop();
      if(last_segment == "all"){
        window.controllers.searchController.models.storyModel.getAllStories(period, category);
      }else if(last_segment == "recent"){
        window.controllers.searchController.models.storyModel.getRecentStories(period, category);
      }else if(last_segment == "popular"){
        window.controllers.searchController.models.storyModel.getPopularStories(period, category);
      }
    }
  });

  return searchResultsView;
});