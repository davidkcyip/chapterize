define([
  'underscore',
  'backbone',
  'text!templates/items/newTopicRight.html',
  'nicEdit'
], function( _, Backbone, NewTopicRightTemplate, nicEdit ){
  var newTopicRightView = Backbone.View.extend({
    newTopicRightTemplate: _.template(NewTopicRightTemplate),

    el : '.rightContent',
    circleDetails : {},

    events : {
      'click #saveTopic' : 'saveTopic'
    },

    initialize: function(options){
      this.circleDetails = options;
      this.render();
    },

    render: function(){
      var that = this;
      that.$el.html(that.newTopicRightTemplate({type: "new"}));
      nicEditors.allTextAreas();
      return this.el;
    },

    saveTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var topic_name = $('#topicName').val();
        var topic_body = new nicEditors.findEditor('topicBody').getContent();
        var that = this;
        if(topic_name.length > 0 && topic_body.length > 2){
          var options = {};
          options.topic_name = topic_name;
          options.topic_body = topic_body;
          options.circle_id = this.circleDetails.id;
          var result = window.controllers.circleController.models.circleModel.newTopic(options);
          $.when(result).done(appRouter.navigate("/circle/"+that.circleDetails.id+"/"+that.circleDetails.groupslug+"/topic/"+result.id+"/"+result.slug, {trigger: true}));
        }else{
          $('.saveTopicError').html("Please fill in all fields.");
          setTimeout( function(){
            $('.saveTopicError').html('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    }

  });

  return newTopicRightView;
});