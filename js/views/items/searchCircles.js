define([
  'underscore',
  'backbone',
  'text!templates/items/searchCircles.html',
], function( _, Backbone, SearchCirclesTemplate){
  var searchCirclesView = Backbone.View.extend({
    searchCirclesTemplate: _.template(SearchCirclesTemplate),

    el : '.rightContent',
    circleOptions: {},

    events : {

    },

    initialize: function(results, options){
      this.circleOptions = options;
      this.render(results, options);
    },

    render: function(results, options){
      console.log(results)
      this.$el.html(this.searchCirclesTemplate({results:results, page: this.circleOptions.page, term: this.circleOptions.term}));
      
      return this.el;
    }

  });

  return searchCirclesView;
});