define([
  'underscore',
  'backbone',
  'text!templates/items/activationError.html',
  'text!templates/items/modal.html'
], function( _, Backbone, ActivationErrorTemplate, ModalTemplate){
  var activationErrorView = Backbone.View.extend({
    activationErrorTemplate: _.template(ActivationErrorTemplate),
    modalTemplate: _.template(ModalTemplate),

    el : '.mainContainer',

    events : {

    },

    initialize: function(){
      this.render();
    },

    render: function(){
      //window.app.vent.trigger('site:removeDivs');
      $('.mainContainer').append(this.modalTemplate());
      $('.modal').append(this.activationErrorTemplate());
      $('.modalBox').css('height','85px');
      
      return this.el;
    }
  });

  return activationErrorView;
});