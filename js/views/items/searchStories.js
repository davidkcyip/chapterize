define([
  'underscore',
  'backbone',
  'text!templates/items/searchStories.html',
  'bootpag'
], function( _, Backbone, SearchStoriesTemplate, bootpag){
  var searchStoriesView = Backbone.View.extend({
    searchStoriesTemplate: _.template(SearchStoriesTemplate),

    el : '.searchDiv',
    thisPeriod : 'allTime',
    chosenCategory : 1,

    events : {
      'change select' : 'periodChange'
    },

    initialize: function(results, page, categories){
      var num_stories = controllers.searchController.models.storyModel.get('numberOfStories');
      this.render(results, page, categories, num_stories);
    },

    render: function(results, page, categories, num_stories){
      var that = this;
      this.$el.html(that.searchStoriesTemplate({stories: results, page: page, period: this.thisPeriod,categories: categories, chosenCategory: that.chosenCategory}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_stories)/10),
           page: parseInt(page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.$el;
    },

    periodChange : function(){
      var period = $('#period').val();
      var category = $('#category').val();
      this.chosenCategory = $('#category').val();
      this.thisPeriod = period;
      var segment_str = window.location.pathname; // return segment1/segment2/segment3/segment4
      var segment_array = segment_str.split( '/' );
      var last_segment = segment_array.pop();
      if(last_segment == "all"){
        window.controllers.searchController.models.storyModel.getAllStories(period, category);
      }else if(last_segment == "recent"){
        window.controllers.searchController.models.storyModel.getRecentStories(period, category);
      }else if(last_segment == "popular"){
        window.controllers.searchController.models.storyModel.getPopularStories(period, category);
      }else{
        window.controllers.searchController.models.storyModel.getAllStories("allTime", category);
      }
    }
  });

  return searchStoriesView;
});