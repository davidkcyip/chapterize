define([
  'underscore',
  'backbone',
  'text!templates/items/composeMessage.html',
  'jqueryui',
  'js/jquery.tagsinput'
], function( _, Backbone, ComposeMessageTemplate, jqueryui, tagsinput){
  var composeMessageView = Backbone.View.extend({
    composeMessageTemplate: _.template(ComposeMessageTemplate),

    el : '.rightContent',

    events : {
      'click #sendNewMessageButton' : 'sendNewMessage'
    },

    initialize: function(){
      this.render();
    },

    render: function(){
      var that = this;
      that.$el.html(that.composeMessageTemplate());
      var nic = new nicEditor();
      nic.addInstance('newMessage');
      $('#messageRecipients').tagsInput({
        width: '100%',
        height: '100%',
        'defaultText':'search users',
        autocomplete_url:'/api/users/get-user'
      });

      return this.$el;
    },

    sendNewMessage : function(){
      var recipients = $('input[name=messageRecipients]').val();
      var message = new nicEditors.findEditor('newMessage').getContent();
      var title = $('#newMessageTitle').val();
      if(title != "" && message != "" && recipients != ""){
        var options = {};
        options.recipient = recipients;
        options.thread_title = title;
        options.message = message;
        var response = window.controllers.messagesController.models.messageModel.startNewMessageThread(options);
        $.when(response).done(appRouter.navigate("/messages/read/"+response.thread_id+"/"+response.slug, {trigger: true}))
      }else{
        $('.sendMessageError').text('Please fill in all fields.');
        setTimeout( function(){
          $('.sendMessageError').text('');
        },2000);
      }
    }

  });

  return composeMessageView;
});