define([
  'underscore',
  'backbone',
  'text!templates/items/newTopicRight.html'
], function( _, Backbone, EditTopicRightTemplate ){
  var editTopicRightView = Backbone.View.extend({
    editTopicRightTemplate: _.template(EditTopicRightTemplate),

    el : '.rightContent',
    circleDetails : {},

    events : {
      'click #saveTopic' : 'saveTopic'
    },

    initialize: function(options, results){
      this.circleDetails = options;
      this.render(results);
    },

    render: function(results){
      var that = this;
      that.$el.html(that.editTopicRightTemplate({type: "edit", topic: results}));
      return this.el;
    },

    saveTopic : function(){
      if(typeof storage.get('profile') == "object"){
        var topic_name = $('#topicName').val();
        var that = this;
        if(topic_name.length > 0){
          var options = {};
          options.topic_name = topic_name;
          options.circle_id = this.circleDetails.id;
          options.old_topic_id = this.circleDetails.topicid;
          var result = window.controllers.circleController.models.circleModel.editTopic(options);
          $.when(result).done(appRouter.navigate("/circle/"+that.circleDetails.id+"/"+that.circleDetails.groupslug+"/topic/"+that.circleDetails.topicid+"/"+result.slug, {trigger: true}));
        }else{
          $('.saveTopicError').html("Please fill in all fields.");
          setTimeout( function(){
            $('.saveTopicError').html('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    }

  });

  return editTopicRightView;
});