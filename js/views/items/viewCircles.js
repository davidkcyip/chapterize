define([
  'underscore',
  'backbone',
  'text!templates/items/viewCircles.html',
], function( _, Backbone, ViewCirclesTemplate){
  var viewCirclesView = Backbone.View.extend({
    viewCirclesTemplate: _.template(ViewCirclesTemplate),

    el : '.rightContent',
    circleOptions : {},

    events : {
      'change select' : 'searchCircles'
    },

    initialize: function(results, circleOptions){
      this.circleOptions = circleOptions;
      this.render(results, circleOptions);
    },

    render: function(results, circleOptions){
      var that = this;
      console.log(this.circleOptions)
      if(this.circleOptions.type == "overview"){
        var browseCirclesOfficial = results.get('browseCirclesOfficial');
        var browseCirclesActive = results.get('browseCirclesActive');
        var browseCirclesPopular = results.get('browseCirclesPopular');
      }else{
        var browseCircles = results.get('browseCircles');
      }
      setTimeout( function(){
        var categories = results.get('categories');
      if(that.circleOptions.page != null){
        var page = that.circleOptions.page;
      }else{
        var page = 1;
      }
      if(that.circleOptions.type != null){
        var type = that.circleOptions.type;
      }else{
        var type = "overview";
      }
      if(that.circleOptions.order != null){
        var order = that.circleOptions.order;
      }else{
        var order = "active";
      }
      if(type == "overview"){
        that.$el.html(that.viewCirclesTemplate({order: order, type: type, categories: categories, page: page, browseCirclesOfficial: browseCirclesOfficial, browseCirclesActive: browseCirclesActive, browseCirclesPopular: browseCirclesPopular}))
      }else{
        that.$el.html(that.viewCirclesTemplate({order: order, type: type, categories: categories, page: page, browseCircles: browseCircles}))
      }
      $('#circleType').val(that.circleOptions.type);

      return that.el;
      },100);
    },

    searchCircles : function(){
      var circleType = $('#circleType').val();
      var orderType = $('#orderType').val();
      this.circleOptions.type = circleType;
      this.circleOptions.order = orderType;
      controllers.circleController.models.circleModel.browseCircles(this.circleOptions);
    }

  });

  return viewCirclesView;
});