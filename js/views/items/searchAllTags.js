define([
  'underscore',
  'backbone',
  'text!templates/items/searchAllTags.html'
], function( _, Backbone, SearchAllTagsTemplate){
  var searchAllTagsView = Backbone.View.extend({
    searchAllTagsTemplate: _.template(SearchAllTagsTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      this.$el.html(this.searchAllTagsTemplate({results: results}));
      return this.el;
    }

  });

  return searchAllTagsView;
});