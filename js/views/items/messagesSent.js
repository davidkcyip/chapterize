define([
  'underscore',
  'backbone',
  'text!templates/items/messagesSent.html'
], function( _, Backbone, MessagesSentTemplate){
  var messagesSentView = Backbone.View.extend({
    messagesSentTemplate: _.template(MessagesSentTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      var that = this;
      this.$el.html(that.messagesSentTemplate({messages: results}));
      Tipped.create(".popup");
      return this.$el;
    }

  });

  return messagesSentView;
});