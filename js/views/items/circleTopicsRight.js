define([
  'underscore',
  'backbone',
  'text!templates/items/circleTopicsRight.html',
  'nicEdit'
], function( _, Backbone, CircleTopicsRightTemplate, nicEdit ){
  var circleTopicsRightView = Backbone.View.extend({
    circleTopicsRightTemplate: _.template(CircleTopicsRightTemplate),

    el : '.rightContent',
    circleDetails : {},

    events : {
      'click .editCircleButton' : 'editCircle',
      'click .newTopicButton' : 'newTopic',
      'click .joinCircleButton' : 'joinCircle',
      'click .unjoinCircleButton' : 'unjoinCircle',
      'click .requestJoinCircleButton' : 'requestJoinCircle',
      'click .unrequestJoinCircleButton' : 'unrequestJoinCircle',
      'click .manageMembersButton' : 'manageMembers'
    },

    initialize: function(options, results){
      this.circleDetails = options;
      this.topicResults = results;
      this.render(results);
    },

    render: function(results){
      var that = this;
      $('meta[property="og:title"]').attr('content',"Bloqly - " + results[0].group_name);
      $('meta[property="og:description"]').attr('content',results[0].group_description);
      that.$el.html(that.circleTopicsRightTemplate({page: this.circleDetails.page, results: results}));
      nicEditors.allTextAreas();
      return this.el;
    },

    editCircle : function(){
      appRouter.navigate("/circle/"+this.circleDetails.id+"/"+this.circleDetails.groupslug+"/edit", {trigger: true});
    },

    newTopic : function(){
      appRouter.navigate("/circle/"+this.circleDetails.id+"/"+this.circleDetails.groupslug+"/new-topic", {trigger: true});
    },

    joinCircle : function(){
      if(typeof storage.get('profile') == "object"){
        controllers.circleController.models.circleModel.joinCircle(this.circleDetails.id);
        $('.joinCircleButton').addClass('unjoinCircleButton').removeClass('joinCircleButton').html('<i class="fa fa-remove"></i> Leave Circle');
      }else{
        views.mainContentView.login();
      }
    },

    unjoinCircle : function(){
      if(typeof storage.get('profile') == "object"){
        controllers.circleController.models.circleModel.unjoinCircle(this.circleDetails.id);
        $('.unjoinCircleButton').addClass('joinCircleButton').removeClass('unjoinCircleButton').html('<i class="fa fa-plus"></i> Join Circle');
      }else{
        views.mainContentView.login();
      }
    },

    requestJoinCircle : function(){
      if(typeof storage.get('profile') == "object"){
        controllers.circleController.models.circleModel.requestJoinCircle(this.circleDetails.id);
        $('.requestJoinCircleButton').addClass('unrequestJoinCircleButton').removeClass('requestJoinCircleButton').html('<i class="fa fa-remove"></i> Unrequest to Join Circle');
      }else{
        views.mainContentView.login();
      }
    },

    unrequestJoinCircle : function(){
      if(typeof storage.get('profile') == "object"){
        controllers.circleController.models.circleModel.unrequestJoinCircle(this.circleDetails.id);
        $('.unrequestJoinCircleButton').addClass('requestJoinCircleButton').removeClass('unrequestJoinCircleButton').html('<i class="fa fa-plus"></i> Request to Join Circle');
      }else{
        views.mainContentView.login();
      }
    },

    manageMembers : function(){
      appRouter.navigate("/circle/"+this.circleDetails.id+"/"+this.circleDetails.groupslug+"/manage-users", {trigger: true});
    }

  });

  return circleTopicsRightView;
});