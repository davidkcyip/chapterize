define([
  'underscore',
  'backbone',
  'text!templates/items/subscriptionSettings.html',
], function( _, Backbone, SubscriptionSettingsTemplate){
  var subscriptionSettingsView = Backbone.View.extend({
    subscriptionSettingsTemplate: _.template(SubscriptionSettingsTemplate),

    el : '.rightContent',

    events : {
      
    },

    initialize: function(){
      this.render();
    },

    render: function(){
      this.$el.html(this.subscriptionSettingsTemplate());
      return this.el;
    }
  });

  return subscriptionSettingsView;
});