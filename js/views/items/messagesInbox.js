define([
  'underscore',
  'backbone',
  'text!templates/items/messagesInbox.html'
], function( _, Backbone, MessagesInboxTemplate){
  var messagesInboxView = Backbone.View.extend({
    messagesInboxTemplate: _.template(MessagesInboxTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      var that = this;
      this.$el.html(that.messagesInboxTemplate({messages: results}));
      Tipped.create(".popup");
      return this.$el;
    }

  });

  return messagesInboxView;
});