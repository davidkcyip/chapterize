define([
  'underscore',
  'backbone',
  'text!templates/items/newCircle.html'
], function( _, Backbone, NewCircleTemplate ){
  var newCircleView = Backbone.View.extend({
    newCircleTemplate: _.template(NewCircleTemplate),

    el : '.rightContent',
    circleOptions: {},

    events: {
      'click #saveCircle' : 'saveCircle'
    },

    initialize: function(options){
      this.circleOptions = options;
      this.render(options);
    },

    render: function(){
      this.$el.html(this.newCircleTemplate({type: 'new'}));
      //this.delegateEvents();
      return this.el;
    },

    saveCircle : function(){
      if(typeof storage.get('profile') == "object"){
        var circleName = $('#circleName').val();
        var circleDescription = $('#circleDescription').val();
        var circleType = $('#circleType').val();
        var circleCategory = $('#circleCategory').val();
        var circleAlbum = $('.album').attr('src');

        if(circleName != null && circleName != "" && circleDescription != null && circleDescription != "" && circleType != null && circleType != "" && circleCategory != null && circleCategory != ""){
          var options = {};
          options.circle_name = circleName;
          options.circle_description = circleDescription;
          options.circle_type = circleType;
          options.circle_album = circleAlbum;
          options.circle_category = circleCategory;
          var results = window.controllers.circleController.models.circleModel.newCircle(options);
          $.when(results).done(appRouter.navigate("/circle/"+results.circle_id+"/"+results.slug, {trigger: true}));
        }else{
          $('.saveCircleError').text('Please fill in all fields.');
          setTimeout( function(){
            $('.saveCircleError').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

  });

  return newCircleView;
});