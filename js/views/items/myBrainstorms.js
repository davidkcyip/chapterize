define([
  'underscore',
  'backbone',
  'text!templates/items/myBrainstorms.html'
], function( _, Backbone, MyBrainstormsTemplate){
  var myBrainstormsView = Backbone.View.extend({
    myBrainstormsTemplate: _.template(MyBrainstormsTemplate),

    el : '.myBrainstorms',
    thisStory: {},

    events : {
    },

    initialize: function(result){
      this.render(result);
    },

    render: function(result){
      this.$el.html(this.myBrainstormsTemplate({brainstorms:result}));
      return this.el;
    }

  });

  return myBrainstormsView;
});