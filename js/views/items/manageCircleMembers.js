define([
  'underscore',
  'backbone',
  'text!templates/items/manageCircleMembers.html'
], function( _, Backbone, ManageCircleMembersTemplate){
  var manageCircleMembersView = Backbone.View.extend({
    manageCircleMembersTemplate: _.template(ManageCircleMembersTemplate),

    el : '.rightContent',
    circleMembers: {},
    circleOptions: {},

    events : {
      'click .makeMemberAdmin' : 'makeAdmin',
      'click .revokeMemberAdmin' : 'revokeAdmin',
      'click .approveMember' : 'approveMember',
      'click .removeMember' : 'removeMember'
    },

    initialize: function(results, options){
      this.circleMembers = results;
      this.circleOptions = options;
      this.render(results, options);
    },

    render: function(results, options){
      var that = this;
      that.$el.html(that.manageCircleMembersTemplate({results: results, options: options}));
      return this.el;
    },

    makeAdmin : function(e){
      if(typeof storage.get('profile') == "object"){
        var author_id = $(e.currentTarget).attr('id');
        var circle_id = this.circleOptions.id;
        var parents = $(e.currentTarget).parents();
        var options = {};
        options.author_id = author_id;
        options.circle_id = circle_id;
        controllers.circleController.models.circleModel.makeAdmin(options);
        $(parents[2]).find('#memberStatus').html('● Admin');
        $(e.currentTarget).addClass('revokeMemberAdmin').removeClass('makeMemberAdmin').text('Revoke Admin');
      }else{
        view.mainContentView.login();
      }
    },

    revokeAdmin : function(e){
      if(typeof storage.get('profile') == "object"){
        var author_id = $(e.currentTarget).attr('id');
        var circle_id = this.circleOptions.id;
        var parents = $(e.currentTarget).parents();
        var options = {};
        options.author_id = author_id;
        options.circle_id = circle_id;
        controllers.circleController.models.circleModel.revokeAdmin(options);
        $(parents[2]).find('#memberStatus').html('● Member');
        $(e.currentTarget).addClass('makeMemberAdmin').removeClass('revokeMemberAdmin').text('Make Admin');
      }else{
        views.mainContentView.login();
      }
    },

    approveMember : function(e){
      if(typeof storage.get('profile') == "object"){
        var author_id = $(e.currentTarget).attr('id');
        var circle_id = this.circleOptions.id;
        var parents = $(e.currentTarget).parents();
        var options = {};
        options.author_id = author_id;
        options.circle_id = circle_id;
        controllers.circleController.models.circleModel.approveMember(options);
        $(parents[2]).find('#memberStatus').html('● Member');
        $(parents[2]).find('#adminFunctions').html('<a href="#" class="makeMemberAdmin" style="color:#33163f;vertical-align:top;" id="'+author_id+'">Make Admin</a> | <a href="#" class="removeMember" style="color:#33163f;vertical-align:top;" id="'+author_id+'">Remove Author</a>');
        var timeago = $(parents[2]).find('#timeagoJoined').html();
        timeago = timeago.replace('Requested access','Joined');
        $(parents[2]).find('#timeagoJoined').html(timeago);
      }else{
        views.mainContentView.login();
      }
    },

    removeMember : function(e){
      if(typeof storage.get('profile') == "object"){
        var author_id = $(e.currentTarget).attr('id');
        var circle_id = this.circleOptions.id;
        var options = {};
        options.author_id = author_id;
        options.circle_id = circle_id;
        controllers.circleController.models.circleModel.removeMember(options);
        var parents = $(e.currentTarget).parents();
        $(parents[2]).remove();
      }else{
        views.mainContentView.login();
      }
    },

  });

  return manageCircleMembersView;
});