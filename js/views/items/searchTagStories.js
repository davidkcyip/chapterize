define([
  'underscore',
  'backbone',
  'text!templates/items/searchTagStories.html',
  'bootpag'
], function( _, Backbone, SearchTagStoriesTemplate, bootpag){
  var searchTagStoriesView = Backbone.View.extend({
    searchTagStoriesTemplate: _.template(SearchTagStoriesTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(results, page){
      var num_stories = controllers.searchController.models.searchModel.get('searchTagStoriesNumberOfStories');
      this.render(results, page, num_stories);
    },

    render: function(results, page, num_stories){
      this.$el.html(this.searchTagStoriesTemplate({results: results, page: page}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_stories)/10),
           page: parseInt(page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.el;
    }

  });

  return searchTagStoriesView;
});