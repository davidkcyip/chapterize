define([
  'underscore',
  'backbone',
  'text!templates/items/readMessage.html',
  'nicEdit'
], function( _, Backbone, ReadMessageTemplate, nicEdit ){
  var readMessageView = Backbone.View.extend({
    readMessageTemplate: _.template(ReadMessageTemplate),

    el : '.rightContent',
    thread_id: 0,
    slug:'',

    events : {
      'click #replyMessageButton' : 'sendMessage'
    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      var that = this;
      this.thread_id = results[0].thread_id;
      this.slug = results[0].thread_slug;
      that.$el.html(that.readMessageTemplate({messages: results}));
      window.controllers.mainContentController.checkUnreadMessages();
      Tipped.create(".popup");
      var nic = new nicEditor();
      nic.addInstance('replyMessage');
      return this.$el;
    },

    sendMessage : function(){
      var message = new nicEditors.findEditor('replyMessage').getContent();
      console.log(message)
      if(message != null && message != "" && typeof message != "undefined"){
        var options = {};
        options.message = message;
        options.thread_id = this.thread_id;
        window.controllers.messagesController.models.messageModel.startNewMessage(options);
        options.slug = this.slug;
        options.id = this.thread_id;
        window.controllers.messagesController.models.messageModel.getMessage(options);
      }
    }

  });

  return readMessageView;
});