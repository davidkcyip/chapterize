define([
  'underscore',
  'backbone'
], function( _, Backbone ){
  var userFormView = Backbone.View.extend({
    userFormTemplate: '',

    el : '.mainContent .rightContent',

    events : {
      'change select[data-type=date]' : 'test'
    },

    initialize: function(userForm){
      //this.el = $('.mainContainer');
      this.userFormTemplate = _.template(userForm);
      this.render(this.userFormTemplate);
    },

    render: function(){
      //window.app.vent.trigger('site:removeDivs');
      this.$el.append(this.userFormTemplate());
      $('div[data-fieldsets]').find('#c3_birthday').datepicker();
      return this.el;
    },

    test : function(el){
      //window.app.vent.trigger('test',{thisEl: '.test', test:'6'});
      //var thisValue = window.app.getValue(el);
      //console.log(thisValue);
    }

  });

  return userFormView;
});