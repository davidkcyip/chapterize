define([
  'underscore',
  'backbone',
  'text!templates/items/newCircle.html'
], function( _, Backbone, EditCircleTemplate ){
  var editCircleView = Backbone.View.extend({
    editCircleTemplate: _.template(EditCircleTemplate),

    el : '.rightContent',
    circleOptions: {},

    events: {
      'click #saveCircle' : 'saveCircle'
    },

    initialize: function(options, results){
      this.circleOptions = options;
      this.render(options, results);
    },

    render: function(options, results){
      this.circleOptions = options;
      this.$el.html(this.editCircleTemplate({circle: results.get('circleDetails'), type: 'edit'}));
      $('.album').attr('src',results.get('circleDetails')[0].group_cover);
      setTimeout( function(){
        $('#circleCategory').val(results.get('circleDetails')[0].group_category);
      },1);
      return this.el;
    },

    saveCircle : function(){
      if(typeof storage.get('profile') == "object"){
        var circleName = $('#circleName').val();
        var circleDescription = $('#circleDescription').val();
        var circleType = $('#circleType').val();
        var circleCategory = $('#circleCategory').val();
        var circleAlbum = $('.album').attr('src');

        if(circleName != null && circleName != "" && circleDescription != null && circleDescription != "" && circleType != null && circleType != ""){
          var options = {};
          options.circle_name = circleName;
          options.circle_description = circleDescription;
          options.circle_type = circleType;
          options.circle_album = circleAlbum;
          options.circle_id = this.circleOptions.id;
          options.circle_category = circleCategory;
          var results = window.controllers.circleController.models.circleModel.editCircle(options);
          $.when(results).done(appRouter.navigate("/circle/"+results.circle_id+"/"+results.slug+'/topics', {trigger: true}));
        }else{
          $('.saveCircleError').text('Please fill in all fields.');
          setTimeout( function(){
            $('.saveCircleError').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    updateAlbum : function(){
      if(typeof storage.get('profile') == "object"){
        var that = this;
        require(['text!templates/items/modal.html','text!templates/items/updateAvatar.html', 'js/croppic.min'], function(ModalTemplate, UpdateAvatarTemplate, Crop){
          var modalTemplate = _.template(ModalTemplate);
          var updateAvatarTemplate = _.template(UpdateAvatarTemplate);
          that.$el.append(modalTemplate());
          $('.modal').append(updateAvatarTemplate());
          $('.modalBox').css('width','350px').css('height','350px');
          var cropperOptions = {
            uploadUrl:'/api/uploadAlbumInitial',
            cropUrl:'/api/cropAlbumInitial',
            imgEyecandy:true,
            imgEyecandyOpacity:0,
            outputUrlId:'outputUrlId',
            customUploadButtonId:'updateAvatarButton',
            onAfterImgCrop: function(){
              var newAvatar = that.cropperHeader.croppedImg.valueOf().attr('src');
              
              $('img[class="album"]').attr('src',newAvatar);
              that.cropperHeader.destroy();
              $('.modalBox').remove();
              $('.modal').remove();
            }
          }
          //var crop = Crop;
          //var cropHead = new crop('uploadAvatar', cropperOptions);
          that.cropperHeader = new Croppic('uploadAvatar', cropperOptions);
          //cropperHeader.form.find('input[type="file"]').trigger('click');
        });
      }else{
        views.mainContentView.login();
      }
    }

  });

  return editCircleView;
});