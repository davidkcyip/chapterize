define([
  'underscore',
  'backbone',
  'text!templates/items/newBrainstormTopic.html'
], function( _, Backbone, NewBrainstormTopicTemplate, ModalTemplate){
  var newBrainstormTopicView = Backbone.View.extend({
    newBrainstormTopicTemplate: _.template(NewBrainstormTopicTemplate),

    el : '.rightContent',
    thisStory: {},

    events : {
      'click #sendNewTopicButton' : 'sendNewtopic'
    },

    initialize: function(result, story){
      this.thisStory = story;
      this.render(result,story);
    },

    render: function(result, story){
      this.$el.html(this.newBrainstormTopicTemplate({categories:result,story:story}));
      var nic = new nicEditor();
      nic.addInstance('newTopic');
      return this.el;
    },

    sendNewtopic : function(){
      if(typeof storage.get('profile') == "object"){
        var message = new nicEditors.findEditor('newTopic').getContent();
        var title = $('#newTopicTitle').val();
        var category = $('#newTopicCategory').val();
        if(title != "" && message != "" && category != ""){
          var options = {};
          options.category = category;
          options.title = title;
          options.message = message;
          options.story_id = this.thisStory[0].id;
          var response = window.controllers.brainstormController.models.brainstormModel.startNewTopic(options);
          //$.when(response).done(console.log(response));
          $.when(response).done(appRouter.navigate("/brainstorm/"+ this.thisStory[0].id +"/"+this.thisStory[0].slug+"/read/"+response.thread_id+'/'+response.slug, {trigger: true}))
        }else{
          $('.sendTopicError').text('Please fill in all fields.');
          setTimeout( function(){
            $('.sendTopicError').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    }

  });

  return newBrainstormTopicView;
});