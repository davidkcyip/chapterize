define([
  'underscore',
  'backbone',
  'text!templates/items/verify.html',
  'text!templates/items/modal.html'
], function( _, Backbone, VerifyTemplate, ModalTemplate){
  var verifyView = Backbone.View.extend({
    verifyTemplate: _.template(VerifyTemplate),
    modalTemplate: _.template(ModalTemplate),

    el : 'body',

    events : {
      'click #submitPennamePassword' : 'verifyForm'
    },

    initialize: function(){
      this.render();
    },

    render: function(){
      //window.app.vent.trigger('site:removeDivs');
      $('.mainContent').append(this.modalTemplate);
      $('.modal').append(this.verifyTemplate);
      $('.modalBox').css('height','310px');
      
      return this.el;
    },

    verifyForm : function(){
      window.app.vent.trigger('verify:verifyForm');
    }
  });

  return verifyView;
});