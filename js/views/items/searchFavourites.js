define([
  'underscore',
  'backbone',
  'text!templates/items/searchBookmarkFavourites.html'
], function( _, Backbone, SearchBookmarkFavouritesTemplate ){
  var searchFavouritesView = Backbone.View.extend({
    searchFavouritesTemplate: _.template(SearchBookmarkFavouritesTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(favourites){
      this.render(favourites);
    },

    render: function(favourites){
      //window.app.vent.trigger('site:removeDivs');
      this.$el.html(this.searchFavouritesTemplate({results:favourites,type:"favourites"}));
      
      return this.el;
    }
  });

  return searchFavouritesView;
});