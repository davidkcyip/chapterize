define([
  'underscore',
  'backbone',
  'text!templates/items/searchBookmarkFavourites.html'
], function( _, Backbone, SearchBookmarkFavouritesTemplate ){
  var searchBookmarksView = Backbone.View.extend({
    searchBookmarksTemplate: _.template(SearchBookmarkFavouritesTemplate),

    el : '.rightContent',

    events : {

    },

    initialize: function(bookmarks){
      this.render(bookmarks);
    },

    render: function(bookmarks){
      //window.app.vent.trigger('site:removeDivs');
      this.$el.html(this.searchBookmarksTemplate({results:bookmarks,type:"bookmarks"}));
      
      return this.el;
    }
  });

  return searchBookmarksView;
});