define([
  'underscore',
  'backbone',
  'text!templates/items/accountSettings.html',
], function( _, Backbone, AccountSettingsTemplate){
  var accountSettingsView = Backbone.View.extend({
    accountSettingsTemplate: _.template(AccountSettingsTemplate),

    el : '.rightContent',
    error: null,

    events : {
      'keyup #email' : 'checkEmail',
      'keyup #password1' : 'checkPasswords',
      'keyup #password2' : 'checkPasswords',
      'click .changeEmailSubmit' : 'changeEmail',
      'click .changePasswordSubmit' : 'changePassword'
    },

    initialize: function(){
      this.render();
    },

    render: function(){
      this.$el.html(this.accountSettingsTemplate());
      return this.el;
    },

    checkEmail : function(){
      var email = $('#email').val();
      if(typeof email != "undefined"){
      var atCountRegex = new RegExp("@", "g");
      var dotCountRegex = new RegExp("\\.", "g");
      var atCount = email.toString().match(atCountRegex) ? email.toString().match(atCountRegex).length : 0;
      var dotCount = email.toString().match(dotCountRegex) ? email.toString().match(dotCountRegex).length : 0;

      if(email != "" && atCount == 1 && dotCount == 1){
        $.getJSON('/api/verify/email/'+email, function(data){
          if(data.error == 0){
            $('#email').addClass('valid').removeClass('invalid');
          }else if(data.error == 1){
            $('#email').addClass('invalid').removeClass('valid');
            var error = 'Your email is already being used';
            return error;
          }
        });
      }else{
        $('#email').removeClass('invalid').removeClass('valid');
        var error = 'You have not entered a valid email.';
      }
      }
      return error;
    },

    checkPasswords : function(){
      this.error = null;
      var oldPassword = $("#oldPassword").val();
      var password1 = $("#password1").val();
      var password2 = $("#password2").val();

      if(oldPassword == ""){
        $("#oldPassword").removeClass("valid").addClass("invalid");
        var error = 'Please enter your old password.';
      }else if(!(password1 === password2) && password1 != "" && password2 != ""){
        $("#password1").removeClass("valid");
        $("#password2").removeClass("valid").addClass("invalid");
        var error = 'Your passwords do not match.';
      }else if((password1 === password2) && password1 != "" && password2 != ""){
        $("#password1").removeClass("invalid").addClass("valid");
        $("#password2").removeClass("invalid").addClass("valid");
      }else{
        $("#oldPassword").removeClass("invalid");
        $("#password1").removeClass("valid").addClass("invalid");
        $("#password2").removeClass("valid").addClass("invalid");
        var error = 'Please enter your new password.';
      }
      return error;
    },

    changeEmail : function(){
      var email = $('#email').val();
      $('.emailError').html('');
      var error = this.checkEmail();
      console.log(error)
      if(error == null){
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: '/api/user/email/change',
          data: {"email": email},
          success: function(data){
            if(data.success == 1){
              var profile = window.storage.get('profile');
              profile.email = email;
              window.storage.set('profile',profile);
              $('.emailError').html("Your email has been successfully changed.").removeClass("emailSuccess").addClass("emailError");
            }else{  
              $('.emailError').html("There was an error changing your email, please try again later.").removeClass("emailError").addClass("emailSuccess");
            }
          }
        });
      }else{
        $('.emailError').html(error);
      }
    },

    changePassword : function(){
      var oldPassword = $("#oldPassword").val();
      var newPassword = $("#password1").val();
      var error = this.checkPasswords();
      console.log(error)
      if(error == null){
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: '/api/user/password/change',
          data: {"oldPassword": oldPassword, "newPassword": newPassword},
          success: function(data){
            if(data.success == 1){
              $('.passwordError').html(data.errorMessage).removeClass("passwordSuccess").addClass("passwordError");
            }else{
              $('.passwordError').html(data.errorMessage).removeClass("passwordError").addClass("passwordSuccess");
            }
          }
        });
      }else{
        $('.passwordError').html(error);
      }
    }

  });

  return accountSettingsView;
});