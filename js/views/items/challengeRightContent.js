define([
  'underscore',
  'backbone',
  'text!templates/items/newChallenge.html',
  'text!templates/items/activeChallenges.html',
  'text!templates/items/previousChallenges.html',
  'text!templates/items/myChallenges.html',
  'text!templates/items/challengeWinners.html',
  'text!templates/items/readChallenge.html',
  'text!templates/items/submitChallenge.html',
  'text!templates/items/singleChallengeComment.html',
  'text!templates/items/challengeLeaderboard.html',
  'jqueryui',
  'js/jquery.tagsinput',
  'chosen',
  'nicEdit',
  'bootpag'
], function( _, Backbone, NewChallengeTemplate, ActiveChallengesTemplate, PreviousChallengesTemplate, MyChallengesTemplate, ChallengeWinnersTemplate, ReadChallengeTemplate, SubmitChallengeTemplate, SingleChallengeCommentTemplate, ChallengeLeaderboardTemplate, jqueryui, tagsinput, chosen, nicEdit, bootpag ){
  var challengeRightContentView = Backbone.View.extend({
    newChallengeTemplate: _.template(NewChallengeTemplate),
    activeChallengesTemplate: _.template(ActiveChallengesTemplate),
    previousChallengesTemplate: _.template(PreviousChallengesTemplate),
    myChallengesTemplate: _.template(MyChallengesTemplate),
    challengeWinnersTemplate: _.template(ChallengeWinnersTemplate),
    readChallengeTemplate: _.template(ReadChallengeTemplate),
    submitChallengeTemplate: _.template(SubmitChallengeTemplate),
    singleChallengeCommentTemplate: _.template(SingleChallengeCommentTemplate),
    challengeLeaderboardTemplate: _.template(ChallengeLeaderboardTemplate),
    challengeOptions: {},
    thisAction : "",
    challengeDetails: {},

    el : '.rightContent',
    filter: 'recent',
    leaderboardFilter: '7days',

    events : {
      'click #saveChallenge' : 'saveChallenge',
      'change select#challengeFilterType' : 'changeChallengeFilter',
      'change select#challengeLeaderboardFilterType' : 'changeChallengeLeaderboardFilter',
      'click #publishSubmission' : 'publishSubmission',
      'click .challengeCommentButton' : 'challengeComment',
      'click .editChallengeButton' : 'editChallenge',
      'mouseover .subscribedToChallenge' : 'changeToUnsubscribe',
      'mouseout .unsubscribeToChallenge' : 'changeToSubscribed',
      'click .subscribeToChallenge' : 'subscribeToChallenge',
      'click .unsubscribeToChallenge' : 'unsubscribeToChallenge',
      'click .viewAllTable' : 'revealEntireTable'
    },

    initialize: function(action, results, options){
      this.challengeOptions = options;
      this.thisAction = action;
      console.log(options)
      if(action == "newChallenge"){
        this.renderNewChallenge();
      }else if(action == "activeChallenges"){
        this.activeChallenges(results);
      }else if(action == "previousChallenges"){
        this.previousChallenges(results);
      }else if(action == "myChallenges"){
        this.myChallenges(results);
      }else if(action == "challengeWinners"){
        this.challengeWinners(results);
      }else if(action == "readChallenge"){
        this.readChallenge(results);
      }else if(action == "submitChallenge"){
        this.submitChallenge(results);
      }else if(action == "editChallenge"){
        this.challengeDetails = results[0];
        this.renderEditChallenge(results);
      }else if(action == "challengeLeaderboard"){
        this.challengeLeaderboard(results);
      }
    },

    challengeLeaderboard : function(results){
      this.$el.html(this.challengeLeaderboardTemplate({filter: this.leaderboardFilter, results: results}));
      if($('.leaderboardTable tr').length > 10){
        if($('.leaderboardTable .me').length > 0){
          var myPosition = $('.leaderboardTable .me td:first').text().trim();
          if(myPosition > 10){
            var topToDisplay = parseInt(myPosition) - 3;
            var bottomToDisplay = parseInt(myPosition) + 2;
            if(topToDisplay >= 4){
              $('.leaderboardTable tr[id="'+topToDisplay+'"]').before('<tr class="rowDivider" style="background:#ffffff;"><td colspan="5">---</td></tr>');
            }
            if(bottomToDisplay < results.length){
              $('.leaderboardTable tr[id="'+bottomToDisplay+'"]').after('<tr class="rowDivider" style="background:#ffffff;"><td colspan="5">---</td></tr>');
            }
            $('.leaderboardTable tr').hide();
            $('.leaderboardTable tr:first').toggle();
            $('.leaderboardTable .rowDivider').show();
            for(var i = topToDisplay; i <= bottomToDisplay; i++){
              $('.leaderboardTable tr[id="'+i+'"]').toggle();
            }
            for(var i = 1; i <= 3; i++){
              $('.leaderboardTable tr[id="'+i+'"]').show();
            }
          }else{
            var topToDisplay = 1;
            var bottomToDisplay = 10;
            $('.leaderboardTable tr').hide();
            $('.leaderboardTable tr:first').toggle();
            for(var i = topToDisplay; i <= bottomToDisplay; i++){
              $('.leaderboardTable tr[id="'+i+'"]').toggle();
            }
          }
          $('.viewAllButtonContainer').show();
        }else{
          var topToDisplay = 1;
          var bottomToDisplay = 10;
          $('.leaderboardTable tr').hide();
          $('.leaderboardTable tr:first').toggle();
          for(var i = topToDisplay; i <= bottomToDisplay; i++){
            $('.leaderboardTable tr[id="'+i+'"]').toggle();
          }
          $('.viewAllButtonContainer').show();
        }
      }else{
        $('.viewAllButtonContainer').hide();
      }
      return this.el;
    },

    revealEntireTable : function(){
      $('.leaderboardTable tr').hide();
      $('.leaderboardTable tr').show();
      $('.viewAllButtonContainer').hide();
      $('.leaderboardTable .rowDivider').remove();
    },

    renderNewChallenge : function(){
      this.$el.html(this.newChallengeTemplate());
      $('#challengeStart').datepicker({ dateFormat: 'yy-mm-dd' });
      $('#challengeEnd').datepicker({ dateFormat: 'yy-mm-dd' });
      nicEditors.allTextAreas();
      return this.el;
    },

    render: function(){
      this.$el.html(this.challengeTemplate());
      //this.delegateEvents();
      return this.el;
    },

    saveChallenge : function(){
      if(typeof storage.get('profile') == "object"){
        var challengeTitle = $('#challengeName').val();
        var challengeStart = $('#challengeStart').val();
        var challengeEnd = $('#challengeEnd').val();
        var challengeDescription = new nicEditors.findEditor('challengeDescription').getContent();

        if(challengeTitle != null && challengeTitle != "" && challengeStart != null && challengeStart != "" && challengeEnd != null && challengeEnd != "" && challengeDescription != null && challengeDescription != ""){
          var options = {};
          options.challenge_name = challengeTitle;
          options.challenge_description = challengeDescription;
          options.challenge_start_date = challengeStart;
          options.challenge_end_date = challengeEnd;
          if(this.thisAction == "newChallenge"){
            var results = window.controllers.challengeController.models.challengeModel.newChallenge(options);
          }else{
            options.challenge_id = this.challengeDetails.id;
            var results = window.controllers.challengeController.models.challengeModel.editChallenge(options);
          }

          $.when(results).done(appRouter.navigate("/challenge/"+results.challenge_id+"/"+results.slug, {trigger: true}));
        }else{
          $('.saveChallengeError').text('Please fill in all fields.');
          setTimeout( function(){
            $('.saveChallengeError').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    activeChallenges : function(results){
      var that = this;
      var num_challenges = controllers.challengeController.models.challengeModel.get('numberActiveChallenges');
      $('meta[property="og:title"]').attr('content',"Bloqly - Active Challenges");
      this.$el.html(this.activeChallengesTemplate({challenges:results, filter: this.filter}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_challenges)/10),
           page: parseInt(that.challengeOptions.page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.el;
    },

    previousChallenges : function(results){
      var that = this;
      var num_challenges = controllers.challengeController.models.challengeModel.get('numberPreviousChallenges');
      $('meta[property="og:title"]').attr('content',"Bloqly - Previous Challenges");
      this.$el.html(this.previousChallengesTemplate({challenges:results, filter: this.filter}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_challenges)/10),
           page: parseInt(that.challengeOptions.page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.el;
    },

    myChallenges : function(results){
      var that = this;
      var num_challenges = controllers.challengeController.models.challengeModel.get('numberMyChallenges');
      this.$el.html(this.myChallengesTemplate({challenges:results, filter: this.filter}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_challenges)/10),
           page: parseInt(that.challengeOptions.page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.el;
    },

    challengeWinners : function(results){
      var that = this;
      var num_challenges = controllers.challengeController.models.challengeModel.get('numberChallengeWinners');
      this.$el.html(this.challengeWinnersTemplate({challenges:results, filter: this.filter}));
      var url = window.location.pathname;
      url = url.split("/");
      var thisUrl = "";
      if(!isNaN(url[url.length-1])){
        for(var i = 1; i < url.length-1; i++){
          thisUrl = thisUrl + "/" + url[i];
        }
      }else{
        thisUrl = window.location.pathname;
      }
      $( ".page" ).ready(function() {
        $('.page').bootpag({
           total: Math.ceil(parseInt(num_challenges)/10),
           page: parseInt(that.challengeOptions.page),
           maxVisible: 10,
           leaps: true,
           next: '>',
           prev: '<'
        }).on('page', function(event, num){
          appRouter.navigate(thisUrl+"/"+num, {trigger: true});
        });;
      });
      return this.el;
    },

    readChallenge : function(results){
      $('meta[property="og:title"]').attr('content',"Challenge - " + results[0].challenge_name);
      $('meta[property="og:description"]').attr('content',results[0].challenge_description);
      this.$el.html(this.readChallengeTemplate({challenge:results, filter: this.filter}));
      return this.el;
    },

    changeChallengeFilter : function(){
      var filter = $('#challengeFilterType').val();
      if(this.thisAction == "actionChallenges"){
        window.controllers.challengeController.models.challengeModel.getActiveChallenges(filter);
      }else if(this.thisAction == "previousChallenges"){
        window.controllers.challengeController.models.challengeModel.getPreviousChallenges(filter);
      }
    },

    changeChallengeLeaderboardFilter : function(){
      var filter = $('#challengeLeaderboardFilterType').val();
      this.leaderboardFilter = filter;
      console.log(filter);
      window.controllers.challengeController.models.challengeModel.getLeaderboard(filter);
    },

    submitChallenge : function(options){
      this.$el.html(this.submitChallengeTemplate({options:options}));
      $('.rightContent #tags').tagsInput({
        width: '100%',
        height: '100%',
        'defaultText':'add a tag',
      });
      $('#category').chosen({max_selected_options: 3});
      nicEditors.allTextAreas();
      return this.el;
    },

    publishSubmission : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        var that = this;
        options.story_name = $('#storyName').val();
        options.story_description = $('#storyDescription').val();
        options.story_type = $('#storyType').val();
        options.categories = $('#category').val();
        options.story_tags = $('input[name=tags]').val();
        options.is_public = 1;
        options.chapter_title = $('#chapterName').val();
        options.chapter_text = new nicEditors.findEditor('storyContent').getContent();
        options.challenge_id = this.challengeOptions.id;
        if(options.title != "" && options.description != "" && options.categories != "" && options.tags != "" && options.chapter_title != "" && options.chapter_text != "" && options.chllenge_id != ""){
          var response = window.controllers.challengeController.models.challengeModel.submitChallenge(options);
          $.when(response).done(appRouter.navigate('/story/'+response.story_id+'/'+response.slug, {trigger: true}));
        }else{
          $('.step2Error').text('Please fill in all fields.');
          setTimeout(function(){
            $('.step2Error').text('');
          },2000);
        }
      }else{
        views.mainContentView.login();
      }
    },

    challengeComment : function(){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        var that = this;
        options.comment = $('.challengeComment').val();
        options.challenge_id = this.challengeOptions.id;
        var response = window.controllers.challengeController.models.challengeModel.challengeComment(options);
        $.when(response).done(that.loadComment(response.comment_id, options));
      }else{
        views.mainContentView.login();
      }
    },

    loadComment : function(comment_id, options){
      var reply = {
        id: comment_id,
        pen_name: window.storage.get('profile').pen_name,
        real_name: window.storage.get('profile').real_name,
        avatar: window.storage.get('profile').avatar,
        commented_at: new Date().getTime(),
        comment: options.comment
      }
      if($('.replies').length > 0){
        $('.commentsContainer').append(this.singleChallengeCommentTemplate({reply: reply}));
      }else{
        $('.commentsContainer').html(this.singleChallengeCommentTemplate({reply: reply}));
      }
      $('.challengeComment').val('');
    },

    renderEditChallenge : function(results){
      this.$el.html(this.newChallengeTemplate({challenge:results}));
      $('#challengeStart').datepicker({ dateFormat: 'yy-mm-dd' });
      $('#challengeEnd').datepicker({ dateFormat: 'yy-mm-dd' });
      nicEditors.allTextAreas();
      return this.el;
    },

    editChallenge : function(){
      appRouter.navigate('/challenge/'+this.challengeOptions.id+'/'+this.challengeOptions.slug+'/edit', {trigger: true});
    },

    changeToUnsubscribe : function(e){
      $(e.currentTarget).removeClass('subscribedToChallenge').addClass('unsubscribeToChallenge').html('<i class="fa fa-bookmark"></i> Unsubscribe');
    },

    changeToSubscribed : function(e){
      $(e.currentTarget).removeClass('unsubscribeToChallenge').addClass('subscribedToChallenge').html('<i class="fa fa-bookmark"></i> Subscribed');
    },

    subscribeToChallenge : function(e){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.challenge_id = this.challengeOptions.id;
        window.controllers.challengeController.models.challengeModel.subscribeChallenge(options);
        $(e.currentTarget).removeClass('subscribeToChallenge').addClass('subscribedToChallenge').html('<i class="fa fa-bookmark"></i> Subscribed');
      }else{
        views.mainContentView.login();
      }
    },

    unsubscribeToChallenge : function(e){
      if(typeof storage.get('profile') == "object"){
        var options = {};
        options.challenge_id = this.challengeOptions.id;
        window.controllers.challengeController.models.challengeModel.unsubscribeChallenge(options);
        $(e.currentTarget).removeClass('unsubscribeToChallenge').addClass('subscribeToChallenge').html('<i class="fa fa-bookmark"></i> Subscribe');
      }else{
        views.mainContentView.login();
      }
    }

  });

  return challengeRightContentView;
});