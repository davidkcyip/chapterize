define([
  'underscore',
  'backbone',
  'text!templates/items/unreadNotifications.html',
], function( _, Backbone, UnreadNotificationsTemplate){
  var unreadNotificationsView = Backbone.View.extend({
    unreadNotificationsTemplate: _.template(UnreadNotificationsTemplate),

    el : '.notificationsMenu',

    events : {
      'click li' : 'readNotification'
    },

    initialize: function(results){
      this.render(results);
    },

    render: function(results){
      //window.app.vent.trigger('site:removeDivs');
      if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
        this.$el.html(this.unreadNotificationsTemplate({notifications: results}));
      }else{
        $('.notificationsMenu .sub-menu').html(this.unreadNotificationsTemplate({notifications: results}));
      }
      $('.menuNotificationsNumber').html(results.length);
      return this.el;
    },

    readNotification : function(e){
      var notification_id = $(e.currentTarget).attr('id');
      if(notification_id != "none"){
        window.controllers.mainContentController.models.notificationModel.readNotification(notification_id);
      }
    }
  });

  return unreadNotificationsView;
});