define([
  'underscore',
  'backbone',
  'text!templates/items/brainstormList.html'
], function( _, Backbone, BrainstormListTemplate){
  var brainstormListView = Backbone.View.extend({
    brainstormListTemplate: _.template(BrainstormListTemplate),

    el : '.rightContent',
    thisStory: {},
    brainstormOptions:{},

    events : {
      'click .subscribeToBrainstorm' : 'subscribeToBrainstorm',
      'click .unsubscribeToBrainstorm' : 'unsubscribeToBrainstorm'
    },

    initialize: function(result,options,haveISubscribed){
      this.brainstormOptions = options;
      this.brainstormResults = result;
      this.render(result,options,haveISubscribed);
    },

    render: function(result,options,haveISubscribed){
      this.$el.html(this.brainstormListTemplate({brainstorms:result,options:options,haveISubscribed:haveISubscribed}));
      return this.el;
    },

    subscribeToBrainstorm : function(){
      if(typeof storage.get('profile') == "object"){
        window.controllers.brainstormController.models.brainstormModel.subscribe(this.brainstormOptions);
        $('.subscribeToBrainstorm').addClass('unsubscribeToBrainstorm').removeClass('subscribeToBrainstorm').html('<i class="fa fa-bookmark"></i> Unsubscribe');
        window.controllers.brainstormController.models.brainstormModel.getMyBrainstorms();
      }else{
        views.mainContentView.login();
      }
    },

    unsubscribeToBrainstorm : function(){
      if(typeof storage.get('profile') == "object"){
        window.controllers.brainstormController.models.brainstormModel.unsubscribe(this.brainstormOptions);
        $('.unsubscribeToBrainstorm').addClass('subscribeToBrainstorm').removeClass('unsubscribeToBrainstorm').html('<i class="fa fa-bookmark"></i> Subscribe');
        window.controllers.brainstormController.models.brainstormModel.getMyBrainstorms();
      }else{
        views.mainContentView.login();
      }
    }

  });

  return brainstormListView;
});