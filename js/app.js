define([
  'underscore',
  'backbone',
  'js/router',
  'js/storage',
  'forms',
  'toastr'
], function(_, Backbone, Router, Storage, Forms, Toastr){

  var initialize = function(){

    window.storage = Storage;
    window.views = {};
    window.controllers = {};

    //router
    window.appRouter = new Router();
    Backbone.history.start({pushState: true});
  };

  var notification = function(message, url, notification_id){
    Toastr.options = {
      "closeButton": false,
      "debug": true,
      "progressBar": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut",
      "preventDuplicates": true
    }
    Toastr.success("<a href='"+url+"' id='"+notification_id+"' class='notificationModal'>"+message+"</a>");
  };

  function getOrdinal(n) {
       var s=["th","st","nd","rd"],
           v=n%100;
       return n+(s[(v-20)%10]||s[v]||s[0]);
  };

  var fadeOutAndIn = function(DOMToFadeOut, HTMLToAttach){
    $(DOMToFadeOut).html(HTMLToAttach);
  };

  var vent = _.extend({}, Backbone.Events);

  var getValue = function(el){
    return el.target.value;
  };

  var deferredRenders = [];

  var getActiveElement = function(element){
    return element.$el.context.activeElement;
  };

  var timeago = function(date){
      if(!$.isNumeric(date)){
        date = new Date(app.convertDate(date));
      }else{
        date = new Date(date);
      }

      var seconds = Math.floor((new Date() - date) / 1000);

      var interval = Math.floor(seconds / 31536000);

      if (interval > 1) {
          return interval + "y";
      }
      interval = Math.floor(seconds / 2592000);
      if (interval > 1) {
          return interval + "mn";
      }
      interval = Math.floor(seconds / 86400);
      if (interval > 1) {
          return interval + "d";
      }
      interval = Math.floor(seconds / 3600);
      if (interval > 1) {
          return interval + "h";
      }
      interval = Math.floor(seconds / 60);
      if (interval > 1) {
          return interval + "m";
      }
      return "< 1m";
  }

  var timeToShort = function(datetime, todayPrefix){
    var current, time;
    if (!todayPrefix) {
      todayPrefix = "";
    }
    if (!window.app.isNumber(datetime)){
      
      if (typeof datetime == "string") {
        var regex = new RegExp('-','g');
        datetime = datetime.replace(regex,"/");
        time = (new Date(datetime)).getTime();
      } else {
        time = datetime.getTime();
      }

    } else {
      time = datetime;
    }
    current = (new Date()).getTime();
    time = new Date(time);
    return time.getDate()+' '+window.app.month_names_short[time.getMonth()] + ' ' + time.getFullYear();
  }

  var convertDate = function(datetime){
    var regex = new RegExp('-','g');
    datetime = datetime.replace(regex,"/");
    return datetime;
  }

  var shortenTitle = function(title) {
    if(title.length > 20){
      return title.substring(0,20) + "...";
    }else{
      return title;
    }
  }

  var isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  var blockAdblockUser = function() {
    if ($('.leftBid').height() == 0) {
      $('.leftBidFallback').show();
    }else{
      $('.leftBidFallback').hide();
    }
  }

  var month_names_short = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

  return {
    initialize : initialize,
    fadeOutAndIn : fadeOutAndIn,
    vent : vent,
    deferredRenders : deferredRenders,
    getActiveElement : getActiveElement,
    timeago : timeago,
    timeToShort : timeToShort,
    isNumber : isNumber,
    month_names_short : month_names_short,
    shortenTitle : shortenTitle,
    notification: notification,
    convertDate: convertDate,
    getOrdinal: getOrdinal,
    blockAdblockUser: blockAdblockUser
  };

});