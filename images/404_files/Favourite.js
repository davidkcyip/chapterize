define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var FavouriteModel = Backbone.Model.extend({
		favourites: {},
		getFavourites : function(){
			var that = this;
			$.getJSON('/api/favourites/get/'+window.storage.getSession('profile').id, function(data){
          		that.set('favourites',data.results);
          		console.log(data)
        	});
		},

		favouriteChapter : function(chapterId, storyId){
			var that = this;
			$.ajax({
		        type: 'POST',
		        url: '/api/chapter/actions/favourite',
		            dataType: "json",
		            data: { "chapter_id": chapterId, "story_id": storyId},
		            success: function(data){
		              that.set('favourites',data.results);
          				console.log(data)
		            }
		      });
		},

		unfavouriteChapter : function(chapterId, storyId){
			var that = this;
			$.ajax({
		        type: 'POST',
		        url: '/api/chapter/actions/unfavourite',
		            dataType: "json",
		            data: { "chapter_id": chapterId, "story_id": storyId},
		            success: function(data){
		              	that.set('favourites',data.results);
          				console.log(data)
		            }
		      });
		}
	});

	var FavouriteCollection = Backbone.Collection.extend({
		model: FavouriteModel,

	});

	return {
		FavouriteModel : FavouriteModel,
		FavouriteCollection : FavouriteCollection
	};
});