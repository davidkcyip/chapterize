require.config({
    baseUrl: '/',
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        jquery: "js/jquery-1.11.1.min",
        jqueryui: "js/jquery-ui/jquery-ui.min",
        text: "js/require-text",
        underscore: "js/underscore.min",
        backbone: "js/backbone.min",
        forms: "js/backbone-forms",
        controller: "js/backbone.controller",
        amplify: "js/amplify.min",
        tock: "js/tock",
        tipped: "js/tipped",
        imagesLoaded: "js/imagesloaded.pkgd.min",
        relational: "js/backbone-relational",
        steps: "js/jquery.steps.min",
        chosen: "/js/chosen.jquery.min",
        rating: "/js/rating.jquery",
        tipi: "/js/tipi.jquery",
        //tree": "/js/jquery.jOrgChart"
        tree: "/js/jquery.tree",
        nicEdit: "/js/nicEdit"
    },
    shim: {
        backbone: {
          deps: ['underscore', 'jquery'],
          exports: 'Backbone'
        },
        controller: {
          deps: ['underscore', 'backbone']
        },
        app: ['controller']
    }
});


require(['js/app','js/controllers/mainContent'], function(App,MainContent){
    // The "app" dependency is passed in as "App"
    //Storage.initStorage();
    App.initialize();
    //window.storage = Storage;
    window.app = App;
    window.controllers.mainContentController = new MainContent();
});



