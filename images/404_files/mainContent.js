define([
  'underscore',
  'controller',
  'js/views/pages/mainContent',
  'tock',
  'js/models/Status',
  'js/models/Bookmark',
  'js/models/Favourite',
  'js/models/Message'
], function( _, BackBoneController, MainContentView, Tock, StatusModel, BookmarkModel, FavouriteModel, MessageModel ){
  var mainContentController = Backbone.Controller.extend({
    timer: 0,
    secs : 0,
    minutes : 0,
    el: $('body'),
    error: '',
    models: {},
    collections:{},

    initialize : function(){

      //tock is an accurate timer function, this calls timerCallBack every second
      this.timer =  new Tock({
       interval : 1000,
       callback : _.bind(this.timerCallback, this)
      });
      this.timer.start();

    	this.renderView();

      this.models.messagesModel = new MessageModel.MessageModel();

      this.checkLogin();
      this.checkUnreadMessages();
      window.app.vent.on('tock:30sec',this.checkLogin, this);
      window.app.vent.on('tock:minute',this.checkUnreadMessages, this);
      window.app.vent.on('tock:minute',this.updateTime, this);
      window.app.vent.on('menu:deactivateButton', this.deactivateButton, this);
      window.app.vent.on('home:openRegistrationBox', this.openRegistrationBox, this);
      window.app.vent.on('site:closeModal', this.closeModal, this);
      window.app.vent.on('home:goToDiv', this.goToDiv, this);
      window.app.vent.on('site:removeDivs', this.removeDivs, this);
      window.app.vent.on('site:scrollToTop', this.scrollToTop, this);
      window.app.vent.on('site:checkPenname', this.checkPenname, this);
      window.app.vent.on('site:checkEmail', this.checkEmail, this);
      window.app.vent.on('site:checkPasswords', this.checkPasswords, this);
      window.app.vent.on('site:checkName', this.checkName, this);
      window.app.vent.on('site:signUp', this.signUp, this);
      window.app.vent.on('site:logout', this.logout, this);
      window.app.vent.on('verify:verifyForm', this.verifyVerifyForm, this);
      window.app.vent.on('tenSeconds', this.tenSeconds, this);
      window.app.vent.on('site:loginSubmit', this.loginSubmit, this);
      window.app.vent.on('site:sendNewPassword', this.forgotPassword, this);
      window.app.vent.on('profile:unfollow',this.unfollow, this);
      window.app.vent.on('home:likeStatus', this.likeStatus, this);
      window.app.vent.on('home:likeReply', this.likeReply, this);
      window.app.vent.on('home:unlikeStatus', this.unlikeStatus, this);
      window.app.vent.on('home:unlikeReply', this.unlikeReply, this);
      window.app.vent.on('home:sendReplyStatus', this.sendReplyStatus, this);
      window.app.vent.on('home:openCloseReplyArea', this.openCloseReplyArea, this);
      window.app.vent.on('home:submitStatus', this.submitStatus, this);
      window.app.vent.on('home:deleteStatus', this.deleteStatus, this);
      window.app.vent.on('home:deleteReply', this.deleteReply, this);
    },

    renderView : function(){
      if(typeof window.views.mainContentView != "undefined"){
        window.views.mainContentView.remove();
      }
      window.views.mainContentView = new MainContentView();
      this.mainContentPostRender();
    },

    mainContentPostRender : function(){
      var that = this;
      if(typeof window.storage.getSession('profile') != "undefined"){
        this.models.bookmarkModel = new BookmarkModel.BookmarkModel();
        this.models.bookmarkModel.on('change:bookmarks', this.getBookmarkSuccess, this);
        this.models.bookmarkModel.getBookMarks();
        this.models.favouriteModel = new FavouriteModel.FavouriteModel();
        this.models.favouriteModel.on('change:favourites', this.getFavouriteSuccess, this);
        this.models.favouriteModel.getFavourites();
      }
    },

    getBookmarkSuccess : function(response){
      window.views.mainContentView.postRenderBookmark(response);
    },

    getFavouriteSuccess : function(response){
      window.views.mainContentView.postRenderFavourite(response);
    },

    unfollow : function(user_id){
      $.ajax({
        type: 'POST',
        url: '/api/user/followUnfollow',
        dataType: "json",
        data: {"followUnfollow": "unfollow", "type": "user", "user_id": user_id},
        success: function(data){
          window.app.vent.trigger('profile:unfollowSuccess');
        }
      });
    },

    checkLogin : function(){
      if(window.storage.getSession('profile')){
        $.ajax({
          type: 'GET',
          url: '/api/checkLogin',
          success : function(data){
            if(data == 0){
              window.storage.destroyAllSession();
              appRouter.navigate('/', {trigger: true});
            }
          }
        });
      }
    },

    checkUnreadMessages : function(){
      if(window.storage.getSession('profile')){
        $.ajax({
          type: 'GET',
          url: '/api/messages/message/getUnreadNumber',
          success : function(data){
            $('.menuMessagesNumber').text(JSON.parse(data).unread);
          }
        });
      }
    },

    submitStatus : function(to_id){
      var statusModel = new StatusModel();
      var status = $('#submitStatus').val();
      $.ajax({
        type: 'POST',
        url: '/api/status/submit',
              dataType: "json",
              async:false,
              data: {"status": status, "to": to_id},
              success: function(data){
                 window.app.vent.trigger("home:submitStatusSuccess", data.id, to_id);
              }
      });
    },

    updateTime : function(){
      _.each($('.timeago'), function(timestamp){
        var time = window.app.timeago($(timestamp).attr('time'));
        $(timestamp).text(time);
      });
    },
    
    removeDivs : function(){
      $('.leftContent').remove();
      $('.rightContent').remove();
      $('.fullWidth').remove();
      $('.errorDiv').remove();
    },

    activeButton : function(element){
      //$(element).addClass("activeButton");
    },

    deactivateButton : function(){
      //$('#wrapper #headerMenu button').removeClass("activeButton");
    },

    scrollToTop : function(){
      $('html,body').animate({scrollTop: 0},'slow');
    },

    goToDiv : function(element){
        $('html,body').animate({scrollTop: $(element).offset().top},'slow');
    },

    sendReplyStatus : function(options){
      var statusModel = new StatusModel();
      $.ajax({
        type: 'POST',
        url: '/api/status/sendReply',
        dataType: "json",
        async:false,
        data: {"reply": options.reply, "id": options.id},
        success: function(data){
          window.app.vent.trigger("home:submitReplySuccess", data.id, options.id, options.reply);
        }
      });

    },

    openRegistrationBox : function(){
      require(['text!templates/items/modal.html','text!templates/items/homepageSignUpForm.html'], function(ModalTemplate, SignUpFormTemplate){

        var modalTemplate = _.template(ModalTemplate);
        var homeSignUpFormTemplate = _.template(SignUpFormTemplate);
        $('#wrapper').append(modalTemplate());
        $('.modal').append(homeSignUpFormTemplate());
        $('.modalBox').css('height','610px');
      });
    },

    //timerCallback triggers an event every 10, 20, 30, 40, 50 seconds and 1 minute and 2 minutes
    timerCallback : function () {
      var dr;
      //app.vent.trigger("tock:tick", new Date().getTime());

      if (!(this.secs % 10)) {
        app.vent.trigger("tock:10sec", new Date().getTime());
      }
      if(!(this.secs % 20)){
        app.vent.trigger("tock:20sec", new Date().getTime());
      }
      if(!(this.secs % 30)){
        app.vent.trigger("tock:30sec", new Date().getTime());
      }
      if(!(this.secs % 40)){
        app.vent.trigger("tock:40sec", new Date().getTime());
      }
      if(!(this.secs % 50)){
        app.vent.trigger("tock:50sec", new Date().getTime());
      }

      if (this.secs == 60) {
        app.vent.trigger("tock:minute", new Date().getTime());
        this.secs = 0;
        this.minutes++;
        if (this.minutes%2) {
          app.vent.trigger("tock:2minutes", new Date().getTime());
        }
      } else {
        this.secs++;
      }

      while (dr = app.deferredRenders.pop()) {
        console.log("Executing deferred render");
        dr.render();
      }
    },

    closeModal : function(e){
      if(e.target.className == "bg"){
        $('.modal').remove();
        $('.tipi').remove();
      }
    },

    openCloseReplyArea : function(e,f){

      if(f != "replyReplyButton"){
        $('#replyArea_'+e).toggle();
        $('#post_'+e+' .repliesContainer').toggle();
      }
      //$('#replyArea_'+e+' input').focus();
    },

    checkPenname : function(){
      var penname = $('#penname').val();
      if(penname != ""){
        $.getJSON('/api/verify/penname/'+penname, function(data){
          if(data.error == 0){
            $('#penname').addClass('valid').removeClass('invalid');
            $('.pennameError').html('');
            var error = 0;
          }else if(data.error == 1){
            $('.pennameError').html('Username is taken.');
            $('#penname').addClass('invalid').removeClass('valid');
            var error = 'Your chosen pen name is already being used';
          }
        });
      }else{
        $('#penname').removeClass('invalid').removeClass('valid');
        var error = 'You have not chosen a pen name.';
      }
      this.error = error;
    },

    checkEmail : function(){
      var email = $('#email').val();
      if(typeof email != "undefined"){
      var atCountRegex = new RegExp("@", "g");
      var dotCountRegex = new RegExp("\\.", "g");
      var atCount = email.toString().match(atCountRegex) ? email.toString().match(atCountRegex).length : 0;
      var dotCount = email.toString().match(dotCountRegex) ? email.toString().match(dotCountRegex).length : 0;

      console.log(atCount + " " + dotCount);
      if(email != "" && atCount == 1 && dotCount == 1){
        $.getJSON('/api/verify/email/'+email, function(data){
          if(data.error == 0){
            $('#email').addClass('valid').removeClass('invalid');
            $('.emailError').html('');
            var error = 0;
          }else if(data.error == 1){
            $('.emailError').html('Email is taken.');
            $('#email').addClass('invalid').removeClass('valid');
            var error = 'Your email is already being used';
          }
        });
      }else{
        $('#email').removeClass('invalid').removeClass('valid');
        var error = 'You have not entered a valid email.';
      }
      this.error = error;
      }
    },

    checkPasswords : function(){
      var password1 = $("#password1").val();
      var password2 = $("#password2").val();

      if(!(password1 === password2) && password1 != "" && password2 != ""){
        $("#password1").removeClass("valid");
        $("#password2").removeClass("valid").addClass("invalid");
        var error = 'Your passwords do not match';
      }else if((password1 === password2) && password1 != "" && password2 != ""){
        $("#password1").removeClass("invalid").addClass("valid");
        $("#password2").removeClass("invalid").addClass("valid");
        var error = 0;
      }else{
        $("#password1").removeClass("invalid").removeClass("valid");
        $("#password2").removeClass("invalid").removeClass("valid");
        var error = 'Please enter a password for your account.';
      }
      this.error = error;
    },

    checkName : function(){
      var name = $("#signUpName").val();

      if(name && name.length > 0 && typeof name != "undefined"){
        $("#signUpName").removeClass("invalid").addClass("valid");
        var error = 0;
      }else{
        $("#signUpName").removeClass("valid").addClass("invalid");
        var error = "Please tell us who you are.";
      }

      this.error = error;
    },

    verifyVerifyForm : function(){
      $('.verify .verifyError').remove();
      var errors = [];
      window.app.vent.trigger('site:checkPenname');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      window.app.vent.trigger('site:checkPasswords');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      if(errors.length > 0){
        errors = _.uniq(errors);
        _.each(errors, function(error){
          $('.verify').append('<div class="clear verifyError">'+ error + '</div>');
        });
      }else{
        var penname = $('#penname').val();
        var password = $("#password2").val();
        $.getJSON('/api/update/pennamePassword?penname='+penname+'&password='+password, function(data){
          if(data.error == 0){
            $('.modal').remove();
            $('.verify').remove();
            window.location.href = "/home";
            var profile = window.storage.getSession('profile');
            profile.pen_name = penname;
            window.storage.setSession('profile', profile);
          }else{
            $('.verify .verifyError').remove();
            $('.verify').append('<div class="clear verifyError">Your details could not be updated. Please try again.</div>');
          }
        });
      }
    },

    signUp : function(){
      $('.verify .verifyError').remove();
      var errors = [];
      window.app.vent.trigger('site:checkPenname');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      window.app.vent.trigger('site:checkPasswords');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      window.app.vent.trigger('site:checkName');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      window.app.vent.trigger('site:checkEmail');
      if(this.error != 0 && typeof this.error != "undefined"){
        errors.push(this.error);
      }
      if(errors.length > 0){
        errors = _.uniq(errors);
        _.each(errors, function(error){
          $('.verify').append('<div class="clear verifyError">'+ error + '</div>');
        });
      }else{
        var penname = $("#penname").val();
        var password = $("#password2").val();
        var name = $("#signUpName").val();
        var email = $("#email").val();
        $.ajax({
           type: "POST",
           url: '/api/register',
           dataType: "json",
           data: {"name" : name, "penname": penname, "password": password, "email": email},
           success: function(data){
              if(data.success == 1){
                window.app.vent.trigger('site:registerSuccess');
              }
           },
           error: function(data){
            console.log(data);
           }
        });
      }
    },

    forgotPassword : function(){
      var email = $('#loginEmail').val();
      $.getJSON("/api/forgotPassword?email="+email, function(data){
        if(data.success == 1){
          window.app.vent.trigger('site:forgotPasswordSuccess');
        }else{
          $(".emailError").html(data.reason);
        }
      });
    },

    loginSubmit : function(){
      var email = $('#loginEmail').val();
      var password = $('#loginPassword').val();

      $.ajax({
        type: 'POST',
        url: '/api/login',
        dataType: 'json',
        data: {"email":  email, "password": password},
        success: function(data){
          console.log(data);
          if(data.success == 1){
            window.storage.setSession('profile',data.profile);
            window.location.href = "/home";
          }else{
            $('.loginError').html(data.reason);
          }
        }
      });
    },

    logout : function(){
      $.getJSON("/api/logout", function(data){
        if(data.success == 1){
          window.storage.destroySession("profile");
          window.location.href = "/";
        }
      });
    },

    likeStatus : function(id){
      var statusModel = new StatusModel();
      var response = statusModel.likeStatus(id);
      $.getJSON('/api/status/get/'+id, function(status){
        window.app.vent.trigger('home:likeStatusSuccess',status);
      });
    },

    likeReply : function(options){
      var statusModel = new StatusModel();
      var response = statusModel.likeReply(options.id);
      $.getJSON('/api/reply/get/'+options.id, function(reply){
        var option = [];
        option['reply'] = reply;
        option['thisPostId'] = options.thisPostId;
        window.app.vent.trigger('home:likeReplySuccess',option);
      });
    },

    unlikeStatus : function(id){
      var statusModel = new StatusModel();
      var response = statusModel.unlikeStatus(id);
       $.getJSON('/api/status/get/'+id, function(status){
        window.app.vent.trigger('home:unlikeStatusSuccess',status);
      });
    },

    unlikeReply : function(options){
      var statusModel = new StatusModel();
      var response = statusModel.unlikeReply(options.id);
      $.getJSON('/api/reply/get/'+options.id, function(reply){
        var option = [];
        option['reply'] = reply;
        option['thisPostId'] = options.thisPostId;
        window.app.vent.trigger('home:unlikeReplySuccess',option);
      });
    },

    deleteStatus : function(id){
      var statusModel = new StatusModel();
      var response = statusModel.deleteStatus(id);
      window.app.vent.trigger("home:deleteStatusSuccess",id);
    },

    deleteReply : function(id){
      var statusModel = new StatusModel();
      var response = statusModel.deleteReply(id);
      window.app.vent.trigger("home:deleteReplySuccess",id);
    }
  });

  return mainContentController;
});