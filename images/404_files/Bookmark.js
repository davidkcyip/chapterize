define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var BookmarkModel = Backbone.Model.extend({
		bookmarks: {},
		getBookMarks : function(){
			var that = this;
			$.getJSON('/api/bookmarks/get/'+window.storage.getSession('profile').id, function(data){
          		that.set('bookmarks',data.results);
          		console.log(data)
        	});
		},

		bookmarkChapter : function(chapterId, storyId){
			var that = this;
			$.ajax({
		        type: 'POST',
		        url: '/api/chapter/actions/bookmark',
		            dataType: "json",
		            data: { "chapter_id": chapterId, "story_id": storyId},
		            success: function(data){
		              that.set('bookmarks',data.results);
          				console.log(data)
		            }
		      });
		},

		unbookmarkChapter : function(chapterId, storyId){
			var that = this;
			$.ajax({
		        type: 'POST',
		        url: '/api/chapter/actions/unbookmark',
		            dataType: "json",
		            data: { "chapter_id": chapterId, "story_id": storyId},
		            success: function(data){
		              	that.set('bookmarks',data.results);
          				console.log(data)
		            }
		      });
		}
	});

	var BookmarkCollection = Backbone.Collection.extend({
		model: BookmarkModel,

	});

	return {
		BookmarkModel : BookmarkModel,
		BookmarkCollection : BookmarkCollection
	};
});