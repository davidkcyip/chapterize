define([
	'underscore',
	'backbone'
], function(_, Backbone ){

	var StatusModel = Backbone.Model.extend({

		deleteStatus : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/status/delete',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		},

		deleteReply : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/reply/delete',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		},

		likeStatus : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/status/like',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		},

		likeReply : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/reply/like',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		},

		unlikeStatus : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/status/unlike',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		},

		unlikeReply : function(id){
			$.ajax({
				type: 'POST',
				url: '/api/reply/unlike',
	            dataType: "json",
	            async: false,
	            data: {"id": id},
	            success: function(data){
	            	return data.id;
	            }
			});
		}
	});

	return StatusModel;
});