define([
  'underscore',
  'controller',
  'js/views/pages/brainstorm',
  'js/models/Brainstorm',
  'js/views/items/newBrainstormTopic',
  'js/views/items/myBrainstorms',
  'js/views/items/brainstormList',
  'js/views/items/myBrainstormList',
  'js/views/items/brainstormTopic'
], function( _, BackBoneController, BrainstormView, BrainstormModel, NewBrainstormTopicView, MyBrainstormsView, BrainstormListView, MyBrainstormListView, BrainstormTopicView ){
  var brainstormController = Backbone.Controller.extend({
  	el : $('.mainContainer'),
    models: {},
    routerOptions:{},
    initialize : function(options){
      this.renderView();
      this.routerOptions = options;
      this.models.brainstormModel = new BrainstormModel.BrainstormModel();
      this.models.brainstormModel.on("change:myBrainstorms",this.myBrainstormsPostRender, this);
      this.models.brainstormModel.on("change:myBrainstormList",this.myBrainstormListPostRender, this);
      this.models.brainstormModel.on("change:storyBrainstorms",this.storyBrainstormsPostRender, this);
      this.models.brainstormModel.on("change:brainstormTopic",this.brainstormTopicPostRender, this);
      if(options.id == null && options.story == null && options.topic == null && options.action == null && options.thread_id == null){
        this.models.brainstormModel.getMyBrainstorms();
        this.models.brainstormModel.getMyBrainstormList();
      }else if(options.id != null && options.story != null && options.topic == null && options.action == null && options.thread_id == null){
        this.models.brainstormModel.getMyBrainstorms();
        this.models.brainstormModel.getStoryBrainstormTopics(options);
      }else if(options.id != null && options.story != null && options.topic == null && options.action != null && options.thread_id == null){
        if(options.action == "new"){
          this.models.brainstormModel.getMyBrainstorms();
          this.newBrainstormTopicPostRender();
        }
        console.log('new topic')
      }else if(options.id != null && options.story != null && options.topic != null && options.action != null && options.thread_id != null){
        if(options.action == "edit"){
          this.models.brainstormModel.getMyBrainstorms();
          this.models.brainstormModel.getBrainstormTopic(options);
        }else if(options.action == "read"){
          this.models.brainstormModel.getMyBrainstorms();
          this.models.brainstormModel.getBrainstormTopic(options);
        }
      }
    },

    renderView : function(){
      var that = this;
      if(typeof window.views.brainstormView != "undefined"){
        window.views.brainstormView.remove();
        $('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.brainstormView = new BrainstormView();
    },

    myBrainstormsPostRender : function(result){
      var result = result.get('myBrainstorms');
      if(typeof window.views.myBrainstormsView != "undefined"){
        window.views.myBrainstormsView.remove();
        //$('.mainContainer').html('<div class="mainContent"></div>');
      }
      window.views.myBrainstormsView = new MyBrainstormsView(result);
    },

    newBrainstormTopicPostRender : function(){
      var that = this;
      if(typeof window.views.newBrainstormTopicView != "undefined"){
        window.views.newBrainstormTopicView.remove();
      }
      $.getJSON('/api/brainstorm/categories', function(data){
        $.getJSON('/api/brainstorm/getStoryDetails/'+that.routerOptions.id, function(data2){
          window.views.newBrainstormTopicView = new NewBrainstormTopicView(data.result, data2.result);
        });
      });
    },

    storyBrainstormsPostRender : function(result){
      var that = this;
      if(typeof window.views.brainstormListView != "undefined"){
        window.views.brainstormListView.remove();
      }
      window.views.brainstormListView = new BrainstormListView(result.get('storyBrainstorms'));
    },

    myBrainstormListPostRender : function(result){
      var that = this;
      if(typeof window.views.myBrainstormListView != "undefined"){
        window.views.myBrainstormListView.remove();
      }
      window.views.myBrainstormListView = new MyBrainstormListView(result.get('myBrainstormList'));
    },

    brainstormTopicPostRender : function(result){
      var that = this;
      if(typeof window.views.brainstormTopicView != "undefined"){
        window.views.brainstormTopicView.remove();
      }
      $.ajax({
          type: 'GET',
          url: '/api/brainstorm/get',
          data: {"id": that.routerOptions.id, "slug": that.routerOptions.story},
            dataType: "json",
            success: function(data){
              window.views.brainstormTopicView = new BrainstormTopicView(result.get('brainstormTopic'), data.results, that.routerOptions);
            }
      });
    }
  });

  return brainstormController;
});