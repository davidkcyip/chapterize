define([
  'underscore',
  'backbone',
  'text!templates/items/headerContainer.html',
  'text!templates/items/footer.html',
  'text!templates/items/header-loggedin.html',
  'text!templates/items/header.html',
  'text!templates/items/headerMenu.html',
  'text!templates/items/headerMenu-loggedin.html',
  'text!templates/items/headerMenu-loggedin-mobile.html',
  'text!templates/items/registerSuccess.html',
  'text!templates/items/loginModal.html',
  'text!templates/items/modal.html',
  'text!templates/items/bookmarks.html',
  'text!templates/items/favourites.html',
  'nicEdit'
], function( _, Backbone, HeaderContainerTemplate, FooterTemplate, HeaderLoggedInTemplate, HeaderTemplate, HeaderMenuTemplate, HeaderMenuLoggedInTemplate, HeaderMenuLoggedInMobileTemplate, RegisterSuccess, LoginModalTemplate, ModalTemplate, BookmarksTemplate, FavouritesTemplate, nicEdit ){
  var mainContentView = Backbone.View.extend({
    mainContentTemplate: _.template(HeaderContainerTemplate + FooterTemplate),

    el : 'body',

    events : {
      'click #wrapper #headerMenu button' : 'activeButton',
      'click #wrapper .footer a' : 'deactivateButton',
      'click #wrapper #headerMenu #about' : 'goToAbout',
      'click #wrapper #headerMenu #register' : 'goToRegister',
      'click #wrapper #headerMenu #features' : 'goToFeatures',
      'click #arrowUp' : 'scrollToTop',
      'click a' : 'dontRefresh',
      'click #wrapper .bg' : 'closeDialog',
      'click #wrapper .modalBox #closeModal span' : 'closeDialog',
      'click #signUpButton' : 'signUp',
      'keyup #signUpBox' : 'signUp',
      'keyup #penname' : 'checkPenname',
      'keyup #email' : 'checkEmail',
      'keyup #password2' : 'checkPasswords',
      'keyup #signUpName' : 'checkName',
      'click #login' : 'login',
      'keyup #signUpBody input' : 'loginSubmit',
      'click #loginButton' : 'loginSubmit',
      'click #logout' : 'logout',
      'click .forgotPassword' : 'forgotPassword',
      'click #forgotPasswordButton' : 'sendNewPassword',
      'click .likeStatusButton' : 'likeStatus',
      'click .unlikeStatusButton' : 'unlikeStatus',
      'click .likeReplyButton' : 'likeReply',
      'click .unlikeReplyButton' : 'unlikeReply',
      'click .replyButton' : 'showHideReplyInput',
      'click .submitReplyButton' : 'submitReply',
      'click #submitStatusButton' : 'submitStatus',
      'click .deleteStatusButton' : 'deleteStatus',
      'click .deleteReplyButton' : 'deleteReply',
      'click .mobileMenuStories' : 'toggleMobileStories',
      'click .mobileMenuSearch' : 'toggleMobileSearch',
      'click .mobileMenuChallenges' : 'toggleMobileChallenges',
      'click .mobileMenuMessages' : 'toggleMobileMessages',
      'click .mobileMenuBookmarks' : 'toggleMobileBookmarks',
      'click .mobileMenuFavourites' : 'toggleMobileFavourites',
      'click .sendMessageConfirm' : 'sendMessageConfirm'
    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
      window.app.vent.on('site:registerSuccess', this.registerSuccess, this);
      window.app.vent.on('site:forgotPasswordSuccess', this.forgotPasswordSuccess, this);
      window.app.vent.on('home:submitStatusSuccess', this.submitStatusSuccess, this);
      window.app.vent.on('home:submitReplySuccess', this.submitReplySuccess, this);
      window.app.vent.on('home:deleteReplySuccess', this.deleteReplySuccess, this);
      window.app.vent.on('home:deleteStatusSuccess', this.deleteStatusSuccess, this);
      window.app.vent.on('home:likeStatusSuccess', this.likeUnlikeStatusSuccess, this);
      window.app.vent.on('home:unlikeStatusSuccess', this.likeUnlikeStatusSuccess, this);
      window.app.vent.on('home:likeReplySuccess', this.likeUnlikeReplySuccess, this);
      window.app.vent.on('home:unlikeReplySuccess', this.likeUnlikeReplySuccess, this);
    },

    render: function(){
      if(window.storage.getSession('profile')){
        var headerTemplate = _.template(HeaderLoggedInTemplate);
        if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
          var headerMenuTemplate = _.template(HeaderMenuLoggedInTemplate);
        }else{
          var headerMenuTemplate = _.template(HeaderMenuLoggedInMobileTemplate);
          //var HeaderMenuTemplate = _.template("<div></div>");
        }
        this.$el.html(this.mainContentTemplate({header: headerTemplate(), headerMenu: headerMenuTemplate()}));
      }else{
        var headerTemplate = _.template(HeaderTemplate);
        if(!navigator.userAgent.match(/Android/i) && !navigator.userAgent.match(/webOS/i) && !navigator.userAgent.match(/iPhone/i)&& !navigator.userAgent.match(/iPad/i) && !navigator.userAgent.match(/iPod/i) && !navigator.userAgent.match(/BlackBerry/i) && !navigator.userAgent.match(/Windows Phone/i)) {
          var headerMenuTemplate = _.template(HeaderMenuTemplate);
        }else{
          //var headerMenuTemplate = _.template("<div></div>");
          var headerMenuTemplate = _.template(HeaderMenuTemplate);
        }
        this.$el.html(this.mainContentTemplate({header: headerTemplate(), headerMenu: headerMenuTemplate()}));
      }
      var touch   = $('#touch-menu');
      var menu  = $('.menu');
      $(touch).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
      });
      
      $(window).resize(function(){
        var w = $(window).width();
        if(w > 767 && menu.is(':hidden')) {
          menu.removeAttr('style');
        }
      });
      return this.el;
    },

    toggleMobileBookmarks : function(){
      $('.mobileMenuBookmarks .sub-menu').slideToggle();
    },

    toggleMobileMessages : function(){
      $('.mobileMenuMessages .sub-menu').slideToggle();
    },

    toggleMobileChallenges : function(){
      $('.mobileMenuChallenges .sub-menu').slideToggle();
    },

    toggleMobileSearch : function(){
      $('.mobileMenuSearch .sub-menu').slideToggle();
    },

    toggleMobileStories : function(){
      $('.mobileMenuStories .sub-menu').slideToggle();
    },

    toggleMobileFavourites : function(){
      $('.mobileMenuFavourites .sub-menu').slideToggle();
    },

    postRenderBookmark : function(results){
      var bookmarksTemplate = _.template(BookmarksTemplate);
      $('.bookmarksMenu .sub-menu').html(bookmarksTemplate({bookmarks:results.get('bookmarks')}));
    },

    postRenderFavourite : function(results){
      var favouritesTemplate = _.template(FavouritesTemplate);
      $('.favouritesMenu .sub-menu').html(favouritesTemplate({favourites:results.get('favourites')}));
    },

    showHideReplyInput : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");

      window.app.vent.trigger('home:openCloseReplyArea',thisId[1], thisId[0]);
    },

    submitStatus : function(e){
      window.app.vent.trigger('home:submitStatus',$('#submitStatus').attr('to'));
    },

    submitStatusSuccess : function(id,to_id,to_real_name,to_pen_name){
      var status = $('#submitStatus').val();
      $('#submitStatus').val('');
      if(typeof window.views.profileView != "undefined"){
        var to_real_name = window.views.profileView.thisProfile.real_name;
        var to_pen_name = window.views.profileView.thisProfile.pen_name;
      }else{
        var to_real_name = window.storage.getSession('profile').real_name;
        var to_pen_name = window.storage.getSession('profile').pen_name;
      }
      var feed = [{
        "avatar" : window.storage.getSession('profile').avatar,
        "id" : id,
        "status_by_id" : window.storage.getSession('profile').id,
        "status_to_id" : to_id,
        "by_real_name" : window.storage.getSession('profile').real_name,
        "by_pen_name" : window.storage.getSession('profile').pen_name,
        "status_to_real_name" : to_real_name,
        "status_to_pen_name" : to_pen_name,
        "status" : status,
        "status_time" : new Date().getTime(),
        "replies" : [],
        "type": "status"
      }];
      require(['text!templates/items/homeNewsfeed.html'], function(HomeNewsfeedTemplate){
        var homeNewsfeedTemplate = _.template(HomeNewsfeedTemplate);
        var homeNewsfeedTemplate2 = homeNewsfeedTemplate({feed: feed});
        $('.postsList').prepend(homeNewsfeedTemplate2);
      });
    },

    submitReplySuccess : function(id,post_id,reply){
      $('#replyAreaInput_'+post_id).val('');
      var replies = [{
        "avatar" : window.storage.getSession('profile').avatar,
        "id" : id,
        "reply_by_pen_name" : window.storage.getSession('profile').pen_name,
        "reply_by_real_name" : window.storage.getSession('profile').real_name,
        "reply_by_id" : window.storage.getSession('profile').id,
        "replied_at" : new Date().getTime(),
        "reply" : reply,
        "likes" : []
      }];
      require(['text!templates/items/postReply.html'], function(PostReplyTemplate){
        var postReplyTemplate = _.template(PostReplyTemplate);
        var postReplyTemplate2 = postReplyTemplate({replies: replies,postid: post_id});
        $('#post_'+post_id+' .repliesContainer').prepend('<div class="replies" id="reply_'+id+'">'+postReplyTemplate2+"</div>");
      });
    },

    submitReply : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      var reply = $('#replyAreaInput_'+thisId[1]).val();
      var options = [];
      options['reply'] = reply;
      options['id'] = thisId[1];
      if(reply != "" && reply){
        window.app.vent.trigger('home:sendReplyStatus',options);
      }
    },

    activeButton : function(){
      window.app.vent.trigger('menu:activeButton',window.app.getActiveElement(this));
    },

    deactivateButton : function(){
      window.app.vent.trigger('menu:deactivateButton');
    },

    goToAbout : function(){
      window.app.vent.trigger('home:goToDiv', '#descriptionSpan');
    },

    goToFeatures : function(){
      window.app.vent.trigger('home:goToDiv', '#featuresSpan');
    },

    goToRegister : function(){
      window.app.vent.trigger('home:goToDiv', '#registerSpan');
    },

    scrollToTop : function(){
      window.app.vent.trigger('site:scrollToTop');
    },

    dontRefresh : function(e){
      if(e.currentTarget.href.indexOf('/api/login/facebook') == -1 && e.currentTarget.href.indexOf('/avatars') == -1){
        e.preventDefault();
        if((e.currentTarget.href.indexOf('bloqly.com') > -1 || e.currentTarget.href.indexOf('127.0.0.1') > -1)){
          if(e.currentTarget.href.indexOf('#') == -1){
            appRouter.navigate(e.currentTarget.pathname, {trigger: true});
            //window.app.vent.trigger('home:goToDiv','.mainContent');
          }
        }else{
          window.open(e.currentTarget.href,'_blank');
        }
      }
    },

    closeDialog : function(e){
      window.app.vent.trigger('site:closeModal',e);
    },

    checkPenname : function(){
      window.app.vent.trigger('site:checkPenname');
    },

    checkEmail : function(){
      window.app.vent.trigger('site:checkEmail');
    },

    checkPasswords : function(){
      window.app.vent.trigger('site:checkPasswords');
    },

    checkName : function(){
      window.app.vent.trigger('site:checkName');
    },

    signUp : function(e){
      if((typeof e.keyCode != "undefined" && e.keyCode == 13) || e.type == "click"){
        window.app.vent.trigger('site:signUp');
      }
    },

    login : function(){
      $('.mainContainer').append(_.template(ModalTemplate));
      $('.modal').append(_.template(LoginModalTemplate));
      $('.modalBox').css('height','380px');
    },

    loginSubmit : function(e){
      if((typeof e.keyCode != "undefined" && e.keyCode == 13) || e.type == "click"){
        window.app.vent.trigger('site:loginSubmit'); 
      }
    },

    logout : function(){
      window.app.vent.trigger('site:logout');
    },

    forgotPassword : function(){
      require(['text!templates/items/forgotPassword.html'], function(ForgotPasswordTemplate){
        var forgotPasswordTemplate = _.template(ForgotPasswordTemplate);
        $('.modalBox').remove();
        $('.modal').append(forgotPasswordTemplate);
        $('.modalBox').css('height','210px');
      });
    },

    forgotPasswordSuccess : function(){
      require(['text!templates/items/forgotPasswordSuccess.html'], function(ForgotPasswordSuccessTemplate){
        var forgotPasswordSuccessTemplate = _.template(ForgotPasswordSuccessTemplate);
        $('.modalBox').remove();
        $('.modal').append(forgotPasswordSuccessTemplate);
        $('.modalBox').css('height','80px');
      });
    },

    sendNewPassword : function(){
      window.app.vent.trigger('site:sendNewPassword');
    },

    registerSuccess : function(){
      $('.modalBox').html(_.template(RegisterSuccess)).css('height','65px');
    },

    likeStatus : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      window.app.vent.trigger('home:likeStatus',thisId[1]);
    },

    likeReply : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      thisPostId = $(e.currentTarget).attr('data-parent');
      var options = [];
      options['id'] = thisId[1];
      options['thisPostId'] = thisPostId;
      window.app.vent.trigger('home:likeReply',options);
    },

    likeUnlikeStatusSuccess : function(feed){
      require(['text!templates/items/postStatus.html'], function(PostStatusTemplate){
        var postStatusTemplate = _.template(PostStatusTemplate);
        $('#post_'+feed[0].id+ " .statusContainer").html(postStatusTemplate({feed: feed}));
        Tipped.create(".likesPopup");
        $('.repliesContainer').hide();
      });
    },

    unlikeStatus : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      window.app.vent.trigger('home:unlikeStatus',thisId[1]);
    },

    unlikeReply : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      thisPostId = $(e.currentTarget).attr('data-parent');
      var options = [];
      options['id'] = thisId[1];
      options['thisPostId'] = thisPostId;
      window.app.vent.trigger('home:unlikeReply',options);
    },

    likeUnlikeReplySuccess : function(options){
      require(['text!templates/items/postReply.html'], function(PostReplyTemplate){
        var postReplyTemplate = _.template(PostReplyTemplate);
        $('#reply_'+options.reply[0].id).html(postReplyTemplate({replies: options.reply, postid: options.thisPostId}));
        Tipped.create(".likesPopup");
      });
    },

    deleteStatus : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      window.app.vent.trigger('home:deleteStatus',thisId[1]);
    },

    deleteStatusSuccess : function(id){
      $('#post_'+id).remove();
    },

    deleteReply : function(e){
      var thisId = e.currentTarget.id;
      thisId = thisId.split("_");
      window.app.vent.trigger('home:deleteReply',thisId[1]);
    },

    deleteReplySuccess : function(id){
      $('#reply_'+id).remove();
    },

    sendMessageConfirm : function(){
      var title = $('#messageTitle').val(), message = new nicEditors.findEditor('message').getContent();
      console.log('hi')
      if(title != null && message != null && title != "" && message != ""){
        var options = {};
        options.recipient = window.views.profileView.thisProfile.id;
        options.thread_title = title;
        options.message = message;
        var result = window.controllers.mainContentController.models.messagesModel.startNewMessageThread(options);

        $.when(result).done($('.modal').remove());
      }else{
        $('.sendMessageModalError').text('Please enter a title and message.');
        $('.modalBox').css('height','360px');
        setTimeout( function(){
          $('.sendMessageModalError').text('').css('height','340px');
          $('.modalBox').css('height','340px');
        },2000);
      }
    }

  });

  return mainContentView;
});