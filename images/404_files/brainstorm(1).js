define([
  'underscore',
  'backbone',
  'text!templates/pages/brainstorm.html'
], function( _, Backbone, BrainstormTemplate ){
  var brainstormView = Backbone.View.extend({
    brainstormTemplate: _.template(BrainstormTemplate),

    el : '.mainContent',

    events : {

    },

    initialize: function(){
      //this.el = $('.mainContainer');
      this.render();
    },

    render: function(){
      this.$el.html(this.brainstormTemplate());
      //this.delegateEvents();
      return this.el;
    },

  });

  return brainstormView;
});