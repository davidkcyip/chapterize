define([
  'underscore',
  'backbone',
  'text!templates/items/brainstormList.html'
], function( _, Backbone, BrainstormListTemplate){
  var brainstormListView = Backbone.View.extend({
    brainstormListTemplate: _.template(BrainstormListTemplate),

    el : '.rightContent',
    thisStory: {},

    events : {
    },

    initialize: function(result){
      this.render(result);
    },

    render: function(result){
      this.$el.html(this.brainstormListTemplate({brainstorms:result}));
      return this.el;
    }

  });

  return brainstormListView;
});