define([
	'underscore',
	'backbone',
], function(_, Backbone){

	var AppRouter = Backbone.Router.extend({
		controllers: {},
		home: {},
		profile: {},

		initialize : function(){
			
		},

		routes : {
			'' : 'homepage',
			'about' : 'about',
			'help' : 'help',
			'contact' : 'contact',
			'home' : 'home',
			'profile/:username' : 'profile',
			'profile' : 'profile',
			'verify' : 'verify',
			'activate/:id/:activation' : 'activateAccount',
			'story/new' : 'newStory',
			'story/edit/:slug/:id' : 'editStory',
			'profile/:author/story' : 'storyList',
			'story/:id/:stori' : 'story',
			'story/:id/:stori/:chapter' : 'story',
			'chapter/:story/:chapterId/:action' : 'chapter',
			'search' : 'search',
			'search/:type' : 'search',
			'search/:type/:settings' : 'search',
			'search/:type/:settings/:page' : 'search',
			'mobileLogin' : 'mobileLogin',
			'mobileRegister' : 'mobileRegister',
			'mobileForgotPassword' : 'mobileForgotPassword',
			'messages/:action' : 'messages',
			'messages/:action/:id/:slug' : 'messages',
			'brainstorm' : 'brainstorm',
			'brainstorm/:story' : 'brainstorm',
			'brainstorm/:id/:story' :'brainstorm',
			'brainstorm/:id/:story/:action' :'brainstorm',
			'brainstorm/:id/:story/:action/:thread_id/:topic' :'brainstorm',
			'*notFound': 'notFound'
		},

		homepage : function(){
			var that = this;
			require(['js/controllers/homepage'], function(HomeController){
		        window.controllers.homepage = new HomeController();
		    });
		},

		about : function(){
			require(['js/controllers/about'], function(AboutController){
		        window.controllers.about = new AboutController();
		    });
		},

		help : function(){
			require(['js/controllers/help'], function(HelpController){
		        window.controllers.help = new HelpController();
		    });
		},

		contact : function(){
			require(['js/controllers/contact'], function(ContactController){
		        window.controllers.contact = new ContactController();
		    });
		},

		home : function(){
			require(['js/controllers/home'], function(HomeController){
		        window.controllers.home = new HomeController();
		    });
		},

		profile : function(username){
			var that = this;
			if(username == null || typeof username == "undefined"){
				username = windows.storage.getSession('profile').pen_name;
			}
			require(['js/controllers/profile'], function(ProfileController){
		        window.controllers.profile = new ProfileController({username: username});
		    });
		},

		verify : function(){
			require(['js/controllers/home','js/controllers/verify'], function(HomeController,VerifyController){
		        window.controllers.home = new HomeController();
		        window.controllers.verify = new VerifyController();
		    });
		},

		activateAccount : function(id, activation_code){
			$.ajax({
				type: "POST",
	            url: '/api/activate',
	            dataType: "json",
	            data: {"id": id, "activation_code": activation_code},
	            success: function(data){
	            	console.log(data);
	            	if(data.error == 0){
	            		require(['js/controllers/homepage','js/controllers/activationSuccess'], function(HomePageController,ActivationSuccessController){
	            			window.controllers.homepage = new HomePageController();
							window.controllers.activationSuccess = new ActivationSuccessController();
						});
	            	}else{
	            		require(['js/controllers/homepage','js/controllers/activationError'], function(HomePageController,ActivationErrorController){
	            			window.controllers.homepage = new HomePageController();
							window.controllers.activationError = new ActivationErrorController();
						});
	            	}
	            }
			});
		},

		newStory : function(){
			require(['js/controllers/newStory'], function(NewStoryController){
				window.controllers.newStoryController = new NewStoryController();
			});
		},

		chapter : function(story, chapterId, action){
			if(action == "new"){
				require(['js/controllers/newChapter'], function(NewChapterController){
					var options = {};
					options.story = story;
					options.chapterId = chapterId;
					options.action = action;
					window.controllers.newChapterController = new NewChapterController(options);
				});
			}else if(action == "edit"){
				require(['js/controllers/editChapter'], function(EditChapterController){
					var options = {};
					options.story = story;
					options.chapterId = chapterId;
					options.action = action;
					window.controllers.editChapterController = new EditChapterController(options);
				});
			}
		},

		storyList : function(author){
			var options = {};
			options.author = author;
			if(typeof window.controllers.storyController != "undefined"){
				window.controllers.storyController.unbind();
				window.controllers.storyController.remove();
				console.log('zombie controller')
			}
			require(['js/controllers/story'], function(StoryController){
				window.controllers.storyController = new StoryController(options);
			});
		},

		story : function(id, stori, chapter){
			var options = {};
			options.id = id;
			options.story = stori;
			options.chapter = chapter;
			if(typeof window.controllers.storyController != "undefined"){
				window.controllers.storyController.unbind();
				window.controllers.storyController.remove();
				console.log('zombie controller')
			}
			require(['js/controllers/story'], function(StoryController){
				console.log(id)
				window.controllers.storyController = new StoryController(options);
			});
		},

		editStory : function(slug, id){
			var options = {};
			options.slug = slug;
			options.id = id;
			if(typeof window.controllers.editStoryController != "undefined"){
				window.controllers.editStoryController.unbind();
				window.controllers.editStoryController.remove();
			}
			require(['js/controllers/editStory'], function(EditStoryController){
				window.controllers.editStoryController = new EditStoryController(options);
			});
		},

		search : function(type, settings, page){
			var options = {};
			options.type = type;
			options.settings = settings;
			options.page = page;
			console.log(page);
			if(typeof window.controllers.searchController != "undefined"){
				window.controllers.searchController.remove();
			}
			require(['js/controllers/search'], function(SearchController){
				window.controllers.searchController = new SearchController(options);
			});
		},

		mobileLogin : function(){
			if(typeof window.controllers.mobileLoginController != "undefined"){
				window.controllers.mobileLoginController.remove();
			}
			require(['js/controllers/mobileLogin'], function(MobileLoginController){
				window.controllers.mobileLoginController = new MobileLoginController();
			});
		},

		mobileRegister : function(){
			if(typeof window.controllers.mobileRegisterController != "undefined"){
				window.controllers.mobileRegisterController.remove();
			}
			require(['js/controllers/mobileRegister'], function(MobileRegisterController){
				window.controllers.mobileRegisterController = new MobileRegisterController();
			});
		},

		mobileForgotPassword : function(){
			if(typeof window.controllers.mobileForgotPasswordController != "undefined"){
				window.controllers.mobileForgotPasswordController.remove();
			}
			require(['js/controllers/mobileForgotPassword'], function(MobileForgotPasswordController){
				window.controllers.mobileForgotPasswordController = new MobileForgotPasswordController();
			});
		},

		messages : function(action, id, slug){
			if(typeof window.controllers.messagesController != "undefined"){
				window.controllers.messagesController.remove();
			}
			var options = {};
			options.action = action;
			options.slug = slug;
			options.id = id;
			require(['js/controllers/messages'], function(MessagesController){
				window.controllers.messagesController = new MessagesController(options);
			});
		},

		brainstorm : function(id, story, action, thread_id, topic){
			if(typeof window.controllers.brainstormController != "undefined"){
				window.controllers.brainstormController.remove();
			}
			var options = {};
			options.id = id;
			options.story = story;
			options.topic = topic;
			options.action = action;
			options.thread_id = thread_id;

			require(['js/controllers/brainstorm'], function(BrainstormController){
				window.controllers.brainstormController = new BrainstormController(options);
			});
		},

		notFound : function(){
			require(['js/controllers/notFound'], function(notFoundController){
		        window.controllers.notFound = new notFoundController();
		    });
		}

	});
    

	return AppRouter;
});