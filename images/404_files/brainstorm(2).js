define([
	'underscore',
	'backbone'
], function(_, Backbone){
	var BrainstormModel = Backbone.Model.extend({
		
		getMyBrainstorms : function(){
			var that = this;
			$.ajax({
		        type: 'GET',
		        url: '/api/brainstorm/get-mine',
		            dataType: "json",
		            success: function(data){
		              that.set('myBrainstorms',data.results);
		            }
		      });
		},

		getMyBrainstormList : function(){
			var that = this;
			$.ajax({
		        type: 'GET',
		        url: '/api/brainstorm/get-my-list',
		            dataType: "json",
		            success: function(data){
		              that.set('myBrainstormList',data.results);
		            }
		      });
		},

		getStoryBrainstormTopics : function(options){
			var that = this;
			$.ajax({
		        type: 'GET',
		        url: '/api/brainstorm/get',
		        data: {"id": options.id, "slug": options.story},
	            dataType: "json",
	            success: function(data){
	              that.set('storyBrainstorms',data.results);
	            }
		    });
		},

		getBrainstormTopic : function(options){
			var that = this;
			$.ajax({
		        type: 'GET',
		        url: '/api/brainstorm/topic/get',
		        data: {"id": options.id, "slug": options.story, "thread_id": options.thread_id},
	            dataType: "json",
	            success: function(data){
	              that.set('brainstormTopic',data.results);
	            }
		    });
		},

		startNewTopic : function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/brainstorm/new-topic',
		        async: false,
		        data: {"title": options.title, "message":options.message, "category":options.category,"story_id":options.story_id},
		            dataType: "json",
		            success: function(data){
		              data2 = data;
		            }

		      });
			return data2;
		},

		replyTopic: function(options){
			var that = this;
			var data2 = {};
			$.ajax({
		        type: 'POST',
		        url: '/api/brainstorm/topic/reply',
		        async: false,
		        data: {"message": options.message, "story_id":options.story_id, "thread_id":options.thread_id},
		            dataType: "json",
		            success: function(data){
		              data2 = data;
		            }

		      });
			return data2;
		}
	});

	var BrainstormCollection = Backbone.Collection.extend({
		model: BrainstormModel,

	});

	return {
		BrainstormModel : BrainstormModel,
		BrainstormCollection : BrainstormCollection
	};
});