define([
  'amplify'
], function(amplify){

  var initStorage = function(){
    window.siteStorage = amplify.store();
  }

  var set = function(key,value){
    window.amplify.store(key,value);
  };

  var get = function(key){
    return window.amplify.store(key);
  };

  var setSession = function(key,value){
    window.amplify.store.sessionStorage(key,value);
  };

  var getSession = function(key){
    return window.amplify.store.sessionStorage(key);
  };

  var destroy = function(key){
    window.amplify.store(key,null);
  };

  var destroyAll = function(){
    _.each(window.amplify.store(),function(value,key){
      window.amplify.store(key,null);
    });
  };

  var destroySession = function(key){
    window.amplify.store.sessionStorage(key,null);
  };

  var destroyAllSession = function(){
    _.each(window.amplify.store.sessionStorage(),function(value,key){
      window.amplify.store.sessionStorage(key,null);
    });
  };

  var subscribe = function(channel){
    window.amplify.subscribe(channel, function(data){
      console.log(data);
    });
  };

  var publish = function(channel, message){
    window.amplify.publish(channel, message);
  };

  var unsubscribe = function(channel){
    window.amplify.unsubscribe(channel);
  };

  return {
    set : set,
    get : get,
    setSession : setSession,
    getSession : getSession,
    initStorage : initStorage,
    destroy : destroy,
    destroyAll : destroyAll,
    destroySession : destroySession,
    destroyAllSession : destroyAllSession,
    subscribe : subscribe,
    publish : publish,
    unsubscribe : unsubscribe
  };

});