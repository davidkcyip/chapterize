define([
  'underscore',
  'backbone',
  'text!templates/pages/notFound.html'
], function( _, Backbone, notFoundTemplate ){
  var notFoundView = Backbone.View.extend({
    notFoundTemplate: _.template(notFoundTemplate),

    el : '.mainContent',

    initialize: function(){
      $('.breadcrumbs #second').html("404 <i class='fa fa-angle-double-right'></i>");
      $('.breadcrumbs #third').html("");
      $('.breadcrumbs #fourth').html("");
      $('.breadcrumbs #fifth').html("");
      document.title = "404";

      this.render();
    },

    render: function(){
      this.$el.html(this.notFoundTemplate);
      return this.el;
    }
  });

  return notFoundView;
});