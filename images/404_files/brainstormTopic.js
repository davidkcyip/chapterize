define([
  'underscore',
  'backbone',
  'text!templates/items/brainstormTopic.html',
  'text!templates/items/brainstormTopicList.html'
], function( _, Backbone, BrainstormTopicTemplate, BrainstormTopicListTemplate){
  var brainstormTopicView = Backbone.View.extend({
    brainstormTopicTemplate: _.template(BrainstormTopicTemplate),
    brainstormTopicListTemplate: _.template(BrainstormTopicListTemplate),

    el : '.rightContent',
    thisStory: {},
    topicOptions:{},

    events : {
      'click #replyTopicButton' : 'replyTopic'
    },

    initialize: function(result,brainstorms,options){
      this.topicOptions = options;
      this.render(result,brainstorms);
    },

    render: function(result,brainstorms){
      this.$el.html(this.brainstormTopicTemplate({topic:result}));
      $('.leftContent').prepend(this.brainstormTopicListTemplate({brainstorms:brainstorms}));
      var nic = new nicEditor();
      nic.addInstance('replyTopic');
      return this.el;
    },

    replyTopic: function(){
      var that = this;
      var message = new nicEditors.findEditor('replyTopic').getContent();
      if(message != null && message != "" && typeof message != "undefined"){
        var options = {};
        options.message = message;
        options.story_id = this.topicOptions.id;
        options.thread_id = this.topicOptions.thread_id;
        window.controllers.brainstormController.models.brainstormModel.replyTopic(options);
        Backbone.history.loadUrl(Backbone.history.fragment, true);
      }
    }

  });

  return brainstormTopicView;
});