<?php
require('app/core/autoloader.php');

Router::get('/', 'index@index');
Router::get('/test', 'test@index');
Router::get('/checkLogin','user@isLoggedIn');

//registration and login
Router::post('/register','user@register');
Router::get('/login/facebook', 'user@facebook');
Router::get('/login/compareSession','user@compareSession');
Router::post('/login','user@login');
Router::get('/logout','user@logout');
Router::get('/verify/penname/(:any)','user@checkPennameExists');
Router::get('/verify/email/(:any)','user@checkEmailExists');
Router::get('/forgotPassword','user@forgotPassword');
Router::get('/update/pennamePassword','user@updatePennamePassword');
Router::post('/activate','user@activateUser');
Router::get('/getProfile/(:any)','user@getProfile');
Router::get('/getProfile','user@getOwnProfile');
Router::post('/user/edit','user@editProfile');
Router::get('/user/followers/(:any)', 'user@getFollowers');
Router::get('/user/following/(:any)', 'user@getFollowing');
Router::post('/user/email/change', 'user@changeEmail');
Router::post('/user/password/change', 'user@changePassword');

Router::get('/user/get-all/(:any)/(:any)','user@getAllProfiles');
Router::get('/user/get-recent/(:any)/(:any)','user@getRecentProfiles');
Router::get('/user/get-popular/(:any)/(:any)','user@getPopularProfiles');

Router::get('/categories', 'story@getCategories');
Router::get('/storyTypes', 'story@getStoryTypes');

Router::get('/feed/getInitial/(:any)/(:any)/(:any)','feed@getInitialItems');
Router::get('/feed/getStoryUpdates','feed@getStoryUpdates');
Router::get('/feed/getSubscriptionsList/(:any)', 'feed@getSubscriptionsList');
Router::get('/feed/status','feed@getIndividualStatus');
Router::post('/feed/repost','feed@repost');
Router::post('/status/sendReply','feed@sendReplyStatus');
Router::post('/status/submit','feed@submitStatus');
Router::post('/reply/delete','feed@deleteReply');
Router::post('/status/delete','feed@deleteStatus');
Router::post('/reply/like','feed@likeReply');
Router::post('/status/like','feed@likeStatus');
Router::post('/reply/unlike','feed@unlikeReply');
Router::post('/status/unlike','feed@unlikeStatus');
Router::get('/getBasicStats/(:any)','user@getSubscriptionAndStoryStats');
Router::post('/uploadAvatar','user@uploadAvatar');
Router::post('/cropAvatar','user@cropAvatar');
Router::post('/uploadAlbumInitial','user@uploadAlbumInitial');
Router::post('/cropAlbumInitial','user@cropAlbumInitial');
Router::get('/reply/get/(:any)','feed@getReply');
Router::get('/status/get/(:any)','feed@getStatus');

Router::get('/story/author/get/(:any)/(:any)', 'story@getStoriesFromAuthor');
Router::get('/story/id/get/(:any)', 'story@getStoriesFromId');
Router::post('/story/new/draft/save', 'story@initialSaveDraft');
Router::post('/story/new/publish', 'story@initialPublish');
Router::get('/story/authorId/basic/(:any)', 'story@getBasicStoryList');
Router::post('/story/actions/(:any)','story@storyActions');

Router::post('/chapter/rate','story@rateChapter');
Router::post('/chapter/comment','story@commentChapter');
Router::get('/chapters/comment/getLatestComments','story@getLatestChapterComments');
Router::get('/chapter/get/(:any)','story@getChapter');
Router::get('/chapters/author/get/(:any)/(:any)','story@getChaptersFromAuthor');
Router::post('/chapter/actions/(:any)','story@savePublishChapter');

Router::get('/bookmarks/get/(:any)', 'story@getUserBookmarks');
Router::get('/favourites/get/(:any)', 'story@getUserFavourites');

Router::get('/story/get-all/(:any)/(:any)','story@getAllStories');
Router::get('/story/get-all/(:any)/(:any)/(:any)','story@getAllStories');
Router::get('/story/get-popular/(:any)/(:any)','story@getPopularStories');
Router::get('/story/get-homepage-popular/(:any)/(:any)','story@getHomepagePopularStories');
Router::get('/story/get-popular/(:any)/(:any)/(:any)','story@getPopularStories');
Router::get('/story/get-homepage-popular/(:any)/(:any)/(:any)','story@getHomepagePopularStories');
Router::get('/story/get-recent/(:any)/(:any)','story@getRecentStories');
Router::get('/story/get-recent/(:any)/(:any)/(:any)','story@getRecentStories');

Router::get('/chapter/get-all/(:any)/(:any)','story@getAllChapters');
Router::get('/chapter/get-recent/(:any)/(:any)','story@getRecentChapters');
Router::get('/chapter/get-popular/(:any)/(:any)','story@getPopularChapters');

Router::post('/user/followUnfollow','user@followUnfollow');

Router::get('/search/search-all','search@searchResults');
Router::get('/search/all-tags','search@searchAllTags');
Router::get('/search/tag-stories/(:any)','search@searchTagsStories');

Router::post('/messages/thread/new','message@startNewThread');
Router::get('/messages/message/inbox','message@getInbox');
Router::get('/messages/message/sent','message@getSent');
Router::post('/messages/thread/get','message@getThread');
Router::post('/messages/message/send','message@sendMessage');
Router::get('/messages/message/getUnreadNumber','message@getNumberOfUnreadMessages');

Router::get('/users/get-user','user@autocomplete');

Router::get('/user/fbtest','user@test');
Router::post('/user/report','user@reportUser');

Router::get('/user/suggestions','user@getFollowSuggestions');

Router::get('/brainstorm/get-mine','brainstorm@getMine');
Router::get('/brainstorm/get-my-list','brainstorm@getMyList');
Router::get('/brainstorm/get','brainstorm@getBrainstorm');
Router::get('/brainstorm/categories','brainstorm@brainstormCategories');
Router::get('/brainstorm/getStoryDetails/(:any)','story@getStory');
Router::post('/brainstorm/new-topic','brainstorm@newTopic');
Router::get('/brainstorm/topic/get','brainstorm@getTopic');
Router::post('/brainstorm/topic/reply','brainstorm@replyTopic');
Router::post('/brainstorm/subscribe','brainstorm@subscribe');
Router::post('/brainstorm/unsubscribe','brainstorm@unsubscribe');
Router::get('/brainstorm/haveISubscribed','brainstorm@haveISubscribed');
Router::post('/brainstorm/topic/subscribe','brainstorm@subscribeTopic');
Router::post('/brainstorm/topic/unsubscribe','brainstorm@unsubscribeTopic');

Router::post('/challenge/new','challenge@newChallenge');
Router::post('/challenge/edit','challenge@editChallenge');
Router::get('/challenge/active/(:any)/(:any)','challenge@activeChallenge');
Router::get('/challenge/previous/(:any)/(:any)','challenge@previousChallenge');
Router::get('/challenge/mine/(:any)/(:any)','challenge@myChallenge');
Router::get('/challenge/winners/(:any)/(:any)','challenge@challengeWinners');
Router::get('/challenge/get/(:any)','challenge@getChallenge');
Router::post('/challenge/submit','challenge@submitChallenge');
Router::post('/challenge/comment','challenge@commentChallenge');
Router::get('/challenge/calculateWinner','challenge@checkChallengeWinners');
Router::post('/challenge/details','challenge@getChallengeDetails');
Router::post('/challenge/subscribe','challenge@subscribeChallenge');
Router::post('/challenge/unsubscribe','challenge@unsubscribeChallenge');
Router::post('/challenge/leaderboard/(:any)','challenge@challengeLeaderboard');

Router::post('/settings/email/save', 'settings@updateEmailSettings');
Router::get('/settings/email/get', 'settings@getEmailSettings');

Router::get('/notifications/new','notification@getNewNotifications');
Router::get('/notifications/unread','notification@getAllUnreadNotifications');
Router::get('/notifications/all','notification@getAllNotifications');
Router::get('/notifications/read','notification@readNotification');

Router::get('/circles/mycircles','circle@mycircles');
Router::post('/circles/new','circle@newCircle');
Router::post('/circles/edit','circle@editCircle');
Router::post('/circles/details','circle@circleDetails');
Router::get('/circles/categories','circle@getCategories');
Router::post('/circles/browseCircles','circle@browseCircles');
Router::post('/circles/searchCircles','circle@searchCircles');
Router::post('/circles/topic/new','circle@newTopic');
Router::post('/circles/topic/edit','circle@editTopic');
Router::post('/circles/topic','circle@topicDetails');
Router::post('/circles/join','circle@joinCircle');
Router::post('/circles/unjoin','circle@unjoinCircle');
Router::post('/circles/request','circle@requestJoinCircle');
Router::post('/circles/unrequest','circle@unjoinCircle');
Router::post('/circles/topic/post/edit','circle@editComment');
Router::post('/circles/topic/post/delete','circle@deleteComment');
Router::post('/circles/topic/post/reply','circle@replyComment');
Router::post('/circles/topic/subscribe','circle@subscribeTopic');
Router::post('/circles/topic/unsubscribe','circle@unsubscribeTopic');
Router::post('/circles/topic/pin','circle@pinTopic');
Router::post('/circles/topic/unpin','circle@unpinTopic');
Router::post('/circles/members','circle@getCircleMembers');
Router::post('/circles/members/makeAdmin','circle@makeAdmin');
Router::post('/circles/members/revokeAdmin','circle@revokeAdmin');
Router::post('/circles/members/approveMember','circle@approveMember');
Router::post('/circles/members/removeMember','circle@removeMember');

Router::post('/send-message','message@contactForm');
//if no route found
Router::error('error@index');

Router::get('/transferStories','test@insertStories');
Router::get('/transferComments','test@transferComments');
Router::get('/transferUsers','test@transferUsers');
Router::get('/transferTags','test@transferTags');
Router::get('/transferChallenges','test@transferChallenges');
Router::get('/transferFollows','test@transferFollows');

//execute matched routes
Router::dispatch();
ob_flush();
