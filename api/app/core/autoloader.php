<?php
//turn on output buffering
ob_start();

function autoloader($class) {

   $filename = "app/controllers/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

   $filename = "app/core/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

   $filename = "app/helpers/".strtolower($class).".php";
   if(file_exists($filename)){
      require $filename;
   }

}
require_once 'Mail.php';
require_once 'Mail/mime.php';
//run autoloader
spl_autoload_register('autoloader');
//start sessions
Session::init();
$uri = $_SERVER['SERVER_NAME'];
if($uri == "bloqly.dev" || $uri == "localhost" || $uri == "127.0.0.1"){
   require('app/core/config-localhost.php');
}else{
   require('app/core/config.php');
}
Session::set('hybridConfig','hybrid/hybridauth/config.php');
require( "hybrid/hybridauth/Hybrid/Auth.php" );
