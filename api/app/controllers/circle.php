<?php

class Circle extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function getCategories(){
		$circle_model = $this->loadModel('circle_model');

		$categories = $circle_model->getCategories();

		echo json_encode(array("success"=>1,"results"=>$categories));
	}

	public function mycircles(){
		$me = Session::get('profile')->id;

		$circle_model = $this->loadModel('circle_model');

		$mycircles = $circle_model->getMyCircles($me);

		echo json_encode(array("success"=>1,"results"=>$mycircles));
	}

	public function newCircle(){
		$circle_name = $_POST['circle_name'];
		$circle_description = $_POST['circle_description'];
		$circle_type = $_POST['circle_type'];
		$circle_album = $_POST['circle_album'];
		$circle_category = $_POST['circle_category'];
		$circle_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $circle_name);
		$circle_slug = str_replace(" ", "-", strtolower($circle_slug));
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$array_to_insert = array("group_category"=>$circle_category, "group_name"=>$circle_name,"group_owner"=>$me, "group_description"=>$circle_description, "group_cover"=>$circle_album, "group_type"=>$circle_type, "slug"=>$circle_slug);

		$circle_id = $user_model->insertIntoTable($array_to_insert,"circle");

		$array_to_insert = array("group_id"=>$circle_id, "author_id"=>$me, "is_admin"=>1);

		$circle_user_id = $user_model->insertIntoTable($array_to_insert,"circle_user");

		echo json_encode(array("success"=>1,"circle_id"=>$circle_id,"slug"=>$circle_slug));
	}

	public function editCircle(){
		$circle_name = $_POST['circle_name'];
		$circle_description = $_POST['circle_description'];
		$circle_type = $_POST['circle_type'];
		$circle_album = $_POST['circle_album'];
		$circle_id = $_POST['circle_id'];
		$circle_category = $_POST['circle_category'];
		$circle_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $circle_name);
		$circle_slug = str_replace(" ", "-", strtolower($circle_slug));
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		//get owner of circle and if not me then don't allow edit
		$circle_model = $this->loadModel('circle_model');
		$me = Session::get('profile')->id;
		$circle_owner = $circle_model->getCircleOWner($circle_id);
		if($me == $circle_owner){
			$array_to_update = array("group_category"=>$circle_category, "group_name"=>$circle_name, "group_description"=>$circle_description, "group_cover"=>$circle_album, "group_type"=>$circle_type, "slug"=>$circle_slug);
			$where = array("id"=>$circle_id);

			$user_model->updateTable($array_to_update,$where,"circle");

			echo json_encode(array("success"=>1,"circle_id"=>$circle_id,"slug"=>$circle_slug));
		}
	}

	public function circleDetails(){
		$circle_id = $_POST['circle_id'];
		$me = Session::get('profile')->id;

		$circle_model = $this->loadModel('circle_model');

		$circle_details = $circle_model->getCircleDetails($circle_id,$me);

		foreach($circle_details as $circle){
			//get topics
			$latest_post = $circle_model->getTopicsDetails($circle_id);

			$circle->topics = $latest_post;

			$i = 1;
			foreach($circle->topics as $topic){
				$page_num = ceil($i/10);
				$topic->page = $page_num;
				$i++;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$circle_details));
	}

	public function topicDetails(){
		$circle_id = $_POST['circle_id'];
		$topic_id = $_POST['topic_id'];
		$me = Session::get('profile')->id;

		$circle_model = $this->loadModel('circle_model');

		$topic_details = $circle_model->getTopicDetails($circle_id,$topic_id,$me);

		foreach($topic_details as $topic){
			$posts = $circle_model->getTopicPosts($topic->topic_id);

			$topic->posts = $posts;

			$i = 1;
			foreach($topic->posts as $post){
				$page_num = ceil($i/10);
				$post->page = $page_num;
				if($post->reply_id != null){
					$reply = $circle_model->getReplies($post->reply_id);
					$post->reply = $reply;
				}
				$i++;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$topic_details));
	}

	public function searchCircles(){
		$term = $_POST['term'];

		$circle_model = $this->loadModel('circle_model');

		$results = $circle_model->searchCircles($term);
		
		$i = 1;
		foreach($results as $result){
			$page_num = ceil($i/10);
			$result->page = $page_num;
			$i++;
		}

		echo json_encode(array("success"=>1,"results"=>$results));
	}

	public function newTopic(){
		$circle_id = $_POST['circle_id'];
		$topic_name = $_POST['topic_name'];
		$topic_body = $_POST['topic_body'];
		$topic_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $topic_name);
		$topic_slug = str_replace(" ", "-", strtolower($topic_slug));
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$array_to_insert = array("group_id"=>$circle_id, "topic_name"=>$topic_name, "topic_started_by"=>$me, "is_pinned"=>0, "view_count"=>0, "slug"=>$topic_slug);
		$topic_id = $user_model->insertIntoTable($array_to_insert,"circle_topic");

		$array_to_insert = array("group_topic_id"=>$topic_id, "post_content"=>$topic_body, "posted_by"=>$me);
		$topic_post_id = $user_model->insertIntoTable($array_to_insert,"circle_topic_post");

		echo json_encode(array("success"=>1, "slug"=>$topic_slug, "id"=>$topic_id));
	}

	public function editTopic(){
		$circle_id = $_POST['circle_id'];
		$topic_name = $_POST['topic_name'];
		$old_topic_id = $_POST['old_topic_id'];
		$topic_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $topic_name);
		$topic_slug = str_replace(" ", "-", strtolower($topic_slug));

		$user_model = $this->loadModel('user_model');

		//get topic owner and if not me then don't allow edit
		$circle_model = $this->loadModel('circle_model');
		$me = Session::get('profile')->id;
		$topic_owner = $circle_model->getTopicOwner($old_topic_id);
		if($topic_owner == $me){
			$array_to_update = array("topic_name"=>$topic_name);
			$where = array("group_id"=>$circle_id,"id"=>$old_topic_id);
			$user_model->updateTable($array_to_update,$where,"circle_topic");

			$array_to_update = array("slug"=>$topic_slug);
			$where = array("group_id"=>$circle_id,"id"=>$old_topic_id);
			$user_model->updateTable($array_to_update,$where,"circle_topic");

			echo json_encode(array("success"=>1,"slug"=>$topic_slug));
		}
	}

	public function browseCircles(){
		$circle_model = $this->loadModel('circle_model');
		$type = $_POST['type'];
		$order = $_POST['order']; // if order null then get most recent active

		if($type == "overview"){
			$official = $circle_model->getOfficialCircles();
			$i = 1;
			foreach($official as $o){
				$page_num = ceil($i/10);
				$o->page = $page_num;
				$i++;
			}

			$active = $circle_model->getActiveCircles();
			$i = 1;
			foreach($active as $o){
				$page_num = ceil($i/10);
				$o->page = $page_num;
				$i++;
			}

			$popular = $circle_model->getPopularCircles();
			$i = 1;
			foreach($popular as $o){
				$page_num = ceil($i/10);
				$o->page = $page_num;
				$i++;
			}

			echo json_encode(array("success"=>1,"official"=>$official,"active"=>$active,"popular"=>$popular));
		}else{
			if($order == "active"){
				$active = $circle_model->getActiveCircles($type);
				$i = 1;
				foreach($active as $o){
					$page_num = ceil($i/10);
					$o->page = $page_num;
					$i++;
				}
				echo json_encode(array("success"=>1,"results"=>$active));
			}else if($order == "popular"){
				$popular = $circle_model->getPopularCircles($type);
				$i = 1;
				foreach($popular as $o){
					$page_num = ceil($i/10);
					$o->page = $page_num;
					$i++;
				}
				echo json_encode(array("success"=>1,"results"=>$popular));
			}else if($order == "new"){
				$newest = $circle_model->getNewestCircles($type);
				$i = 1;
				foreach($newest as $o){
					$page_num = ceil($i/10);
					$o->page = $page_num;
					$i++;
				}
				echo json_encode(array("success"=>1,"results"=>$newest));
			}
		}

	}

	public function joinCircle(){
		$me = Session::get('profile')->id;
		$id = $_POST['id'];
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("group_id"=>$id,"author_id"=>$me,"is_admin"=>0,"status"=>1);
		$user_model->insertIntoTable($array_to_insert,"circle_user");
	}

	public function unjoinCircle(){
		$me = Session::get('profile')->id;
		$id = $_POST['id'];
		$user_model = $this->loadModel('user_model');
		$array_to_delete = array("group_id"=>$id,"author_id"=>$me);
		$user_model->deleteRows($array_to_delete,"circle_user");
	}

	public function requestJoinCircle(){
		$me = Session::get('profile')->id;
		$id = $_POST['id'];
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("group_id"=>$id,"author_id"=>$me,"is_admin"=>0,"status"=>0);
		$user_model->insertIntoTable($array_to_insert,"circle_user");

		//alert all admins someone has requested
		$circle_model = $this->loadModel('circle_model');
		$admins = $circle_model->getAllCircleAdmins($id);
		foreach($admins as $admin){
			$array_to_insert = array("activity_by"=>$me, "activity_type_id"=>24, "action_id"=>$id, "user_to_notify"=>$admin->author_id, "is_read"=>0);
			$user_model->insertIntoTable($array_to_insert,"notification");
			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $user_model->returnUserById($admin->author_id);
			$circle_details = $circle_model->getCircleDetailsById($id);
			//check if owner has subscribed to new brainstorm email alert
			$has_subscribed = $notification_model->checkIfSubscribed("24",$admin->author_id);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/circle/".$id."/".$circle_details->slug."/manage-users";
				$circle_request_admin_email = $email_model->circleRequestEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$circle_details->group_name,$status_url);
				$title = Session::get('profile')->real_name." has requested to join ".$circle_details->group_name;
				$newAccountEmail = $email_model->sendEmail($circle_request_admin_email, $title, $user_to_alert->email);
			}
			//email notification end
		}
	}

	public function editComment(){
		$post_id = $_POST['post_id'];
		$edited_comment = $_POST['edited_comment'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("post_content"=>$edited_comment);
		$where = array("id"=>$post_id);
		$user_model->updateTable($array_to_update,$where,"circle_topic_post");
	}

	public function replyComment(){
		$topic_id = $_POST['topic_id'];
		$post = $_POST['post'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("group_topic_id"=>$topic_id, "post_content"=>$post, "posted_by"=>$me);
		$comment_id = $user_model->insertIntoTable($array_to_insert,"circle_topic_post");

		//get everyone who has subscibed to this topic and notify
		$circle_model = $this->loadModel('circle_model');
		$users_to_notify = $circle_model->getTopicSubscribers($topic_id);
		foreach($users_to_notify as $user){
			if($user->subscribed_by != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>22,"action_id"=>$comment_id,"user_to_notify"=>$user->subscribed_by,"is_read"=>0);
				$user_model->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$user_to_alert = $user_model->returnUserById($user->subscribed_by);
				$circle_details = $circle_model->getCircleByTopicId($topic_id);
				$topic_details = $circle_model->getTopicDetailsById($topic_id);
				//check if owner has subscribed to new brainstorm email alert
				$has_subscribed = $notification_model->checkIfSubscribed("22",$user->subscribed_by);
				if($has_subscribed == 1){
					$status_url = "http://www.bloqly.com/circle/".$circle_details->id."/".$circle_details->slug."/topic/".$topic_id."/".$topic_details->slug;
					$new_circle_topic_email = $email_model->newCircleTopicReplyEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$circle_details->group_name,$status_url);
					$title = Session::get('profile')->real_name." has replied to a Circle topic you follow";
					$newAccountEmail = $email_model->sendEmail($new_circle_topic_email, $title, $user_to_alert->email);
				}
				//email notification end
			}
		}

		echo json_encode(array("success"=>1,"id"=>$comment_id,"users"=>$users_to_notify));
	}

	public function deleteComment(){
		$post_id = $_POST['post_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_delete = array("id"=>$post_id);
		$user_model->deleteRows($array_to_delete,"circle_topic_post");

		//delete notifications
		//get topic id
		$array_to_delete = array("activity_type_id"=>22,"action_id"=>$post_id);
		$user_model->deleteRows($array_to_delete,"notification");
	}

	public function subscribeTopic(){
		$topic_id = $_POST['topic_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("subscription_type"=>7, "item_subscribed_to"=>$topic_id, "subscribed_by"=>$me);
		$user_model->insertIntoTable($array_to_insert,"subscription");
	}

	public function unsubscribeTopic(){
		$topic_id = $_POST['topic_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$array_to_delete = array("subscription_type"=>7, "item_subscribed_to"=>$topic_id, "subscribed_by"=>$me);
		$user_model->deleteRows($array_to_delete,"subscription");
	}

	public function pinTopic(){
		$topic_id = $_POST['topic_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("is_pinned"=>1);
		$where = array("id"=>$topic_id);
		$user_model->updateTable($array_to_update,$where,"circle_topic");
	}

	public function unpinTopic(){
		$topic_id = $_POST['topic_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("is_pinned"=>0);
		$where = array("id"=>$topic_id);
		$user_model->updateTable($array_to_update,$where,"circle_topic");
	}

	public function getCircleMembers(){
		$circle_id = $_POST['circle_id'];
		$me = Session::get('profile')->id;
		$circle_model = $this->loadModel('circle_model');
		$members = $circle_model->getCircleMembers($circle_id);
		echo json_encode(array("success"=>1,"results"=>$members));
	}

	public function makeAdmin(){
		$circle_id = $_POST['circle_id'];
		$author_id = $_POST['author_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("is_admin"=>1);
		$where = array("group_id"=>$circle_id,"author_id"=>$author_id);
		$user_model->updateTable($array_to_update,$where,"circle_user");
	}

	public function revokeAdmin(){
		$circle_id = $_POST['circle_id'];
		$author_id = $_POST['author_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("is_admin"=>0);
		$where = array("group_id"=>$circle_id,"author_id"=>$author_id);
		$user_model->updateTable($array_to_update,$where,"circle_user");
	}

	public function approveMember(){
		$circle_id = $_POST['circle_id'];
		$author_id = $_POST['author_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_update = array("status"=>1);
		$me = Session::get('profile')->id;
		$where = array("group_id"=>$circle_id,"author_id"=>$author_id);
		$user_model->updateTable($array_to_update,$where,"circle_user");

		//alert user that they've been accepted
		$array_to_insert = array("activity_by"=>$me, "activity_type_id"=>23, "action_id"=>$circle_id, "user_to_notify"=>$author_id, "is_read"=>0);
		$user_model->insertIntoTable($array_to_insert,"notification");
		//email_notification
		$email_model = $this->loadModel('email_model');
		$notification_model = $this->loadModel('notification_model');
		$circle_model = $this->loadModel('circle_model');
		$user_to_alert = $user_model->returnUserById($author_id);
		$circle_details = $circle_model->getCircleDetailsById($circle_id);
		//check if owner has subscribed to new brainstorm email alert
		$has_subscribed = $notification_model->checkIfSubscribed("23",$author_id);
		if($has_subscribed == 1){
			$status_url = "http://www.bloqly.com/circle/".$circle_id."/".$circle_details->slug;
			$approve_circle_request_email = $email_model->approveCircleRequestEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$circle_details->group_name,$status_url);
			$title = Session::get('profile')->real_name." has approved your request to join ".$circle_details->group_name;
			$newAccountEmail = $email_model->sendEmail($approve_circle_request_email, $title, $user_to_alert->email);
		}
		//email notification end
	}

	public function removeMember(){
		$circle_id = $_POST['circle_id'];
		$author_id = $_POST['author_id'];
		$user_model = $this->loadModel('user_model');
		$array_to_remove = array("group_id"=>$circle_id,"author_id"=>$author_id);
		$user_model->deleteRows($array_to_remove,"circle_user");
	}

}