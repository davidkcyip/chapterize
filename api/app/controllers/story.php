<?php

class Story extends Controller{

	public function __construct(){
		parent::__construct();
	}

	function fixForUri($string){
	 $slug = trim($string); // trim the string
	 $slug= preg_replace('/[^a-zA-Z0-9 -]/','',$slug ); // only take alphanumerical characters, but keep the spaces and dashes too...
	 $slug= str_replace(' ','-', $slug); // replace spaces by dashes
	 $slug= strtolower($slug);  // make it lowercase
	 return $slug;
	}

	public function getLatestChapterComments(){
		$story = $this->loadModel('story_model');

		$latest_comments = $story->getLatestChapterComments();

		echo json_encode(array("success"=>1,"results"=>$latest_comments));
	}

	public function getCategories($request = null){
		$story = $this->loadModel('story_model');

		$categories = $story->returnCategories();

		echo json_encode($categories);
	}

	public function getStoryTypes($request = null){
		$story = $this->loadModel('story_model');

		$storyTypes = $story->returnStoryTypes();

		echo json_encode($storyTypes);
	}

	public function initialSaveDraft(){
		$save = $this->loadModel('user_model');

		$story_name = $_POST['story_name'];
		$story_description = $_POST['story_description'];
		$story_type = $_POST['story_type'];
		$story_category = $_POST['story_category'];
		$written_by = $_POST['written_by'];
		$is_public = $_POST['is_public'];
		$tags = $_POST['tags'];
		$album = $_POST['album'];
		$tags = explode(",", $tags);
		$tags_slug = array();
		foreach($tags as $tag1){
			array_push($tags_slug, str_replace(" ", "-", strtolower($tag1)));
		}
		$id = $_POST['id'];
		$chapters = $_POST['chapters'];
		$slug = self::fixForUri($story_name);
		if($id != null){
			//story
			$array_to_update = array("album"=>$album,"story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>$story_type,"written_by"=>$written_by,"is_public"=>$is_public,"slug"=>$slug);
			$where = array("id"=>$id);
			$response = $save->updateTable($array_to_update,$where,"story");
			//chapter
			$array_to_update2 = array("chapter_name"=>$chapters['chapter_name'], "written_by"=>$chapters['written_by'], "chapter_body"=>$chapters['chapter_body'], "status"=>0, "slug"=>str_replace(" ", "-", strtolower($chapters['chapter_name'])));
			$where2 = array("id"=>$chapters['id']);
			$response2 = $save->updateTable($array_to_update2,$where2,"chapter");
			//tags, delete tags first
			$row_to_delete = array("story_id"=>$id);
			$response3 = $save->deleteRows($row_to_delete, "story_tag");
			//tags, insert new ones
			$i = 0;
			foreach($tags as $tag){
				$tag_to_insert = array("story_id"=>$id,"tag"=>$tag,"slug"=>$tags_slug[$i]);
				$save->insertIntoTable($tag_to_insert,"story_tag");
				$i++;
			}
			//categories, delete categories first
			$row_to_delete = array("story_id"=>$id);
			$response3 = $save->deleteRows($row_to_delete, "story_category");
			//categories, insert new ones
			foreach($story_category as $category){
				$category_to_insert = array("story_id"=>$id,"category_id"=>$category);
				$save->insertIntoTable($category_to_insert,"story_category");
			}

			echo json_encode(array("success"=>1,"type"=>"update"));
		}else{
			//story
			$array_to_insert = array("album"=>$album,"story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>$story_type,"written_by"=>$written_by,"is_public"=>$is_public,"slug"=>$slug);
			$response = $save->insertIntoTable($array_to_insert,"story");
			//chapter
			$array_to_insert2 = array("chapter_name"=>$chapters['chapter_name'], "written_by"=>$chapters['written_by'], "chapter_body"=>$chapters['chapter_body'], "status"=>0, "slug"=>str_replace(" ", "-", strtolower($chapters['chapter_name'])));
			$response2 = $save->insertIntoTable($array_to_insert2,"chapter");
			//map
			$array_to_insert3 = array("story_id"=>$response,"branch_id"=>1,"chapter_id"=>$response2);
			$response3 = $save->insertIntoTable($array_to_insert3,"story_map");
			//tags, insert new ones
			$i = 0;
			foreach($tags as $tag){
				$tag_to_insert = array("story_id"=>$response,"tag"=>$tag,"slug"=>$tags_slug[$i]);
				$save->insertIntoTable($tag_to_insert,"story_tag");
				$i++;
			}
			//categories, insert new ones
			foreach($story_category as $category){
				$category_to_insert = array("story_id"=>$response,"category_id"=>$category);
				$save->insertIntoTable($category_to_insert,"story_category");
			}

			echo json_encode(array("success"=>1,"type"=>"new","story_id"=>$response,"chapter_id"=>$response2));
		}
	}

	public function initialPublish(){
		$save = $this->loadModel('user_model');

		$story_name = $_POST['story_name'];
		$story_description = $_POST['story_description'];
		$story_type = $_POST['story_type'];
		$story_category = $_POST['story_category'];
		$written_by = $_POST['written_by'];
		$is_public = $_POST['is_public'];
		$album = $_POST['album'];
		$tags = $_POST['tags'];
		$tags = explode(",", $tags);
		$tags_slug = array();
		foreach($tags as $tag1){
			array_push($tags_slug, str_replace(" ", "-", strtolower($tag1)));
		}
		$id = $_POST['id'];
		$chapters = $_POST['chapters'];
		$slug = self::fixForUri($story_name);

		if($id != null){
			//story
			$array_to_update = array("album"=>$album,"story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>$story_type,"written_by"=>$written_by,"is_public"=>$is_public,"slug"=>$slug);
			$where = array("id"=>$id);
			$response = $save->updateTable($array_to_update,$where,"story");
			//chapter
			$array_to_update2 = array("chapter_name"=>$chapters['chapter_name'], "written_by"=>$chapters['written_by'], "chapter_body"=>$chapters['chapter_body'], "status"=>1, "slug"=>str_replace(" ", "-", strtolower($chapters['chapter_name'])));
			$where2 = array("id"=>$chapters['id']);
			$response2 = $save->updateTable($array_to_update2,$where2,"chapter");
			//tags, delete tags first
			$row_to_delete = array("story_id"=>$id);
			$response3 = $save->deleteRows($row_to_delete, "story_tag");
			//tags, insert new ones
			$i = 0;
			foreach($tags as $tag){
				$tag_to_insert = array("story_id"=>$id,"tag"=>$tag,"slug"=>$tags_slug[$i]);
				$save->insertIntoTable($tag_to_insert,"story_tag");
				$i++;
			}
			//categories, delete categories first
			$row_to_delete = array("story_id"=>$id);
			$response3 = $save->deleteRows($row_to_delete, "story_category");
			//categories, insert new ones
			foreach($story_category as $category){
				$category_to_insert = array("story_id"=>$id,"category_id"=>$category);
				$save->insertIntoTable($category_to_insert,"story_category");
			}

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$story_model = $this->loadModel('story_model');
			//get people who follow this person
			$followers_of_me = $save->getFollowers(Session::get('profile')->id);
			//check if owner has subscribed to new story email alert
			foreach($followers_of_me as $follower){
				$has_subscribed = $notification_model->checkIfSubscribed("7",$follower->id);
				if($has_subscribed == 1){
					$story_url = "http://www.bloqly.com/story/".$id."/".$slug;
					$follower_new_story_email = $email_model->followerNewStoryEmailBody(Session::get('profile')->real_name,$follower->real_name,$story_name,$story_url);
					$title = Session::get('profile')->real_name." has published a new story";
					$newAccountEmail = $email_model->sendEmail($follower_new_story_email, $title, $follower->email);
				}
			}
			//email notification end

			echo json_encode(array("success"=>1,"type"=>"update"));
		}else{
			//story
			$array_to_insert = array("album"=>$album,"story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>$story_type,"written_by"=>$written_by,"is_public"=>$is_public,"slug"=>$slug);
			$response = $save->insertIntoTable($array_to_insert,"story");
			//chapter
			$array_to_insert2 = array("chapter_name"=>$chapters['chapter_name'], "written_by"=>$chapters['written_by'], "chapter_body"=>$chapters['chapter_body'], "status"=>1, "slug"=>str_replace(" ", "-", strtolower($chapters['chapter_name'])));
			$response2 = $save->insertIntoTable($array_to_insert2,"chapter");
			//map
			$array_to_insert3 = array("story_id"=>$response,"branch_id"=>1,"chapter_id"=>$response2);
			$response3 = $save->insertIntoTable($array_to_insert3,"story_map");
			//tags, insert new ones
			$i = 0;
			foreach($tags as $tag){
				$tag_to_insert = array("story_id"=>$response,"tag"=>$tag,"slug"=>$tags_slug[$i]);
				$save->insertIntoTable($tag_to_insert,"story_tag");
				$i++;
			}
			//categories, insert new ones
			foreach($story_category as $category){
				$category_to_insert = array("story_id"=>$response,"category_id"=>$category);
				$save->insertIntoTable($category_to_insert,"story_category");
			}

			echo json_encode(array("success"=>1,"type"=>"new","story_id"=>$response,"story_slug"=>$slug,"chapter_slug"=>str_replace(" ", "-", strtolower($chapters['chapter_name']))));
		}
	}

	public function storyActions($action){
		if($action == "edit"){
			$written_by = Session::get('profile')->id;
			$id = $_POST['story_id'];
			$story_name = $_POST['story_name'];
			$slug = self::fixForUri($story_name);
			$story_description = $_POST['story_description'];
			$story_type = $_POST['story_type'];
			$story_categories = $_POST['categories'];
			$is_public = $_POST['is_public'];
			$album = $_POST['album'];
			$tags = $_POST['story_tags'];
			$tags = explode(",", $tags);
			$tags_slug = array();
			foreach($tags as $tag1){
				array_push($tags_slug, str_replace(" ", "-", strtolower($tag1)));
			}
			$save = $this->loadModel('user_model');
			//get owner of chapter, if not me the don't allow update
			$story_model = $this->loadModel('story_model');
			$story_owner = $story_model->getOwnerOfStory($id);
			if($story_owner->id == $written_by){
				//story
				$array_to_update = array("album"=>$album, "story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>$story_type,"is_public"=>$is_public,"slug"=>$slug);
				$where = array("id"=>$id,"written_by"=>$written_by);
				$response = $save->updateTable($array_to_update,$where,"story");
				//tags, delete tags first
				$row_to_delete = array("story_id"=>$id);
				$response3 = $save->deleteRows($row_to_delete, "story_tag");
				//tags, insert new ones
				$i = 0;
				foreach($tags as $tag){
					$tag_to_insert = array("story_id"=>$id,"tag"=>$tag,"slug"=>$tags_slug[$i]);
					$save->insertIntoTable($tag_to_insert,"story_tag");
					$i++;
				}
				//categories, delete categories first
				$row_to_delete = array("story_id"=>$id);
				$response3 = $save->deleteRows($row_to_delete, "story_category");
				//categories, insert new ones
				foreach($story_categories as $category){
					$category_to_insert = array("story_id"=>$id,"category_id"=>$category);
					$save->insertIntoTable($category_to_insert,"story_category");
				}

				echo json_encode(array("success"=>1,"slug"=>$slug));
			}
		}else if($action == "delete"){
			//todo
		}else if($action == "subscribe"){
			$me = Session::get('profile')->id;
			$id = $_POST['id'];
			$save = $this->loadModel('user_model');
			$array_to_insert = array("subscription_type"=>2,"item_subscribed_to"=>$id,"subscribed_by"=>$me);
			$save->insertIntoTable($array_to_insert,"subscription");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$story_model = $this->loadModel('story_model');
			$owner_of_story = $story_model->getOwnerOfStory($id);
			//check if owner has subscribed to new chapter email alert
			$has_subscribed = $notification_model->checkIfSubscribed("4",$owner_of_story->id);
			if($has_subscribed == 1 && $me != $owner_of_story->id){
				$story_details = $story_model->getStoryDetails($id);
				$story_url = "http://www.bloqly.com/story/".$id."/".$story_details->slug;
				$story_bookmark_email = $email_model->bookmarkStoryEmailBody($owner_of_story->real_name,Session::get('profile')->real_name,$story_details->story_name,$story_url);
				$title = Session::get('profile')->real_name." has bookmarked a story you have published";
				$newAccountEmail = $email_model->sendEmail($story_bookmark_email, $title, $owner_of_story->email);
			}
			//email notification end

			echo json_encode(array("success"=>1));
		}else if($action == "unsubscribe"){
			$me = Session::get('profile')->id;
			$id = $_POST['id'];
			$save = $this->loadModel('user_model');
			$where = array("subscription_type"=>2,"item_subscribed_to"=>$id,"subscribed_by"=>$me);
			$save->deleteRows($where,"subscription");
			echo json_encode(array("success"=>1));
		}
	}

	public function getStoriesFromAuthor ($author, $page){
		$story = $this->loadModel('story_model');
		$me = Session::get('profile')->id;
		$page = ($page * 10) - 10;
		$response = $story->getStoriesFromAuthor($author,$page);
		$numberOfAuthorStories = $story->getStoriesFromAuthorCount($author);
		foreach($response as $stori){
			//get categories
			$categories = $story->getCategoriesOfStory($stori->id);
			$stori->categories = $categories;
			//get tags
			$tags = $story->getTagsOfStory($stori->id);
			$stori->tags = $tags;
			//get comments
			$number_of_comments = $story->getNumberOfCommentsOfStory($stori->id);
			$stori->number_of_comments = $number_of_comments;
			//get comments for chapters
			//get number of ratings
			$number_of_ratings = $story->getNumberOfRatingsOfStory($stori->id);
			$stori->number_of_ratings = $number_of_ratings;
			//get average ratings
			$average_rating = $story->getAverageRatingOfStory($stori->id);
			$stori->average_rating = $average_rating;
			//get number of story chapter favourites
			$favourites = $story->getNumberOfStoryFavourites($stori->id);
			$stori->favourites = $favourites;

			$bookmarks = $story->getNumberOfStoryBookmarks($stori->id);
			$stori->bookmarks = $bookmarks;

			$read_history = $story->getStoryReadHistory($stori->id);
			$stori->read_history = $read_history;

			$authors = $story->getAuthorsOfStory($stori->id);
			$stori->authors = $authors;

			$number_of_chapters = $story->getNumberOfChaptersOfStory($stori->id);
			$stori->number_of_chapters = $number_of_chapters;
		}
		echo json_encode(array("success"=>1, "results"=>$response,"numberOfAuthorStories"=>$numberOfAuthorStories));
	}

	public function getChaptersFromAuthor($author, $page){
		$story_model = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		$chapters = $story_model->getChaptersFromAuthor($author,$page);
		$numberOfAuthorChapters = $story_model->getChaptersFromAuthorCount($author);
		foreach($chapters as $chapter){
			$comments = $story_model->getCommentsOfChapter($chapter->chapter_id);
			$chapter->comments = $comments;
			$chapter->chapter_body = utf8_encode($chapter->chapter_body);
			$ratings = $story_model->getAverageRatingsOfChapters($chapter->chapter_id);
			$chapter->ratings = $ratings;
			$favourites = $story_model->getNumberOfChapterFavourites($chapter->chapter_id);
			$chapter->favourites = $favourites;
			$story_owner = $story_model->getOwnerOfStory($chapter->story_id);
			$chapter->story_started_by_real_name = $story_owner->story_started_by_real_name;
			$chapter->story_started_by_pen_name = $story_owner->story_started_by_pen_name;
		}
		echo json_encode(array("success"=>1,"response"=>$chapters,"numberOfAuthorChapters"=>$numberOfAuthorChapters));
	}

	public function getStory($story_id){
		$story = $this->loadModel('story_model');
		$me = Session::get('profile')->id;
		$response = $story->getStoriesFromId($story_id,$me);

		echo json_encode(array("success"=>1,"result"=>$response));
	}

	public function getStoriesFromId ($id){
		$story = $this->loadModel('story_model');
		$me = Session::get('profile')->id;

		$response = $story->getStoriesFromId($id,$me);

		foreach($response as $stori){
			//get chapters
			$numOfChapters = $story->getChapters($stori->id);
			$stori->chapters = $numOfChapters;
			//get categories
			$categories = $story->getCategoriesOfStory($stori->id);
			$stori->categories = $categories;
			//get tags
			$tags = $story->getTagsOfStory($stori->id);
			$stori->tags = $tags;
			//get comments for story
			$comments = $story->getCommentsOfStory($stori->id);
			$stori->comments = $comments;
			//challenge
			if($stori->type == "Challenge"){
				$challenge_details = $story->getChallengeDetails($stori->id);
				$stori->challenge_details = $challenge_details;
			}
			//get comments, favourites for chapters
			foreach($stori->chapters as $chapter){
				$chapter->chapter_body = utf8_encode($chapter->chapter_body);
				$ratings = $story->getAverageRatingsOfChapters($chapter->id);
				$chapter->ratings = $ratings;
				$chapter_comments = $story->getCommentsOfChapter($chapter->id);
				$chapter->comments = $chapter_comments;
				foreach($chapter->comments as $comment){
					$review_ratings = $story->getReviewRatings($comment->id);
					$comment->review_ratings = $review_ratings;
				}
				$favourites = $story->getNumberOfChapterFavourites($chapter->id);
				$chapter->favourites = $favourites;
				$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id);
				$chapter->bookmarks = $bookmarks;
				$read_history = $story->getReadHistory($chapter->id);
				$chapter->read_history = $read_history;
				$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
				$chapter->haveIFavourited = $haveIFavourited;
				$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
				$chapter->haveIBookmarked = $haveIBookmarked;
			}
		}

		echo json_encode(array("success"=>1, "results"=>$response));
	}

	public function getBasicStoryList($userId){
		$story = $this->loadModel('story_model');

		$response = $story->getStoriesFromAuthorId($userId);

		echo json_encode(array("success"=>1,"results"=>$response));
	}

	public function rateChapter(){
		$user = Session::get('profile')->id;
		$chapterId = $_POST['chapterId'];
		$storyId = $_POST['storyId'];
		$comment = $_POST['chapterRatingComment'];
		$rating = $_POST['rating'];

		$story = $this->loadModel('user_model');

		$array_to_insert = array('story_id'=>$storyId, 'chapter_id'=>$chapterId, 'rating'=>$rating, 'comment_body'=>$comment, 'commented_by'=>$user);
		$where = "story_comment";

		$storyCommentId = $story->insertIntoTable($array_to_insert,$where);

		//notifications start
		$story_model = $this->loadModel('story_model');
		$chapter_owner = $story_model->getOwnerOfChapter($chapterId);

		if($chapter_owner != $user){
			$array_to_insert = array("activity_by"=>$user,"activity_type_id"=>3,"action_id"=>$storyCommentId,"user_to_notify"=>$chapter_owner);
			$story->insertIntoTable($array_to_insert,"notification");
		}
		//notifications end

		//email_notification
		$email_model = $this->loadModel('email_model');
		$notification_model = $this->loadModel('notification_model');
		$owner_of_chapter = $story_model->getDetailsOfChapterOwner($chapterId);
		//check if owner has subscribed to new chapter email alert
		$has_subscribed = $notification_model->checkIfSubscribed("3",$owner_of_chapter->id);
		if($has_subscribed == 1 && $user != $owner_of_chapter->id){
			$chapter_details = $story_model->getChapterDetails($chapterId);
			$story_details = $story_model->getStoryDetails($storyId);
			$story_url = "http://www.bloqly.com/story/".$storyId."/".$story_details->slug."/".$chapter_details->slug;
			$chapter_rate_email = $email_model->chapterRateEmailBody($owner_of_chapter->real_name,Session::get('profile')->real_name,$chapter_details->chapter_name,$rating,$story_url);
			$title = Session::get('profile')->real_name." has rated a chapter you have published";
			$newAccountEmail = $email_model->sendEmail($chapter_rate_email, $title, $owner_of_chapter->email);
		}
		//email notification end

		echo json_encode(array("success"=>1,"id"=>$storyCommentId));
	}

	public function commentChapter(){
		$user = Session::get('profile')->id;
		$chapterId = $_POST['chapterId'];
		$storyId = $_POST['storyId'];
		$comment = $_POST['comment'];

		$story = $this->loadModel('user_model');

		$array_to_insert = array('story_id'=>$storyId, 'chapter_id'=>$chapterId, 'comment_body'=>$comment, 'commented_by'=>$user);
		$where = "story_comment";

		$storyCommentId = $story->insertIntoTable($array_to_insert,$where);

		//notifications start
		$story_model = $this->loadModel('story_model');
		$chapter_owner = $story_model->getOwnerOfChapter($chapterId);

		if($chapter_owner != $user){
			$array_to_insert = array("activity_by"=>$user,"activity_type_id"=>2,"action_id"=>$storyCommentId,"user_to_notify"=>$chapter_owner);
			$story->insertIntoTable($array_to_insert,"notification");
		}
		//notifications end

		//email_notification
		$email_model = $this->loadModel('email_model');
		$notification_model = $this->loadModel('notification_model');
		$owner_of_chapter = $story_model->getDetailsOfChapterOwner($chapterId);
		//check if owner has subscribed to new chapter email alert
		$has_subscribed = $notification_model->checkIfSubscribed("2",$owner_of_chapter->id);
		if($has_subscribed == 1 && $user != $owner_of_chapter->id){
			$chapter_details = $story_model->getChapterDetails($chapterId);
			$story_details = $story_model->getStoryDetails($storyId);
			$story_url = "http://www.bloqly.com/story/".$storyId."/".$story_details->slug."/".$chapter_details->slug;
			$chapter_comment_email = $email_model->chapterCommentEmailBody($owner_of_chapter->real_name,Session::get('profile')->real_name,$chapter_details->chapter_name,$story_url);
			$title = Session::get('profile')->real_name." has commented on a chapter you have published";
			$newAccountEmail = $email_model->sendEmail($chapter_comment_email, $title, $owner_of_chapter->email);
		}
		//email notification end

		echo json_encode(array("success"=>1,"id"=>$storyCommentId));
	}

	public function getChapter($chapterId){
		$story_model = $this->loadModel('story_model');
		$chapter = $story_model->getChapterFromId($chapterId);
		echo json_encode(array("success"=>1,"response"=>$chapter));
	}

	public function getAllChapters($period,$page){
		$story = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		$chapters = $story->getAllChapters($page);
		$chapters[0]->type = "ALL";
		$i = 1;
		$numberOfChapters = $story->getNumberOfChapters();
		foreach($chapters as $chapter){
			$page_num = ceil($i/10);
			$chapter->page = $page_num;
			//get avg rating
			$chapter->chapter_body = utf8_encode($chapter->chapter_body);
			$ratings = $story->getAverageRatingsOfChapters($chapter->id,$period);
			$chapter->average_rating = $ratings[0]->average_rating;
			$chapter->rating_count = $ratings[0]->rating_count;
			$chapter->rating_sum = $ratings[0]->rating_sum;
			//get comments
			$chapter_comments = $story->getCommentsOfChapter($chapter->id,$period);
			$chapter->comments = $chapter_comments;
			//get # favourited
			$favourites = $story->getNumberOfChapterFavourites($chapter->id,$period);
			$chapter->favourites = $favourites->favourite_count;
			//get # bookmarked
			$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id,$period);
			$chapter->bookmarks = $bookmarks;
			//get people who read
			$read_history = $story->getReadHistory($chapter->id,$period);
			$chapter->read_history = $read_history;
			//get story details
			$story_details = $story->getStoryOfChapter($chapter->id);
			$chapter->story_details = $story_details;
			$i++;
		}
		echo json_encode(array("success"=>1,"results"=>$chapters,"numberOfChapters"=>$numberOfChapters));
	}

	public function getRecentChapters($period,$page){
		$story = $this->loadModel('story_model');
		$chapters = $story->getRecentChapters($page);
		$chapters[0]->type = "RECENT";
		$i = 1;
		$numberOfChapters = $story->getNumberOfChapters();
		foreach($chapters as $chapter){
			$page_num = ceil($i/10);
			$chapter->page = $page_num;
			//get avg rating
			$chapter->chapter_body = utf8_encode($chapter->chapter_body);
			$ratings = $story->getAverageRatingsOfChapters($chapter->id,$period);
			$chapter->average_rating = $ratings[0]->average_rating;
			$chapter->rating_count = $ratings[0]->rating_count;
			$chapter->rating_sum = $ratings[0]->rating_sum;
			//get comments
			$chapter_comments = $story->getCommentsOfChapter($chapter->id,$period);
			$chapter->comments = $chapter_comments;
			//get # favourited
			$favourites = $story->getNumberOfChapterFavourites($chapter->id,$period);
			$chapter->favourites = $favourites->favourite_count;
			//get # bookmarked
			$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id,$period);
			$chapter->bookmarks = $bookmarks;
			//get people who read
			$read_history = $story->getReadHistory($chapter->id,$period);
			$chapter->read_history = $read_history;
			//get story details
			$story_details = $story->getStoryOfChapter($chapter->id);
			$chapter->story_details = $story_details;
			$i++;
		}
		echo json_encode(array("success"=>1,"results"=>$chapters,"numberOfChapters"=>$numberOfChapters));
	}

	public function getPopularChapters($period,$page){
		$story = $this->loadModel('story_model');
		$chapters = $story->getPopularChapters($page);
		$chapters[0]->type = "POPULAR";
		$i = 1;
		$numberOfChapters = $story->getNumberOfChapters();
		foreach($chapters as $chapter){
			$page_num = ceil($i/10);
			$chapter->page = $page_num;
			//get avg rating
			$chapter->chapter_body = utf8_encode($chapter->chapter_body);
			$ratings = $story->getAverageRatingsOfChapters($chapter->id,$period);
			$chapter->average_rating = $ratings[0]->average_rating;
			$chapter->rating_count = $ratings[0]->rating_count;
			$chapter->rating_sum = $ratings[0]->rating_sum;
			//get comments
			$chapter_comments = $story->getCommentsOfChapter($chapter->id,$period);
			$chapter->comments = $chapter_comments;
			//get # favourited
			$favourites = $story->getNumberOfChapterFavourites($chapter->id,$period);
			$chapter->favourites = $favourites->favourite_count;
			//get # bookmarked
			$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id,$period);
			$chapter->bookmarks = $bookmarks;
			//get people who read
			$read_history = $story->getReadHistory($chapter->id,$period);
			$chapter->read_history = $read_history;
			//get story details
			$story_details = $story->getStoryOfChapter($chapter->id);
			$chapter->story_details = $story_details;
			$i++;
		}
		echo json_encode(array("success"=>1,"results"=>$chapters,"numberOfChapters"=>$numberOfChapters));
	}

	public function savePublishChapter($action){
		$parent_id = $_POST['parent_id'];
		$story_id = $_POST['story_id'];
		$chapter_name = $_POST['chapter_name'];
		$chapter_body = $_POST['chapter_body'];
		$newChapterId = $_POST['new_chapter_id'];
		$slug = self::fixForUri($chapter_name);
		$me = Session::get('profile')->id;
		if($action == "save"){
			$public = 0;
		}else{
			$public = 1;
		}
		$database = $this->loadModel('user_model');
		//update
		if($action == "new" || $action == "save" || $action == "publish"){

			if($newChapterId > 0){
				//update chapter
				$array_to_update = array("chapter_name"=>$chapter_name,"chapter_body"=>$chapter_body,"slug"=>$slug, "status"=>$public);
				$where = array("written_by"=>$me,"id"=>$newChapterId);
				$table = "chapter";
				$database->updateTable($array_to_update,$where,$table);

				echo json_encode(array("success"=>1,"response"=>array("id"=>$newChapterId,"slug"=>$slug)));
			}else{ //else insert
				//insert to chapter
				$array_to_insert = array("chapter_name"=>$chapter_name, "written_by"=>$me, "chapter_body"=>$chapter_body, "status"=>$public, "slug"=>$slug);
				$table = "chapter";
				$id = $database->insertIntoTable($array_to_insert,$table);
				//insert to story_map
				$array_to_insert = array("story_id"=>$story_id, "branch_id"=>1, "chapter_id"=>$id, "parent_chapter_id"=>$parent_id);
				$table = "story_map";
				$response = $database->insertIntoTable($array_to_insert,$table);

				//notifications start
				$story_model = $this->loadModel('story_model');
				$story_details = $story_model->getOwnerOfStory($story_id);

				if($story_details->id != $me){
					$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>1,"action_id"=>$id,"user_to_notify"=>$story_details->id);
					$database->insertIntoTable($array_to_insert,"notification");
				}
				//get subscribers of the story and notify
				$followers = $story_model->getFollowersOfStory($story_id,$story_details->id);
				foreach($followers as $follower){
					$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>1,"action_id"=>$id,"user_to_notify"=>$follower->id);
					$database->insertIntoTable($array_to_insert,"notification");
				}
				//notifications end

				//email_notification - alert story owner
				$email_model = $this->loadModel('email_model');
				$story_model = $this->loadModel('story_model');
				$notification_model = $this->loadModel('notification_model');
				$owner_of_story = $story_model->getOwnerOfStory($story_id);
				//check if owner has subscribed to new chapter email alert
				$has_subscribed = $notification_model->checkIfSubscribed("1",$owner_of_story->id);
				if($has_subscribed == 1 && $me != $owner_of_story->id){
					$story_details = $story_model->getStoryDetails($story_id);
					$story_url = "http://www.bloqly.com/story/".$story_id."/".$story_details->slug."/".$slug;
					$new_chapter_email = $email_model->newChapterEmailBody($owner_of_story->real_name,Session::get('profile')->real_name,$story_details->story_name,$chapter_name,$story_url);
					$title = Session::get('profile')->real_name." has published a new chapter in your story";
					$newAccountEmail = $email_model->sendEmail($new_chapter_email, $title, $owner_of_story->email);
				}
				//email notification end

				//email_notification - alert followers of chapter author
				//get people who follow this person
				$followers_of_me = $database->getFollowers(Session::get('profile')->id);
				//check if owner has subscribed to new story email alert
				foreach($followers_of_me as $follower){
					$has_subscribed = $notification_model->checkIfSubscribed("8",$follower->id);
					//exclude owner of the story -- if owner is following the chapter author too
					if($has_subscribed == 1 && $follower->id != $owner_of_story->id){
						$story_details = $story_model->getStoryDetails($story_id);
						$story_url = "http://www.bloqly.com/story/".$story_id."/".$story_details->slug."/".$slug;
						$follower_new_chapter_email = $email_model->followerNewChapterEmailBody($follower->real_name,Session::get('profile')->real_name,$story_details->story_name,$chapter_name,$story_url);
						$title = Session::get('profile')->real_name." has published a new chapter";
						$newAccountEmail = $email_model->sendEmail($follower_new_chapter_email, $title, $follower->email);
					}
				}
				//email notification end
				echo json_encode(array("success"=>1,"response"=>array("id"=>$id,"slug"=>$slug)));
			}
		}else if($action == "edit"){
			$chapter_id = $_POST['chapter_id'];

			$array_to_update = array("chapter_name"=>$chapter_name,"chapter_body"=>$chapter_body,"slug"=>$slug);
			$where = array("id"=>$chapter_id);
			$table = "chapter";

			//get owner of chapter, if not me the don't allow update
			$story_model = $this->loadModel('story_model');
			$chapter_owner = $story_model->getOwnerOfChapter($chapter_id);

			if($chapter_owner == $me){
				$success = $database->updateTable($array_to_update,$where,$table);
				echo json_encode(array("success"=>1,"slug"=>$slug));
			}
		}else if($action == "delete"){
			$chapter_id = $_POST['chapter_id'];

			$row_to_delete = array("chapter_id"=>$chapter_id);
			$row_to_delete2 = array("id"=>$chapter_id);
			$row_to_delete3 = array("activity_type_id"=>1,"action_id"=>$chapter_id);
			$row_to_delete4 = array("activity_type_id"=>2,"action_id"=>$chapter_id);
			$row_to_delete5 = array("activity_type_id"=>3,"action_id"=>$chapter_id);
			$row_to_delete6 = array("activity_type_id"=>5,"action_id"=>$chapter_id);
			$row_to_delete7 = array("activity_type_id"=>6,"action_id"=>$chapter_id);
			$row_to_delete8 = array("activity_type_id"=>8,"action_id"=>$chapter_id);
			$database->deleteRows($row_to_delete,"story_map");
			$database->deleteRows($row_to_delete2,"chapter");
			$database->deleteRows($row_to_delete3,"notification");
			$database->deleteRows($row_to_delete4,"notification");
			$database->deleteRows($row_to_delete5,"notification");
			$database->deleteRows($row_to_delete6,"notification");
			$database->deleteRows($row_to_delete7,"notification");
			$database->deleteRows($row_to_delete8,"notification");
			echo json_encode(array("success"=>1));
		}else if($action == "favourite"){
			$chapter_id = $_POST['chapter_id'];
			$story_id = $_POST['story_id'];

			$array_to_insert = array("chapter_id"=>$chapter_id,"story_id"=>$story_id,"favourited_by"=>$me);
			$response = $database->insertIntoTable($array_to_insert,"favourite");
			//get new favourites
			$story_model = $this->loadModel('story_model');
			$favourites = $story_model->getUserFavourites($me);

			//notifications start
			$chapter_owner = $story_model->getOwnerOfChapter($chapter_id);

			if($chapter_owner != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>6,"action_id"=>$chapter_id,"user_to_notify"=>$chapter_owner);
				$database->insertIntoTable($array_to_insert,"notification");
			}
			//notifications end

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$owner_of_chapter = $story_model->getDetailsOfChapterOwner($chapter_id);
			//check if owner has subscribed to new chapter email alert
			$has_subscribed = $notification_model->checkIfSubscribed("6",$owner_of_chapter->id);
			if($has_subscribed == 1 && $me != $owner_of_chapter->id){
				$story_details = $story_model->getStoryDetails($story_id);
				$chapter_details = $story_model->getChapterDetails($chapter_id);
				$story_url = "http://www.bloqly.com/story/".$story_id."/".$story_details->slug."/".$chapter_details->slug;
				$chapter_favourite_email = $email_model->favouriteChapterEmailBody($owner_of_chapter->real_name,Session::get('profile')->real_name,$story_details->story_name,$chapter_details->chapter_name,$story_url);
				$title = Session::get('profile')->real_name." has favourited a chapter you have published";
				$newAccountEmail = $email_model->sendEmail($chapter_favourite_email, $title, $owner_of_chapter->email);
			}
			//email notification end

			echo json_encode(array("success"=>1,"results"=>$favourites));
		}else if($action == "unfavourite"){
			$chapter_id = $_POST['chapter_id'];
			$story_id = $_POST['story_id'];
			$row_to_delete = array("story_id"=>$story_id,"chapter_id"=>$chapter_id,"favourited_by"=>$me);
			$database->deleteRows($row_to_delete,"favourite");
			//get new favourites
			$story_model = $this->loadModel('story_model');
			$favourites = $story_model->getUserFavourites($me);

			$row_to_delete7 = array("activity_type_id"=>6,"action_id"=>$chapter_id);
			$database->deleteRows($row_to_delete7,"notification");

			echo json_encode(array("success"=>1,"results"=>$favourites));
		}else if($action == "bookmark"){
			$chapter_id = $_POST['chapter_id'];
			$story_id = $_POST['story_id'];

			$array_to_insert = array("chapter_id"=>$chapter_id,"story_id"=>$story_id,"bookmarked_by"=>$me);
			$response = $database->insertIntoTable($array_to_insert,"bookmark");
			//get new bookmarks
			$story_model = $this->loadModel('story_model');
			$bookmarks = $story_model->getUserBookmarks($me);

			//notifications start
			$chapter_owner = $story_model->getOwnerOfChapter($chapter_id);

			if($chapter_owner != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>5,"action_id"=>$chapter_id,"user_to_notify"=>$chapter_owner);
				$database->insertIntoTable($array_to_insert,"notification");
			}
			//notifications end

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$owner_of_chapter = $story_model->getDetailsOfChapterOwner($chapter_id);
			//check if owner has subscribed to new chapter email alert
			$has_subscribed = $notification_model->checkIfSubscribed("5",$owner_of_chapter->id);
			if($has_subscribed == 1 && $me != $owner_of_chapter->id){
				$story_details = $story_model->getStoryDetails($story_id);
				$chapter_details = $story_model->getChapterDetails($chapter_id);
				$story_url = "http://www.bloqly.com/story/".$story_id."/".$story_details->slug."/".$chapter_details->slug;
				$chapter_bookmark_email = $email_model->bookmarkChapterEmailBody($owner_of_chapter->real_name,Session::get('profile')->real_name,$story_details->story_name,$chapter_details->chapter_name,$story_url);
				$title = Session::get('profile')->real_name." has bookmarked a chapter you have published";
				$newAccountEmail = $email_model->sendEmail($chapter_bookmark_email, $title, $owner_of_chapter->email);
			}
			//email notification end

			echo json_encode(array("success"=>1,"results"=>$bookmarks));
		}else if($action == "unbookmark"){
			$chapter_id = $_POST['chapter_id'];
			$story_id = $_POST['story_id'];
			$row_to_delete = array("story_id"=>$story_id,"chapter_id"=>$chapter_id,"bookmarked_by"=>$me);
			$database->deleteRows($row_to_delete,"bookmark");
			//get new bookmarks
			$story_model = $this->loadModel('story_model');
			$bookmarks = $story_model->getUserBookmarks($me);

			$row_to_delete6 = array("activity_type_id"=>5,"action_id"=>$chapter_id);
			$database->deleteRows($row_to_delete6,"notification");

			echo json_encode(array("success"=>1,"results"=>$bookmarks));
		}else if($action == "read"){
			$chapter_slug = $_POST['chapter_slug'];
			$story_id = $_POST['story_id'];

			//find chapter_id
			$story_model = $this->loadModel('story_model');
			if($chapter_slug != null && $chapter_slug != "" && $chapter_slug){
				$chapter_id = $story_model->getChapterId($chapter_slug, $story_id);
			}else{
				$chapter_id = $story_model->getChapterId2($story_id);
			}

			$array_to_insert = array("read_by"=>$me, "story_id"=>$story_id, "chapter_id"=>$chapter_id);
			$database->insertIntoTable($array_to_insert,"read_history");

			echo json_encode(array("success"=>1));
		}else if($action == "helpful"){
			$story_comment_id = $_POST['story_comment_id'];
			$me = Session::get('profile')->id;
			$array_to_insert = array("story_comment_id"=>$story_comment_id, "rated_by"=>$me, "was_helpful"=>1);
			$database->insertIntoTable($array_to_insert, "review_rating");
		}else if($action == "notHelpful"){
			$story_comment_id = $_POST['story_comment_id'];
			$me = Session::get('profile')->id;
			$array_to_insert = array("story_comment_id"=>$story_comment_id, "rated_by"=>$me, "was_helpful"=>0);
			$database->insertIntoTable($array_to_insert, "review_rating");
		}

	}

	public function getUserBookmarks($author){
		$story_model = $this->loadModel('story_model');

		$bookmarks = $story_model->getUserBookmarks($author);

		echo json_encode(array("success"=>1,"results"=>$bookmarks));
	}

	public function getUserFavourites($author){
		$story_model = $this->loadModel('story_model');

		$bookmarks = $story_model->getUserFavourites($author);

		echo json_encode(array("success"=>1,"results"=>$bookmarks));
	}

	public function getAllStories($period,$page,$category = null){
		$story = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		if($category == null){
			$response = $story->getAllStories($period,$page);
		}else{
			$response = $story->getAllStoriesWithCategory($period,$page,$category);
		}
		if(count($response)>0){
			$response[0]->search_type = "ALL";
			$i = 1;
			$numberOfStories = $story->getNumberOfStories();
			foreach($response as $stori){
				$page_num = ceil($i/10);
				$stori->page = $page_num;
				//get chapters
				$numOfChapters = $story->getChapters($stori->id);
				$stori->chapters = $numOfChapters;
				//get categories
				$categories = $story->getCategoriesOfStory($stori->id);
				$stori->categories = $categories;
				//get tags
				$tags = $story->getTagsOfStory($stori->id);
				$stori->tags = $tags;
				//get comments
				$comments = $story->getCommentsOfStory($story->id);
				$stori->comments = $comments;
				//get comments for chapters
				foreach($stori->chapters as $chapter){
					$chapter->chapter_body = utf8_encode($chapter->chapter_body);
					$chapter_comments = $story->getCommentsOfChapter($chapter->id);
					$chapter->comments = $chapter_comments;
					$favourites = $story->getNumberOfChapterFavourites($chapter->id);
					$chapter->favourites = $favourites;
					$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id);
					$chapter->bookmarks = $bookmarks;
					$read_history = $story->getReadHistory($chapter->id);
					$chapter->read_history = $read_history;
					$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
					$chapter->haveIFavourited = $haveIFavourited;
					$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
					$chapter->haveIBookmarked = $haveIBookmarked;
				}
				$i++;
			}
			echo json_encode(array("success"=>1, "results"=>$response,"numberOfStories"=>$numberOfStories));
		}else{
			$response = array();
			array_push($response, array("search_type"=>"ALL"));
			echo json_encode(array("success"=>1,"results"=>$response,"numberOfStories"=>$numberOfStories));
		}
	}

	public function getRecentStories($period,$page,$category = null){
		$story = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		if($category == null){
			$response = $story->getRecentStories($period,$page);
		}else{
			$response = $story->getRecentStoriesWithCategory($period,$page,$category);
		}
		
		if(count($response)>0){
			$response[0]->search_type = "RECENT";
			$i = 1;
			$numberOfStories = $story->getNumberOfStories();
			foreach($response as $stori){
				$page_num = ceil($i/10);
				$stori->page = $page_num;
				//get chapters
				$numOfChapters = $story->getChapters($stori->id);
				$stori->chapters = $numOfChapters;
				//get categories
				$categories = $story->getCategoriesOfStory($stori->id);
				$stori->categories = $categories;
				//get tags
				$tags = $story->getTagsOfStory($stori->id);
				$stori->tags = $tags;
				//get comments
				$comments = $story->getCommentsOfStory($story->id);
				$stori->comments = $comments;
				//get comments for chapters
				foreach($stori->chapters as $chapter){
					$chapter->chapter_body = utf8_encode($chapter->chapter_body);
					$chapter_comments = $story->getCommentsOfChapter($chapter->id);
					$chapter->comments = $chapter_comments;
					$favourites = $story->getNumberOfChapterFavourites($chapter->id);
					$chapter->favourites = $favourites;
					$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id);
					$chapter->bookmarks = $bookmarks;
					$read_history = $story->getReadHistory($chapter->id);
					$chapter->read_history = $read_history;
					$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
					$chapter->haveIFavourited = $haveIFavourited;
					$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
					$chapter->haveIBookmarked = $haveIBookmarked;
				}
				$i++;
			}
			echo json_encode(array("success"=>1, "results"=>$response,"numberOfStories"=>$numberOfStories));
		}else{
			$response = array();
			array_push($response, array("search_type"=>"RECENT"));
			echo json_encode(array("success"=>1,"results"=>$response,"numberOfStories"=>$numberOfStories));
		}
	}

	public function getPopularStories($period,$page,$category = null){
		$story = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		if($category == null){
			$response = $story->getPopularStories($period,$page);
		}else{
			$response = $story->getPopularStoriesWithCategory($period,$page,$category);
		}
		if(count($response)>0){
			$i = 1;
			$numberOfStories = $story->getNumberOfStories();
			foreach($response as $stori){
				$page_num = ceil($i/10);
				$stori->page = $page_num;
				//get chapters
				$numOfChapters = $story->getChapters($stori->id);
				$stori->chapters = $numOfChapters;
				//get categories
				$categories = $story->getCategoriesOfStory($stori->id);
				$stori->categories = $categories;
				//get tags
				$tags = $story->getTagsOfStory($stori->id);
				$stori->tags = $tags;
				//get comments
				$comments = $story->getCommentsOfStory($story->id,$period);
				$stori->comments = $comments;
				//get comments for chapters
				$j = 0;
				foreach($stori->chapters as $chapter){
					$chapter->chapter_body = utf8_encode($chapter->chapter_body);
					$chapter->chapter_name = utf8_encode($chapter->chapter_name);
					$chapter_comments = $story->getCommentsOfChapter($chapter->id,$period);
					$chapter->comments = $chapter_comments;
					$favourites = $story->getNumberOfChapterFavourites($chapter->id,$period);
					$chapter->favourites = $favourites;
					$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id,$period);
					$chapter->bookmarks = $bookmarks;
					$read_history = $story->getReadHistory($chapter->id,$period);
					$chapter->read_history = $read_history;
					$j = $j + count($read_history);
					$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
					$chapter->haveIFavourited = $haveIFavourited;
					$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
					$chapter->haveIBookmarked = $haveIBookmarked;
				}
				$stori->read_count = $j;
				$i++;
			}
			$response[0]->search_type = "POPULAR";
			echo json_encode(array("success"=>1, "results"=>$response,"numberOfStories"=>$numberOfStories));
		}else{
			$response = array();
			array_push($response, array("search_type"=>"POPULAR"));
			echo json_encode(array("success"=>1,"results"=>$response,"numberOfStories"=>$numberOfStories));
		}
	}

	public function getHomepagePopularStories($period,$page,$category = null){
		$story = $this->loadModel('story_model');
		$page = ($page * 10) - 10;
		if($category == null){
			$response = $story->getHomepagePopularStories($period,$page);
		}else{
			$response = $story->getHomepagePopularStoriesWithCategory($period,$page,$category);
		}
		if(count($response)>0){
			$i = 1;
			$numberOfStories = $story->getNumberOfStories();
			foreach($response as $stori){
				$page_num = ceil($i/10);
				$stori->page = $page_num;
				//get chapters
				$numOfChapters = $story->getChapters($stori->id);
				$stori->chapters = $numOfChapters;
				//get categories
				$categories = $story->getCategoriesOfStory($stori->id);
				$stori->categories = $categories;
				//get tags
				$tags = $story->getTagsOfStory($stori->id);
				$stori->tags = $tags;
				//get comments
				$comments = $story->getCommentsOfStory($story->id,$period);
				$stori->comments = $comments;
				//get comments for chapters
				$j = 0;
				foreach($stori->chapters as $chapter){
					$chapter->chapter_body = utf8_encode($chapter->chapter_body);
					$chapter->chapter_name = utf8_encode($chapter->chapter_name);
					$chapter_comments = $story->getCommentsOfChapter($chapter->id,$period);
					$chapter->comments = $chapter_comments;
					$favourites = $story->getNumberOfChapterFavourites($chapter->id,$period);
					$chapter->favourites = $favourites;
					$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id,$period);
					$chapter->bookmarks = $bookmarks;
					$read_history = $story->getReadHistory($chapter->id,$period);
					$chapter->read_history = $read_history;
					$j = $j + count($read_history);
					$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
					$chapter->haveIFavourited = $haveIFavourited;
					$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
					$chapter->haveIBookmarked = $haveIBookmarked;
				}
				$stori->read_count = $j;
				$i++;
			}
			$response[0]->search_type = "POPULAR";
			echo json_encode(array("success"=>1, "results"=>$response,"numberOfStories"=>$numberOfStories));
		}else{
			$response = array();
			array_push($response, array("search_type"=>"POPULAR"));
			echo json_encode(array("success"=>1,"results"=>$response,"numberOfStories"=>$numberOfStories));
		}
	}

	public function sort_on_field(&$objects, $on, $order = 'ASC') {
	    $comparer = ($order === 'DESC')
	        ? "return -strcmp(\$a->{$on},\$b->{$on});"
	        : "return strcmp(\$a->{$on},\$b->{$on});";
	    usort($objects, create_function('$a,$b', $comparer));
	}

}
