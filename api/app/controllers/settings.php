<?php

class Settings extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function updateEmailSettings(){

		$settings = $_POST['emailSettings'];

		$me = Session::get('profile')->id;

		$save = $this->loadModel('user_model');

		$rows_to_delete = array("user_id"=>$me);

		$save->deleteRows($rows_to_delete,"email_settings");

		foreach($settings as $setting){
			$row_to_insert = array("email_settings_type_id"=>$setting,"user_id"=>$me);
			$save->insertIntoTable($row_to_insert,"email_settings");
		}

		echo json_encode(array("success"=>1));
	}

	public function getEmailSettings(){
		$me = Session::get('profile')->id;

		$settings_model = $this->loadModel('settings_model');

		$settings = $settings_model->getEmailSettings($me);

		echo json_encode(array("success"=>1,"results"=>$settings));

	}

}