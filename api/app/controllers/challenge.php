<?php

class Challenge extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function newChallenge(){
		$challenge_name = $_POST['challenge_name'];
		$challenge_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $challenge_name);
		$challenge_slug = str_replace(" ", "-", strtolower($challenge_slug));
		$challenge_description = $_POST['challenge_description'];
		$challenge_start_date = $_POST['challenge_start_date'];
		$challenge_end_date = $_POST['challenge_end_date'];
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$array_to_insert = array("challenge_name"=>$challenge_name,"challenge_by"=>$me,"challenge_description"=>$challenge_description,"challenge_start_date"=>$challenge_start_date,"challenge_end_date"=>$challenge_end_date,"slug"=>$challenge_slug);
		$challenge_id = $user_model->insertIntoTable($array_to_insert,"challenge");

		echo json_encode(array("success"=>1,"results"=>array("challenge_id"=>$challenge_id,"slug"=>$challenge_slug)));
	}

	public function challengeLeaderboard($time){
		$challenge_model = $this->loadModel('challenge_model');

		$leaderboard = $challenge_model->getLeaderboard($time);

		$i = 1;
		foreach($leaderboard as $entry){
			$entry->position = $i;
			$i++;
		}

		echo json_encode(array("success"=>1,"results"=>$leaderboard));
	}

	public function editChallenge(){
		$challenge_name = $_POST['challenge_name'];
		$challenge_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $challenge_name);
		$challenge_slug = str_replace(" ", "-", strtolower($challenge_slug));
		$challenge_description = $_POST['challenge_description'];
		$challenge_start_date = $_POST['challenge_start_date'];
		$challenge_end_date = $_POST['challenge_end_date'];
		$challenge_id = $_POST['challenge_id'];

		$user_model = $this->loadModel('user_model');

		//get owner of this challlenge, if I am no the owner, then don't allow to edit
		$challenge_model = $this->loadModel('challenge_model');
		$me = Session::get('profile')->id;
		$owner = $challenge_model->getChallengeOwner($challenge_id);
		if($me == $owner){
			$row_to_update = array("challenge_name"=>$challenge_name,"challenge_description"=>$challenge_description,"challenge_start_date"=>$challenge_start_date,"challenge_end_date"=>$challenge_end_date,"slug"=>$challenge_slug);
			$where = array("id"=>$challenge_id);
			$user_model->updateTable($row_to_update,$where,"challenge");

			echo json_encode(array("success"=>1,"results"=>array("challenge_id"=>$challenge_id,"slug"=>$challenge_slug)));
		}
	}

	public function activeChallenge($type,$page){
		$challenge_model = $this->loadModel('challenge_model');
		$page = ($page*10)-10;
		if($type == null || $type == "recent"){
			$response = $challenge_model->getActiveChallenges("recent",$page);
		}else if($type == "popular"){
			$response = $challenge_model->getActiveChallenges($type,$page);
		}else if($type == "soonest"){
			$response = $challenge_model->getActiveChallenges($type,$page);
		}

		foreach($response as $challenge){
			$challenge->challenge_name = utf8_encode($challenge->challenge_name);
			$challenge->challenge_description = utf8_encode($challenge->challenge_description);
		}

		$numberActiveChallenges = $challenge_model->getNumberOfActiveChallenges();

		echo json_encode(array("success"=>1,"results"=>$response,"numberActiveChallenges"=>$numberActiveChallenges));
	}

	public function previousChallenge($type,$page){
		$challenge_model = $this->loadModel('challenge_model');
		$page = ($page*10)-10;
		if($type == "popular"){
			$response = $challenge_model->getPreviousChallenges($type,$page);
		}else if($type == "soonest"){
			$response = $challenge_model->getPreviousChallenges($type,$page);
		}

		foreach($response as $challenge){
			$challenge->challenge_name = utf8_encode($challenge->challenge_name);
			$challenge->challenge_description = utf8_encode($challenge->challenge_description);
		}

		$numberPreviousChallenges = $challenge_model->getNumberOfPreviousChallenges();

		echo json_encode(array("success"=>1,"results"=>$response,"numberPreviousChallenges"=>$numberPreviousChallenges));
	}

	public function myChallenge($type,$page){
		$challenge_model = $this->loadModel('challenge_model');
		$page = ($page*10)-10;
		$me = Session::get('profile')->id;
		if($type == "popular"){
			$response = $challenge_model->getMyChallenges($type,$me,$page);
		}else if($type == "soonest"){
			$response = $challenge_model->getMyChallenges($type,$me,$page);
		}

		foreach($response as $challenge){
			$challenge->challenge_name = utf8_encode($challenge->challenge_name);
			$challenge->challenge_description = utf8_encode($challenge->challenge_description);
		}

		$numberMyChallenges = $challenge_model->getNumberOfMyChallenges();

		echo json_encode(array("success"=>1,"results"=>$response,"numberMyChallenges"=>$numberMyChallenges));
	}

	public function challengeWinners($type,$page){
		$challenge_model = $this->loadModel('challenge_model');
		$page = ($page*10)-10;
		if($type == "popular"){
			$response = $challenge_model->getChallengeWinners($type,$page);
		}else if($type == "soonest"){
			$response = $challenge_model->getChallengeWinners($type,$page);
		}

		foreach($response as $challenge){
			$challenge->challenge_name = utf8_encode($challenge->challenge_name);
			$challenge->challenge_description = utf8_encode($challenge->challenge_description);
		}

		$numberChallengeWinners = $challenge_model->getNumberOfChallengeWinners();

		echo json_encode(array("success"=>1,"results"=>$response,"numberChallengeWinners"=>$numberChallengeWinners));
	}

	public function getChallenge($id){
		$challenge_model = $this->loadModel('challenge_model');
		$me = Session::get('profile')->id;
		$response = $challenge_model->getChallenge($id,$me);
		foreach($response as $challenge){
			//get comments
			$comments = $challenge_model->getChallengeComments($challenge->id);
			$challenge->comments = $comments;
			//get submitted stories
			$stories = $challenge_model->getChallengeStories($challenge->id);
			$challenge->stories = $stories;
		}

		echo json_encode(array("success"=>1,"results"=>$response));
	}

	public function getChallengeDetails(){
		$id = $_POST['challenge_id'];
		$challenge_model = $this->loadModel('challenge_model');
		$me = Session::get('profile')->id;
		$response = $challenge_model->getChallenge($id,$me);

		echo json_encode(array("success"=>1,"results"=>$response));
	}

	public function submitChallenge(){
		$save = $this->loadModel('user_model');

		$story_name = $_POST['title'];
		$story_description = $_POST['description'];
		$story_type = $_POST['type'];
		$story_category = $_POST['categories'];
		$written_by = Session::get('profile')->id;
		$is_public = 1;
		$tags = $_POST['tags'];
		$tags = explode(",", $tags);
		$tags_slug = array();
		foreach($tags as $tag1){
			array_push($tags_slug, str_replace(" ", "-", strtolower($tag1)));
		}
		$challenge_id = $_POST['challenge_id'];
		$chapter_title = $_POST['chapter_title'];
		$chapter_text = $_POST['chapter_text'];
		$chapter_slug = str_replace(" ", "-", strtolower($chapter_title));
		$slug = str_replace(" ", "-", strtolower($story_name));

		//story
		$array_to_insert = array("story_name"=>$story_name,"story_description"=>$story_description,"story_type"=>3,"written_by"=>$written_by,"is_public"=>$is_public,"slug"=>$slug);
		$response = $save->insertIntoTable($array_to_insert,"story");
		//chapter
		$array_to_insert2 = array("chapter_name"=>$chapter_title, "written_by"=>$written_by, "chapter_body"=>$chapter_text, "status"=>1, "slug"=>$chapter_slug);
		$response2 = $save->insertIntoTable($array_to_insert2,"chapter");
		//map
		$array_to_insert3 = array("story_id"=>$response,"branch_id"=>1,"chapter_id"=>$response2);
		$response3 = $save->insertIntoTable($array_to_insert3,"story_map");
		//tags, insert new ones
		$i = 0;
		foreach($tags as $tag){
			$tag_to_insert = array("story_id"=>$response,"tag"=>$tag,"slug"=>$tags_slug[$i]);
			$save->insertIntoTable($tag_to_insert,"story_tag");
			$i++;
		}
		//categories, insert new ones
		foreach($story_category as $category){
			$category_to_insert = array("story_id"=>$response,"category_id"=>$category);
			$save->insertIntoTable($category_to_insert,"story_category");
		}
		//insert into challenge_stories
		$array_to_insert4 = array("challenge_id"=>$challenge_id,"story_id"=>$response,"submitted_by"=>$written_by,"is_winner"=>0);
		$response4 = $save->insertIntoTable($array_to_insert4,"challenge_stories");

		//get owner of challenge and notify
		$challenge_model = $this->loadModel('challenge_model');
		$challenge_owner = $challenge_model->getOwnerOfChallenge($challenge_id);

		if($challenge_owner != $written_by){
			$array_to_insert = array("activity_by"=>$written_by, "activity_type_id"=>15, "action_id"=>$response4, "user_to_notify"=>$challenge_owner);
			$save->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $save->returnUserById($challenge_owner);
			$challenge_details = $challenge_model->getChallengeById($challenge_id);
			//check if owner has subscribed to new brainstorm email alert
			$has_subscribed = $notification_model->checkIfSubscribed("15",$challenge_owner);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/story/".$response."/".$slug;
				$challenge_submit_owner_email = $email_model->challengeSubmitOwnerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$challenge_details->story_name,$status_url);
				$title = Session::get('profile')->real_name." has submitted a story to your challenge";
				$newAccountEmail = $email_model->sendEmail($challenge_submit_owner_email, $title, $user_to_alert->email);
			}
			//email notification end
		}

		//now get people who have subscribed to the challenge (not me) and notify
		$challenge_subscribers = $challenge_model->getChallengeSubscribers($challenge_id, $written_by);

		foreach($challenge_subscribers as $subscriber){
			$array_to_insert = array("activity_by"=>$written_by, "activity_type_id"=>20, "action_id"=>$response4, "user_to_notify"=>$subscriber->subscribed_by);
			$save->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $save->returnUserById($subscriber->subscribed_by);
			$challenge_details = $challenge_model->getChallengeById($challenge_id);
			//check if owner has subscribed to new brainstorm email alert
			$has_subscribed = $notification_model->checkIfSubscribed("20",$challenge_owner);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/story/".$response."/".$slug;
				$challenge_submit_follower_email = $email_model->challengeSubmitFollowerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$challenge_details->story_name,$status_url);
				$title = Session::get('profile')->real_name." has submitted a story to a challenge you follow";
				$newAccountEmail = $email_model->sendEmail($challenge_submit_owner_email, $title, $user_to_alert->email);
			}
			//email notification end
		}
		//end notification

		echo json_encode(array("success"=>1,"type"=>"new","story_id"=>$response,"slug"=>$slug));
	}

	public function commentChallenge (){
		$comment = $_POST['comment'];
		$challenge_id = $_POST['challenge_id'];
		$me = Session::get('profile')->id;

		$save = $this->loadModel('user_model');

		$array_to_insert = array("challenge_id"=>$challenge_id,"author_id"=>$me,"comment"=>$comment);
		$comment_id = $save->insertIntoTable($array_to_insert,"challenge_discussion");

		//get owner of challenge and notify
		$challenge_model = $this->loadModel('challenge_model');
		$challenge_owner = $challenge_model->getOwnerOfChallenge($challenge_id);

		if($challenge_owner != $me){
			$array_to_insert = array("activity_by"=>$me, "activity_type_id"=>16, "action_id"=>$comment_id, "user_to_notify"=>$challenge_owner);
			$save->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $save->returnUserById($challenge_owner);
			$challenge_details = $challenge_model->getChallengeById($challenge_id);
			//check if owner has subscribed to new brainstorm email alert
			$has_subscribed = $notification_model->checkIfSubscribed("16",$challenge_owner);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/challenge/".$challenge_id."/".$challenge_details->slug;
				$challenge_comment_owner_email = $email_model->challengeCommentOwnerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$challenge_details->story_name,$status_url);
				$title = Session::get('profile')->real_name." has left a comment on one of your challenges";
				$newAccountEmail = $email_model->sendEmail($challenge_comment_owner_email, $title, $user_to_alert->email);
			}
			//email notification end
		}
		//end notification

		echo json_encode(array("success"=>1,"comment_id"=>$comment_id));
	}

	public function checkChallengeWinners(){
		//get list of challenges that ended yesterday
		$challenge_model = $this->loadModel('challenge_model');
		$user_model = $this->loadModel('user_model');

		$challenges = $challenge_model->getChallengesThatEndedYesterday();
		foreach($challenges as $challenge){

			//notification - get owner and people who submitted story and notify
			$people = $challenge_model->getOwnerOfChallengeAndSubmitters($challenge->id);
			//get the owner
			$owner_id = 0;
			$i = 0;
			$owner_id_number = 0;
			foreach($people as $person){
				if($person->type == "owner"){
					$owner_id = $person->id;
					$owner_id_number = $i;
				}
				$i++;
			}
			array_splice($people, $owner_id_number, 1);
			//notify participants first
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			foreach($people as $person){
				if($person->type == "submitter" && $person->id != $owner_id){
					$array_to_insert = array("activity_type_id"=>17,"action_id"=>$challenge->id,"user_to_notify"=>$person->id);
					$user_model->insertIntoTable($array_to_insert,"notification");

					//email_notification
					$user_to_alert = $user_model->returnUserById($person->id);
					$challenge_details = $challenge_model->getChallengeById($challenge->id);
					//check if owner has subscribed to new brainstorm email alert
					$has_subscribed = $notification_model->checkIfSubscribed("17",$person->id);
					if($has_subscribed == 1){
						$status_url = "http://www.bloqly.com/challenge/".$challenge->id."/".$challenge->slug;
						$challenge_end_submitter_email = $email_model->challengeEndSubmitterEmailBody($user_to_alert->real_name,$challenge->challenge_name,$status_url);
						$title = "The challenge ".$challenge->challenge_name." has ended";
						$newAccountEmail = $email_model->sendEmail($challenge_end_submitter_email, $title, $user_to_alert->email);
					}
					//email notification end
				}
			}
			//now notify owner
			$array_to_insert = array("activity_type_id"=>17,"action_id"=>$challenge->id,"user_to_notify"=>$owner_id);
			$user_model->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$user_to_alert = $user_model->returnUserById($owner_id);
			$challenge_details = $challenge_model->getChallengeById($challenge->id);
			//check if owner has subscribed to new brainstorm email alert
			$has_subscribed = $notification_model->checkIfSubscribed("17",$owner_id);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/challenge/".$challenge->id."/".$challenge->slug;
				$challenge_end_owner_email = $email_model->challengeEndOwnerEmailBody($user_to_alert->real_name,$challenge->challenge_name,$status_url);
				$title = "Your challenge ".$challenge->challenge_name." has ended";
				$newAccountEmail = $email_model->sendEmail($challenge_end_owner_email, $title, $user_to_alert->email);
			}
			//email notification end

			//update challenge_stories with average rating as final rating
			$challenge_stories = $challenge_model->getAllChallengeStories($challenge->story_id);

			foreach($challenge_stories as $challenge_story){
				$average_rating = $challenge_model->getStoryRating($challenge_story->story_id);
				//now update challenge_story
				$row_to_update = array("final_rating"=>$average_rating->avg_rating);
				$where = array("challenge_id"=>$challenge->id,"story_id"=>$challenge_story->story_id);
				$user_model->updateTable($row_to_update,$where,"challenge_stories");
			}

			//get list of stories linked to challenge
			$stories = $challenge_model->getChallengeStoryWithHighestAverageRating($challenge->id);
			//update this story to be winner
			print_r($stories);
			foreach($stories as $story){
				$row_to_update = array("is_winner"=>1);
				$where = array("id"=>$story->challenge_story_id);
				$user_model->updateTable($row_to_update,$where,"challenge_stories");
			
				//get winner's email
				$user = $user_model->returnUserById($story->user_id);
				print_r($user->email);
				//foreach($user as $user1){
					//email user
					$message = "Greetings from Bloqly,\r\n\r\nCongratulations, ".$user1->real_name."\r\n\r\nThe challenge ".$challenge->challenge_name." has ended and your submitted story ".$story->story_name." has won with an average rating of ".round($story->average_rating,2)." stars out of five. If you would like to view other stories submitted in this Challenge, go to http://beta.bloqly.com/challenge/".$challenge->id."/".$challenge->slug;

					$subject = "Your story has won a Bloqly challenge!";
					
					$emailModel = $this->loadModel('email_model');

					$email = $user->email;

					$newPasswordEmail = $emailModel->sendEmail($message,$subject,$email);
				//}
			}
		}
	}

	public function subscribeChallenge(){
		$challenge_id = $_POST['challenge_id'];
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$array_to_insert = array("subscription_type"=>5,"item_subscribed_to"=>$challenge_id,"subscribed_by"=>$me);
		$user_model->insertIntoTable($array_to_insert,"subscription");
	}

	public function unsubscribeChallenge(){
		$challenge_id = $_POST['challenge_id'];
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$row_to_delete = array("subscription_type"=>5,"item_subscribed_to"=>$challenge_id,"subscribed_by"=>$me);
		$user_model->deleteRows($row_to_delete,"subscription");
	}

}
