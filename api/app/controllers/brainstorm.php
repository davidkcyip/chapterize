<?php

class Brainstorm extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function getMine(){
		$me = Session::get('profile')->id;
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$response = $brainstorm_model->getUserBrainstorms($me);

		echo json_encode(array("success"=>1,"results"=>$response));
	}

	public function getMyList(){
		$me = Session::get('profile')->id;
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$subscriptions = $brainstorm_model->getBrainstormThreadsFromList($me);

		foreach($subscriptions as $subscription){
			//get posts
			$post = $brainstorm_model->getBrainstormThreadPosts($subscription->id);
			$subscription->posts = $post;
		}

		echo json_encode(array("success"=>1,"results"=>$subscriptions));
	}

	public function brainstormCategories (){
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$categories = $brainstorm_model->getCategories();

		echo json_encode(array("success"=>1,"result"=>$categories));
	}

	public function getTopic(){
		$story_id = $_GET['id'];
		$story_slug = $_GET['slug'];
		$thread_id = $_GET['thread_id'];
		$me = Session::get('profile')->id;
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$topics = $brainstorm_model->getThread($story_id,$thread_id,$me);
		foreach($topics as $topic){
			$post = $brainstorm_model->getBrainstormThreadPosts($topic->id);
			$topic->posts = $post;
		}
		echo json_encode(array("success"=>1,"results"=>$topics));
	}

	public function newTopic(){
		$title = $_POST['title'];
		$message = $_POST['message'];
		$category = $_POST['category'];
		$story_id = $_POST['story_id'];
		$me = Session::get('profile')->id;
		$title_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
		$title_slug = str_replace(" ", "-", strtolower($title_slug));
		$user_model = $this->loadModel('user_model');
		//insert into thread first
		$array_to_insert = array("story_id"=>$story_id,"thread_title"=>$title,"thread_started_by"=>$me,"thread_type"=>$category,"status"=>1,"slug"=>$title_slug);
		$thread_id = $user_model->insertIntoTable($array_to_insert,"brainstorm_thread");
		//now into post
		$array_to_insert = array("thread_id"=>$thread_id,"post_body"=>$message,"posted_by"=>$me);
		$post_id = $user_model->insertIntoTable($array_to_insert,"brainstorm_post");

		//get owner and subscribers of brainstorm, and notify
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$people = $brainstorm_model->getOwnerAndSubscribersOfBrainstorm($story_id);
		foreach($people as $person){
			if($person->type == "follower" && $person->id != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>13,"action_id"=>$thread_id,"user_to_notify"=>$person->id);
				$user_model->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$story_model = $this->loadModel('story_model');
				$story_details = $story_model->getStoryDetails($story_id);
				$user_to_alert = $user_model->returnUserById($person->id);
				//check if owner has subscribed to new brainstorm email alert
				$has_subscribed = $notification_model->checkIfSubscribed("13",$person->id);
				if($has_subscribed == 1){
					$status_url = "http://www.bloqly.com/brainstorm/".$story_id."/".$story_details->slug."/read/".$thread_id."/".$title_slug;
					$new_brainstorm_topic_follower_email = $email_model->newBrainstormTopicFollowerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$story_details->story_name,$status_url);
					$title = Session::get('profile')->real_name." has started a new Brainstorm topic in a story";
					$newAccountEmail = $email_model->sendEmail($new_brainstorm_topic_follower_email, $title, $user_to_alert->email);
				}
				//email notification end
			}else if($person->type == "owner" && $person->id != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>11,"action_id"=>$thread_id,"user_to_notify"=>$person->id);
				$user_model->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$story_model = $this->loadModel('story_model');
				$story_details = $story_model->getStoryDetails($story_id);
				$user_to_alert = $user_model->returnUserById($person->id);
				//check if owner has subscribed to new brainstorm email alert
				$has_subscribed = $notification_model->checkIfSubscribed("11",$person->id);
				if($has_subscribed == 1){
					$status_url = "http://www.bloqly.com/brainstorm/".$story_id."/".$story_details->slug."/read/".$thread_id."/".$title_slug;
					$new_brainstorm_topic_owner_email = $email_model->newBrainstormTopicOwnerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$story_details->story_name,$status_url);
					$title = Session::get('profile')->real_name." has started a new Brainstorm topic in one of your stories";
					$newAccountEmail = $email_model->sendEmail($new_brainstorm_topic_owner_email, $title, $user_to_alert->email);
				}
				//email notification end
			}
		}
		//end notification
		echo json_encode(array("success"=>1,"thread_id"=>$thread_id,"slug"=>$title_slug));
	}	

	public function replyTopic(){
		$story_id = $_POST['story_id'];
		$message = $_POST['message'];
		$thread_id = $_POST['thread_id'];
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');
		//now into post
		$array_to_insert = array("thread_id"=>$thread_id,"post_body"=>$message,"posted_by"=>$me);
		$post_id = $user_model->insertIntoTable($array_to_insert,"brainstorm_post");

		//get owner of topic and everyone who subscribes and notify
		$brainstorm_model = $this->loadModel('brainstorm_model');
		$people = $brainstorm_model->getOwnerAndSubscribersOfTopic($thread_id);
		foreach($people as $person){
			if($person->type == "follower" && $person->id != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>14,"action_id"=>$thread_id,"user_to_notify"=>$person->id);
				$user_model->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$story_model = $this->loadModel('story_model');
				$story_details = $story_model->getStoryDetails($story_id);
				$user_to_alert = $user_model->returnUserById($person->id);
				$title_slug = $brainstorm_model->getSlug($thread_id);
				//check if owner has subscribed to new brainstorm email alert
				$has_subscribed = $notification_model->checkIfSubscribed("14",$person->id);
				if($has_subscribed == 1){
					$status_url = "http://www.bloqly.com/brainstorm/".$story_id."/".$story_details->slug."/read/".$thread_id."/".$title_slug;
					$brainstorm_topic_reply_follower_email = $email_model->brainstormTopicReplyFollowerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$story_details->story_name,$status_url);
					$title = Session::get('profile')->real_name." has replied to a Brainstorm topic you are following";
					$newAccountEmail = $email_model->sendEmail($brainstorm_topic_reply_follower_email, $title, $user_to_alert->email);
				}
				//email notification end
			}else if($person->type == "owner" && $person->id != $me){
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>12,"action_id"=>$thread_id,"user_to_notify"=>$person->id);
				$user_model->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$story_model = $this->loadModel('story_model');
				$story_details = $story_model->getStoryDetails($story_id);
				$user_to_alert = $user_model->returnUserById($person->id);
				$title_slug = $brainstorm_model->getSlug($thread_id);
				//check if owner has subscribed to new brainstorm email alert
				$has_subscribed = $notification_model->checkIfSubscribed("12",$person->id);
				if($has_subscribed == 1){
					$status_url = "http://www.bloqly.com/brainstorm/".$story_id."/".$story_details->slug."/read/".$thread_id."/".$title_slug;
					$brainstorm_topic_reply_owner_email = $email_model->brainstormTopicReplyOwnerEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$story_details->story_name,$status_url);
					$title = Session::get('profile')->real_name." has replied to a Brainstorm topic you started";
					$newAccountEmail = $email_model->sendEmail($brainstorm_topic_reply_owner_email, $title, $user_to_alert->email);
				}
				//email notification end
			}
		}

		echo json_encode(array("success"=>1,"id"=>$post_id));
	}

	public function getBrainstorm(){
		$id = $_GET['id'];
		$slug = $_GET['slug'];
		$me = Session::get('profile')->id;

		$brainstorm_model = $this->loadModel('brainstorm_model');

		$responses = $brainstorm_model->getBrainstormThreads($id,$me);

		foreach($responses as $response){
			//get posts
			$post = $brainstorm_model->getBrainstormThreadPosts($response->id);
			$response->posts = $post;
		}

		echo json_encode(array("success"=>1,"results"=>$responses));
	}

	public function subscribe(){
		$story_id = $_POST['story_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("subscription_type"=>4,"item_subscribed_to"=>$story_id,"subscribed_by"=>$me);
		$user_model->insertIntoTable($array_to_insert,"subscription");
	}

	public function unsubscribe(){
		$story_id = $_POST['story_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$row_to_delete = array("subscription_type"=>4,"item_subscribed_to"=>$story_id,"subscribed_by"=>$me);
		$user_model->deleteRows($row_to_delete,"subscription");
	}

	public function subscribeTopic(){
		$thread_id = $_POST['thread_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$array_to_insert = array("subscription_type"=>6,"item_subscribed_to"=>$thread_id,"subscribed_by"=>$me);
		$user_model->insertIntoTable($array_to_insert,"subscription");
	}

	public function unsubscribeTopic(){
		$thread_id = $_POST['thread_id'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$row_to_delete = array("subscription_type"=>6,"item_subscribed_to"=>$thread_id,"subscribed_by"=>$me);
		$user_model->deleteRows($row_to_delete,"subscription");
	}

	public function haveISubscribed(){
		$id = $_GET['id'];
		$me = Session::get('profile')->id;

		$brainstorm_model = $this->loadModel('brainstorm_model');

		$haveISubscribed = $brainstorm_model->haveISubscribed($id,$me);

		echo json_encode($haveISubscribed);
	}

}