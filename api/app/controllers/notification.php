<?php

class Notification extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function getNewNotifications(){
		$me = Session::get('profile')->id;
		$timestamp = $_GET['last_checked'];
		$last_checked = new DateTime("@$timestamp");
		$last_checked = $last_checked->format("Y-m-d H:i:s");

		$notification_model = $this->loadModel('notification_model');
		$new_notifications = $notification_model->getNewNotifications($last_checked,$me);

		foreach($new_notifications as $notification){
			if($notification->activity_type == "new_chapter"){
				$chapter = $notification_model->getChapter($notification->action_id);
				$chapter_body = strip_tags($chapter->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$chapter['chapter_body'] = $chapter_body;
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "comment_chapter" || $notification->activity_type == "rate_chapter"){
				$chapter = $notification_model->getChapterComment($notification->action_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "bookmark_chapter"){
				$chapter = $notification_model->getChapterBookmark($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "favourite_chapter"){
				$chapter = $notification_model->getChapterFavourite($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "new_your_brainstorm" || $notification->activity_type == "new_following_brainstorm" || $notification->activity_type == "reply_your_brainstorm" || $notification->activity_type == "reply_following_brainstorm"){
				$brainstorm = $notification_model->getBrainstorm($notification->action_id);
				$notification->brainstorm = $brainstorm;
			}else if($notification->activity_type == "submit_challenge" || $notification->activity_type == "submit_challenge_subscription"){
				$challenge_story = $notification_model->getStory($notification->action_id);
				$chapter_body = strip_tags($challenge_story->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$challenge_story['chapter_body'] = $chapter_body;
				$notification->challenge = $challenge_story;
			}else if($notification->activity_type == "comment_challenge"){
				$challenge_comment = $notification_model->getChallengeComment($notification->action_id);
				$notification->challenge = $challenge_comment;
			}else if($notification->activity_type == "challenge_end"){
				$challenge = $notification_model->getChallengeWinner($notification->action_id);
				$notification->challenge = $challenge;
			}else if($notification->activity_type == "reply_status"){
				$status = $notification_model->getReply($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "status_to_you" || $notification->activity_type == "status_share"){
				$status = $notification_model->getStatus($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "topic_reply"){
				$topic = $notification_model->getTopic($notification->action_id,$notification->activity_user_id);
				$notification->topic = $topic;
			}else if($notification->activity_type == "accept_circle_request" || $notification->activity_type == "request_circle"){
				$circle = $notification_model->getCircle($notification->action_id,$notification->activity_user_id);
				$notification->circle = $circle;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$new_notifications));
	}

	public function getAllUnreadNotifications(){
		$me = Session::get('profile')->id;
		$notification_model = $this->loadModel('notification_model');
		$all_unread_notifications = $notification_model->getAllUnreadNotifications($me);
		foreach($all_unread_notifications as $notification){
			if($notification->activity_type == "new_chapter"){
				$chapter = $notification_model->getChapter($notification->action_id);
				$chapter_body = strip_tags($chapter->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$chapter['chapter_body'] = $chapter_body;
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "comment_chapter" || $notification->activity_type == "rate_chapter"){
				$chapter = $notification_model->getChapterComment($notification->action_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "bookmark_chapter"){
				$chapter = $notification_model->getChapterBookmark($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "favourite_chapter"){
				$chapter = $notification_model->getChapterFavourite($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "new_your_brainstorm" || $notification->activity_type == "new_following_brainstorm"  || $notification->activity_type == "reply_your_brainstorm" || $notification->activity_type == "reply_following_brainstorm"){
				$brainstorm = $notification_model->getBrainstorm($notification->action_id);
				$notification->brainstorm = $brainstorm;
			}else if($notification->activity_type == "submit_challenge" || $notification->activity_type == "submit_challenge_subscription"){
				$challenge_story = $notification_model->getStory($notification->action_id);
				$chapter_body = strip_tags($challenge_story->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$challenge_story['chapter_body'] = $chapter_body;
				$notification->challenge = $challenge_story;
			}else if($notification->activity_type == "comment_challenge"){
				$challenge_comment = $notification_model->getChallengeComment($notification->action_id);
				$notification->challenge = $challenge_comment;
			}else if($notification->activity_type == "challenge_end"){
				$challenge = $notification_model->getChallengeWinner($notification->action_id);
				$notification->challenge = $challenge;
			}else if($notification->activity_type == "reply_status"){
				$status = $notification_model->getReply($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "status_to_you" || $notification->activity_type == "status_share"){
				$status = $notification_model->getStatus($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "topic_reply"){
				$topic = $notification_model->getTopic($notification->action_id,$notification->activity_user_id);
				$notification->topic = $topic;
			}else if($notification->activity_type == "accept_circle_request" || $notification->activity_type == "request_circle"){
				$circle = $notification_model->getCircle($notification->action_id,$notification->activity_user_id);
				$notification->circle = $circle;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$all_unread_notifications));
	}

	public function getAllNotifications(){
		$me = Session::get('profile')->id;
		$notification_model = $this->loadModel('notification_model');
		$all_notifications = $notification_model->getAllNotifications($me);
		foreach($all_notifications as $notification){
			if($notification->activity_type == "new_chapter"){
				$chapter = $notification_model->getChapter($notification->action_id);
				$chapter_body = strip_tags($chapter->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$chapter['chapter_body'] = $chapter_body;
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "comment_chapter" || $notification->activity_type == "rate_chapter"){
				$chapter = $notification_model->getChapterComment($notification->action_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "bookmark_chapter"){
				$chapter = $notification_model->getChapterBookmark($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "favourite_chapter"){
				$chapter = $notification_model->getChapterFavourite($notification->action_id,$notification->activity_user_id);
				$notification->chapter = $chapter;
			}else if($notification->activity_type == "new_your_brainstorm" || $notification->activity_type == "new_following_brainstorm"  || $notification->activity_type == "reply_your_brainstorm" || $notification->activity_type == "reply_following_brainstorm"){
				$brainstorm = $notification_model->getBrainstorm($notification->action_id);
				$notification->brainstorm = $brainstorm;
			}else if($notification->activity_type == "submit_challenge" || $notification->activity_type == "submit_challenge_subscription"){
				$challenge_story = $notification_model->getStory($notification->action_id);
				$chapter_body = strip_tags($challenge_story->chapter_body);
				$chapter_body = substr($chapter_body, 0, 200);
				$challenge_story['chapter_body'] = $chapter_body;
				$notification->challenge = $challenge_story;
			}else if($notification->activity_type == "comment_challenge"){
				$challenge_comment = $notification_model->getChallengeComment($notification->action_id);
				$notification->challenge = $challenge_comment;
			}else if($notification->activity_type == "challenge_end"){
				$challenge = $notification_model->getChallengeWinner($notification->action_id);
				$notification->challenge = $challenge;
			}else if($notification->activity_type == "reply_status"){
				$status = $notification_model->getReply($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "status_to_you" || $notification->activity_type == "status_share"){
				$status = $notification_model->getStatus($notification->action_id);
				$notification->status = $status;
			}else if($notification->activity_type == "topic_reply"){
				$topic = $notification_model->getTopic($notification->action_id,$notification->activity_user_id);
				$notification->topic = $topic;
			}else if($notification->activity_type == "accept_circle_request" || $notification->activity_type == "request_circle"){
				$circle = $notification_model->getCircle($notification->action_id,$notification->activity_user_id);
				$notification->circle = $circle;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$all_notifications));
	}

	public function readNotification(){
		$notification_id = $_GET['id'];
		$save = $this->loadModel('user_model');
		$array_to_update = array("is_read"=>1);
		$where = array("id"=>$notification_id);
		if($save->updateTable($array_to_update,$where,"notification")){
			self::getAllUnreadNotifications();
		}else{
			echo json_encode(array("success"=>0));
		}
	}

}