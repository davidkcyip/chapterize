<?php

class Message extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function test(){
		$recipients = "Test (davey)";
		$user_model = $this->loadModel('user_model');
		$recipients = explode(",", $recipients);
		$temp_recipients = array();
		foreach($recipients as $recipient){
			if(!is_numeric($recipient)){
				$recipient_explode = explode("(", $recipient);
				$pen_name = explode(")", $recipient_explode[1]);
				echo json_encode($pen_name);
				$user = $user_model->returnUserByPenname($pen_name[0]);
				array_push($temp_recipients,$user->id);
			}
		}
		$recipients = $temp_recipients;
		echo json_encode($recipients);
	}

	public function startNewThread(){
		$sender = $_POST['sender'];
		$recipients = $_POST['recipient'];
		$title = $_POST['thread_title'];
		$message = $_POST['message'];
		$title_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
		$title_slug = str_replace(" ", "-", strtolower($title_slug));
		$temp_recipients = array();
		$user_model = $this->loadModel('user_model');
		if(strrpos($recipients, "(") !== false){
			$recipients = explode(",", $recipients);
			foreach($recipients as $recipient){
				if(!is_numeric($recipient)){
					$recipient_explode = explode("(", $recipient);
					$pen_name = explode(")", $recipient_explode[1]);
					$user = $user_model->returnUserByPenname($pen_name[0]);
					array_push($temp_recipients,$user->id);
				}
			}
			$recipients = $temp_recipients;
		}
		
		//insert into pm_thread
		$array_to_insert = array("message_title"=>$title,"created_by"=>$sender,"slug"=>$title_slug);
		$thread_id = $user_model->insertIntoTable($array_to_insert,"pm_thread");
		//insert into pm_post
		$array_to_insert = array("pm_thread_id"=>$thread_id,"message_body"=>$message,"sent_by"=>$sender);
		$post_id = $user_model->insertIntoTable($array_to_insert,"pm_post");
		//insert into pm_recipient, if recipients is an array of users then iterate, else do once
		if(is_array($recipients)){
			foreach($recipients as $recipient){
				$array_to_insert = array("thread_id"=>$thread_id,"author_id"=>$recipient);
				$user_model->insertIntoTable($array_to_insert,"pm_recipients");
				$array_to_insert = array("thread_id"=>$thread_id,"post_id"=>$post_id,"author_id"=>$recipient,"has_read"=>0);
				$user_model->insertIntoTable($array_to_insert,"pm_post_read");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$this_recipient = $user_model->returnUserById($recipient);
				//check if owner has subscribed to new follower email alert
				$has_subscribed = $notification_model->checkIfSubscribed("10",$this_recipient->id);
				if($has_subscribed == 1){
					$message_url = "http://www.bloqly.com/message/read/".$thread_id."/".$title_slug;
					$new_message_email = $email_model->newPrivateMessageEmailBody($this_recipient->real_name,Session::get('profile')->real_name,$message_url);
					$title = Session::get('profile')->real_name." has sent you a private message";
					$newAccountEmail = $email_model->sendEmail($new_message_email, $title, $this_recipient->email);
				}
				//email notification end
			}
		}else{
			$array_to_insert = array("thread_id"=>$thread_id,"author_id"=>$recipients);
			$user_model->insertIntoTable($array_to_insert,"pm_recipients");

			$array_to_insert = array("thread_id"=>$thread_id,"post_id"=>$post_id,"author_id"=>$recipients,"has_read"=>0);
			$user_model->insertIntoTable($array_to_insert,"pm_post_read");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$this_recipient = $user_model->returnUserById($recipients);
			//check if owner has subscribed to new follower email alert
			$has_subscribed = $notification_model->checkIfSubscribed("10",$this_recipient->id);
			if($has_subscribed == 1){
				$message_url = "http://www.bloqly.com/message/read/".$thread_id."/".$title_slug;
				$new_message_email = $email_model->newPrivateMessageEmailBody($this_recipient->real_name,Session::get('profile')->real_name,$message_url);
				$title = Session::get('profile')->real_name." has sent you a private message";
				$newAccountEmail = $email_model->sendEmail($new_message_email, $title, $this_recipient->email);
			}
			//email notification end
		}
		//insert sender into pm_recipient and pm_post_read
		$array_to_insert = array("thread_id"=>$thread_id,"author_id"=>$sender);
		$user_model->insertIntoTable($array_to_insert,"pm_recipients");

		$array_to_insert = array("thread_id"=>$thread_id,"post_id"=>$post_id,"author_id"=>$sender,"has_read"=>1);
		$user_model->insertIntoTable($array_to_insert,"pm_post_read");

		echo json_encode(array("success"=>1,"thread_id"=>$thread_id,"slug"=>$title_slug));
	}

	public function getInbox(){
		$me = Session::get('profile')->id;

		$message_model = $this->loadModel('message_model');
		$messages = $message_model->getInbox($me);

		//get recipients
		foreach($messages as $message){
			$recipients = $message_model->getRecipients($message->thread_id);
			$message->recipients = $recipients;
		}

		echo json_encode(array("success"=>1,"result"=>$messages));
	}

	public function getSent(){
		$me = Session::get('profile')->id;

		$message_model = $this->loadModel('message_model');
		$messages = $message_model->getSent($me);

		//get recipients
		foreach($messages as $message){
			$recipients = $message_model->getRecipients($message->thread_id);
			$message->recipients = $recipients;
		}

		echo json_encode(array("success"=>1,"result"=>$messages));
	}

	public function getThread(){
		$thread_id = $_POST['id'];
		$slug = $_POST['slug'];
		$me = Session::get('profile')->id;

		$message_model = $this->loadModel('message_model');
		$messages = $message_model->getThread($thread_id,$slug);

		//update pm_post_read table
		$user_model = $this->loadModel('user_model');

		$row_to_update = array("has_read"=>1);
		$where = array("thread_id"=>$thread_id,"author_id"=>$me);
		$user_model->updateTable($row_to_update,$where,"pm_post_read");

		//get recipients
		foreach($messages as $message){
			$recipients = $message_model->getRecipients($thread_id);
			$message->recipients = $recipients;
		}
		$amIARecipient = 0;
		foreach($message->recipients as $recipient){
			if($recipient->id == $me){
				$amIARecipient = 1;
			}
		}
		if($amIARecipient == 1){
			echo json_encode(array("success"=>1,"result"=>$messages));
		}
	}

	public function sendMessage(){
		$message = $_POST['message'];
		$thread_id = $_POST['thread_id'];
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		$array_to_insert = array("pm_thread_id"=>$thread_id,"message_body"=>$message,"sent_by"=>$me);
		$post_id = $user_model->insertIntoTable($array_to_insert,"pm_post");

		//find recipients
		$message_model = $this->loadModel('message_model');
		$recipients = $message_model->getRecipients($thread_id);
		$thread_details = $message_model->getThreadById($thread_id);

		foreach($recipients as $recipient){
			if($recipient->id != $me){
				$array_to_insert = array("thread_id"=>$thread_id,"post_id"=>$post_id,"author_id"=>$recipient->id,"has_read"=>0);
				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$this_recipient = $user_model->returnUserById($recipient->id);
				//check if owner has subscribed to new follower email alert
				$has_subscribed = $notification_model->checkIfSubscribed("10",$this_recipient->id);
				if($has_subscribed == 1){
					$message_url = "http://www.bloqly.com/message/read/".$thread_id."/".$thread_details->slug;
					$new_message_email = $email_model->newPrivateMessageEmailBody($this_recipient->real_name,Session::get('profile')->real_name,$message_url);
					$title = Session::get('profile')->real_name." has sent you a private message";
					$newAccountEmail = $email_model->sendEmail($new_message_email, $title, $this_recipient->email);
				}
				//email notification end
			}else{
				$array_to_insert = array("thread_id"=>$thread_id,"post_id"=>$post_id,"author_id"=>$recipient->id,"has_read"=>1);
				
			}
			$post_id = $user_model->insertIntoTable($array_to_insert,"pm_post_read");
		}
	}

	public function getNumberOfUnreadMessages(){
		$me = Session::get('profile')->id;

		$message_model = $this->loadModel('message_model');
		$unread = $message_model->getNumberOfUnreadMessages($me);

		echo json_encode(array("success"=>1, "unread"=>$unread));
	}

	public function contactForm(){
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$title = "New message from the contact form";
		$email_model = $this->loadModel('email_model');

		$message = "Name: ".$name."\r\n\r\n".$message;

		$email_model->sendEmail($message,$title,"support@bloqly.com",$email);
		echo json_encode(array("success"=>1));
	}

}