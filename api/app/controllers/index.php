<?php

class Index extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index($request = null){

		$data['title'] = 'Home';

		$this->view->rendertemplate('header',$data);
		$this->view->render('index/index',$data);
		$this->view->rendertemplate('footer',$data);
	}

}