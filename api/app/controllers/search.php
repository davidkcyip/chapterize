<?php

class Search extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function searchResults(){
		$query = $_GET['query'];
		$page = ($_GET['page']*10)-10;

		$results_model = $this->loadModel('search_model');
		$story_model = $this->loadModel('story_model');

		$results = $results_model->searchResults($query,$page);
		$storyCount = $results_model->searchResultsStoryCount($query);
		$chapterCount = $results_model->searchResultsChapterCount($query);
		$authorCount = $results_model->searchResultsAuthorCount($query);
		$totalCount = $storyCount+$chapterCount+$authorCount;
		foreach($results as $result){
			$result->bio = utf8_encode($result->bio);
			$result->pen_name = utf8_encode($result->pen_name);
			$result->real_name = utf8_encode($result->real_name);
			if($result->type == "user"){
				$user = $this->loadModel('user_model');
				//get number of stories
				$numStories = $user->getNumberOfStories($result->user_id);
				$result->story_count = $numStories;
				//get number of chapters
				$numChapters = $story_model->getNumberOfChaptersOfUser($result->user_id);
				$result->chapter_count = $numChapters;
				//get number of comments made
				$numComments = $user->getNumberOfComments($result->user_id);
				$result->comment_count = $numComments;
				//get number of ratings made
				$numRatings = $user->getNumberOfRatings($result->user_id);
				$result->ratings_made = $numRatings;
				//get number of ratings made by users on this peron's chapters
				$num_ratings_for_author = $user->getNumberOfPeopleWhoRated($result->user_id);
				$result->num_ratings_for_author = $num_ratings_for_author;
				//get number of chapters this user read
				$read_history = $user->getNumberOfReadsMade($result->user_id);
				$result->read_history = $read_history;
				//get number of chapters this user read unique
				$read_history_unique = $user->getNumberOfReadsMadeUnique($result->user_id);
				$result->read_history_unique = $read_history_unique;
				//get avg rating of this person's hcapters
				$avg_rating = $user->getAverageRating($result->user_id);
				$result->avg_rating = $avg_rating;
				//get number o fpeople who read this person's chapters
				$num_reads = $user->getNumberOfReads($result->user_id);
				$result->num_reads = $num_reads;
				//get num favourites
				$num_favourites = $user->getNumberOfFavourites($result->user_id);
				$result->num_favourites = $num_favourites;
			}else if($result->type == "story"){
				$result->story_description = utf8_encode($result->story_description);
				$result->story_slug = utf8_encode($result->story_slug);
				$result->story_name = utf8_encode($result->story_name);
				$story = $this->loadModel('story_model');
				$numOfChapters = $story->getChapters($result->story_id);
				$result->chapters = $numOfChapters;
				//get categories
				$categories = $story->getCategoriesOfStory($result->story_id);
				$result->categories = $categories;
				//get tags
				$tags = $story->getTagsOfStory($result->story_id);
				$result->tags = $tags;
				//get comments
				$comments = $story->getCommentsOfStory($result->story_id);
				$result->comments = $comments;
				//get comments for chapters
				foreach($result->chapters as $chapter){
					$chapter_comments = $story->getCommentsOfChapter($chapter->id);
					$chapter->comments = $chapter_comments;
					$favourites = $story->getNumberOfChapterFavourites($chapter->id);
					$chapter->favourites = $favourites;
					$bookmarks = $story->getNumberOfChapterBookmarks($chapter->id);
					$chapter->bookmarks = $bookmarks;
					$read_history = $story->getReadHistory($chapter->id);
					$chapter->read_history = $read_history;
					$haveIFavourited = $story->checkIfIHaveFavouritedChapter($chapter->id,$me);
					$chapter->haveIFavourited = $haveIFavourited;
					$haveIBookmarked = $story->checkIfIHaveBookmarkedChapter($chapter->id,$me);
					$chapter->haveIBookmarked = $haveIBookmarked;
				}
			}else{
				$story = $this->loadModel('story_model');
				//get avg rating
				$ratings = $story->getAverageRatingsOfChapters($result->chapter_id);
				$result->average_rating = $ratings[0]->average_rating;
				$result->rating_count = $ratings[0]->rating_count;
				$result->rating_sum = $ratings[0]->rating_sum;
				//get comments
				$chapter_comments = $story->getCommentsOfChapter($result->chapter_id);
				$result->comments = $chapter_comments;
				//get # favourited
				$favourites = $story->getNumberOfChapterFavourites($result->chapter_id);
				$result->favourites = $favourites->favourite_count;
				//get # bookmarked
				$bookmarks = $story->getNumberOfChapterBookmarks($result->chapter_id);
				$result->bookmarks = $bookmarks;
				//get people who read
				$read_history = $story->getReadHistory($result->chapter_id);
				$result->read_history = $read_history;
				//get story details
				$story_details = $story->getStoryOfChapter($result->chapter_id);
				$result->story_details = $story_details;
			}
		}

		echo json_encode(array("success"=>1,"results"=>$results,"searchCount"=>$totalCount));
	}

	public function searchAllTags(){
		$search_model = $this->loadModel('search_model');
		$tags = $search_model->getAllTags();
		$numberOfTags = $search_model->getTotalNumberOfTags();
		foreach($tags as $tag){
			$tag->tag = utf8_encode($tag->tag);
			$tag->slug = utf8_encode($tag->slug);
			$num = $search_model->getTagNum($tag->tag);
			$tag_percentage = round($num/$numberOfTags,4)*100;
			$tag->tag_percentage = $tag_percentage;
		}
		echo json_encode(array("success"=>1,"results"=>$tags));
	}

	public function searchTagsStories($page){
		$tag = $_GET['query'];
		$page = ($page * 10) - 10;
		$search_model = $this->loadModel('search_model');
		$results = $search_model->getStoriesBasedOnTags($tag,$page);
		$searchTagStoriesNumberOfStories = $search_model->getNumberOfStoriesPerTag($tag);
		$story = $this->loadModel('story_model');
		foreach($results as $result){
			$result->story_description = utf8_encode($result->story_description);
			$result->story_name = utf8_encode($result->story_name);
			$result->bio = utf8_encode($result->bio);
			//get categories
			$categories = $story->getCategoriesOfStory($result->story_id);
			$result->categories = $categories;
			//get tags
			$tags = $story->getTagsOfStory($result->story_id);
			$result->tags = $tags;
			//get number of comments
			$number_of_comments = $story->getNumberOfCommentsOfStory($result->story_id);
			$result->number_of_comments = $number_of_comments;
			//get number of ratings
			$number_of_ratings = $story->getNumberOfRatingsOfStory($result->story_id);
			$result->number_of_ratings = $number_of_ratings;
			//get average ratings
			$average_rating = $story->getAverageRatingOfStory($result->story_id);
			$result->average_rating = $average_rating;
			//get number of story chapter favourites
			$favourites = $story->getNumberOfStoryFavourites($result->story_id);
			$result->favourites = $favourites;

			$bookmarks = $story->getNumberOfStoryBookmarks($result->story_id);
			$result->bookmarks = $bookmarks;

			$read_history = $story->getStoryReadHistory($result->story_id);
			$result->read_history = $read_history;

			$authors = $story->getAuthorsOfStory($result->story_id);
			$result->authors = $authors;

			$number_of_chapters = $story->getNumberOfChaptersOfStory($result->story_id);
			$result->number_of_chapters = $number_of_chapters;
		}
		echo json_encode(array("success"=>1,"results"=>$results,'numberOfStories'=>$searchTagStoriesNumberOfStories));
	}

}

?>