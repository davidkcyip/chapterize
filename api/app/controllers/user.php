<?php

class User extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function test(){
	  // init hybridauth
	  $hybridauth = new Hybrid_Auth( Session::get('hybridConfig') );
	 
	  // try to authenticate with twitter
	  $adapter = $hybridauth->authenticate( "facebook" );
	 
	  // grab the user's friends list
	  $user_contacts = $adapter->getUserContacts();
	 
	  print_r($user_contacts);
	}

	public function isLoggedIn(){
		if(Session::get('profile')){
			echo 1;
		}else{
			echo 0;
		}
	}

	public function getFollowSuggestions(){
		$me = Session::get('profile')->id;

		$user_model = $this->loadModel('user_model');

		//get all ids of people you follow
		$people_i_follow = $user_model->getFollowing($me);

		$people_list = array();
		$person_i_follow_list = array();
		foreach($people_i_follow as $person_i_follow){
			array_push($person_i_follow_list,$person_i_follow->id);
		}
		//now for each user, get the people they follow
		foreach($people_i_follow as $person){
			$people_this_person_follows = $user_model->getFollowing($person->id);
			foreach($people_this_person_follows as $person2){
				if($person2->id != $me && !in_array($person2->id, $person_i_follow_list)){
					array_push($people_list, $person2->id);
				}
			}
		}
		//count occurrences
		$people_i_should_follow = array_count_values($people_list);
		arsort($people_i_should_follow);
		//$keys=key($people_i_should_follow);
		$people_following_suggested_person_list = array();
		$i = 0;
		$j = 0;
		foreach($people_i_should_follow as $key => $value){
			if($i < 3){
				$person = $user_model->returnUserById($key);
				array_push($people_following_suggested_person_list, $person);
				$people_who_follows_this_person = $user_model->getFollowers($key);
				//get people who you follow and who this person is followed by
				foreach($people_i_follow as $person_i_follow){
					foreach($people_who_follows_this_person as $person_who_follows_this_person){
						if($person_i_follow->id == $person_who_follows_this_person->id && $person_who_follows_this_person->id != $me){
							$people_following_suggested_person_list[$i]->followed_by[$j] = $person_who_follows_this_person;
							$j++;
						}
					}
				}
				$j = 0;
			}else{
				break;
			}
			$i++;
		}
		echo json_encode($people_following_suggested_person_list);
	}

	public function changeEmail(){
		$email = $_POST['email'];
		$me = Session::get('profile')->id;
		
		$myProfile = Session::get('profile');
		$myProfile->email = $email;
		Session::set('profile',$myProfile);

		$save = $this->loadModel('user_model');

		$array_to_update = array("email"=>$email);
		$where = array("id"=>$me);

		if($save->updateTable($array_to_update,$where,"user")){
			echo json_encode(array("success"=>1));
		}else{
			echo json_encode(array("success"=>0));
		}
	}

	public function changePassword(){
		$oldPassword = $_POST['oldPassword'];
		$newPassword = $_POST['newPassword'];
		$me = Session::get('profile');

		$save = $this->loadModel('user_model');

		$passwordSalt = $save->getPasswordSalt($me->pen_name);
		$databasePassword = $save->returnPassword($me->email);

		$passwordHash = $save->generatePasswordHash($oldPassword.$passwordSalt);

		if($passwordHash == $databasePassword->password){
			$newSalt = $save->generateSalt();
			$newPasswordHash = $save->generatePasswordHash($newPassword.$newSalt);
			$array_to_update = array("password"=>$newPasswordHash,"salt"=>$newSalt);
			$where = array("id"=>$me->id);
			if($save->updateTable($array_to_update,$where,"user")){
				echo json_encode(array("success"=>1,"errorMessage"=>"Your password has been successfully changed."));
			}else{
				echo json_encode(array("success"=>0,"errorMessage"=>"There was an error updating your password."));
			}
		}else{
			echo json_encode(array("success"=>0,"errorMessage"=>"Your old password is not correct."));
		}
	}

	public function reportUser(){
		$message = $_POST['message'];
		$message = str_replace("<br>","\r\n",$message);
		$message = str_replace("<div>", "", $message);
		$message = str_replace("</div>","",$message);
		$message = strip_tags($message);
		$user_reported = $_POST['id'];
		$me = Session::get('profile')->id;

		$user = $this->loadModel('user_model');
		$array_to_insert = array("reported_by"=>$me, "user_reported"=>$user_reported,"message"=>$message);
		$user->insertIntoTable($array_to_insert,"report");

		$userProfile = $user->returnUserById($me);
		$reportedUserProfile = $user->returnUserById($user_reported);

		$emailModel = $this->loadModel('email_model');

		$emailMessage = $userProfile->real_name . " (" . $userProfile->pen_name . ") has reported ". $reportedUserProfile->real_name . " (" . $reportedUserProfile->pen_name . ") with the message:\r\n\r\n".$message;
        $subject = $userProfile->real_name . " (" . $userProfile->pen_name . ") has reported ". $reportedUserProfile->real_name . " (" . $reportedUserProfile->pen_name . ")";
        $email = "support@bloqly.com";

        $newAccountEmail = $emailModel->sendEmail($emailMessage, $subject, $email, Session::get('profile')->email);
		echo json_encode(array("success"=>1));
	}

	public function getFollowers($id){
		$user = $this->loadModel('user_model');
		$followers = $user->getFollowers($id);

		foreach($followers as $follower){
			$follower->bio = strip_tags($follower->bio);
			$follower->bio = substr($follower->bio,0,100);
		}

		echo json_encode(array("success"=>1,"results"=>$followers));
	}

	public function getFollowing($id){
		$user = $this->loadModel('user_model');
		$followings = $user->getFollowing($id);
		foreach($followings as $following){
			$following->bio = strip_tags($following->bio);
			$following->bio = substr($following->bio,0,100);
		}

		echo json_encode(array("success"=>1,"results"=>$followings));
	}

	public function getOwnProfile(){
		echo json_encode(Session::get('profile'));
	}

	public function getProfile($penname){
		$user = $this->loadModel('user_model');

		$user_profile = $user->returnUserByPenname($penname);
		echo json_encode($user_profile);
	}

	public function autocomplete(){
		$user = $_GET['term'];
		$me = Session::get('profile')->pen_name;

		$user_model = $this->loadModel('user_model');
		$users = $user_model->autocomplete($user);
		$results = array();

		foreach($users as $user){
			if(strpos($user->name,$me) === false){
				array_push($results, $user->name);
			}
		}

		echo json_encode($results);
	}

	public function getAllProfiles($period,$page){
		$user = $this->loadModel('user_model');
		$page = ($page * 10) - 10;
		$user_profiles = $user->getAllProfiles($page);
		$numberOfAuthors = $user->getNumberOfAuthors();
		foreach($user_profiles as $user_profile){
			$user_profile->pen_name = utf8_encode($user_profile->pen_name);
			$user_profile->real_name = utf8_encode($user_profile->real_name);
			$user_profile->bio = utf8_encode($user_profile->bio);
			$user_profile->search_type = "ALL";
		}
		echo json_encode(array("success"=>1,"results"=>$user_profiles,"numberOfAuthors"=>$numberOfAuthors));
	}

	public function getRecentProfiles($period,$page){
		$user = $this->loadModel('user_model');
		$page = ($page * 10) - 10;
		$user_profiles = $user->getRecentProfiles($page);
		$numberOfAuthors = $user->getNumberOfAuthors();
		foreach($user_profiles as $user_profile){
			$user_profile->pen_name = utf8_encode($user_profile->pen_name);
			$user_profile->real_name = utf8_encode($user_profile->real_name);
			$user_profile->bio = utf8_encode($user_profile->bio);
			$user_profile->search_type = "RECENT";
		}
		echo json_encode(array("success"=>1,"results"=>$user_profiles,"numberOfAuthors"=>$numberOfAuthors));
	}

	public function getPopularProfiles($period,$page){
		$user = $this->loadModel('user_model');
		$page = ($page * 10) - 10;
		$user_profiles = $user->getPopularProfiles($page);
		$numberOfAuthors = $user->getNumberOfAuthors($page);
		foreach($user_profiles as $user_profile){
			$user_profile->pen_name = utf8_encode($user_profile->pen_name);
			$user_profile->real_name = utf8_encode($user_profile->real_name);
			$user_profile->bio = utf8_encode($user_profile->bio);

			$user_profile->search_type = "POPULAR";
		}
		echo json_encode(array("success"=>1,"results"=>$user_profiles,"numberOfAuthors"=>$numberOfAuthors));
	}

	public function compareSession(){
		$user = $this->loadModel('user_model');

		$email = $_GET['email'];
		$session_id = $_GET['session_id'];

		$session_id = $user->checkValidSession($email,$session_id);

		echo $session_id;
	}

	public function register(){
		$user = $this->loadModel('user_model');
		$salt = $user->generateSalt();
		$name = $_POST['name'];
		$pen_name = $_POST['penname'];
		$email = $_POST['email'];
		$password = $user->generatePasswordHash($_POST['password'].$salt);
		$activation = $user->generateActivationCode();

		$array_to_insert = array("pen_name"=>$pen_name, "real_name"=>$name, "email"=> $email, "password"=>$password, "salt"=>$salt, "activation_code"=>$activation, "is_active"=>"0");

		$user_id = $user->insertIntoTable($array_to_insert,"user");

		if($user_id){
			$array_to_insert = array("user_id"=>$user_id, "role_id"=>2);
			$user_role_id = $user->insertIntoTable($array_to_insert,"user_role");

            $message = "Greetings from Bloqly,\r\n\r\nThank you for registering with Bloqly, you are now required to activate your account.\r\nTo activate your account, please go to: ".WEBSITE_URL."activate/".$user_id."/".$activation;
            $subject = 'Thanks for your interest in Bloqly';
            
            $emailModel = $this->loadModel('email_model');

            $newAccountEmail = $emailModel->sendEmail($message, $subject, $email);

            if ($newAccountEmail == 0) {
	           	echo json_encode(array("success"=>"0","reason" => "There has been a technical issue sending you an activation email. Please contact support."));
	        } else {
	        	$title = $name." has registered";
				$message = $name." has registered (".$email.")";
				$newAccountEmail = $emailModel->sendEmail($message, $title, "support@bloqly.com");
	           	echo json_encode(array("success" => "1", "id" => $user_id));
	        }
		}else{
			echo json_encode(array("success" => "0","reason"=>"This user could not be created, please contact the admin for assistance."));
		}
	}

	public function activateUser(){
		$user_id = $_POST['id'];
		$activation_code = $_POST['activation_code'];
		$user = $this->loadModel('user_model');
        $isActive = $user->checkIfActivationCodeIsCorrect($user_id,$activation_code);
        if($isActive == 1){
            $where = array("id"=>$user_id);
            $value = array("is_active"=>"1");
            $user->updateTable($value,$where,'user');
            echo json_encode(array("error"=>0,"reason"=>"Thank you for activating your account. You may now login to your account."));
        }else{
            echo json_encode(array("error"=>1,"reason"=>"Your activation code is incorrect. Please contact the admin for assistance."));
        }
	}

	public function login(){
		$user = $this->loadModel('user_model');
        $username = $_POST['email'];
        $salt = $user->getPasswordSalt($username);
        $password = $user->generatePasswordHash($_POST['password'] . $salt);
        $response = $user->login($username,$password);
        $response_json = json_decode($response);
        if($response_json->success == "1"){
        	$active = $user->checkIfUserIsActive($username);
        	if($active == 1){
        		$session = $user->generateSession();
        		$array_to_update = array("session"=>$session);
        		$where = array("id"=>$response_json->id);
        		$user->updateTable($array_to_update,$where,"user");
        		$array_to_insert = array("user_id"=>$response_json->id);
				$user->insertIntoTable($array_to_insert,"login");
        		$user_profile = $user->returnUserById($response_json->id);
            	Session::set('profile',$user_profile);

				echo json_encode(array("success"=>"1","profile"=>$user_profile));
			}else{
				echo json_encode(array("success"=>"0","reason"=>"You have not activated your account. Please check your email."));
			}
        }else{
        	echo json_encode(array("success"=>"0","reason"=>"Your login details are incorrect."));
        }
	}

	public function logout(){
		Session::destroy();
		echo json_encode(array("success"=>1));
	}

	public function forgotPassword(){
		$user = $this->loadModel('user_model');
		$email = $_GET['email'];
		$emailExists = $user->checkEmailExists($email);
		if($emailExists > 0){
			$salt = $user->generateSalt();
			$newPassword = $user->generateNewPassword();
			$newPasswordHash = $user->generatePasswordHash($newPassword . $salt);

			$update = array("password"=>$newPasswordHash,"salt"=>$salt);
			$where = array("email"=>$email);
			$table = "user";

			$updateTable = $user->updateTable($update,$where,$table);
			
			$message = "Greetings from Bloqly,\r\n\r\nYou have requested a new password for your account at Bloqly.\r\nYour new password is: ".$newPassword;

			$subject = "Your new Bloqly password";
			
			$emailModel = $this->loadModel('email_model');

			$newPasswordEmail = $emailModel->sendEmail($message,$subject,$email);

			if($newPasswordEmail = 0){
				echo json_encode(array("success"=>"0","reason" => "There has been a technical issue sending you a new password. Please contact support."));
			}else{
				echo json_encode(array("success"=>"1"));
			}
		}else{
			echo json_encode(array("success"=>"0","reason" => "That email is not registered with us."));
		}
	}

	public function checkPennameExists($penname){
		$user = $this->loadModel('user_model');
		$user_exists = $user->checkPennameExists($penname);
		if($user_exists == 0){
			echo json_encode(array("error"=>0));
		}else{
			echo json_encode(array("error"=>1));
		}	
	}

	public function checkEmailExists($email){
		$user = $this->loadModel('user_model');
		$email_exists = $user->checkEmailExists($email);
		if($email_exists == 0){
			echo json_encode(array("error"=>0));
		}else{
			echo json_encode(array("error"=>1));
		}	
	}

	public function updatePennamePassword(){
		$penname = $_REQUEST['penname'];
		$password = $_REQUEST['password'];
		$user = $this->loadModel('user_model');
		$salt = $user->generateSalt();
		$passwordHash = $user->generatePasswordHash($password.$salt);
		$array_to_update = array("password"=>$passwordHash,"pen_name"=>$penname,"salt"=>$salt);
		$user_profile = Session::get('profile');
		$user_id = $user_profile->id;
		$user_profile->pen_name = $penname;
		Session::set('profile',$user_profile);
		$where = array('id'=>$user_id);
		$user->updateTable($array_to_update,$where,"user");
		echo json_encode(array("error"=>0,"user_profile"=>$user_profile));
	}

	public function facebook($request = null){
		$provider_name = "facebook";
		// initialize Hybrid_Auth class with the config file
		$hybridauth = new Hybrid_Auth( Session::get('hybridConfig') );
 
		// try to authenticate with the selected provider
		try{

			$adapter = $hybridauth->authenticate( $provider_name );
	 		
		}catch( Exception $e ){
		    // User not connected?
		    if( $e->getCode() == 6 || $e->getCode() == 7 ){ 
		        // log the user out (erase his session locally)
		        $adapter->logout();

		        // try to authenticate again 
		        $adapter = $hybridauth->authenticate( $provider_name );
		    }
		}

		$user_profile = $adapter->getUserProfile();
		$sessionPenname = $this->checkUserOrRegister($user_profile);

		Session::set('session',$sessionPenname->session);
		
		$user = $this->loadModel('user_model');
		
		$user_profile = $user->returnUserByEmail($user_profile->email);
        Session::set('profile',$user_profile);

		header("Location: ".WEBSITE_URL."verify");
	}

	public function checkUserOrRegister($user_profile){
		$user = $this->loadModel('user_model');

		$user_exists = $user->checkUserExists($user_profile->email);

		$session_id = $user->generateSession();
		if(!$user_exists){
			//upload user
			$user_id = $user_profile->identifier;
			$user_email = $user_profile->email;
			$user_first_name = $user_profile->firstName;
			$user_last_name = $user_profile->lastName;
			$user_country = $user_profile->country;
			$user_bio = $user_profile->description;
			/**if(count(explode(" ",$user_profile->displayName)) == 1){
				$user_pen_name = $user_profile->displayName;
			}else{
				$user_pen_name = $user_first_name . $user_last_name;
			}**/
			$user_profile_page = $user_profile->profileURL;
			$avatar = str_replace("width=150&height=150","width=350&height=350",$user_profile->photoURL);
			$user_avatar = $avatar;
			$array_to_insert = array("email"=>$user_email, "real_name"=> $user_first_name . " " . $user_last_name, "bio"=>$user_bio, "country"=> $user_country,"avatar"=>$user_avatar, "facebook_link"=>$user_profile_page, "session"=> $session_id, "is_active"=>"1");
			$user_id = $user->insertIntoTable($array_to_insert,"user");

			$array_to_insert = array("user_id"=>$user_id, "role_id"=>2);
			$user_role_id = $user->insertIntoTable($array_to_insert,"user_role");
			//send email
			$message = "Greetings from Bloqly,\r\n\r\nThank you for registering with Bloqly, you are now required to activate your account.";
            $subject = 'Thanks for your interest in Bloqly';

			$emailModel = $this->loadModel("email_model");

			$newAccountEmail = $emailModel->sendEmail($message, $subject, $user_email);

			$title = $user_first_name." ".$user_last_name." has registered";
			$message = $user_first_name." ".$user_last_name." has registered (".$user_email.")";
			$newAccountEmail = $email_model->sendEmail($message, $title, "support@bloqly.com");
		}else{
			$array_to_update = array("session"=>$session_id,"facebook_link"=>$user_profile->profileURL,"is_active"=>"1");
			$where = array("id"=>$user_exists);
			$user->updateTable($array_to_update,$where,"user");
			$array_to_insert = array("user_id"=>$user_exists);
			$user->insertIntoTable($array_to_insert,"login");
		}
		$penname = $user->returnPenname($user_profile->email);
		$password = $user->returnPassword($user_profile->email);
		if((!is_null($penname) && $penname && $penname != "") && (!is_null($password) && $password && $password != "")){
			return array("session"=>$session_id,"penname"=>$penname, "password"=>$password);
		}else{
			return array("session"=>$session_id,"penname"=>"","password"=>"");
		}
	}

	public function getSubscriptionAndStoryStats($user_id){
		$user = $this->loadModel('user_model');

		$subscriptionStats = $user->getSubscriptionStats($user_id);
		$subscriberStats = $user->getSubscriberStats($user_id);
		$storyStats = $user->getNumberOfStories($user_id);
		$chapterStats = $user->getNumberOfChapters($user_id);

		echo json_encode(array("success"=>1,"subscribers"=>$subscriberStats,"subscriptions"=>$subscriptionStats,"stories"=>$storyStats,"chapters"=>$chapterStats));
	}

	public function followUnfollow(){
		$profile = Session::get('profile');
		$me = $profile->id;

		$followUnfollow = $_POST['followUnfollow'];
		$type = $_POST['type'];
		$user_id = $_POST['user_id'];

		$user = $this->loadModel('user_model');
		if($followUnfollow == "follow"){
			if($type == "user"){
				$array_to_insert = array("subscription_type"=>1, "item_subscribed_to"=>$user_id, "subscribed_by"=>$me);
				$table = "subscription";
				$response = $user->insertIntoTable($array_to_insert,$table);

				//notification
				$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>9,"action_id"=>$user_id,"user_to_notify"=>$user_id);
				$user->insertIntoTable($array_to_insert,"notification");

				//email_notification
				$email_model = $this->loadModel('email_model');
				$notification_model = $this->loadModel('notification_model');
				$followed_person = $user->returnUserById($user_id);
				//check if owner has subscribed to new follower email alert
				$has_subscribed = $notification_model->checkIfSubscribed("9",$followed_person->id);
				if($has_subscribed == 1){
					$person_url = "http://www.bloqly.com/profile/".Session::get('profile')->pen_name;
					$new_follower_email = $email_model->newFollowerEmailBody($followed_person->real_name,Session::get('profile')->real_name,$person_url);
					$title = Session::get('profile')->real_name." has started following you";
					$newAccountEmail = $email_model->sendEmail($new_follower_email, $title, $followed_person->email);
				}
				//email notification end
			}
		}else{
			if($type == "user"){
				$array_to_delete = array("subscription_type"=>1, "item_subscribed_to"=>$user_id, "subscribed_by"=>$me);
				$table = "subscription";
				$response = $user->deleteRows($array_to_delete,$table);
			}
		}
		echo json_encode(array("success"=>1));
	}

	public function editProfile (){
		$realName = $_POST['real_name'];
		$bio = $_POST['bio'];
		$country = $_POST['country'];
		$website = $_POST['website_link'];
		$facebook = $_POST['facebook_link'];
		$twitter = $_POST['twitter_link'];
		$google = $_POST['google_link'];

		$profile = Session::get('profile');
		$profile->real_name = $realName;
		$profile->bio = $bio;
		$profile->country = $country;
		$profile->website_link = $website;
		$profile->facebook_link = $facebook;
		$profile->twitter_link = $twitter;
		$profile->google_link = $google;

		$me = $profile->id;

		Session::set('profile',$profile);

		$user = $this->loadModel('user_model');

		$array_to_update = array("real_name"=>$realName,"bio"=>$bio,"country"=>$country,"website_link"=>$website,"facebook_link"=>$facebook,"twitter_link"=>$twitter,"google_link"=>$google);
		$where = array("id"=>$me);

		$user->updateTable($array_to_update,$where,"user");

		echo json_encode(array("success"=>1));
	}

	public function uploadAvatar(){

    $imagePath = "../avatars/";

	$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
	$temp = explode(".", $_FILES["img"]["name"]);
	$extension = end($temp);

	if ( in_array($extension, $allowedExts))
	  {
	  if ($_FILES["img"]["error"] > 0)
		{
			 $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
			);
			echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
		}
	  else
		{
			
		  $filename = $_FILES["img"]["tmp_name"];
		  list($width, $height) = getimagesize( $filename );

		  move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

		  $response = array(
			"status" => 'success',
			"url" => "/avatars/".$_FILES["img"]["name"],
			"width" => $width,
			"height" => $height
		  );

		}
	  }
	else
	  {
	   $response = array(
			"status" => 'error',
			"message" => 'something went wrong',
		);
	  }
	  
	  print json_encode($response);

	}

	public function cropAvatar (){
		$imgUrl = $_POST['imgUrl'];
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];

		$jpeg_quality = 100;
		$filename = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 48);
		$output_filename = $_SERVER["DOCUMENT_ROOT"]."/avatars/".$filename;

		$what = getimagesize($_SERVER["DOCUMENT_ROOT"].$imgUrl);

		switch(strtolower($what['mime']))
		{
		    case 'image/png':
		        $img_r = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.png';
		        break;
		    case 'image/jpeg':
		        $img_r = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.jpeg';
		        break;
		    case 'image/gif':
		        $img_r = imagecreatefromgif($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefromgif($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.gif';
		        break;
		    default: die('image type not supported');
		}
		  $user = Session::get('profile');
		  $user->avatar = "/avatars/".$filename.$type;
		  Session::set('profile',$user);

		  $me = $user->id;
		  $user = $this->loadModel('user_model');
		  $where = array("id"=>$me);
		  $array_to_update = array("avatar"=>"/avatars/".$filename.$type);
		  $upload_avatar = $user->updateTable($array_to_update,$where,"user");
			
			$resizedImage = imagecreatetruecolor($imgW, $imgH);

			imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, 
						$imgH, $imgInitW, $imgInitH);	
			
			
			$dest_image = imagecreatetruecolor($cropW, $cropH);
			imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, 
						$cropH, $cropW, $cropH);	


			imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
			
			$response = array(
					"status" => 'success',
					"url" => "/avatars/".$filename.$type 
				  );
			 print json_encode($response);
	}

	public function uploadAlbumInitial(){

    $imagePath = $_SERVER["DOCUMENT_ROOT"]."/covers/";

	$allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
	$temp = explode(".", $_FILES["img"]["name"]);
	$extension = end($temp);

	if ( in_array($extension, $allowedExts))
	  {
	  if ($_FILES["img"]["error"] > 0)
		{
			 $response = array(
				"status" => 'error',
				"message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
			);
			echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
		}
	  else
		{
			
		  $filename = $_FILES["img"]["tmp_name"];
		  list($width, $height) = getimagesize( $filename );

		  move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

		  $response = array(
			"status" => 'success',
			"url" => "/covers/".$_FILES["img"]["name"],
			"width" => $width,
			"height" => $height
		  );

		}
	  }
	else
	  {
	   $response = array(
			"status" => 'error',
			"message" => 'something went wrong',
		);
	  }
	  
	  print json_encode($response);

	}

	public function cropAlbumInitial (){
		$imgUrl = $_POST['imgUrl'];
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];

		$jpeg_quality = 100;
		$filename = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 48);
		$output_filename = $_SERVER["DOCUMENT_ROOT"]."/covers/".$filename;

		$what = getimagesize($_SERVER["DOCUMENT_ROOT"].$imgUrl);

		switch(strtolower($what['mime']))
		{
		    case 'image/png':
		        $img_r = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.png';
		        break;
		    case 'image/jpeg':
		        $img_r = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.jpeg';
		        break;
		    case 'image/gif':
		        $img_r = imagecreatefromgif($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$source_image = imagecreatefromgif($_SERVER["DOCUMENT_ROOT"].$imgUrl);
				$type = '.gif';
		        break;
		    default: die('image type not supported');
		}
			
			$resizedImage = imagecreatetruecolor($imgW, $imgH);

			imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, 
						$imgH, $imgInitW, $imgInitH);	
			
			
			$dest_image = imagecreatetruecolor($cropW, $cropH);
			imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, 
						$cropH, $cropW, $cropH);	


			imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
			
			$response = array(
					"status" => 'success',
					"url" => "/covers/".$filename.$type 
				  );
			 print json_encode($response);
	}

}