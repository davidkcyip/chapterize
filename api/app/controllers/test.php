<?php

class Test extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function insertStories(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');

		$stories = $test_model->getStories();

		foreach($stories as $story){
			$story_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $story->title);
			$story_slug = str_replace(" ", "-", strtolower($story_slug));
			if($story->sequel_to == null){ //new story
				//id, title, body, published_at, tags, sequel_to, prequel_to, cached_html, snippet, user_id -- stories
				//id, story_name, written_by, story_description, created_at, is_public, story_type, collaborative, album, slug -- story
				//id, chapter_name, written_by, chapter_body, written_at, last_edited_at, status, slug -- chapter
				//insert story
				$array_to_insert = array("written_by"=>$story->user_id,"story_name"=>utf8_encode($story->title),"created_at"=>$story->published_at,"is_public"=>1,"story_type"=>1,"collaborative"=>1,"slug"=>utf8_encode($story_slug));
				$story_id = $user_model->insertIntoTable($array_to_insert,"story");
				//insert chapter
				$array_to_insert = array("id"=>$story->id,"written_by"=>$story->user_id,"chapter_name"=>utf8_encode($story->title),"chapter_body"=>utf8_encode($story->body),"written_at"=>$story->published_at,"last_edited_at"=>$story->published_at,"status"=>1,"slug"=>utf8_encode($story_slug));
				$chapter_id = $user_model->insertIntoTable($array_to_insert,"chapter");
				//insert story_map
				$array_to_insert = array("story_id"=>$story_id,"chapter_id"=>$story->id,"parent_chapter_id"=>$story->id);
				$story_map_id = $user_model->insertIntoTable($array_to_insert,"story_map");
			}else{
				$story_id = $test_model->getStoryIdOfChapter($story->sequel_to);
				//insert chapter
				$array_to_insert = array("id"=>$story->id,"written_by"=>$story->user_id,"chapter_name"=>utf8_encode($story->title),"chapter_body"=>utf8_encode($story->body),"written_at"=>$story->published_at,"last_edited_at"=>$story->published_at,"status"=>1,"slug"=>utf8_encode($story_slug));
				$chapter_id = $user_model->insertIntoTable($array_to_insert,"chapter");
				//insert story_map
				$array_to_insert = array("story_id"=>$story_id,"chapter_id"=>$story->id,"parent_chapter_id"=>$story->sequel_to);
				$story_map_id = $user_model->insertIntoTable($array_to_insert,"story_map");
			}
		}
	}

	public function transferComments(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');

		$comments = $test_model->getComments();
		//id, user_id, body, resource_id, resource_type = Story, is_deleted = 0, created_at -- comments
		//id, resource_id, resource_type = Story, user_id, score, created_at
		//id, story_id, chapter_id, comment_body, commented_by, commented_at, rating
		foreach($comments as $comment){
			$story_id = $test_model->getStoryIdOfChapter($comment->resource_id);
			$array_to_insert = array("story_id"=>$story_id,"chapter_id"=>$comment->resource_id,"comment_body"=>utf8_encode($comment->body),"commented_by"=>$comment->user_id,"commented_at"=>$comment->created_at);
			$comment_id = $user_model->insertIntoTable($array_to_insert,"story_comment");

			$ratings = $test_model->getRating($comment->user_id,$comment->resource_id,$comment->created_at);
			foreach($ratings as $rating){
				$rows_to_update = array("rating"=>$rating->score);
				$where = array("id"=>$comment_id);
				$user_model->updateTable($rows_to_update,$where,"story_comment");
			}
		}
	}

	public function transferUsers(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');

		$users = $test_model->getUsers();

		foreach($users as $user){
			//id, name, created_at, uri_name, bio, url
			//id, pen_name, real_name, bio, website_link, is_active
			$array_to_insert = array("id"=>$user->id,"real_name"=>utf8_encode($user->name),"pen_name"=>utf8_encode($user->uri_name),"bio"=>$user->bio,"website_link"=>$user->url,"registration_date"=>$user->created_at);
			$user_model->insertIntoTable($array_to_insert,"user");
		}
	}

	public function transferTags(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');

		$tags = $test_model->getTags();

		foreach($tags as $tag){
			$tag_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $tag->name);
			$tag_slug = str_replace(" ", "-", strtolower($tag_slug));
			$array_to_insert = array("story_id"=>$tag->story_id,"tag"=>$tag->name,"slug"=>$tag_slug);
			$user_model->insertIntoTable($array_to_insert,"story_tag");
		}
	}

	public function transferChallenges(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');
		//challenge
		//id, challenge_name, challenge_by, challenge_description, challenge_start_date, challenge_end_date, slug, challenge_added
		//id, title, body, starts_at, ends_at, winner_id, user_id, created_at
		//challenge_stories
		//id, challenge_id, story_id, submitted_by, submitted_at, is_winner, final_rating
		//id, story_id, challenge_id, user_id, created_at
		//challenge_discussion
		//id, challenge_id, author_id, comment, commented_at
		//user_id, body, resource_id, resource_type = 'Challenge', created_at

		$challenges = $test_model->getChallenges();

		foreach($challenges as $challenge){
			$challenge_slug = preg_replace("/[^A-Za-z0-9 ]/", '', $challenge->title);
			$challenge_slug = str_replace(" ", "-", strtolower($challenge_slug));

			$array_to_insert = array("id"=>$challenge->id,"challenge_name"=>utf8_encode($challenge->title),"challenge_by"=>$challenge->user_id,"challenge_description"=>utf8_encode($challenge->body),"challenge_start_date"=>$challenge->starts_at,"challenge_end_date"=>$challenge->ends_at,"slug"=>utf8_encode($challenge_slug),"challenge_added"=>$challenge->created_at);
			$challenge_id = $user_model->insertIntoTable($array_to_insert,"challenge");

			$challenge_stories = $test_model->getChallengeStories($challenge->id);
			foreach($challenge_stories as $story){
				if($story->user_id == $challenge->winner_id && $challenge->winner_id != null){
					$array_to_insert = array("id"=>$story->id,"challenge_id"=>$challenge_id,"story_id"=>$story->story_id,"submitted_by"=>$story->user_id,"submitted_at"=>$story->created_at,"is_winner"=>1);
				}else{
					$array_to_insert = array("id"=>$story->id,"challenge_id"=>$challenge_id,"story_id"=>$story->story_id,"submitted_by"=>$story->user_id,"submitted_at"=>$story->created_at,"is_winner"=>0);
				}
				$challenge_story_id = $user_model->insertIntoTable($array_to_insert,"challenge_stories");
			}

			$challenge_comments = $test_model->getChallengeComments($challenge->id);
			foreach($challenge_comments as $comment){
				$array_to_insert = array("challenge_id"=>$challenge->id,"author_id"=>$comment->user_id,"comment"=>utf8_encode($comment->body),"commented_at"=>$comment->created_at);
				$challenge_comment_id = $user_model->insertIntoTable($array_to_insert,"challenge_discussion");
			}
		}
	}

	public function transferFollows(){
		ini_set('memory_limit', '-1');
		$test_model = $this->loadModel('test_model');
		$user_model = $this->loadModel('user_model');
		//subscription
		//id, subscription_type, item_subscribed_to, subscribed_by, subscribed_date
		//id, user_id, friend_id, created_at, updated_at
		$friendships = $test_model->getFriendships();
		foreach($friendships as $friendship){
			$array_to_insert = array("subscription_type"=>1,"item_subscribed_to"=>$friendship->friend_id,"subscribed_by"=>$friendship->user_id,"subscribed_date"=>$friendship->created_at);
			$user_model->insertIntoTable($array_to_insert,"subscription");
		}
	}

}