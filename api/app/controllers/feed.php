<?php

class Feed extends Controller{

	public function __construct(){
		parent::__construct();
	}

	public function getLatestItems($timestamp, $type){

	}

	public function getEarlierItems($timestamp, $type){

	}

	public function repost(){
		$repostId = $_POST['id'];
		$shareText = $_POST['shareText'];
		$me = Session::get('profile')->id;
		$user_model = $this->loadModel('user_model');
		$feed_model = $this->loadModel('feed_model');
		//check if repost id is original or not
		$repost_id = $feed_model->getShareId($repostId);
		if($repost_id->share_id > 0 && $repost_id->share_id != null){
			$repostId = $repost_id->share_id;
			$user_to_notify = $user_model->getOwnerOfStatus($repost_id->share_id);
		}else{
			$user_to_notify = $repost_id->status_by;
		}

		$array_to_insert = array("status_by"=>$me, "status_body"=>$shareText, "status_to"=>$me, "share_id"=>$repostId);
		$status_id = $user_model->insertIntoTable($array_to_insert,"status");

		//now alert the original status owner
		if($repost_id->status_by != $me){
			$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>21,"action_id"=>$status_id,"user_to_notify"=>$user_to_notify);
			$user_model->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $user_model->returnUserById($repost_id->status_by);
			//check if owner has subscribed to new follower email alert
			$has_subscribed = $notification_model->checkIfSubscribed("21",$repost_id->status_by);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/profile/".Session::get('profile')->pen_name."/status/".$status_id;
				$repost_email = $email_model->repostEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$status_url);
				$title = Session::get('profile')->real_name." has reposted one of your statuses";
				$newAccountEmail = $email_model->sendEmail($repost_email, $title, $user_to_alert->email);
			}
			//email notification end
		}

		echo json_encode(array("success"=>1, "results"=>$status_id));
	}

	public function getReply($replyId){
		$feed = $this->loadModel('feed_model');
		$response = $feed->getReply($replyId);
		echo json_encode($response);
	}

	public function getStatus($statusId){
		$feed = $this->loadModel('feed_model');
		$response = $feed->getStatus($statusId);
		echo json_encode($response);
	}

	public function getInitialItems($username, $page, $type){
		if($username != null && $username == Session::get('profile')->pen_name){
			$user = Session::get('profile');
			$user_id = $user->id;
		}else if($username != null && $username != Session::get('profile')->pen_name){
			$user_model = $this->loadModel('user_model');
			$thisUser = $user_model->returnUserByPenname($username);
			$user_id = $thisUser->id;
		}

		$feed = $this->loadModel('feed_model');
		$feedItems = array();
		$user_list = array();
		$story_list = array();
		$chapter_list = array();
		$brainstorm_list = array();
		$challenge_list = array();

		if($type == "all" || $type == null){
			$latest = null;
			$earliest = null;
		}

		if(Session::get('latestHomeFeedTime') && Session::get('latestHomeFeedTime') != null && Session::get('latestHomeFeedTime') != ""){
			$latest = Session::get('latestHomeFeedTime');
		}else{
			$latest = null;
		}

		if(Session::get('earliestHomeFeedTime') && Session::get('earliestHomeFeedTime') != null && Session::get('earliestHomeFeedTime') != ""){
			$earliest = Session::get('earliestHomeFeedTime');
		}else{
			$earliest = null;
		}
		//$feedItems = $feed->getStatusItems(null, null, $user_id);
		array_push($user_list, $user_id);
		if($page != null && ($page != "profileAll" && $page != "profile" && $page != "profileChallenges" && $page != "profileStoryUpdates" && $page != "homeMyFollowers" && $page != "singleFollowerAll" && $page != "homepageStoryUpdates")){
			$subscriptions = $feed->getListOfSubscribedUser($user_id, "user");
			array_push($subscriptions, array("item_subscribed_to"=>$user_id,"subscription_type"=>"user"));
			
			foreach($subscriptions as $subscription){
				if($subscription->item_subscribed_to != null){
					array_push($user_list,$subscription->item_subscribed_to);
				}
			}
		}else if($page != null && $page == "homeMyFollowers"){
			$user_list = array();
			$subscriptions = $feed->getListOfFollowers($user_id, "user");
			foreach($subscriptions as $subscription){
				if($subscription->subscribed_by != "" && $subscription->subscribed_by != Session::get('profile')->id){
					array_push($user_list,$subscription->subscribed_by);
				}
			}
		}else if($page != null && $page == "homepageStoryUpdates" && $username == "all"){
			$user_list = array();
			
		}
		if($type == "user"){
			
			$feedItems = $feed->getStatusItems(null, null, $user_list, null, null, null, null, $user_id, $page);
			//echo json_encode($feedItems);
			
		}elseif($type == "story"){
			$feedItems = $feed->getStatusItems(null, null, null, $story_list, null, null, null, $user_id, $page);
		}elseif($type == "chapter"){
			$feedItems = $feed->getStatusItems(null, null, null, null, $chapter_list, null, null, $user_id, $page);
		}elseif($type == "brainstorm"){
			$feedItems = $feed->getStatusItems(null, null, null, null, null, $brainstorm_list, null, $user_id, $page);
		}elseif($type == "all"){
			$feedItems = $feed->getStatusItems(null, null, $user_list, $user_id, $page);
		}elseif($type == "previous"){
			$feedItems = $feed->getStatusItems($earliest, null, $user_list, $user_id, $page);
		}elseif($type == "next"){
			$feedItems = $feed->getStatusItems(null, $latest, $user_list, $user_id, $page);
		}

		if($feedItems[0]->status_time != null){
			Session::set('latestHomeFeedTime', $feedItems[0]->status_time);
		}else if($feedItems[0]->replied_at != null){
			Session::set('latestHomeFeedTime', $feedItems[0]->replied_at);
		}else if($feedItems[0]['status_time']){
			Session::set('latestHomeFeedTime', $feedItems[0]['status_time']);
		}else if($feedItems[0]['replied_at']){
			Session::set('latestHomeFeedTime', $feedItems[0]['replied_at']);
		}
		if($feedItems[count($feedItems)-1]->status_time != null){
			Session::set('earliestHomeFeedTime', $feedItems[count($feedItems)-1]->status_time);
		}else if($feedItems[count($feedItems)-1]->replied_at != null){
			Session::set('earliestHomeFeedTime', $feedItems[count($feedItems)-1]->replied_at);
		}else if($feedItems[count($feedItems)-1]['replied_at'] != null){
			Session::set('earliestHomeFeedTime', $feedItems[count($feedItems)-1]['replied_at']);
		}else if($feedItems[count($feedItems)-1]['status_time'] != null){
			Session::set('earliestHomeFeedTime', $feedItems[count($feedItems)-1]['status_time']);
		}

		echo json_encode($feedItems);
	}

	public function getSubscriptionsList($user_id){

		$feed = $this->loadModel('feed_model');

		$subscriber_list = $feed->getListOfSubscribedUser($user_id, "all");
		echo json_encode($subscriber_list);
	}

	public function getIndividualStatus(){
		$feed = $this->loadModel('feed_model');
		$status_id = $_GET['id'];

		$statuses = $feed->getIndividualStatus($status_id);

		echo json_encode(array("success"=>1,"results"=>$statuses));
	}

	public function sendReplyStatus(){
		$user = Session::get('profile');
		$me = $user->id;

		$reply = $_POST['reply'];
		$statusId = $_POST['id'];

		$feed = $this->loadModel('user_model');
		$array_to_insert = array("status_id"=>$statusId, "reply_by"=>$me, "reply_body"=>$reply);
		$response = $feed->insertIntoTable($array_to_insert,"status_reply");

		//if this reply was to a status started not by me, then alert that person
		$status_owner = $feed->getOwnerOfStatus($statusId);

		if($status_owner != $me){
			$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>18,"action_id"=>$response,"user_to_notify"=>$status_owner);
			$feed->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $feed->returnUserById($status_owner);
			//check if owner has subscribed to new follower email alert
			$has_subscribed = $notification_model->checkIfSubscribed("18",$status_owner);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/profile/".$user_to_alert->pen_name."/status/".$statusId;
				$post_reply_email = $email_model->postReplyEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$status_url);
				$title = Session::get('profile')->real_name." has replied to one of your statuses";
				$newAccountEmail = $email_model->sendEmail($post_reply_email, $title, $user_to_alert->email);
			}
			//email notification end
		}

		echo json_encode(array("success"=>1, "id"=>$response));
	}

	public function submitStatus(){
		$status = $_POST['status'];
		$to = $_POST['to'];

		$user = Session::get('profile');
		$me = $user->id;

		$feed = $this->loadModel('user_model');
		$array_to_insert = array("status_by"=>$me, "status_to"=>$to, "status_body"=>$status);

		$response = $feed->insertIntoTable($array_to_insert,"status");

		//if this was a wall post to different person, alert that person
		if($to != $me){
			$array_to_insert = array("activity_by"=>$me,"activity_type_id"=>19,"action_id"=>$response,"user_to_notify"=>$to);
			$feed->insertIntoTable($array_to_insert,"notification");

			//email_notification
			$email_model = $this->loadModel('email_model');
			$notification_model = $this->loadModel('notification_model');
			$user_to_alert = $feed->returnUserById($to);
			//check if owner has subscribed to new follower email alert
			$has_subscribed = $notification_model->checkIfSubscribed("19",$to);
			if($has_subscribed == 1){
				$status_url = "http://www.bloqly.com/profile/".$user_to_alert->pen_name."/status/".$response;
				$wall_post_email = $email_model->wallPostEmailBody($user_to_alert->real_name,Session::get('profile')->real_name,$status_url);
				$title = Session::get('profile')->real_name." has posted a status on your wall";
				$newAccountEmail = $email_model->sendEmail($wall_post_email, $title, $user_to_alert->email);
			}
			//email notification end
		}

		echo json_encode(array("success"=>1, "id"=>$response));
	}

	public function deleteReply(){
		$replyId = $_POST['id'];

		$where = array("id"=>$replyId);
		$likes_to_delete = array("status_id"=>$replyId);

		$feed = $this->loadModel('user_model');

		//delete notifications
		$array_to_delete = array("activity_type_id"=>18,"action_id"=>$replyId);
		$feed->deleteRows($array_to_delete,"notification");

		$response = $feed->deleteRows($where, "status_reply");
		$deleteTheLikes = $feed->deleteRows($likes_to_delete, "likes");

		echo json_encode(array("success"=>1));
	}

	public function deleteStatus(){
		$statusId = $_POST['id'];

		$where = array("id"=>$statusId);
		$replies_to_delete = array("status_id"=>$statusId);
		$likes_to_delete = array("status_id"=>$statusId,"like_type"=>1);
		$notifications_to_delete = array("activity_type_id"=>19,"action_id"=>$statusId);
		$notifications_to_delete2 = array("activity_type_id"=>21,"action_id"=>$statusId);

		$feed = $this->loadModel('user_model');
		$feed_model = $this->loadModel('feed_model');

		$replyIds = $feed_model->getReplyIds($statusId);

		$deleteThisStatus = $feed->deleteRows($where, "status");
		$deleteTheReplies = $feed->deleteRows($replies_to_delete, "status_reply");
		$deleteTheLikes = $feed->deleteRows($likes_to_delete, "likes");
		$deleteTheNotifications = $feed->deleteRows($notifications_to_delete,"notification");
		$deleteTheNotifications2 = $feed->deleteRows($notifications_to_delete2,"notification");
		
		foreach($replyIds as $replyId){
			$replyNotificationsToDelete = array("activity_type_id"=>18,"action_id"=>$replyId->id);
			$reply_likes_to_delete = array("status_id"=>$replyId->id,"like_type"=>2);
			$deleteTheReplyLikes = $feed->deleteRows($reply_likes_to_delete,"likes");
			$deleteTheReplyNotifications = $feed->deleteRows($replyNotificationsToDelete,"notification");
		}

		echo json_encode(array("success"=>1));
	}

	public function likeReply(){
		$status_id = $_POST['id'];

		$user = Session::get('profile');
		$me = $user->id;

		$array_to_insert = array("status_id"=>$status_id, "liked_by"=>$me, "like_type"=>2);

		$feed = $this->loadModel('user_model');
		$response = $feed->insertIntoTable($array_to_insert,"likes");

		echo json_encode(array("success"=>1));
	}

	public function likeStatus(){
		$status_id = $_POST['id'];

		$user = Session::get('profile');
		$me = $user->id;

		$array_to_insert = array("status_id"=>$status_id, "liked_by"=>$me, "like_type"=>1);

		$feed = $this->loadModel('user_model');
		$response = $feed->insertIntoTable($array_to_insert,"likes");

		echo json_encode(array("success"=>1));
	}

	public function unlikeReply(){
		$status_id = $_POST['id'];

		$user = Session::get('profile');
		$me = $user->id;

		$array_to_delete = array("status_id"=>$status_id, "liked_by"=>$me, "like_type"=>2);

		$feed = $this->loadModel('user_model');
		$response = $feed->deleteRows($array_to_delete,"likes");

		echo json_encode(array("success"=>1));
	}

	public function unlikeStatus(){
		$status_id = $_POST['id'];

		$user = Session::get('profile');
		$me = $user->id;

		$array_to_delete = array("status_id"=>$status_id, "liked_by"=>$me, "like_type"=>1);

		$feed = $this->loadModel('user_model');
		$response = $feed->deleteRows($array_to_delete,"likes");

		echo json_encode(array("success"=>1));
	}

}
