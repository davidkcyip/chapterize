<?php

class Brainstorm_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getUserBrainstorms($user_id){
		$row = $this->_db->select("select 	c.id as story_id,
											c.story_name as story_name,
											c.slug as story_slug
										from subscription a inner join subscription_type b on a.subscription_type = b.id 
										left join story c on a.item_subscribed_to = c.id
										where (a.subscribed_by = :user_id and b.subscription_type = 'brainstorm') 
											", array(":user_id"=>$user_id));
		return $row;
	}	

	public function getCategories(){
		$row = $this->_db->select("select * from brainstorm_thread_type");
		return $row;
	}

	public function getBrainstormThreads($story_id,$me){
		$row = $this->_db->select("select a.id, a.thread_title, a.started_at, a.thread_type as thread_type_id, b.thread_type, a.slug as thread_slug, c.id as thread_started_by_id, c.pen_name as thread_started_by_pen_name, c.real_name as thread_started_by_real_name, c.avatar as thread_started_by_avatar, max(d.posted_at) as latest_post, e.id as story_id, e.story_name, e.slug as story_slug, case when f.subscribed_by = :user_id then 1 else 0 end as have_i_subscribed, g.id as story_owner from brainstorm_thread a inner join brainstorm_thread_type b on a.thread_type = b.id inner join user c on c.id = a.thread_started_by inner join brainstorm_post d on d.thread_id = a.id inner join story e on a.story_id = e.id left join subscription f on f.item_subscribed_to = e.id and f.subscription_type = 4 inner join user g on g.id = e.written_by where a.story_id = :story_id group by a.id order by latest_post desc",array(":story_id"=>$story_id,":user_id"=>$me));
		return $row;
	}

	public function getThread($story_id,$thread_id,$me){
		$row = $this->_db->select("select case when f.id is not null then 1 else 0 end as have_i_subscribed, a.id, a.thread_title, a.started_at, a.thread_type as thread_type_id, b.thread_type, a.slug as thread_slug, c.id as thread_started_by_id, c.pen_name as thread_started_by_pen_name, c.real_name as thread_started_by_real_name, c.avatar as thread_started_by_avatar, max(d.posted_at) as latest_post, e.id as story_id, e.story_name, e.slug as story_slug from brainstorm_thread a inner join brainstorm_thread_type b on a.thread_type = b.id inner join user c on c.id = a.thread_started_by inner join brainstorm_post d on d.thread_id = a.id inner join story e on a.story_id = e.id left join subscription f on f.item_subscribed_to = a.id and f.subscribed_by = :me and f.subscription_type = 6 where a.story_id = :story_id and a.id = :thread_id group by a.id order by latest_post desc",array(":story_id"=>$story_id,":thread_id"=>$thread_id,":me"=>$me));
		return $row;
	}

	public function getBrainstormThreadsFromList($user_id){
		$row = $this->_db->select("select a.id, a.thread_title, a.started_at, a.thread_type as thread_type_id, b.thread_type, a.slug as thread_slug, c.id as thread_started_by_id, c.pen_name as thread_started_by_pen_name, c.real_name as thread_started_by_real_name, c.avatar as thread_started_by_avatar, max(d.posted_at) as latest_post, e.id as story_id, e.story_name, e.slug as story_slug from brainstorm_thread a inner join brainstorm_thread_type b on a.thread_type = b.id inner join user c on c.id = a.thread_started_by inner join brainstorm_post d on d.thread_id = a.id inner join story e on a.story_id = e.id left join subscription f on f.item_subscribed_to = e.id left join user g on g.id = e.written_by inner join subscription_type h on h.id = f.subscription_type where f.subscribed_by = :user_id and h.subscription_type = 'brainstorm' group by a.id order by latest_post desc",array(":user_id"=>$user_id));
		return $row;
	}

	public function getBrainstormThreadPosts($thread_id){
		$row = $this->_db->select("select a.id, a.post_body, a.posted_at, c.id as posted_by_id, c.real_name as posted_by_real_name, c.pen_name as posted_by_pen_name, c.avatar as posted_by_avatar from brainstorm_post a inner join brainstorm_thread b on a.thread_id = b.id inner join user c on c.id = a.posted_by where b.id = :thread_id",array(":thread_id"=>$thread_id));
		return $row;
	}

	public function haveISubscribed($id,$me){
		$row = $this->_db->select("select case when (item_subscribed_to = :id and subscribed_by = :user_id) or (b.id = :id and b.written_by = :user_id)then 1 else 0 end as haveISubscribed from subscription a left join story b on a.item_subscribed_to = b.id where subscription_type = 4 order by haveISubscribed desc limit 1",array(":id"=>$id,":user_id"=>$me));
		return $row[0]->haveISubscribed;
	}

	public function getOwnerAndSubscribersOfBrainstorm($story_id){
		$row = $this->_db->select("select distinct id, type from (select distinct d.id, 'follower' as type from subscription a inner join subscription_type b on a.subscription_type = b.id inner join story c on c.id = a.item_subscribed_to inner join user d on d.id = a.subscribed_by where c.id = :story_id) a union select * from (select distinct a.id, 'owner' as type from user a inner join story b on a.id = b.written_by where b.id = :story_id) b",array(":story_id"=>$story_id));
		return $row;
	}

	public function getOwnerAndSubscribersOfTopic($thread_id){
		$row = $this->_db->select("select distinct id, type from (select d.id, 'follower' as type from subscription a inner join subscription_type b on a.subscription_type = b.id inner join brainstorm_thread c on c.id = a.item_subscribed_to inner join user d on d.id = a.subscribed_by where c.id = :thread_id and b.subscription_type = 'brainstorm_topic') a union select * from (select distinct a.id, 'owner' as type from user a inner join brainstorm_thread b on a.id = b.thread_started_by where b.id = :thread_id) b",array(":thread_id"=>$thread_id));
		return $row;
	}

	public function getSlug($thread_id){
		$row = $this->_db->select("select slug from brainstorm_thread where id = :thread_id",array(":thread_id"=>$thread_id));
		return $row[0]->slug;
	}

}