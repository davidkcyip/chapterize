<?php

class Notification_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getNewNotifications($last_checked = null,$me){
		if($last_checked){
			$row = $this->_db->select("select a.id as notification_id, a.is_read, a.action_id, b.activity_type, c.id as activity_user_id, c.pen_name, c.real_name, c.avatar, a.action_done_at from notification a inner join activity_type b on a.activity_type_id = b.id inner join user c on a.activity_by = c.id where a.user_to_notify = :me and a.is_read = 0 and a.action_done_at > :action_done_at", array(":me"=>$me,":action_done_at"=>$last_checked));
		}else{
			$row = $this->_db->select("select a.id as notification_id, a.is_read, a.action_id, b.activity_type, c.id as activity_user_id, c.pen_name, c.real_name, c.avatar, a.action_done_at from notification a inner join activity_type b on a.activity_type_id = b.id inner join user c on a.activity_by = c.id where a.user_to_notify = :me and a.is_read = 0", array(":me"=>$me));
		}
		return $row;
	}

	public function getAllUnreadNotifications($me){
		$row = $this->_db->select("select a.id as notification_id, a.is_read, a.action_id, b.activity_type, c.id as activity_user_id, c.pen_name, c.real_name, c.avatar, a.action_done_at from notification a inner join activity_type b on a.activity_type_id = b.id inner join user c on a.activity_by = c.id where a.user_to_notify = :me and a.is_read = 0 order by a.action_done_at desc", array(":me"=>$me));
		return $row;
	}

	public function getAllNotifications($me){
		$row = $this->_db->select("select a.id as notification_id, a.is_read, a.action_id, b.activity_type, c.id as activity_user_id, c.pen_name, c.real_name, c.avatar, a.action_done_at from notification a inner join activity_type b on a.activity_type_id = b.id inner join user c on a.activity_by = c.id where a.user_to_notify = :me and a.action_done_at >= NOW() - INTERVAL 14 DAY order by a.action_done_at desc", array(":me"=>$me));
		return $row;
	}

	public function getChapter($chapter_id){
		$row = $this->_db->select("select a.chapter_body, a.id as chapter_id, a.chapter_name as chapter_name, a.slug as chapter_slug, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name, d.avatar, a.written_at from chapter a inner join story_map b on a.id = b.chapter_id inner join story c on b.story_id = c.id inner join user d on d.id = a.written_by where a.id = :chapter_id", array(":chapter_id"=>$chapter_id));
		return $row;
	}

	public function getChapterComment($comment_id){
		$row = $this->_db->select("select a.chapter_body, a.id as chapter_id, a.chapter_name as chapter_name, a.slug as chapter_slug, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name, d.avatar, a.written_at, e.id as comment_id, e.comment_body, e.rating from chapter a inner join story_map b on a.id = b.chapter_id inner join story c on b.story_id = c.id inner join story_comment e on e.chapter_id = a.id inner join user d on e.commented_by = d.id where e.id = :comment_id", array(":comment_id"=>$comment_id));
		return $row;
	}

	public function getChapterFavourite($chapter_id, $me){
		$row = $this->_db->select("select a.chapter_body, a.id as chapter_id, a.chapter_name as chapter_name, a.slug as chapter_slug, d.id as story_id, d.story_name as story_name, d.slug as story_slug, b.id as user_id, b.pen_name, b.real_name, b.avatar, a.written_at from chapter a inner join favourite e on e.chapter_id = a.id inner join user b on e.favourited_by = b.id inner join story_map c on c.chapter_id = a.id inner join story d on d.id = c.story_id where a.id = :chapter_id and b.id = :id", array(":chapter_id"=>$chapter_id,":id"=>$me));
		return $row;
	}

	public function getChapterBookmark($chapter_id, $me){
		$row = $this->_db->select("select a.chapter_body, a.id as chapter_id, a.chapter_name as chapter_name, a.slug as chapter_slug, d.id as story_id, d.story_name as story_name, d.slug as story_slug, b.id as user_id, b.pen_name, b.real_name, b.avatar, a.written_at from chapter a inner join bookmark e on e.chapter_id = a.id inner join user b on e.bookmarked_by = b.id inner join story_map c on c.chapter_id = a.id inner join story d on d.id = c.story_id where a.id = :chapter_id and b.id = :id", array(":chapter_id"=>$chapter_id,":id"=>$me));
		return $row;
	}

	public function getBrainstorm($thread_id){
		$row = $this->_db->select("select a.id as thread_id, a.thread_title, a.slug as thread_slug, b.post_body, c.id as story_id, c.slug as story_slug, c.story_name from brainstorm_thread a inner join brainstorm_post b on a.id = b.thread_id inner join story c on c.id = a.story_id where a.id = :thread_id order by b.id desc",array(":thread_id"=>$thread_id));
		return $row;
	}

	public function getStory($challenge_story_id){
		$row = $this->_db->select("select c.id as story_id, c.slug as story_slug, c.story_name, e.chapter_body, e.id as chapter_id, e.slug as chapter_slug, e.chapter_name, b.challenge_name, b.id as challenge_id, b.slug as challenge_slug from challenge_stories a inner join challenge b on a.challenge_id = b.id inner join story c on c.id = a.story_id inner join story_map d on d.story_id = c.id inner join chapter e on e.id = d.chapter_id where a.id = :challenge_story_id order by e.written_at asc",array(":challenge_story_id"=>$challenge_story_id));
		return $row;
	}

	public function getChallengeComment($challenge_comment_id){
		$row = $this->_db->select("select b.id as challenge_id, b.slug as challenge_slug, b.challenge_name, a.comment from challenge_discussion a inner join challenge b on a.challenge_id = b.id where a.id = :challenge_comment_id",array(":challenge_comment_id"=>$challenge_comment_id));
		return $row;
	}

	public function getChallengeWinner($challenge_id){
		$row = $this->_db->select("select a.id as challenge_id, a.challenge_name, a.challenge_description, a.slug as challenge_slug, c.id as story_id, c.story_name, c.story_description, c.slug as story_slug from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join story c on c.id = b.story_id where a.id = :challenge_id and b.is_winner = 1",array(":challenge_id"=>$challenge_id));
		return $row;
	}

	public function getReply($reply_id){
		$row = $this->_db->select("select a.id as reply_id, a.reply_body, b.id as status_id, c.id as status_by, c.real_name, c.pen_name, c.avatar from status_reply a inner join status b on a.status_id = b.id inner join user c on c.id = b.status_to where a.id = :reply_id",array(":reply_id"=>$reply_id));
		return $row;
	}

	public function getStatus($status_id){
		$row = $this->_db->select("select a.id as status_id, a.status_body from status a where a.id = :status_id",array(":status_id"=>$status_id));
		return $row;
	}

	public function getTopic($comment_id,$user){
		$row = $this->_db->select("select b.id as topic_id, b.topic_name, b.slug as topic_slug, a.id as circle_id, a.group_name, a.slug as group_slug, a.id as group_id, c.post_content, d.id as author_id, d.pen_name, d.real_name, d.avatar from circle a inner join circle_topic b on a.id = b.group_id inner join circle_topic_post c on c.group_topic_id = b.id inner join user d on d.id = c.posted_by where c.id = :comment_id and d.id = :user",array(":comment_id"=>$comment_id,":user"=>$user));
		return $row;
	}

	public function getCircle($circle_id,$user){
		$row = $this->_db->select("select c.id as user_id, c.pen_name, c.real_name, c.avatar, a.id as group_id, a.group_name, a.slug as group_slug from circle a inner join circle_user b on b.group_id = a.id inner join user c on c.id = b.author_id where a.id = :circle_id and b.author_id = :user",array(":circle_id"=>$circle_id,":user"=>$user));
		return $row;
	}

	public function checkIfSubscribed($email_settings_type_id, $user_id){
		$row = $this->_db->select("select case when a.user_id = :user_id and b.id = :email_settings_type_id then 1 else 0 end as has_subscribed from email_settings a left join email_settings_type b on b.id = a.email_settings_type_id order by has_subscribed desc limit 1",array(":email_settings_type_id"=>$email_settings_type_id,":user_id"=>$user_id));
		return $row[0]->has_subscribed;
	}

}