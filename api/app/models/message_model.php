<?php

class Message_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getInbox($user_id){
		$row = $this->_db->select("select b.id as thread_id, b.message_title as thread_title, b.created_at as thread_created_at, b.slug as thread_slug, count(c.id) as message_count, min(d.has_read) as has_read, e.id as thread_starter_id, e.pen_name as thread_starter_pen_name, e.real_name as thread_starter_real_name, e.avatar as thread_starter_avatar, max(c.sent_at) as latest_message from pm_recipients a inner join pm_thread b on a.thread_id = b.id inner join pm_post c on b.id = c.pm_thread_id inner join pm_post_read d on d.thread_id = b.id and d.post_id = c.id and d.author_id = :user_id inner join user e on e.id = b.created_by where a.author_id = :user_id group by a.id order by latest_message desc",array(":user_id"=>$user_id));
		return $row;
	}

	public function getRecipients($thread_id){
		$row = $this->_db->select("select a.id, a.pen_name, a.real_name, a.avatar from user a inner join pm_recipients b on a.id = b.author_id where b.thread_id = :thread_id",array(":thread_id"=>$thread_id));
		return $row;
	}

	public function getSent($user_id){
		$row = $this->_db->select("select b.id as thread_id, b.message_title as thread_title, b.created_at as thread_created_at, b.slug as thread_slug, e.id as thread_starter_id, e.pen_name as thread_starter_pen_name, e.real_name as thread_starter_real_name, e.avatar as thread_starter_avatar, c.message_body, c.sent_at as message_sent_at from pm_recipients a inner join pm_thread b on a.thread_id = b.id inner join pm_post c on b.id = c.pm_thread_id inner join pm_post_read d on d.thread_id = b.id and d.post_id = c.id and d.author_id = :user_id inner join user e on e.id = b.created_by where c.sent_by = :user_id group by c.id order by message_sent_at desc",array(":user_id"=>$user_id));
		return $row;
	}

	public function getThread($thread_id,$slug){
		$row = $this->_db->select("select b.id as thread_id, b.message_title as thread_title, b.created_at as thread_created_at, b.slug as thread_slug, e.id as message_by_id, e.pen_name as message_by_pen_name, e.real_name as message_by_real_name, e.avatar as thread_starter_avatar, c.message_body, c.sent_at as message_sent_at from pm_recipients a inner join pm_thread b on a.thread_id = b.id inner join pm_post c on b.id = c.pm_thread_id inner join pm_post_read d on d.thread_id = b.id and d.post_id = c.id inner join user e on e.id = c.sent_by where b.id = :thread_id and b.slug = :slug group by c.id order by message_sent_at asc",array(":thread_id"=>$thread_id,":slug"=>$slug));
		return $row;
	}

	public function getThreadById($thread_id){
		$row = $this->_db->select("select b.id as thread_id, b.message_title as thread_title, b.created_at as thread_created_at, b.slug as thread_slug, e.id as message_by_id, e.pen_name as message_by_pen_name, e.real_name as message_by_real_name, e.avatar as thread_starter_avatar, c.message_body, c.sent_at as message_sent_at from pm_recipients a inner join pm_thread b on a.thread_id = b.id inner join pm_post c on b.id = c.pm_thread_id inner join pm_post_read d on d.thread_id = b.id and d.post_id = c.id inner join user e on e.id = c.sent_by where b.id = :thread_id group by c.id order by message_sent_at asc",array(":thread_id"=>$thread_id));
		return $row[0];
	}

	public function getNumberOfUnreadMessages($me){
		$row = $this->_db->select("select count(*) as unread_count from pm_post_read where author_id = :me and has_read = 0",array(":me"=>$me));
		return $row[0]->unread_count;
	}

}