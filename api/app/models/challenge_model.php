<?php

class Challenge_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getChallengeOwner($challenge_id){
		$row = $this->_db->select("select challenge_by from challenge where id = :challenge_id", array(":challenge_id"=>$challenge_id));
		return $row[0]->challenge_by;
	}

	public function getLeaderboard($time){
		if($time == "allTime"){
			$row = $this->_db->select("select c.id as author_id, c.pen_name, c.real_name, c.avatar, (select count(id) from challenge_stories where submitted_by = c.id and is_winner) as numWins, count(b.id) as numSubmitted, max(b.final_rating) as max_final_rating from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join user c on c.id = b.submitted_by group by c.id order by numWins desc, numSubmitted asc, max_final_rating desc, c.real_name asc");
		}else if($time == "7days"){
			$row = $this->_db->select("select c.id as author_id, c.pen_name, c.real_name, c.avatar, (select count(id) from challenge_stories where submitted_by = c.id and is_winner) as numWins, count(b.id) as numSubmitted, max(b.final_rating) as max_final_rating from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join user c on c.id = b.submitted_by where a.challenge_end_date >= NOW() - INTERVAL 7 DAY group by c.id order by numWins desc, numSubmitted asc, max_final_rating desc, c.real_name asc");
		}else if($time == "lastMonth"){
			$row = $this->_db->select("select c.id as author_id, c.pen_name, c.real_name, c.avatar, (select count(id) from challenge_stories where submitted_by = c.id and is_winner) as numWins, count(b.id) as numSubmitted, max(b.final_rating) as max_final_rating, max(final_rating) as max_final_rating from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join user c on c.id = b.submitted_by where a.challenge_end_date >= NOW() - INTERVAL 30 DAY group by c.id order by numWins desc, numSubmitted asc, max_final_rating desc, c.real_name asc");
		}
		return $row;
	}

	public function getActiveChallenges($type,$page){
		if($type == "recent"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date > NOW() order by a.challenge_added desc limit $page, 10");
		}else if($type == "soonest"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date > NOW() order by a.challenge_end_date asc limit $page, 10");
		}else if($type == "popular"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date > NOW() order by story_count, comment_count desc limit $page, 10");
		}
		return $row;
	}

	public function getNumberOfActiveChallenges(){
		$row = $this->_db->select("select count(*) as challenge_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date > NOW()");
		return $row[0]->challenge_count;
	}

	public function getPreviousChallenges($type,$page){
		if($type == "soonest"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date < NOW() order by a.challenge_end_date asc limit $page, 10");
		}else if($type == "popular"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date < NOW() order by story_count, comment_count desc limit $page, 10");
		}
		return $row;
	}

	public function getNumberOfPreviousChallenges(){
		$row = $this->_db->select("select count(*) as challenge_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_end_date < NOW()");
		return $row[0]->challenge_count;
	}

	public function getMyChallenges($type,$me,$page){
		if($type == "soonest"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_by = :user_id group by a.id order by a.challenge_end_date asc limit $page, 10", array(":user_id"=>$me));
		}else if($type == "popular"){
			$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.pen_name, b.real_name, b.avatar, (select count(id) from challenge_stories where challenge_id = a.id) as story_count, (select count(id) from challenge_discussion where challenge_id = a.id) as comment_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_by = :user_id or c.submitted_by = :user_id group by a.id order by story_count, comment_count desc limit $page, 10", array(":user_id"=>$me));
		}
		return $row;
	}

	public function getNumberOfMyChallenges(){
		$row = $this->_db->select("select count(*) as challenge_count from challenge a inner join user b on a.challenge_by = b.id where a.challenge_by = :user_id", array(":user_id"=>$me));
		return $row[0]->challenge_count;
	}

	public function getChallengeWinners($type,$page){
		if($type == "soonest"){
			$row = $this->_db->select("select a.id as story_id, a.story_name, a.story_description, a.slug as story_slug, d.id as challenge_id, d.challenge_name, d.challenge_by, d.challenge_description, d.challenge_start_date, d.challenge_end_date, d.slug as challenge_slug, d.challenge_added, e.pen_name, e.real_name, e.avatar, count(distinct a.id) as story_count, count(f.id) as comment_count, round(avg(b.rating),2) as story_rating from story a left join story_comment b on a.id = b.story_id and b.rating is not null inner join challenge_stories c on c.story_id = a.id inner join challenge d on d.id = c.challenge_id inner join user e on e.id = c.submitted_by left join challenge_discussion f on f.challenge_id = d.id where c.is_winner = 1 order by d.challenge_end_date desc limit $page, 10");
		}else if($type == "popular"){
			$row = $this->_db->select("select a.id as story_id, a.story_name, a.story_description, a.slug as story_slug, d.id as challenge_id, d.challenge_name, d.challenge_by, d.challenge_description, d.challenge_start_date, d.challenge_end_date, d.slug as challenge_slug, d.challenge_added, e.pen_name, e.real_name, e.avatar, count(distinct a.id) as story_count, count(f.id) as comment_count, round(avg(b.rating),2) as story_rating from story a left join story_comment b on a.id = b.story_id and b.rating is not null inner join challenge_stories c on c.story_id = a.id inner join challenge d on d.id = c.challenge_id inner join user e on e.id = c.submitted_by left join challenge_discussion f on f.challenge_id = d.id where c.is_winner = 1 order by story_count, comment_count desc limit $page, 10");
		}
		return $row;
	}

	public function getNumberOfChallengeWinners(){
		$row = $this->_db->select("select count(*) as challenge_count from challenge_stories where is_winner = 1");
		return $row[0]->challenge_count;
	}

	public function getChallenge($id,$me){
		$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.real_name, b.pen_name, b.avatar, case when c.subscribed_by = :me then 1 else 0 end as have_i_subscribed from challenge a inner join user b on a.challenge_by = b.id left join subscription c on c.item_subscribed_to = a.id and c.subscription_type = 5 where a.id = :id",array(":id"=>$id,":me"=>$me));
		return $row;
	}

	public function getChallengeById($id){
		$row = $this->_db->select("select a.id, a.challenge_name, a.challenge_by, a.challenge_description, a.challenge_start_date, a.challenge_end_date, a.slug, a.challenge_added, b.real_name, b.pen_name, b.avatar from challenge a inner join user b on a.challenge_by = b.id left join subscription c on c.item_subscribed_to = a.id and c.subscription_type = 5 where a.id = :id",array(":id"=>$id));
		return $row[0];
	}

	public function getChallengeStories($id){
		$row = $this->_db->select("select c.id as story_id, c.story_name, c.story_description, c.slug, d.id as user_id, d.pen_name, d.real_name, d.avatar, b.submitted_at, b.is_winner, round(avg(e.rating),2) as average_rating, b.id as challenge_story_id from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join story c on c.id = b.story_id inner join user d on d.id = c.written_by left join story_comment e on e.story_id = c.id where a.id = :id group by c.id",array(":id"=>$id));
		return $row;
	}

	public function getChallengeComments($id){
		$row = $this->_db->select("select b.id as comment_id, b.author_id, b.comment, b.commented_at, c.pen_name, c.real_name, c.avatar from challenge a inner join challenge_discussion b on b.challenge_id = a.id inner join user c on c.id = b.author_id where a.id = :id",array(":id"=>$id));
		return $row;
	}

	public function getChallengesThatEndedYesterday(){
		$row = $this->_db->select("select * from challenge where DATE(challenge_end_date) = CURDATE() - INTERVAL 1 DAY");
		return $row;
	}

	public function getAllChallengeStories($challenge_id){
		$row = $this->_db->select("select b.story_id from challenge a inner join challenge_stories b on a.id = b.challenge_id where a.id = :challenge_id",array(":challenge_id"=>$challenge_id));
		return $row;
	}

	public function getStoryRating($story_id){
		$row = $this->_db->select("select avg(b.rating) as avg_rating from story a inner join story_comment b on a.id = b.story_id where a.id = :story_id and b.rating is not null",array(":story_id"=>$story_id));
		return $row;
	}

	public function getChallengeStoryWithHighestAverageRating($id){
		$row = $this->_db->select("select c.id as story_id, c.story_name, c.story_description, c.slug, d.id as user_id, d.pen_name, d.real_name, d.avatar, b.submitted_at, b.is_winner, round(max(b.final_rating),2) as average_rating, b.id as challenge_story_id from challenge a inner join challenge_stories b on a.id = b.challenge_id inner join story c on c.id = b.story_id inner join user d on d.id = c.written_by left join story_comment e on e.story_id = c.id where a.id = :id group by c.id order by average_rating desc LIMIT 1",array(":id"=>$id));
		return $row;
	}

	public function getOwnerOfChallenge($challenge_id){
		$row = $this->_db->select("select challenge_by from challenge a where id = :challenge_id",array(":challenge_id"=>$challenge_id));
		return $row[0]->challenge_by;
	}

	public function getChallengeSubscribers($challenge_id, $me){
		$row = $this->_db->select("select subscribed_by from subscription where item_subscribed_to = :challenge_id and subscription_type = 5 and subscribed_by <> :me",array(":challenge_id"=>$challenge_id,":me"=>$me));
		return $row;
	}

	public function getOwnerOfChallengeAndSubmitters($challenge_id){
		$row = $this->_db->select("select distinct * from (select distinct challenge_by as id, 'owner' as type from challenge a where id = :challenge_id) a union select distinct * from (select submitted_by as id, 'submitter' as type from challenge_stories where challenge_id = :challenge_id) b",array(":challenge_id"=>$challenge_id));
		return $row;
	}

}