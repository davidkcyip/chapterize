<?php

class Email_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function sendEmail($message, $subject, $email, $reply_to = null){
		$from = EMAIL_INFO_FROM;

		$server = SMTP_SERVER;
		$username = SMTP_USERNAME;
		$password = SMTP_PASSWORD;
		$port = SMTP_PORT;

		$smtp = Mail::factory('smtp',
		   array ('host' => $server,
		     'auth' => true,
		     'port' => $port,
		     'username' => $username,
		     'password' => $password));
		if($reply_to == null){
			$headers = array ('From' => $from,
					'To' => $email,
					'Subject' => $subject);
		}else{
			$headers = array ('From' => $from,
					'To' => $email,
					'Reply-To' => $reply_to,
					'Subject' => $subject);
		}
		$mail = $smtp->send($email, $headers, $message);
		if (PEAR::isError($mail)) {
		   return 0;
		} else {
		   return 1;
		}
	}

	public function newChapterEmailBody($owner_real_name, $chapter_starter_real_name, $story_name, $chapter_name, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$chapter_starter_real_name." has published ".$chapter_name." in your story ".$story_name.".\r\n\r\nTo view this story go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function chapterCommentEmailBody($owner_real_name, $commenter_real_name, $chapter_name, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$commenter_real_name." has commented on your chapter ".$chapter_name.".\r\n\r\nTo view the comment, go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function chapterRateEmailBody($owner_real_name, $commenter_real_name, $chapter_name, $rating, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$commenter_real_name." has rated your chapter ".$chapter_name." ".$rating."/5\r\n\r\nTo view the rating, go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function bookmarkStoryEmailBody($owner_real_name, $bookmark_real_name, $story_name, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$bookmark_real_name." has bookmarked your story ".$story_name.".\r\n\r\nTo view the story, go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function bookmarkChapterEmailBody($owner_real_name, $chapter_starter_real_name, $story_name, $chapter_name, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$chapter_starter_real_name." has bookmarked your chapter ".$chapter_name." in the story ".$story_name.".\r\n\r\nTo view this story go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function favouriteChapterEmailBody($owner_real_name, $chapter_starter_real_name, $story_name, $chapter_name, $story_url){
		return "Greetings ".$owner_real_name.",\r\n\r\n".$chapter_starter_real_name." has favourited your chapter ".$chapter_name." in the story ".$story_name.".\r\n\r\nTo view this story go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function followerNewStoryEmailBody($story_starter_real_name, $person_to_alert_real_name, $story_name, $story_url){
		return "Greetings ".$person_to_alert_real_name.",\r\n\r\n".$story_starter_real_name." has published a new story ".$story_name.".\r\n\r\nTo view the story, go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function followerNewChapterEmailBody($follower_real_name, $chapter_starter_real_name, $story_name, $chapter_name, $story_url){
		return "Greetings ".$follower_real_name.",\r\n\r\n".$chapter_starter_real_name." has published ".$chapter_name." in the story ".$story_name.".\r\n\r\nTo view this story go to:\r\n\r\n".$story_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function newFollowerEmailBody($followed_person, $follower, $person_url){
		return "Greetings ".$followed_person.",\r\n\r\n".$follower." has started following you.\r\n\r\nTo view this person's profile, go to:\r\n\r\n".$person_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function newPrivateMessageEmailBody($messaged_person, $messager, $message_url){
		return "Greetings ".$messaged_person.",\r\n\r\n".$messager." has sent you a new private message.\r\n\r\nTo view the message, go to:\r\n\r\n".$message_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function repostEmailBody($reposted_person, $reposter, $repost_url){
		return "Greetings ".$reposted_person.",\r\n\r\n".$reposter." has reposted one of your statuses.\r\n\r\nTo view the status, go to:\r\n\r\n".$repost_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function wallPostEmailBody($posted_person, $poster, $post_url){
		return "Greetings ".$posted_person.",\r\n\r\n".$poster." has posted a status on your wall.\r\n\r\nTo view the status, go to:\r\n\r\n".$post_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function postReplyEmailBody($status_owner, $replier, $post_url){
		return "Greetings ".$status_owner.",\r\n\r\n".$replier." has replied to one of your statuses.\r\n\r\nTo view the status, go to:\r\n\r\n".$post_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function newBrainstormTopicOwnerEmailBody($story_owner, $topic_starter, $story_name, $topic_url){
		return "Greetings ".$story_owner.",\r\n\r\n".$topic_starter." has started a new Brainstorm topic in your story ".$story_name."\r\n\r\nTo view the topic, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function newBrainstormTopicFollowerEmailBody($brainstorm_follower, $topic_starter, $story_name, $topic_url){
		return "Greetings ".$brainstorm_follower.",\r\n\r\n".$topic_starter." has started a new topic in the Brainstorm area of ".$story_name." you are following.\r\n\r\nTo view the topic, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function brainstormTopicReplyOwnerEmailBody($topic_owner, $topic_replier, $story_name, $topic_url){
		return "Greetings ".$topic_owner.",\r\n\r\n".$topic_replier." has replied to a Brainstorm topic you started in the story ".$story_name."\r\n\r\nTo view the topic, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function brainstormTopicReplyFollowerEmailBody($topic_follower, $topic_replier, $story_name, $topic_url){
		return "Greetings ".$topic_follower.",\r\n\r\n".$topic_replier." has replied to a Brainstorm topic you are following in the story ".$story_name."\r\n\r\nTo view the topic, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function challengeSubmitOwnerEmailBody($challenge_owner, $story_submitter, $challenge_name, $topic_url){
		return "Greetings ".$challenge_owner.",\r\n\r\n".$story_submitter." has submitted a new story to your challenge ".$challenge_name."\r\n\r\nTo view the story, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function challengeSubmitFollowerEmailBody($challenge_follower, $story_submitter, $challenge_name, $topic_url){
		return "Greetings ".$challenge_follower.",\r\n\r\n".$story_submitter." has submitted a new story to your challenge ".$challenge_name." you are following.\r\n\r\nTo view the story, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function challengeCommentOwnerEmailBody($challenge_owner, $comment_submitter, $challenge_name, $topic_url){
		return "Greetings ".$challenge_owner.",\r\n\r\n".$comment_submitter." has left a comment on your challenge ".$challenge_name."\r\n\r\nTo view the comment, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function challengeEndOwnerEmailBody($challenge_owner, $challenge_name, $topic_url){
		return "Greetings ".$challenge_owner.",\r\n\r\nYour challenge ".$challenge_name." has ended.\r\n\r\nTo view the challenge winner, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function challengeEndSubmitterEmailBody($challenge_submitter, $challenge_name, $topic_url){
		return "Greetings ".$challenge_submitter.",\r\n\r\nThe challenge ".$challenge_name." has ended.\r\n\r\nTo view the challenge winner, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function newCircleTopicReplyEmailBody($topic_follower, $topic_commenter, $circle_name, $topic_url){
		return "Greetings ".$topic_follower.",\r\n\r\n".$topic_commenter." has replied to a Circle topic you follow in ".$circle_name."\r\n\r\nTo view the reply, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function circleRequestEmailBody($admin, $circle_requester, $circle_name, $topic_url){
		return "Greetings ".$admin.",\r\n\r\n".$circle_requester." has requested to join ".$circle_name."\r\n\r\nTo approve this author, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

	public function approveCircleRequestEmailBody($circle_requester, $admin, $circle_name, $topic_url){
		return "Greetings ".$circle_requester.",\r\n\r\n".$admin." has approved your request to join ".$circle_name."\r\n\r\nTo view the circle, go to:\r\n\r\n".$topic_url."\r\n\r\nRegards,\r\nBloqly Support.\r\n\r\n---------------\r\n\r\nIf you would like to unsubscribe from these alerts, go to http://www.bloqly.com/settings/email";
	}

}