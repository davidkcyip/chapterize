<?php

class Search_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function searchResults($query, $page){
		$row = $this->_db->select("SELECT * from ( select
										'story' as type,
										a.id as user_id, 
										a.avatar as avatar, 
										a.country as country, 
										a.pen_name as pen_name, 
										a.real_name as real_name, 
										a.bio as bio, 
										DATE(a.registration_date) as registration_date,
										null as chapter_id,
										null as chapter_name,
										null as chapter_written_by,
										null as chapter_date,
										null as chapter_slug,
										c.id as story_id,
										c.album as album,
										c.story_name as story_name,
										c.written_by as story_written_by,
										c.created_at as story_date,
										c.story_type as story_type,
										c.story_description as story_description,
										c.is_public as is_public,
										c.slug as story_slug
										from story c inner join user a on a.id = c.written_by left join story_tag b on b.story_id = c.id where c.is_public = 1 and c.story_name like '%$query%' or b.tag like '%$query%') a
										union select * from (select
										'chapter' as type,
										a.id as user_id, 
										a.avatar as avatar, 
										a.country as country, 
										a.pen_name as pen_name, 
										a.real_name as real_name, 
										a.bio as bio,
										DATE(a.registration_date) as registration_date,
										b.id as chapter_id,
										b.chapter_name as chapter_name,
										b.written_by as chapter_written_by,
										DATE(b.written_at) as chapter_date,
										b.slug as chapter_slug,
										d.id as story_id,
										d.album as album,
										d.story_name as story_name,
										d.written_by as story_written_by,
										d.created_at as story_date,
										d.story_type as story_type,
										d.story_description as story_description,
										d.is_public as is_public,
										d.slug as story_slug
										from chapter b inner join user a on a.id = b.written_by inner join story_map c on c.chapter_id = b.id inner join story d on d.id = c.story_id where d.is_public = 1 and b.chapter_name like '%$query%') b
										union select * from (
										select
										'user' as type,
										a.id as user_id, 
										a.avatar as avatar, 
										a.country as country, 
										a.pen_name as pen_name, 
										a.real_name as real_name, 
										a.bio as bio, 
										DATE(a.registration_date) as registration_date,
										null as chapter_id,
										null as chapter_name,
										null as chapter_written_by,
										null as chapter_date,
										null as chapter_slug,
										null as story_id,
										null as album,
										null as story_name,
										null as story_written_by,
										null as story_date,
										null as story_type,
										null as story_description,
										null as is_public,
										null as story_slug
										from user a where a.pen_name like '%$query%' or a.real_name like '%$query%') c limit $page, 10
										");
		return $row;
	}

	public function searchResultsStoryCount($query){
		$row = $this->_db->select("select count(*) as story_count from story c inner join user a on a.id = c.written_by left join story_tag b on b.story_id = c.id where c.is_public = 1 and c.story_name like '%$query%' or b.tag like '%$query%'");
		return $row[0]->story_count;
	}

	public function searchResultsChapterCount($query){
		$row = $this->_db->select("select count(*) as chapter_count from chapter b inner join user a on a.id = b.written_by inner join story_map c on c.chapter_id = b.id inner join story d on d.id = c.story_id where d.is_public = 1 and b.chapter_name like '%$query%'");
		return $row[0]->chapter_count;
	}

	public function searchResultsAuthorCount($query){
		$row = $this->_db->select("SELECT count(*) as author_count from user a where a.pen_name like '%$query%' or a.real_name like '%$query%'");
		return $row[0]->author_count;
	}

	public function getTotalNumberOfTags(){
		$row = $this->_db->select("select count(*) as tag_count from story_tag where tag <> '' and tag is not null");
		return $row[0]->tag_count;
	}

	public function getAllTags(){
		$row = $this->_db->select("select distinct tag, slug, count(*) as tag_count from story_tag where tag <> '' and tag is not null group by tag order by tag_count desc limit 100");
		return $row;
	}

	public function getTagNum($tag){
		$row = $this->_db->select("select count(*) as tag_num from story_tag where tag = :tag and tag <> '' and tag is not null",array(":tag"=>$tag));
		return $row[0]->tag_num;
	}

	public function getNumberOfStoriesPerTag($tag){
		$row = $this->_db->select("select count(distinct story_id) as tag_num from story_tag where tag = :tag",array(":tag"=>$tag));
		return $row[0]->tag_num;
	}

	public function getStoriesBasedOnTags($tag,$page){
		$row = $this->_db->select("select a.id as user_id, 
										a.avatar as avatar, 
										a.country as country, 
										a.pen_name as pen_name, 
										a.real_name as real_name, 
										a.bio as bio, 
										DATE(a.registration_date) as registration_date,
										c.id as story_id,
										c.album as album,
										c.story_name as story_name,
										c.written_by as story_written_by,
										c.created_at as story_date,
										c.story_type as story_type,
										c.story_description as story_description,
										c.is_public as is_public,
										c.slug as story_slug,
										b.tag
										from story c inner join user a on a.id = c.written_by left join story_tag b on b.story_id = c.id where c.is_public = 1 and b.slug = :tag limit $page, 10", array(":tag"=>$tag));
		return $row;
	}
}