<?php

class Settings_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getEmailSettings($me){
		$row = $this->_db->select("select email_settings_type_id from email_settings where user_id = :user_id", array(":user_id"=>$me));
		return $row;
	}

}