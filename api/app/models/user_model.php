<?php

class User_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getFollowers($id){
		$row = $this->_db->select("select a.id, a.pen_name, a.real_name, a.country, a.bio, a.avatar, a.email from user a inner join subscription b on a.id = b.subscribed_by inner join subscription_type c on c.id = b.subscription_type where b.item_subscribed_to = :user_id and c.subscription_type = 'user'", array(":user_id"=>$id));
		return $row;
	}

	public function getFollowing($id){
		$row = $this->_db->select("select a.id, a.pen_name, a.real_name, a.country, a.bio, a.avatar from user a inner join subscription b on a.id = b.item_subscribed_to inner join subscription_type c on c.id = b.subscription_type where b.subscribed_by = :user_id and c.subscription_type = 'user'", array(":user_id"=>$id));
		return $row;
	}

	public function autocomplete($user){
		$row = $this->_db->select("SELECT CONCAT(real_name, ' (', pen_name, ')') as name from user where pen_name like '%$user%' or real_name like '%$user%'");
		return $row;
	}

	public function checkUserExists($email){
		$row = $this->_db->select("SELECT id FROM user WHERE (email = :email and email is not null)", array(':email' => $email));
		return $row[0]->id;
	}

	public function checkValidSession($email, $session){
		$row = $this->_db->select("SELECT count(*) as session_count FROM user WHERE session = :session and email = :email", array(':session' => $session, 'email'=> $email));
		return $row[0]->session_count;
	}

	public function checkValidPenname($penname){
		$row = $this->_db->select("SELECT count(*) as username_count FROM user WHERE pen_name = :pen_name", array(':pen_name' => $penname));
		return $row[0]->username_count;
	}

	public function checkIfActivationCodeIsCorrect($user_id, $activation_code){
		$row = $this->_db->select("SELECT count(*) as count FROM user WHERE id = :user_id and activation_code = :activation_code", array(':user_id' => $user_id,':activation_code'=>$activation_code));
		return $row[0]->count;
	}

	public function getPasswordSalt($userOrEmail){
		$row = $this->_db->select("SELECT salt FROM user WHERE email = :userOrEmail or pen_name = :userOrEmail", array(':userOrEmail' => $userOrEmail));
		return $row[0]->salt;
	}

	public function checkIfUserIsActive($user){
		$row = $this->_db->select("SELECT is_active from user where pen_name = :user or email = :user",array(":user"=>$user));
		return $row[0]->is_active;
	}

	public function checkPennameExists($penname){
		$row = $this->_db->select("SELECT count(*) as penname_count FROM user WHERE pen_name = :penname", array(':penname' => $penname));
		return $row[0]->penname_count;
	}

	public function checkEmailExists($email){
		$row = $this->_db->select("SELECT count(*) as email_count FROM user WHERE email = :email", array(':email' => $email));
		return $row[0]->email_count;
	}

	public function returnPenname($email){
		$row = $this->_db->select("SELECT pen_name FROM user WHERE email = :email",array(":email"=>$email));
		return $row[0];
	}

	public function returnPassword($email){
		$row = $this->_db->select("SELECT password FROM user WHERE email = :email",array(":email"=>$email));
		return $row[0];
	}

	public function returnUserById($user_id){
		$row = $this->_db->select("SELECT a.id, a.pen_name, a.real_name, a.email, a.country, a.avatar, a.bio, a.website_link, a.facebook_link, a.twitter_link, a.google_link, a.session, DATE(a.registration_date) as registration_date, b.login_time as last_login_time FROM user a left join login b on a.id = b.user_id WHERE a.id = :user_id order by b.login_time desc limit 1",array(":user_id"=>$user_id));
		return $row[0];
	}

	public function returnUserByEmail($email){
		$row = $this->_db->select("SELECT id, pen_name, real_name, email, country, avatar, bio, website_link, facebook_link, twitter_link, google_link, session, DATE(registration_date) as registration_date FROM user WHERE email = :email",array(":email"=>$email));
		return $row[0];
	}

	public function returnUserByPenname($penname){
		$row = $this->_db->select("SELECT a.id, a.pen_name, a.real_name, a.email, a.country, a.avatar, a.bio, a.website_link, a.facebook_link, a.twitter_link, a.google_link, DATE(a.registration_date) as registration_date, b.login_time as login_time FROM user a left join login b on a.id = b.user_id WHERE a.pen_name = :pen_name order by b.login_time desc limit 1",array(":pen_name"=>$penname));
		return $row[0];
	}

	public function getAllProfiles($page){
		$row = $this->_db->select("SELECT distinct i.id, i.avatar, i.country, i.pen_name, i.real_name, i.bio, DATE(i.registration_date) as registration_date, (SELECT avg(c.rating) from chapter a inner join user b on a.written_by = b.id inner join story_comment c on c.chapter_id = a.id where b.id = i.id) as avg_rating, (SELECT count(distinct c.chapter_id) as num_ratings from user a inner join chapter b on a.id = b.written_by inner join story_comment c on b.id = c.chapter_id where c.rating is not null and b.written_by = i.id and c.commented_by <> i.id) as num_ratings_for_author, (SELECT count(*) as favourite_count from user a inner join chapter b on a.id = b.written_by inner join favourite c on b.id = c.chapter_id where a.id = i.id) as num_favourites from user i limit $page, 10");
		return $row;
	}

	public function getPopularProfiles($page){
		$row = $this->_db->select("SELECT distinct i.id, i.avatar, i.country, i.pen_name, i.real_name, i.bio, DATE(i.registration_date) as registration_date, (SELECT avg(c.rating) from chapter a inner join user b on a.written_by = b.id inner join story_comment c on c.chapter_id = a.id where b.id = i.id) as avg_rating, (SELECT count(distinct c.chapter_id) as num_ratings from user a inner join chapter b on a.id = b.written_by inner join story_comment c on b.id = c.chapter_id where c.rating is not null and b.written_by = i.id and c.commented_by <> i.id) as num_ratings_for_author, (SELECT count(*) as favourite_count from user a inner join chapter b on a.id = b.written_by inner join favourite c on b.id = c.chapter_id where a.id = i.id) as num_favourites from user i order by avg_rating desc limit $page, 10");
		return $row;
	}

	public function getRecentProfiles($page){
		$row = $this->_db->select("SELECT distinct i.id, i.avatar, i.country, i.pen_name, i.real_name, i.bio, DATE(i.registration_date) as registration_date, (SELECT avg(c.rating) from chapter a inner join user b on a.written_by = b.id inner join story_comment c on c.chapter_id = a.id where b.id = i.id) as avg_rating, (SELECT count(distinct c.chapter_id) as num_ratings from user a inner join chapter b on a.id = b.written_by inner join story_comment c on b.id = c.chapter_id where c.rating is not null and b.written_by = i.id and c.commented_by <> i.id) as num_ratings_for_author, (SELECT count(*) as favourite_count from user a inner join chapter b on a.id = b.written_by inner join favourite c on b.id = c.chapter_id where a.id = i.id) as num_favourites from user i order by i.registration_date desc limit $page, 10");
		return $row;
	}

	public function getNumberOfAuthors(){
		$row = $this->_db->select("select count(*) as user_count from user");
		return $row[0]->user_count;
	}

	public function login($user, $pass){
		$username = $user;
		$password = $pass;
		$row = $this->_db->select("SELECT count(*) as login_count, a.id, c.role_name, a.pen_name FROM user a inner join user_role b on a.id = b.user_id inner join role c on b.role_id = c.id WHERE (a.pen_name = :username or a.email = :username) and a.password = :password", array(':username' => $username,':password' => $password));
		$userid = $row[0]->id;
		$count = $row[0]->login_count;
		$role_name = $row[0]->role_name;
		$username = $row[0]->pen_name;
		if($count > 0){
			$success = array("success"=>"1","username"=>$username,"id"=>$userid);
			return json_encode($success);
		}else{
			$success = array("success"=>"0");
			return json_encode($success);
		}	
	}

	public function getSubscriberStats($user_id){
		$row = $this->_db->select("SELECT count(*) as subscriber_count from subscription a inner join subscription_type b on a.subscription_type = b.id where b.subscription_type = 'user' and a.item_subscribed_to = :user_id", array(":user_id"=>$user_id));
		return $row[0]->subscriber_count;
	}

	public function getSubscriptionStats($user_id){
		$row = $this->_db->select("SELECT count(*) as subscription_count from subscription a inner join subscription_type b on a.subscription_type = b.id where b.subscription_type = 'user' and a.subscribed_by = :user_id", array(":user_id"=>$user_id));
		return $row[0]->subscription_count;
	}

	public function getNumberOfStories($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as story_count from story a  where written_by = :user_id", array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as story_count from story a  where written_by = :user_id and DATE(created_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()", array(":user_id"=>$user_id));
		}
		return $row[0]->story_count;
	}

	public function getNumberOfChapters($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as chapter_count from chapter a inner join story_map b on a.id = b.chapter_id where a.written_by = :user_id", array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as chapter_count from chapter a inner join story_map b on a.id = b.chapter_id where a.written_by = :user_id and DATE(a.written_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()", array(":user_id"=>$user_id));
		}
		return $row[0]->chapter_count;
	}

	public function getNumberOfComments($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as comment_count from story_comment a inner join user b on a.commented_by = b.id where a.rating is null and b.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as comment_count from story_comment a inner join user b on a.commented_by = b.id where a.rating is null and b.id = :user_id and DATE(a.commented_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->comment_count;
	}

	public function getNumberOfRatings($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as rating_count from story_comment a inner join user b on a.commented_by = b.id where a.rating is not null and b.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as rating_count from story_comment a inner join user b on a.commented_by = b.id where a.rating is not null and b.id = :user_id and DATE(a.commented_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->rating_count;
	}

	public function getNumberOfReadsMade($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as reads_made_count from user a inner join read_history b on a.id = b.read_by where a.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as reads_made_count from user a inner join read_history b on a.id = b.read_by where a.id = :user_id and DATE(b.read_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->reads_made_count;
	}

	public function getNumberOfReadsMadeUnique($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(distinct b.chapter_id) as reads_made_count from user a inner join read_history b on a.id = b.read_by where a.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(distinct b.chapter_id) as reads_made_count from user a inner join read_history b on a.id = b.read_by where a.id = :user_id and DATE(b.read_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->reads_made_count;
	}

	public function getAverageRating($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT avg(c.rating) as rating_average from chapter a inner join user b on a.written_by = b.id inner join story_comment c on c.chapter_id = a.id where b.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT avg(c.rating) as rating_average from chapter a inner join user b on a.written_by = b.id inner join story_comment c on c.chapter_id = a.id where b.id = :user_id and DATE(c.commented_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->rating_average;
	}

	public function getNumberOfPeopleWhoRated($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(distinct c.chapter_id) as num_ratings from user a inner join chapter b on a.id = b.written_by inner join story_comment c on b.id = c.chapter_id where c.rating is not null and b.written_by = :user_id and c.commented_by <> :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(distinct c.chapter_id) as num_ratings from user a inner join chapter b on a.id = b.written_by inner join story_comment c on b.id = c.chapter_id where c.rating is not null and b.written_by = :user_id and c.commented_by <> :user_id and DATE(c.commented_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->num_ratings;
	}

	public function getNumberOfReads($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as read_count from user a inner join chapter b on a.id = b.written_by inner join read_history c on b.id = c.chapter_id where a.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as read_count from user a inner join chapter b on a.id = b.written_by inner join read_history c on b.id = c.chapter_id where a.id = :user_id and DATE(c.read_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->read_count;
	}

	public function getNumberOfReadsUnique($user_id,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(distinct *) as read_count from user a inner join chapter b on a.id = b.written_by inner join read_history c on b.id = c.chapter_id where a.id = :user_id",array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(distinct *) as read_count from user a inner join chapter b on a.id = b.written_by inner join read_history c on b.id = c.chapter_id where a.id = :user_id and DATE(c.read_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()",array(":user_id"=>$user_id));
		}
		return $row[0]->read_count;
	}

	public function getNumberOfFavourites($user_id,$period = null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as favourite_count from user a inner join chapter b on a.id = b.written_by inner join favourite c on b.id = c.chapter_id where a.id = :user_id", array(":user_id"=>$user_id));
		}else{
			$row = $this->_db->select("SELECT count(*) as favourite_count from user a inner join chapter b on a.id = b.written_by inner join favourite c on b.id = c.chapter_id where a.id = :user_id and DATE(c.favourited_at) BETWEEN date_add(NOW(), INTERVAL -7 DAY) AND NOW()", array(":user_id"=>$user_id));
		}
		return $row[0]->favourite_count;
	}

	public function getOwnerOfStatus($status_id){
		$row = $this->_db->select("select status_by from status where id = :status_id",array(":status_id"=>$status_id));
		return $row[0]->status_by;
	}

	public function insertIntoTable($values, $table){
		//self::connectToDatabase();
		$insert = $this->_db->insert($table,$values);
		return $this->_db->lastInsertId('id');
    }

    public function updateTable($data, $where, $table){
    	$update = $this->_db->update($table,$data, $where);
    	return $update;
    }

    public function deleteRows($values, $table){
    	$update = $this->_db->delete($table,$values);
    	return $update;
    }

    public function generateSalt($length = 64){
    	return substr(str_shuffle("0123456789" . microtime() . "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" . microtime()), 0, $length);
    }

    public function generateSession($length = 20){
    	return substr(str_shuffle("0123456789" . microtime() . "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" . microtime()), 0, $length);
    }

    public function generatePasswordHash($pass){
    	return md5($pass);
    }

    public function generateActivationCode(){
    	return rtrim(base64_encode(md5(microtime())),"=");
    }

    public function generateNewPassword($length = 10){
    	return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}