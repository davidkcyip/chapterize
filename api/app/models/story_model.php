<?php

class Story_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getLatestChapterComments(){ 
		$row = $this->_db->select("select c.id as author_id, c.pen_name, c.real_name, c.avatar, a.id as story_id, a.slug as story_slug, a.story_name, b.chapter_id, b.comment_body, b.commented_at, d.slug as chapter_slug from story a inner join story_comment b on b.story_id = a.id inner join user c on c.id = b.commented_by inner join chapter d on d.id = b.chapter_id order by b.commented_at desc limit 5");
		return $row;
	}

	public function returnCategories(){
		$row = $this->_db->select("SELECT distinct * FROM category");
		return $row;
	}

	public function returnStoryTypes(){
		$row = $this->_db->select("SELECT * FROM story_type where type <> 'Challenge'");
		return $row;
	}

	public function getStoriesFromAuthor($author, $page){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id inner join user g on g.id = c.written_by where d.pen_name = :author or g.pen_name = :author limit $page, 10", array(":author"=>$author));
		return $row;
	}

	public function getStoriesFromAuthorCount($author){
		$row = $this->_db->select("SELECT count(distinct a.id) as story_count FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id inner join user g on g.id = c.written_by where d.pen_name = :author or g.pen_name = :author", array(":author"=>$author));
		return $row[0]->story_count;
	}

	public function getStoriesFromAuthorId($author){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id where d.id = :user_id", array(":user_id"=>$author));
		return $row;
	}

	public function getStoriesFromId($id,$user_id){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.pen_name, d.real_name, case when g.id is null then 0 else 1 end as have_i_subscribed FROM story a inner join story_map b on a.id = b.story_id left join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id left join subscription g on g.subscription_type = 2 and g.item_subscribed_to = a.id and g.subscribed_by = :user_id where a.id = :id", array(":id"=>$id,":user_id"=>$user_id));
		return $row;
	}

	public function getNumberOfChaptersOfUser($storyId){
		$row = $this->_db->select("SELECT distinct count(*) as chapter_count FROM story a left join story_map b on a.id = b.story_id left join chapter c on b.chapter_id = c.id where a.id = :story_id", array("story_id"=>$storyId));
		return $row[0]->chapter_count;
	}

	public function getNumberOfChapters(){
		$row = $this->_db->select("SELECT count(*) as chapter_count FROM chapter");
		return $row[0]->chapter_count;
	}

	public function getNumberOfChaptersOfStory($story_id){
		$row = $this->_db->select("SELECT count(a.id) as chapter_count FROM chapter a inner join story_map b on b.chapter_id = a.id inner join story c on c.id = b.story_id where c.id = :story_id",array(":story_id"=>$story_id));
		return $row[0]->chapter_count;
	}

	public function getNumberOfStories(){
		$row = $this->_db->select("select count(*) as story_count from story");
		return $row[0]->story_count;
	}

	public function getChapters($storyId){
		$row = $this->_db->select("SELECT distinct c.id, c.chapter_name, c.written_by, c.chapter_body, c.written_at, c.last_edited_at, c.status, c.slug, b.branch_id, b.parent_chapter_id, d.pen_name, d.real_name, d.id as user_id, d.avatar FROM story a left join story_map b on a.id = b.story_id left join chapter c on b.chapter_id = c.id left join user d on c.written_by = d.id left join story_comment e on e.chapter_id = c.id where a.id = :story_id order by c.id asc", array("story_id"=>$storyId));
		return $row;
	}

	public function getChapter($chapterId){
		$row = $this->_db->select("SELECT distinct * from chapters where ",array(":chapter_id"=>$chapter_id));
	}

	public function getCategoriesOfStory($storyId){
		$row = $this->_db->select("SELECT distinct c.id, c.category, c.slug FROM story a inner join story_category b on a.id = b.story_id inner join category c on b.category_id = c.id where a.id = :story_id", array("story_id"=>$storyId));
		return $row;
	}

	public function getTagsOfStory($storyId){
		$row = $this->_db->select("SELECT distinct b.id, b.tag, b.slug FROM story a inner join story_tag b on a.id = b.story_id where a.id = :story_id", array("story_id"=>$storyId));
		return $row;
	}

	public function getCommentsOfStory($storyId,$period = null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT distinct b.id, b.comment_body, b.commented_by, b.commented_at, b.rating, c.pen_name, c.real_name, c.id as user_id, c.avatar, b.rating from story a left join story_comment b on a.id = b.story_id inner join user c on c.id = a.written_by where a.id = :story_id and b.chapter_id is null order by b.commented_at desc", array(":story_id"=>$storyId));
		}else{
			$row = $this->_db->select("SELECT distinct b.id, b.comment_body, b.commented_by, b.commented_at, b.rating, c.pen_name, c.real_name, c.id as user_id, c.avatar, b.rating from story a left join story_comment b on a.id = b.story_id inner join user c on c.id = a.written_by where a.id = :story_id and b.chapter_id is null and DATE(b.commented_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW() order by b.commented_at desc", array(":story_id"=>$storyId));
		}
		return $row;
	}

	public function getNumberOfCommentsOfStory($storyId){
		$row = $this->_db->select("select count(b.id) as comment_count from story a inner join story_comment b on b.story_id = a.id where a.id = :story_id",array(":story_id"=>$storyId));
		return $row[0]->comment_count;
	}

	public function getNumberOfRatingsOfStory($storyId){
		$row = $this->_db->select("select count(b.id) as rating_count from story a inner join story_comment b on b.story_id = a.id where a.id = :story_id and b.rating is not null",array(":story_id"=>$storyId));
		return $row[0]->rating_count;
	}

	public function getAverageRatingOfStory($storyId){
		$row = $this->_db->select("select avg(b.rating) as average_rating from story a inner join story_comment b on b.story_id = a.id where a.id = :story_id and b.rating is not null group by a.id",array(":story_id"=>$storyId));
		return $row[0]->average_rating;
	}

	public function getCommentsOfChapter($chapterId,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT distinct b.id, b.comment_body, b.commented_by, b.commented_at, c.pen_name, c.real_name, c.avatar, b.rating from chapter a inner join story_comment b on a.id = b.chapter_id inner join user c on c.id = b.commented_by where a.id = :chapter_id", array(":chapter_id"=>$chapterId));
		}else{
			$row = $this->_db->select("SELECT distinct b.id, b.comment_body, b.commented_by, b.commented_at, c.pen_name, c.real_name, c.avatar, b.rating from chapter a inner join story_comment b on a.id = b.chapter_id inner join user c on c.id = b.commented_by where a.id = :chapter_id and DATE(b.commented_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW()", array(":chapter_id"=>$chapterId));
		}
		return $row;
	}

	public function getAverageRatingsOfChapters($chapterId,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT distinct avg(a.rating) as average_rating, sum(a.rating) as rating_sum, count(a.rating) as rating_count from story_comment a left join chapter b on a.chapter_id = b.id where b.id = :chapter_id and a.rating is not null",array(":chapter_id"=>$chapterId));
		}else{
			$row = $this->_db->select("SELECT distinct avg(a.rating) as average_rating, sum(a.rating) as rating_sum, count(a.rating) as rating_count from story_comment a left join chapter b on a.chapter_id = b.id where b.id = :chapter_id and a.rating is not null and DATE(a.commented_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW()",array(":chapter_id"=>$chapterId));
		}
		return $row;
	}

	public function getReviewRatings($story_comment_id){
		$row = $this->_db->select("SELECT a.id, a.story_comment_id, a.rated_by, a.was_helpful, a.rated_at from review_rating a inner join story_comment b on a.story_comment_id = b.id where b.id = :story_comment_id",array(":story_comment_id"=>$story_comment_id));
		return $row;
	}

	public function getChapterFromId($storyId){
		$row = $this->_db->select("SELECT distinct a.id, a.chapter_name, a.written_by, a.chapter_body, a.slug, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name FROM chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id inner join user d on d.id = a.written_by where a.id = :story_id", array(":story_id"=>$storyId));
		return $row;
	}

	public function getAllChapters($page){
		$row = $this->_db->select("SELECT distinct a.id, a.chapter_name, a.written_by, a.chapter_body, a.slug, a.status, a.written_at, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name, c.album FROM chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id inner join user d on d.id = a.written_by limit $page, 10", array(":story_id"=>$storyId));
		return $row;
	}

	public function getRecentChapters($page){
		$row = $this->_db->select("SELECT distinct a.id, a.chapter_name, a.written_by, a.chapter_body, a.slug, a.status, a.written_at, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name, c.album FROM chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id inner join user d on d.id = a.written_by order by a.written_at desc limit $page, 10", array(":story_id"=>$storyId));
		return $row;
	}

	public function getPopularChapters($page){
		$row = $this->_db->select("SELECT distinct a.id, a.chapter_name, a.written_by, a.chapter_body, a.slug, a.status, a.written_at, c.id as story_id, c.story_name as story_name, c.slug as story_slug, d.id as user_id, d.pen_name, d.real_name, c.album FROM chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id inner join user d on d.id = a.written_by limit $page, 10", array(":story_id"=>$storyId));
		return $row;
	}

	public function getChaptersFromAuthor($author, $page){
		$row = $this->_db->select("SELECT distinct a.id as user_id, a.pen_name, a.real_name, a.avatar, b.id as chapter_id, b.chapter_name, b.chapter_body, b.written_at, b.slug as chapter_slug, b.status, d.id as story_id, d.story_name, d.slug as story_slug, d.written_by as story_written_by_id, d.album  FROM user a inner join chapter b on a.id = b.written_by inner join story_map c on c.chapter_id = b.id inner join story d on d.id = c.story_id WHERE a.pen_name = :author limit $page, 10",array(":author"=>$author));
		return $row;
	}

	public function getChaptersFromAuthorCount($author){
		$row = $this->_db->select("SELECT count(distinct b.id) as chapter_count FROM user a inner join chapter b on a.id = b.written_by inner join story_map c on c.chapter_id = b.id inner join story d on d.id = c.story_id WHERE a.pen_name = :author",array(":author"=>$author));
		return $row[0]->chapter_count;
	}

	public function getOwnerOfStory($storyId){
		$row = $this->_db->select("SELECT distinct a.pen_name as story_started_by_pen_name, a.real_name as story_started_by_real_name, a.id, a.email from user a inner join story b on a.id = b.written_by where b.id = :story_id",array(":story_id"=>$storyId));
		return $row[0];
	}

	public function getOwnerOfChapter($chapterId){
		$row = $this->_db->select("SELECT distinct b.id as id from chapter a inner join user b on b.id = a.written_by where a.id = :chapter_id",array(":chapter_id"=>$chapterId));
		return $row[0]->id;
	}

	public function getDetailsOfChapterOwner($chapter_id){
		$row = $this->_db->select("SELECT b.* from chapter a inner join user b on b.id = a.written_by where a.id = :chapter_id",array(":chapter_id"=>$chapter_id));
		return $row[0];
	}

	public function getFollowersOfStory($storyId,$story_owner){
		$row = $this->_db->select("select d.id from subscription a inner join story b on a.item_subscribed_to = b.id inner join subscription_type c on a.subscription_type = c.id and c.subscription_type = 'story' inner join user d on d.id = a.subscribed_by where b.id = :story_id and b.id <> :story_owner",array(":story_id"=>$storyId,":story_owner"=>$story_owner));
		return $row;
	}

	public function getNumberOfChapterFavourites($chapterId,$period=null){
		if($period==null||$period=="allTime"){
			$row = $this->_db->select("SELECT count(*) as favourite_count from favourite a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id",array(":chapter_id"=>$chapterId));
		}else{
			$row = $this->_db->select("SELECT count(*) as favourite_count from favourite a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id and DATE(a.favourited_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW()",array(":chapter_id"=>$chapterId));
		}
		return $row[0];
	}

	public function getNumberOfStoryFavourites($storyId){
		$row = $this->_db->select("select count(a.id) as favourite_count from favourite a inner join story b on b.id = a.story_id where b.id = :story_id",array(":story_id"=>$storyId));
		return $row[0]->favourite_count;
	}

	public function getNumberOfStoryBookmarks($storyId){
		$row = $this->_db->select("select count(a.id) as bookmark_count from bookmark a inner join story b on b.id = a.story_id where b.id = :story_id",array(":story_id"=>$storyId));
		return $row[0]->bookmark_count;
	}

	public function getNumberOfChapterBookmarks($chapterId,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT count(*) as bookmark_count from bookmark a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id",array(":chapter_id"=>$chapterId));
		}else{
			$row = $this->_db->select("SELECT count(*) as bookmark_count from bookmark a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id and DATE(a.bookmarked_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW()",array(":chapter_id"=>$chapterId));
		}
		return $row[0];
	}

	public function checkIfIHaveFavouritedChapter($chapterId, $user_id){
		$row = $this->_db->select("SELECT CASE WHEN a.favourited_by IS NOT NULL THEN 1 ELSE 0 END AS haveIFavourited from favourite a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id and a.favourited_by = :user_id",array(":chapter_id"=>$chapterId,":user_id"=>$user_id));
		return $row[0]->haveIFavourited;
	}

	public function checkIfIHaveBookmarkedChapter($chapterId, $user_id){
		$row = $this->_db->select("SELECT CASE WHEN a.bookmarked_by IS NOT NULL THEN 1 ELSE 0 END AS haveIBookmarked from bookmark a inner join chapter b on a.chapter_id = b.id where b.id = :chapter_id and a.bookmarked_by = :user_id",array(":chapter_id"=>$chapterId,":user_id"=>$user_id));
		return $row[0]->haveIBookmarked;
	}

	public function getUserBookmarks($author){
		$row = $this->_db->select("SELECT e.id as story_id, e.story_name, e.slug as story_slug, b.id as chapter_id, b.slug as chapter_slug, b.chapter_name, f.pen_name as story_started_by_pen_name, c.pen_name as chapter_written_pen_name, c.id as chapter_written_id, c.real_name as chapter_written_real_name, c.avatar as chapter_written_avatar, e.album as story_album, b.written_at as written_at FROM bookmark a left join chapter b on a.chapter_id = b.id left join user c on b.written_by = c.id left join story_map d on d.chapter_id = b.id left join story e on e.id = d.story_id left join user f on f.id = e.written_by where a.bookmarked_by = :author order by a.bookmarked_at desc",array(":author"=>$author));
		return $row;
	}

	public function getUserFavourites($author){
		$row = $this->_db->select("SELECT e.id as story_id, e.story_name, e.slug as story_slug, b.id as chapter_id, b.slug as chapter_slug, b.chapter_name, f.pen_name as story_started_by_pen_name, c.pen_name as chapter_written_pen_name, c.id as chapter_written_id, c.real_name as chapter_written_real_name, c.avatar as chapter_written_avatar, e.album as story_album, b.written_at as written_at FROM favourite a left join chapter b on a.chapter_id = b.id left join user c on b.written_by = c.id left join story_map d on d.chapter_id = b.id left join story e on e.id = d.story_id left join user f on f.id = e.written_by where a.favourited_by = :author order by a.favourited_at desc",array(":author"=>$author));
		return $row;
	}

	public function getChapterId($chapter_slug, $story_id){
		$row = $this->_db->select("SELECT distinct a.id as id from chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id where c.id = :story_id and a.slug = :chapter_slug limit 1", array(":story_id"=>$story_id,":chapter_slug"=>$chapter_slug));
		return $row[0]->id;
	}

	public function getChapterId2($story_id){
		$row = $this->_db->select("SELECT distinct a.id as id from chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id where c.id = :story_id", array(":story_id"=>$story_id));
		return $row[0]->id;
	}

	public function getStoryOfChapter($chapter_id){
		$row = $this->_db->select("SELECT c.id as story_id, c.story_name, c.slug, c.created_at FROM chapter a inner join story_map b on a.id = b.chapter_id inner join story c on c.id = b.story_id where a.id = :chapter_id",array(":chapter_id"=>$chapter_id));
		return $row;
	}

	public function getReadHistory($chapterId,$period=null){
		if($period == null || $period == "allTime"){
			$row = $this->_db->select("SELECT c.id, c.pen_name, c.real_name from chapter a inner join read_history b on a.id = b.chapter_id inner join user c on b.read_by = c.id where a.id = :chapter_id",array(":chapter_id"=>$chapterId));
		}else{
			$row = $this->_db->select("SELECT c.id, c.pen_name, c.real_name from chapter a inner join read_history b on a.id = b.chapter_id inner join user c on b.read_by = c.id where a.id = :chapter_id and DATE(b.read_at) BETWEEN DATE_ADD(NOW(), INTERVAL -7 DAY) AND NOW()",array(":chapter_id"=>$chapterId));
		}
		return $row;
	}

	public function getStoryReadHistory($storyId){
		$row = $this->_db->select("select count(a.id) as read_history from read_history a inner join story b on b.id = a.story_id where b.id = :story_id",array(":story_id"=>$storyId));
		return $row[0]->read_history;
	}

	public function getAllStories($period,$page){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id where a.is_public = 1 limit $page, 10");
		return $row;
	}

	public function getAllStoriesWithCategory($period,$page,$category){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id inner join story_category g on g.story_id = a.id inner join category h on h.id = g.category_id where a.is_public = 1 and h.id = :category limit $page, 10", array(":category"=>$category));
		return $row;
	}

	public function getRecentStories($period,$page){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id where a.is_public = 1 order by a.created_at desc limit $page, 10");
		return $row;
	}

	public function getRecentStoriesWithCategory($period,$page,$category){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on a.written_by = d.id inner join story_type f on a.story_type = f.id inner join story_category g on g.story_id = a.id inner join category h on h.id = g.category_id where a.is_public = 1 and h.id = :category order by a.created_at desc limit $page, 10", array(":category"=>$category));
		return $row;
	}

	public function getPopularStories($period,$page){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on c.written_by = d.id inner join story_type f on a.story_type = f.id left join story_comment g on c.id = g.chapter_id where a.is_public = 1 group by a.id order by avg(g.rating) desc limit $page, 10");
		return $row;
	}

	public function getHomepagePopularStories($period,$page){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on c.written_by = d.id inner join story_type f on a.story_type = f.id left join story_comment g on c.id = g.chapter_id where a.is_public = 1 group by a.id order by avg(g.rating) desc limit 5");
		return $row;
	}

	public function getPopularStoriesWithCategory($period,$page,$category){
		$row = $this->_db->select("SELECT distinct a.id as id, a.album, a.story_name, a.story_description, f.type, a.written_by, a.is_public, a.slug, DATE(a.created_at) as created_at, d.id as user_id, d.pen_name, d.real_name FROM story a inner join story_map b on a.id = b.story_id inner join chapter c on b.chapter_id = c.id inner join user d on c.written_by = d.id inner join story_type f on a.story_type = f.id left join story_comment g on c.id = g.chapter_id inner join story_category h on h.story_id = a.id inner join category i on i.id = h.category_id where a.is_public = 1 and i.id = :category group by a.id order by avg(g.rating) desc limit $page, 10", array(":category"=>$category));
		return $row;
	}

	public function getChallengeDetails($storyId){
		$row = $this->_db->select("SELECT a.id as challenge_id, a.challenge_name, a.slug as challenge_slug, b.is_winner from challenge a inner join challenge_stories b on a.id = b.challenge_id where b.story_id = :story_id",array(":story_id"=>$storyId));
		return $row;
	}

	public function getStoryDetails($story_id){
		$row = $this->_db->select("select * from story where id = :story_id",array(":story_id"=>$story_id));
		return $row[0];
	}

	public function getChapterDetails($chapter_id){
		$row = $this->_db->select("select * from chapter where id = :chapter_id",array(":chapter_id"=>$chapter_id));
		return $row[0];
	}

	public function getAuthorsOfStory($storyId){
		$row = $this->_db->select("select a.pen_name, a.real_name, a.id from user a inner join chapter b on b.written_by = a.id inner join story_map c on c.chapter_id = b.id inner join story d on d.id = c.story_id where d.id = :story_id",array(":story_id"=>$storyId));
		return $row;
	}
}
