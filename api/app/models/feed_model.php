<?php

class Feed_Model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getStatusItems($earliest = null, $latest = null, $user_list, $own_id, $page = null){
		$user_list_string = "";
		foreach($user_list as $user_id){
			$user_list_string .= "$user_id" . ",";
		}
		$user_list_string = rtrim($user_list_string, ",");
		
		if($earliest != null && $latest == null){
			$timeStatus = "and a.written_at < '".$earliest."'";
			$timeStory = "and a.created_at < '".$earliest."'";
			$timeChapter = "and a.written_at < '".$earliest."'";
			$timeChapterComment = "and a.commented_at < '".$earliest."'";
			$timeChallengeSubmission = "and b.submitted_at < '".$earliest."'";
			$timeChallengeComment = "and a.commented_at < '".$earliest."'";
		}else if($earliest == null && $latest != null){
			$timeStatus = "and a.written_at > '".$latest."'";
			$timeStory = "and a.created_at > '".$latest."'";
			$timeChapter = "and a.written_at > '".$latest."'";
			$timeChapterComment = "and a.commented_at > '".$latest."'";
			$timeChallengeSubmission = "and b.submitted_at > '".$latest."'";
			$timeChallengeComment = "and a.commented_at > '".$latest."'";
		}else if($earliest != null && $latest != null){
			$timeStatus = "and a.written_at BETWEEN '".$earliest."' and '".$latest."'";
			$timeStory = "and a.created_at BETWEEN '".$earliest."' and '".$latest."'";
			$timeChapter = "and a.written_at BETWEEN '".$earliest."' and '".$latest."'";
			$timeChapterComment = "and a.commented_at BETWEEN '".$earliest."' and '".$latest."'";
			$timeChallengeSubmission = "and b.submitted_at BETWEEN '".$earliest."' and '".$latest."'";
			$timeChallengeComment = "and a.commented_at BETWEEN '".$earliest."' and '".$latest."'";
		}else{
			if($page == "homepageStoryUpdates"){
				$timeStatus = "and a.written_at > '2014-01-01'";
				$timeStory = "and a.created_at > '2014-01-01'";
				$timeChapter = "and a.written_at > '2014-01-01'";
				$timeChapterComment = "and a.commented_at > '2014-01-01'";
				$timeChallengeSubmission = "and b.submitted_at > '2014-01-01'";
				$timeChallengeComment = "and a.commented_at > '2014-01-01'";
			}else{
				$timeStatus = "";
				$timeStory = "";
				$timeChapter = "";
				$timeChapterComment = "";
				$timeChallengeSubmission = "";
				$timeChallengeComment = "";
			}
		}

		$challenge_list_string = rtrim($challenge_list_string, ",");

			if($page == null || $page == "homeAll" || $page == "homepage"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE (a.status_by in ($user_list_string) and a.status_to in ($user_list_string)) $timeStatus
								ORDER BY a.written_at desc ) a union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($user_list_string) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($user_list_string) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select null as id,
										null as share_id,
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								story_map d on d.story_id = b.id inner join
								chapter e on d.chapter_id = e.id
								where b.is_public = 1 and a.commented_by in ($user_list_string) $timeChapterComment order by status_time desc LIMIT 10) d union select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($user_list_string) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($user_list_string) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}else if($page == "homeMyFollowers"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE (a.status_by in ($user_list_string) ) $timeStatus
								ORDER BY a.written_at desc ) a union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($user_list_string) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($user_list_string) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select null as id,
										null as share_id,
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								story_map d on d.story_id = b.id inner join
								chapter e on d.chapter_id = e.id
								where b.is_public = 1 and a.commented_by in ($user_list_string) $timeChapterComment order by status_time desc LIMIT 10) d union select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($user_list_string) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($user_list_string) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}else if($page == "singleFollowerAll"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE (a.status_by in ($user_id) ) $timeStatus
								ORDER BY a.written_at desc ) a union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($user_id) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($user_id) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select null as id,
										null as share_id,
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								story_map d on d.story_id = b.id inner join
								chapter e on d.chapter_id = e.id
								where b.is_public = 1 and a.commented_by in ($user_id) $timeChapterComment order by status_time desc LIMIT 10) d union select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($user_id) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($user_id) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}else if($page == "home"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE (a.status_by in ($user_list_string) and a.status_to in ($user_list_string)) $timeStatus
								ORDER BY a.written_at desc limit 10) a";
			}else if($page == "homeStoryUpdates"){
				$query = "select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($user_list_string) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($user_list_string) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select distinct null as id,
										null as share_id,
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								chapter e on e.id = a.chapter_id
								where b.is_public = 1 and a.commented_by in ($user_list_string) $timeChapterComment order by status_time desc LIMIT 10) d order by status_time desc limit 10";
			}else if($page == "homepageStoryUpdates"){
				$query = "select * from
								(select 
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by is not null and a.is_public = 1 $timeStory order by a.created_at desc LIMIT 10) b union select * from
								(select 
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by is not null $timeChapter order by a.written_at desc LIMIT 10) c union select * from
								(select distinct 
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								chapter e on e.id = a.chapter_id
								where b.is_public = 1 and a.commented_by is not null $timeChapterComment order by a.commented_at desc LIMIT 10) d order by status_time desc limit 10";
			}else if($page == "homeChallengeUpdates"){
				$query = "select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($user_list_string) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($user_list_string) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}else if($page == "profileAll"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE ((a.status_by in ($user_list_string) and a.status_to in ($user_list_string))
								or (a.status_by not in ($user_list_string) and a.status_to in ($user_list_string))) $timeStatus
								ORDER BY a.written_at desc limit 10) a union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($own_id) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select null as id,
										null as share_id,
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($own_id) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select null as id,
										null as share_id,
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug,
										null as is_winner,
										null as challenge_id,
										null as challenge_name,
										null as challenge_by,
										null as challenge_description,
										null as challenge_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								story_map d on d.story_id = b.id inner join
								chapter e on d.chapter_id = e.id
								where b.is_public = 1 and a.commented_by in ($own_id) $timeChapterComment order by status_time desc LIMIT 10) d union select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($own_id) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($own_id) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}else if($page == "profile"){
				$query = "SELECT * from (select a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type,
								null as story_id,
								null as story_name,
								null as story_slug,
								null as chapter_body,
								null as chapter_name,
								null as chapter_id,
								null as chapter_rating,
								null as chapter_slug,
								null as is_winner,
								null as challenge_id,
								null as challenge_name,
								null as challenge_by,
								null as challenge_description,
								null as challenge_slug
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								WHERE ((a.status_by in ($user_list_string) and a.status_to in ($user_list_string))
								or (a.status_by not in ($user_list_string) and a.status_to in ($user_list_string))) $timeStatus
								ORDER BY a.written_at desc limit 10) a";
			}else if($page == "profileStoryUpdates"){
				$query = "select * from (select 
										b.id as status_by_id,
										a.story_description as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.created_at as status_time,
										'new_story' as type,
										a.id as story_id,
										a.story_name as story_name,
										a.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug
								from story a inner join
								user b on a.written_by = b.id
								where a.written_by in ($own_id) and a.is_public = 1 $timeStory order by status_time desc LIMIT 10) b union select * from
								(select 
										b.id as status_by_id,
										a.chapter_body as status_text,
										b.pen_name as status_by_pen_name,
										b.real_name as status_by_real_name,
										b.avatar as status_by_avatar,
										a.written_at as status_time,
										'new_chapter' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										a.chapter_body as chapter_body,
										a.chapter_name as chapter_name,
										a.id as chapter_id,
										null as chapter_rating,
										a.slug as chapter_slug
								from chapter a inner join
								user b on a.written_by = b.id inner join
								story_map c on a.id = c.chapter_id inner join
								story d on d.id = c.story_id
								where d.is_public = 1 and a.written_by in ($own_id) $timeChapter order by status_time desc LIMIT 10) c union select * from
								(select 
										c.id as status_by_id,
										a.comment_body as status_text,
										c.pen_name as status_by_pen_name,
										c.real_name as status_by_real_name,
										c.avatar as status_by_avatar,
										a.commented_at as status_time,
										'chapter_comment' as type,
										b.id as story_id,
										b.story_name as story_name,
										b.slug as story_slug,
										null as chapter_body,
										e.chapter_name as chapter_name,
										e.id as chapter_id,
										a.rating as chapter_rating,
										e.slug as chapter_slug
								from story_comment a inner join
								story b on a.story_id = b.id inner join
								user c on a.commented_by = c.id inner join
								story_map d on d.story_id = b.id inner join
								chapter e on d.chapter_id = e.id
								where b.is_public = 1 and a.commented_by in ($own_id) $timeChapterComment order by status_time desc LIMIT 10) d order by status_time desc limit 10";
			}else if($page == "profileChallenges"){
				$query = "select * from
								(select null as id,
										null as share_id,
										d.id as status_by_id,
										c.story_description as status_text,
										d.pen_name as status_by_pen_name,
										d.real_name as status_by_real_name,
										d.avatar as status_by_avatar,
										b.submitted_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_submission' as type,
										c.id as story_id,
										c.story_name as story_name,
										c.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										b.is_winner as is_winner,
										a.id as challenge_id,
										a.challenge_name as challenge_name,
										a.challenge_by as challenge_by,
										a.challenge_description as challenge_description,
										a.slug as challenge_slug
								from challenge a inner join
								challenge_stories b on a.id = b.challenge_id inner join
								story c on c.id = b.story_id inner join
								user d on d.id = b.submitted_by 
								where c.is_public = 1 and b.submitted_by in ($own_id) $timeChallengeSubmission order by status_time desc  LIMIT 10 ) e union select * from
								(select null as id,
										null as share_id,
										e.id as status_by_id,
										a.comment as status_text,
										e.pen_name as status_by_pen_name,
										e.real_name as status_by_real_name,
										e.avatar as status_by_avatar,
										a.commented_at as status_time,
										null as status_to_id,
										null as status_to_pen_name,
										null as status_to_real_name,
										'challenge_comment' as type,
										d.id as story_id,
										d.story_name as story_name,
										d.slug as story_slug,
										null as chapter_body,
										null as chapter_name,
										null as chapter_id,
										null as chapter_rating,
										null as chapter_slug,
										null as is_winner,
										b.id as challenge_id,
										b.challenge_name as challenge_name,
										b.challenge_by as challenge_by,
										b.challenge_description as challenge_description,
										b.slug as challenge_slug
								from challenge_discussion a inner join
								challenge b on a.challenge_id = b.id inner join
								challenge_stories c on c.challenge_id = b.id inner join
								story d on d.id = c.story_id inner join 
								user e on e.id = a.author_id
								where d.is_public = 1 and a.author_id in ($own_id) $timeChallengeComment order by status_time desc  LIMIT 10) f order by status_time desc limit 10";
			}
			$row = $this->_db->select($query);

		$array_to_return = array();
		foreach($row as $key => $status){
			if($status->type == "status"){
			array_push($array_to_return, array("share_id"=>$status->share_id, "type"=>"status","id"=>$status->id,"status_by_id"=>$status->status_by_id, "by_pen_name"=>$status->status_by_pen_name,"by_real_name"=>$status->status_by_real_name,"avatar"=>$status->avatar, "status"=>$status->status_text,"status_time"=>$status->status_time,"status_to_id"=>$status->status_to_id,"status_to_pen_name"=>$status->status_to_pen_name,"status_to_real_name"=>$status->status_to_real_name,"replies"=>array(),"likes"=>array()));

			if($status->share_id != null && $status->share_id > 0){
				$shared_status = self::getStatus($status->share_id);
				$array_to_return[$key]['shared_status'] = $shared_status;
			}

			$row3 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'status'", array(":status_id"=>$status->id));
			foreach($row3 as $key3=>$like){
				array_push($array_to_return[$key]['likes'], array("liked_by_id"=>$like->id, "liked_by_real_name"=>$like->real_name, "liked_by_pen_name"=>$like->pen_name));
			}

			$row2 = $this->_db->select("SELECT a.id as id, a.status_id as status_id, a.reply_by as reply_by_id, b.real_name as reply_by_real_name, b.pen_name as reply_by_pen_name, b.avatar as avatar, a.replied_at as replied_at, a.reply_body as reply from status_reply a inner join user b on a.reply_by = b.id WHERE a.status_id = :status_id ORDER BY a.replied_at desc", array(":status_id"=>$status->id));
			foreach($row2 as $key2 => $reply){
				array_push($array_to_return[$key]['replies'], array("id"=>$reply->id, "status_id"=>$reply->status_id, "reply_by_id"=>$reply->reply_by_id, "reply_by_real_name"=>$reply->reply_by_real_name,"avatar"=>$reply->avatar, "reply_by_pen_name"=>$reply->reply_by_pen_name, "replied_at"=>$reply->replied_at, "reply"=>$reply->reply, "likes"=>array()));
				$row5 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'reply'", array(":status_id"=>$reply->id));
				foreach($row5 as $key5=>$like2){
					array_push($array_to_return[$key]['replies'][$key2]['likes'], array("liked_by_id"=>$like2->id, "liked_by_real_name"=>$like2->real_name, "liked_by_pen_name"=>$like2->pen_name));
				}
			}
			}else{
				if($status->type == "new_chapter" || $status->type == "new_story" || $status->type == "challenge_submission"){
					$status->status_text = strip_tags($status->status_text);
					$status->status_text = substr($status->status_text,0,200);
				}
				array_push($array_to_return,$status);
			}
		}

		return $array_to_return;
	}

	public function getAllItems($earliest = null, $latest = null, $user_list, $own_id, $type = null){

	}

	public function getStoryItems($earliest = null, $latest = null, $user_id){

	}

	public function getRatingItems($earliest = null, $latest = null, $user_id){

	}

	public function getReplyIds($status_id){
		$row = $this->_db->select("SELECT id from status_reply where status_id = :status_id", array(":status_id"=>$status_id));
		return $row;
	}

	public function getReply($reply_id){
		$array_to_return = array();
		$row2 = $this->_db->select("SELECT a.id as id, a.status_id as status_id, a.reply_by as reply_by_id, b.real_name as reply_by_real_name, b.pen_name as reply_by_pen_name, b.avatar as avatar, a.replied_at as replied_at, a.reply_body as reply, d.id as status_by_id from status_reply a inner join user b on a.reply_by = b.id inner join status c on c.id = a.status_id inner join user d on d.id = c.status_by WHERE a.id = :reply_id ORDER BY a.replied_at desc", array(":reply_id"=>$reply_id));
		foreach($row2 as $key2 => $reply){
			array_push($array_to_return, array("status_by_id"=>$reply->status_by_id, "id"=>$reply->id, "status_id"=>$reply->status_id, "reply_by_id"=>$reply->reply_by_id, "reply_by_real_name"=>$reply->reply_by_real_name,"avatar"=>$reply->avatar, "reply_by_pen_name"=>$reply->reply_by_pen_name, "replied_at"=>$reply->replied_at, "reply"=>$reply->reply, "likes"=>array()));
			$row5 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'reply'", array(":status_id"=>$reply->id));
			foreach($row5 as $key5=>$like2){
				array_push($array_to_return[$key2]['likes'], array("liked_by_id"=>$like2->id, "liked_by_real_name"=>$like2->real_name, "liked_by_pen_name"=>$like2->pen_name));
			}
		}
		return $array_to_return;
	}

	public function getStatus($status_id){
		$row = $this->_db->select("SELECT 	a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								count(d.id) as reply_count
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								left join status_reply d on a.id = d.status_id
								WHERE a.id = :status_id
								ORDER BY a.written_at desc LIMIT 10", array(":status_id"=>$status_id));
		$array_to_return = array();
		foreach($row as $key => $status){
			array_push($array_to_return, array("share_id"=>$status->share_id, "type"=>"status","id"=>$status->id,"status_by_id"=>$status->status_by_id, "by_pen_name"=>$status->status_by_pen_name,"by_real_name"=>$status->status_by_real_name,"avatar"=>$status->avatar, "status"=>$status->status_text,"status_time"=>$status->status_time,"status_to_id"=>$status->status_to_id,"status_to_pen_name"=>$status->status_to_pen_name,"status_to_real_name"=>$status->status_to_real_name,"reply_count"=>$status->reply_count, "replies"=>array(),"likes"=>array()));
			$row3 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'status'", array(":status_id"=>$status->id));
			foreach($row3 as $key3=>$like){
				array_push($array_to_return[$key]['likes'], array("liked_by_id"=>$like->id, "liked_by_real_name"=>$like->real_name, "liked_by_pen_name"=>$like->pen_name));
			}
			$row2 = $this->_db->select("SELECT a.id as id, a.status_id as status_id, a.reply_by as reply_by_id, b.real_name as reply_by_real_name, b.pen_name as reply_by_pen_name, b.avatar as avatar, a.replied_at as replied_at, a.reply_body as reply from status_reply a inner join user b on a.reply_by = b.id WHERE a.status_id = :status_id ORDER BY a.replied_at desc", array(":status_id"=>$status->id));
			foreach($row2 as $key2 => $reply){
				array_push($array_to_return[$key]['replies'], array("id"=>$reply->id, "status_id"=>$reply->status_id, "reply_by_id"=>$reply->reply_by_id, "reply_by_real_name"=>$reply->reply_by_real_name,"avatar"=>$reply->avatar, "reply_by_pen_name"=>$reply->reply_by_pen_name, "replied_at"=>$reply->replied_at, "reply"=>$reply->reply, "likes"=>array()));
				$row5 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'reply'", array(":status_id"=>$reply->id));
				foreach($row5 as $key5=>$like2){
					array_push($array_to_return[$key]['replies'][$key2]['likes'], array("liked_by_id"=>$like2->id, "liked_by_real_name"=>$like2->real_name, "liked_by_pen_name"=>$like2->pen_name));
				}
			}
			if($status->share_id != null && $status->share_id > 0){
				$shared_status = self::getStatus($status->share_id);
				$array_to_return[$key]['shared_status'] = $shared_status;
			}
		}
		return $array_to_return;
	}

	public function getIndividualStatus($status_id){
		$row = $this->_db->select("SELECT 	a.id as id, 
								a.share_id as share_id,
								a.status_by as status_by_id, 
								a.status_body as status_text, 
								b.pen_name as status_by_pen_name, 
								b.real_name as status_by_real_name, 
								b.avatar as avatar, 
								a.written_at as status_time, 
								a.status_to as status_to_id, 
								c.pen_name as status_to_pen_name, 
								c.real_name as status_to_real_name,
								'status' as type
								FROM status a 
								left join user b on a.status_by = b.id 
								left join user c on a.status_to = c.id
								where a.id = :status_id",array(":status_id"=>$status_id));
		$array_to_return = array();
		foreach($row as $key => $status){
			array_push($array_to_return, array("type"=>"status","id"=>$status->id,"status_by_id"=>$status->status_by_id, "by_pen_name"=>$status->status_by_pen_name,"by_real_name"=>$status->status_by_real_name,"avatar"=>$status->avatar, "status"=>$status->status_text,"status_time"=>$status->status_time,"status_to_id"=>$status->status_to_id,"status_to_pen_name"=>$status->status_to_pen_name,"status_to_real_name"=>$status->status_to_real_name,"replies"=>array(),"likes"=>array()));

			$row3 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'status'", array(":status_id"=>$status->id));
			foreach($row3 as $key3=>$like){
				array_push($array_to_return[$key]['likes'], array("liked_by_id"=>$like->id, "liked_by_real_name"=>$like->real_name, "liked_by_pen_name"=>$like->pen_name));
			}

			$row2 = $this->_db->select("SELECT a.id as id, a.status_id as status_id, a.reply_by as reply_by_id, b.real_name as reply_by_real_name, b.pen_name as reply_by_pen_name, b.avatar as avatar, a.replied_at as replied_at, a.reply_body as reply from status_reply a inner join user b on a.reply_by = b.id WHERE a.status_id = :status_id ORDER BY a.replied_at desc", array(":status_id"=>$status->id));
			foreach($row2 as $key2 => $reply){
				array_push($array_to_return[$key]['replies'], array("id"=>$reply->id, "status_id"=>$reply->status_id, "reply_by_id"=>$reply->reply_by_id, "reply_by_real_name"=>$reply->reply_by_real_name,"avatar"=>$reply->avatar, "reply_by_pen_name"=>$reply->reply_by_pen_name, "replied_at"=>$reply->replied_at, "reply"=>$reply->reply, "likes"=>array()));
				$row5 = $this->_db->select("SELECT a.id, a.pen_name, a.real_name from user a inner join likes b on a.id = b.liked_by inner join like_type c on b.like_type = c.id where b.status_id = :status_id and c.like_type = 'reply'", array(":status_id"=>$reply->id));
				foreach($row5 as $key5=>$like2){
					array_push($array_to_return[$key]['replies'][$key2]['likes'], array("liked_by_id"=>$like2->id, "liked_by_real_name"=>$like2->real_name, "liked_by_pen_name"=>$like2->pen_name));
				}
			}
			if($status->share_id != null && $status->share_id > 0){
				$shared_status = self::getStatus($status->share_id);
				$array_to_return[$key]['shared_status'] = $shared_status;
			}
		}
		return $array_to_return;
	}

	public function getListOfSubscribedUser($user_id, $type){
		if($type != "all"){
			$row = $this->_db->select("SELECT distinct a.item_subscribed_to, b.subscription_type, c.pen_name, c.real_name, c.avatar from subscription a inner join subscription_type b on a.subscription_type = b.id left join user c on a.item_subscribed_to = c.id where a.subscribed_by = :user_id and b.subscription_type = :subscription_type", array(":user_id"=>$user_id, ":subscription_type"=>$type));
		}else{
			$row = $this->_db->select("SELECT distinct a.item_subscribed_to, b.subscription_type, c.pen_name, c.real_name, c.avatar from subscription a inner join subscription_type b on a.subscription_type = b.id left join user c on a.item_subscribed_to = c.id where a.subscribed_by = :user_id", array(":user_id"=>$user_id));
		}
		return $row;
	}

	public function getShareId($status_id){
		$row = $this->_db->select("select share_id, status_by from status where id = :status_id",array(":status_id"=>$status_id));
		return $row[0];
	}

	public function getListOfFollowers($user_id, $type){
		$row = $this->_db->select("SELECT distinct a.subscribed_by, b.subscription_type, c.pen_name, c.real_name, c.avatar from subscription a inner join subscription_type b on a.subscription_type = b.id left join user c on a.subscribed_by = c.id where a.item_subscribed_to = :user_id and b.subscription_type = :subscription_type", array(":user_id"=>$user_id, ":subscription_type"=>$type));
		return $row;
	}

	public function getAllUsers(){
		$row = $this->_db->select("SELECT distinct id from user");
		return $row;
	}

	public function getListOfSubscribedStories($user_id){
		$row = $this->_db->select("select distinct a.id from story a inner join subscription b on a.id = b.item_subscribed_to inner join subscription_type c on b.subscription_type = c.id where c.subscription_type = 'story' and b.subscribed_by = :user_id",array(":user_id"=>$user_id));
		return $row;
	}

	public function getListOfSubscribedChapters($user_id){
		$row = $this->_db->select("select distinct a.id from chapter a inner join subscription b on a.id = b.item_subscribed_to inner join subscription_type c on b.subscription_type = c.id where c.subscription_type = 'chapter' and b.subscribed_by = :user_id",array(":user_id"=>$user_id));
		return $row;
	}

	public function getListOfSubscribedBrainstorms($user_id){
		$row = $this->_db->select("select distinct a.id from brainstorm_thread a inner join subscription b on a.id = b.item_subscribed_to inner join subscription_type c on b.subscription_type = c.id where c.subscription_type = 'brainstorm' and b.subscribed_by = :user_id",array(":user_id"=>$user_id));
		return $row;
	}

	public function getListOfSubscribedChallenges($user_id){
		$row = $this->_db->select("select distinct a.id from challenge a inner join subscription b on a.id = b.item_subscribed_to inner join subscription_type c on b.subscription_type = c.id where c.subscription_type = 'challenge' and b.subscribed_by = :user_id",array(":user_id"=>$user_id));
		return $row;
	}
}
