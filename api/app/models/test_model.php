<?php

class Test_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getStories(){
		$row = $this->_db->select("select * from stories");
		return $row;
	}

	public function getComments(){
		$row = $this->_db->select("select distinct user_id, body, resource_id, created_at from comments where resource_type = 'Story' and is_deleted = 0");
		return $row;
	}

	public function getRating($user_id, $resource_id, $created_at){
		$row = $this->_db->select("select distinct * from ratings where user_id = :user_id and resource_id = :resource_id and created_at = :created_at",array(":user_id"=>$user_id,":resource_id"=>$resource_id,":created_at"=>$created_at));
		return $row;
	}

	public function getTags(){
		$row = $this->_db->select("select c.story_id, b.name from taggings a inner join tags b on a.tag_id = b.id inner join story_map c on c.chapter_id = a.taggable_id where taggable_type = 'Story'");
		return $row;
	}

	public function getChallenges(){
		$row = $this->_db->select("select * from challenges");
		return $row;
	}

	public function getChallengeStories($challenge_id){
		$row = $this->_db->select("select * from challenge_submissions where challenge_id = :challenge_id",array(":challenge_id"=>$challenge_id));
		return $row;
	}

	public function getChallengeComments($challenge_id){
		$row = $this->_db->select("select * from comments where resource_id = :challenge_id and resource_type = 'Challenge'",array(":challenge_id"=>$challenge_id));
		return $row;
	}

	public function getUsers(){
		$row = $this->_db->select("select * from users");
		return $row;
	}

	public function getStoryIdOfChapter($chapterId){
		$row = $this->_db->select("select story_id from story_map where chapter_id = :chapter_id",array(":chapter_id"=>$chapterId));
		return $row[0]->story_id;
	}

	public function getFriendships(){
		$row = $this->_db->select("select * from friendships");
		return $row;
	}

}