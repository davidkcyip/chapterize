<?php

class Circle_model extends Model {

	public function __construct(){
		parent::__construct();
	}

	public function getMyCircles($me){
		$row = $this->_db->select("select a.* from circle a inner join circle_user b on a.id = b.group_id where b.author_id = :me",array(":me"=>$me));
		return $row;
	}

	public function getCircleDetails($id,$me){
		$row = $this->_db->select("select a.*, case when b.author_id = :me then 1 else 0 end as have_i_joined, case when b.author_id = :me and is_admin = 1 then 1 else 0 end as am_i_admin, case when b.author_id = :me and b.status = 1 then 1 else 0 end as status, case when (b.author_id = :me and b.status = 1) or (b.author_id = :me and b.status = 0) then 1 else 0 end as have_i_requested from circle a left join circle_user b on a.id = b.group_id where a.id = :circle_id group by a.id, b.id order by am_i_admin desc, have_i_joined desc, have_i_requested desc, status desc limit 1",array(":circle_id"=>$id,":me"=>$me));
		return $row;
	}

	public function getCircleDetailsById($circle_id){
		$row = $this->_db->select("select * from circle where id = :circle_id",array(":circle_id"=>$circle_id));
		return $row[0];
	}

	public function getCircleByTopicId($topic_id){
		$row = $this->_db->select("select a.* from circle a inner join circle_topic b on a.id = b.group_id where b.id = :topic_id",array(":topic_id"=>$topic_id));
		return $row[0];
	}

	public function getCategories(){
		$row = $this->_db->select("select * from circle_category");
		return $row;
	}

	public function getOfficialCircles(){
		$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 1 group by a.id");
		return $row;
	}

	public function getActiveCircles($type = null){
		if($type == null){
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 0 group by a.id order by c.posted_at desc");
		}else{
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 0 and e.slug = :type group by a.id order by c.posted_at desc",array(":type"=>$type));
		}
		return $row;
	}

	public function getPopularCircles($type = null){
		if($type == null){
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 0 group by a.id order by count_users desc, topic_count desc, posts_count desc");
		}else{
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 0 and e.slug = :type group by a.id order by count_users desc, topic_count desc, posts_count desc",array(":type"=>$type));
		}
		return $row;
	}

	public function getNewestCircles($type = null){
		if($type == null){
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 1 group by a.id order by a.group_started desc");
		}else{
			$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and a.is_official = 1 and e.slug = :type group by a.id order by a.group_started desc",array(":type"=>$type));
		}
		return $row;
	}

	public function searchCircles($term){
		$row = $this->_db->select("select a.group_name, a.id as group_id, a.group_description, a.group_cover, a.is_official, a.slug as group_slug, a.group_started, e.circle_category as group_category, e.id as group_category_id, b.id as topic_id, b.topic_name, b.slug as topic_slug, b.view_count as topic_view_count, d.id as author_id, d.pen_name, d.real_name, d.avatar, count(distinct g.author_id) as count_users, count(distinct b.id) as topic_count, count(c.id) as posts_count from circle a left join circle_topic b on a.id = b.group_id left join circle_topic_post c on c.group_topic_id = b.id left join user d on d.id = c.posted_by inner join circle_category e on e.id = a.group_category inner join circle_type f on f.id = a.group_type left join circle_user g on a.id = g.group_id where f.group_type <> 'private' and (a.group_name like '%$term%' or a.group_description like '%$term%') group by a.id");
		return $row;
	}

	public function getTopicsDetails($circle_id){
		$row = $this->_db->select("select b.id as topic_id, a.group_name, b.group_id as group_id, b.topic_name, b.is_pinned, b.view_count, b.slug, c.id as started_by_author_id, c.pen_name as started_by_pen_name, c.real_name as started_by_real_name, c.avatar as started_by_avatar, d.id as post_id, d.group_topic_id, d.post_content, d.reply_id, d.posted_at, e.id as last_post_author_id, e.pen_name as last_post_pen_name, e.real_name as last_post_real_name, e.avatar as last_post_avatar, count(d.id) as post_count, b.started_at from circle a inner join circle_topic b on a.id = b.group_id inner join user c on c.id = b.topic_started_by left join circle_topic_post d on d.group_topic_id = b.id left join user e on e.id = d.posted_by where a.id = :circle_id group by a.id, b.id order by b.is_pinned desc, d.posted_at desc",array(":circle_id"=>$circle_id));
		return $row;
	}

	public function getTopicDetails($circle_id, $topic_id, $me){
		$row = $this->_db->select("select b.id as topic_id, a.group_name, a.slug as group_slug, b.group_id as group_id, b.topic_name, b.is_pinned, b.view_count, b.slug as topic_slug, c.id as author_id, c.pen_name, c.real_name, c.avatar, d.id as post_id, d.group_topic_id, d.post_content, d.reply_id, d.posted_at, case when g.subscribed_by = :me then 1 else 0 end as have_i_subscribed from circle a inner join circle_topic b on a.id = b.group_id inner join user c on c.id = b.topic_started_by left join circle_topic_post d on d.group_topic_id = b.id left join user e on e.id = d.posted_by left join subscription g on g.item_subscribed_to = b.id and g.subscription_type = 7 where a.id = :circle_id and b.id = :id group by a.id, b.id order by d.posted_at asc limit 1",array(":circle_id"=>$circle_id,":id"=>$topic_id,":me"=>$me));
		return $row;
	}

	public function getTopicDetailsById($topic_id){
		$row = $this->_db->select("select * from circle_topic where id = :topic_id",array("topic_id"=>$topic_id));
		return $row[0];
	}

	public function getTopicPosts($topic_id){
		$row = $this->_db->select("select b.id as post_id, b.group_topic_id as topic_id, b.post_content, b.reply_id, b.posted_at, c.id as author_id, c.pen_name, c.real_name, c.avatar from circle_topic a inner join circle_topic_post b on a.id = b.group_topic_id inner join user c on c.id = b.posted_by where a.id = :topic_id",array(":topic_id"=>$topic_id));
		return $row;
	}

	public function getReplies($post_id){
		$row = $this->_db->select("select b.id as post_id, b.group_topic_id as topic_id, b.post_content, b.posted_at, c.id as author_id, c.pen_name, c.real_name, c.avatar from circle_topic_post b inner join user c on c.id = b.posted_by where b.id = :post_id",array(":post_id"=>$post_id));
		return $row;
	}

	public function getCircleMembers($circle_id){
			$row = $this->_db->select("select a.id as circle_member_id, a.group_id, a.author_id, a.is_admin, a.date_added, a.status, b.pen_name, b.real_name, b.pen_name, b.avatar, c.group_name, case when c.group_owner = a.author_id then 1 else 0 end as is_owner from circle_user a inner join user b on a.author_id = b.id inner join circle c on c.id = a.group_id where a.group_id = :circle_id group by a.id order by is_owner desc, a.is_admin desc, a.status asc, a.date_added desc",array(":circle_id"=>$circle_id));
			return $row;
	}

	public function getTopicSubscribers($topic_id){
		$row = $this->_db->select("select a.subscribed_by from subscription a inner join subscription_type b on b.id = a.subscription_type where a.item_subscribed_to = :topic_id and b.subscription_type = 'circle_topic'",array(":topic_id"=>$topic_id));
		return $row;
	}

	public function getTopicIdOfComment($comment_id){
		$row = $this->_db->select("select group_topic_id from circle_topic_post where id = :comment_id",array(":comment_id"=>$comment_id));
		return $row[0]->group_topic_id;
	}

	public function getAllCircleAdmins($circle_id){
		$row = $this->_db->select("select author_id from circle_user where group_id = :circle_id and is_admin = 1",array(":circle_id"=>$circle_id));
		return $row;
	}

	public function getTopicOwner($circle_topic_id){
		$row = $this->_db->select("select topic_started_by from circle_topic where id = :circle_topic_id",array(":circle_topic_id"=>$circle_topic_id));
		return $row[0]->topic_started_by;
	}

	public function getCircleOwner($circle_id){
		$row = $this->_db->select("select group_owner from circle where id = :circle_id",array(":circle_id"=>$circle_id));
		return $row[0]->group_owner;
	}

}