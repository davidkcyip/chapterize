Repository for chapterize.com, a collaborative storytelling platform aimed at making novel-writing more fun and interactive.

Chapterize uses Javascript and PHP frameworks, including Backbone, Backbone.Controller, Underscore, RequireJS, Amplify, jQuery and SimpleMVCFramework.

